//
//  TagFriendsListViewController.swift
//  Rendenwoo
//
//  Created by goyal on 06/03/18.
//  Copyright © 2018 ankita. All rights reserved.
//


import UIKit
import ADMozaicCollectionViewLayout
import FBSDKCoreKit
import FBSDKLoginKit
import FBSDKShareKit
import CropViewController
class TagFriendsListViewController: UIViewController ,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout ,ADMozaikLayoutDelegate, UITableViewDataSource,UITableViewDelegate,UIImagePickerControllerDelegate,
UINavigationControllerDelegate,CropViewControllerDelegate{
    
    @IBOutlet weak var singleTakenHeight: NSLayoutConstraint!
    @IBOutlet weak var singleTakenView: UIView!
    var   personTagInfo = [PersonTag]()
      var   filterPersonTagInfo = [Person]()
    @IBOutlet weak var btnNext: UIButton!
    @IBOutlet weak var adLayout: ADMozaikLayout!
    @IBOutlet weak var btnAddMore: UIButton!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var tableView: UITableView!
    let picker = UIImagePickerController()
    
    @IBOutlet weak var lblConnectToInstagram: UILabel!
    
    var selectedFriendIndex = IndexPath()
    var imageIndex = Int()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.adLayout.delegate = self
        self.btnAddMore.setTitleColor(appColor, for: .normal)
        self.btnNext.backgroundColor = appColor
        self.tableView.tableFooterView = UIView()
        self.view.backgroundColor = appColor
        self.checkPersonTagged()
        
         var arr = [Person]()
        for i in 0..<personTagInfo.count {
            arr.append(contentsOf: personTagInfo[i].tagPerson)
         }
        
        var filteredArray = arr.map({$0.personId})
            filteredArray = filteredArray.removingDuplicates()
            print(filteredArray)
        
    
        for id in filteredArray{
            let details = arr.filter({$0.personId == id})
            if details.count > 0{
                filterPersonTagInfo.append(details[0])
            }
        }
        print(filterPersonTagInfo.count)
        

        
        // Do any additional setup after loading the view.
    }
    
    
    func checkPersonTagged(){
        UserInfo.sharedInstance.tagggedFriendCount = 0
        
        for i in 0..<personTagInfo.count {
            
            if personTagInfo[i].tagPerson.count > 0{
                UserInfo.sharedInstance.tagggedFriendCount  += 1
            }
        }
    
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if ( standard.value(forKey: "instagram") as? String ) != nil {
            lblConnectToInstagram.text = "Disconnect Instagram"
            
        }
            
        else{
            lblConnectToInstagram.text = "Connect Instagram"
        }
        
    }
    
    
    @IBAction func connectToInstramTapped(_ sender: UITapGestureRecognizer) {
        
        if (standard.value(forKey: "instagram") as? String) != nil {
            let parameter : Parameters = ["user_id": String(UserInfo.sharedInstance.userId ?? 0)]
            
            self.instagaramDisconnectApiCall(parameters: parameter)
            
        }
        else{
            
            let story = UIStoryboard.init(name: "Home", bundle: nil)
            let vc = story.instantiateViewController(withIdentifier: "InstagramLoginVC") as! InstagramLoginVC
            
            self.navigationController?.pushViewController(vc, animated: true)
        }
        
        
    }
    
    func instagaramDisconnectApiCall(parameters: Parameters){
        AccountApiInterface.sharedInstance.postRequestWithParam(fromViewController: self, actionName: "disconnect_insta", dictParam: parameters as [String : AnyObject], isShowIndicator: true, success: { (json) in
            
            print(json)
            
            DispatchQueue.main.async {
                standard.set(nil, forKey: "instagram")
                standard.set(nil, forKey: "insta_name")
                self.lblConnectToInstagram.text = "Connect Instagram"
                let cookieJar : HTTPCookieStorage = HTTPCookieStorage.shared
                for cookie in cookieJar.cookies! as [HTTPCookie]{
                    NSLog("cookie.domain = %@", cookie.domain)
                    
                    if cookie.domain == "www.instagram.com" ||
                        cookie.domain == "api.instagram.com"{
                        
                        cookieJar.deleteCookie(cookie)
                    }
                }

            }
        }) { (error) in
            self.showAlert(messageStr: error)
        }
        
    }
    @objc func btnTakenRelationShip(sender : UIButton){
        
        let point = sender.convert(CGPoint.zero, to: self.tableView)
        guard let indexPath = self.tableView.indexPathForRow(at: point) else {
            return
        }
        
        selectedFriendIndex = indexPath
        
        
        
        filterPersonTagInfo[indexPath.row].status = "1"
      
        self.tableView.reloadData()
        self.showAlertWithTitle(strTitle: "Are you sure you want to change \(  filterPersonTagInfo[indexPath.row].name)'s relationship status to Taken?", subTitle: "We will send a verification alert to \(  filterPersonTagInfo[indexPath.row].name).", perform: "taken")
        
        
    }
    @objc func btnSingleRelationShip(sender : UIButton){
        let point = sender.convert(CGPoint.zero, to: self.tableView)
        guard let indexPath = self.tableView.indexPathForRow(at: point) else {
            return
        }
        
    selectedFriendIndex = indexPath
    filterPersonTagInfo[indexPath.row].status = "0"

        self.tableView.reloadData()
        self.showAlertWithTitle(strTitle: "Are you sure you want to change \(  filterPersonTagInfo[indexPath.row].name)'s relationship status to Single?", subTitle: "We will send a verification alert to \(  filterPersonTagInfo[indexPath.row].name).", perform: "single")
        
        
    }
    
    
    func showAlertWithTitle(strTitle: String,subTitle : String,perform : String){
        let font = UIFont.init(name: "ProximaNova-Semibold", size: 17)
        let vc = AlertWithTwoTitle.init(frame: self.view.frame, title: strTitle, Subtitle: subTitle, cancelTitle: "Don't Change", doneTitle: "Yes", titleFont: font!, isShowImage: false,secondImage : #imageLiteral(resourceName: "imgTime"),delegatesStr:perform)
        vc.delegates = self
        self.view.addSubview(vc)
        
    }
    
    
    
    
    func photoFromLibrary(){
        picker.allowsEditing = false
        picker.delegate = self
        picker.sourceType = .photoLibrary
        picker.mediaTypes = UIImagePickerController.availableMediaTypes(for: .photoLibrary)!
        picker.modalPresentationStyle = .popover
        present(picker, animated: true, completion: nil)
        picker.popoverPresentationController?.sourceView = self.view
        
    }
    
    
    func shareTextViaSocialApp(shareText: String){
        
        let vc = UIActivityViewController(activityItems: [shareText], applicationActivities: [])
        vc.popoverPresentationController?.sourceView = self.view
              vc.setValue("\(UserInfo.sharedInstance.name ?? "") wants you on Rendezwoo.", forKey: "Subject")
        
        vc.completionWithItemsHandler = {(activityType: UIActivityType?, completed: Bool, returnedItems: [Any]?, error: Error?) in
            if !completed {
                // User canceled
                return
            }
            
            UserInfo.sharedInstance.tagggedFriendCount += 1
            
        }
        
        appDelegates.window?.rootViewController?.present(vc, animated: true, completion: nil)
        
        
    }
    
    
    
    
    func photoFromCamera(){
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            picker.allowsEditing = false
            picker.delegate = self
            picker.sourceType = UIImagePickerControllerSourceType.camera
            picker.cameraCaptureMode = .photo
            picker.modalPresentationStyle = .fullScreen
            present(picker,animated: true,completion: nil)
        } else {
            self.showAlert(messageStr: "Sorry, this device has no camera")
        }
        
    }
    
    func showActionSheet(title : String){
        
        let alertController = UIAlertController(title: title, message: nil, preferredStyle: .actionSheet)
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (action) in
            
        }
        alertController.addAction(cancelAction)
        
        let takePhoto = UIAlertAction(title: "Facebook", style: .default) { (action) in
            self.photoFromCamera()
        }
        alertController.addAction(takePhoto)
        
        let selectPhoto = UIAlertAction(title: "Photo Gallery", style: .default) { (action) in
            self.photoFromLibrary()
        }
        alertController.addAction(selectPhoto)
        
        
        
        
        
        alertController.popoverPresentationController?.sourceView = self.view
        self.present(alertController, animated: true) {
            
        }
    }
    //MARK: - PickerView Delegates
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        let img = info[UIImagePickerControllerOriginalImage] as! UIImage
        print(img)
        
        
        dismiss(animated:true, completion: nil)
        self.presentCropViewController(image: img)
        
        
    }
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated:true, completion: nil)
    }
    
    //MARK:- Show Alert Function
    func showAlert () {
        
        let vc = AlertWithOneTitle.init(frame: self.view.frame, title: "Please tag at least 2 friends in your photos to proceed. If you can't find a friend to tag, just invite 2 friends to install the app.", cancelTitle: "Tag friends", doneTitle: "Invite friends", delegatesStr: "tag")
        
        vc.delegates = self
        
        self.view.addSubview(vc)
        
    }
    
    
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func btnAddMoreTapped(_ sender: Any) {
        
        imageIndex = -1
        self.showActionSheet(title:"Import pictures from")
        
        
        
    }
    @IBAction func btnNextTapped(_ sender: Any) {
        
        self.popViewToMainViewController()
    }
    
    
    @objc func btnDeleteTapped(sender: UIButton){
        
        if personTagInfo.count < 6 {
            imageIndex = sender.tag
            self.showActionSheet(title: "Upload a replacement from")
        }
        else{
            let  parameters : Parameters =   [ "user_id":UserInfo.sharedInstance.userId ?? 0,"image_id":personTagInfo[sender.tag].imageId]
            
            self.deleteApiCall(parameters: parameters, index:sender.tag)
        }
    }
    
    
    func popViewToMainViewController(){
        
        if UserInfo.sharedInstance.tagggedFriendCount < 2 {
            self.showAlert()
        }
        else{
            if UserInfo.sharedInstance.relationshipStatus == "single" {
                self.popViewTopartnerViewController()
            }
            else{
                
                self.updateUserProfileApi()
            }
        }
        
    }
    func popViewTopartnerViewController(){
        let viewControllers: [UIViewController] = self.navigationController!.viewControllers
        for aViewController in viewControllers {
            if aViewController is RelationshipViewController {
                let vc  = aViewController as! RelationshipViewController
                vc.x = 5
                vc.personTagInfo = self.personTagInfo
                
                self.navigationController!.popToViewController(aViewController, animated: false)
            }
        }
    }
    //MARK:- CollectionView Datasource
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return personTagInfo.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! PhotoCollectionViewCell
        
        if personTagInfo[indexPath.row].tagPerson.count > 0 {
            cell.trailing.constant = 5
            cell.top.constant = 5
            cell.btnTag.setImage(UIImage.init(named: "imgTagged"), for: .normal)
            
        }
        else{
            cell.trailing.constant = -7
            cell.top.constant = -7
            cell.btnTag.setImage(UIImage.init(named: "imgNotTag"), for: .normal)
        }
        
        let imageURL = personTagInfo[indexPath.row].imageUrl
        
        
        cell.imageView.sd_setImage(with: URL(string: imageURL), placeholderImage: #imageLiteral(resourceName: "imgPlaceHolder"), options: [], completed: nil)
        
        
        cell.btnRmoveAdd.tag = indexPath.row
        cell.btnRmoveAdd.addTarget(self, action: #selector(self.btnDeleteTapped(sender:)), for: .touchUpInside)
        return cell
        
    }
    //MARK:- CollectionView Delegates
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        
        
        return CGSize(width: collectionView.frame.size.width, height: collectionView.frame.size.height )
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    
    
    
    //MARK: - ADMozaikLayoutDelegate
    
    func collectonView(_ collectionView: UICollectionView, mozaik layoyt: ADMozaikLayout, geometryInfoFor section: ADMozaikLayoutSection) -> ADMozaikLayoutSectionGeometryInfo {
        
        var rowHeight: CGFloat = 20.0
        
        rowHeight = (self.collectionView.frame.size.height / 15)
        
        
        
        let width = ADMozaikLayoutColumn(width: collectionView.frame.size.width / 25)
        var columns = [width]
        
        for _ in 0..<28{
            columns.append(width)
        }
        
        
        let geometryInfo = ADMozaikLayoutSectionGeometryInfo(rowHeight: rowHeight,
                                                             columns: columns,
                                                             minimumInteritemSpacing: 0,
                                                             minimumLineSpacing: 0,
                                                             sectionInset: UIEdgeInsets(top: 5, left: 5, bottom: 5, right: 5),
                                                             headerHeight: 0, footerHeight: 0)
        
        return geometryInfo
    }
    
    func collectionView(_ collectionView: UICollectionView, mozaik layout: ADMozaikLayout, mozaikSizeForItemAt indexPath: IndexPath) -> ADMozaikLayoutSize {
        
        if indexPath.row == 0 {
            return ADMozaikLayoutSize(numberOfColumns: 12, numberOfRows: 8)
        }
        else  if indexPath.row == 1 {
            return ADMozaikLayoutSize(numberOfColumns: 12, numberOfRows: 8)
        }
            
        else{
            return ADMozaikLayoutSize(numberOfColumns: 8, numberOfRows: 7)
        }
        
    }
    
    //MARK:- TableView Data Source Functions
    

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        singleTakenView.isHidden = true
        singleTakenHeight.constant = 0
        
            if filterPersonTagInfo.count > 0 {
                singleTakenView.isHidden = false
                singleTakenHeight.constant = 44
            }

        return filterPersonTagInfo.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let  cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! TaggedFriendTableViewCell
        
        cell.btnTaken.addTarget(self, action: #selector(self.btnTakenRelationShip(sender:)), for: .touchUpInside)
        
        
        cell.btnSingle.addTarget(self, action: #selector(self.btnSingleRelationShip(sender:)), for: .touchUpInside)
        
        cell.lblName.text =  filterPersonTagInfo[indexPath.row].name
        if  filterPersonTagInfo[indexPath.row].status == "0"{
            
            cell.btnSingle.setImage(#imageLiteral(resourceName: "imgSingleSelected"), for: .normal)
            cell.btnTaken.setImage(#imageLiteral(resourceName: "imgTakenUnselected"), for: .normal)
        }
        else{
            cell.btnSingle.setImage(#imageLiteral(resourceName: "imgSingleUnselected"), for: .normal)
            cell.btnTaken.setImage(#imageLiteral(resourceName: "imgTakenSelect"), for: .normal)
        }
        
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    //MARK:- Get Substring Function
    func getSubstring(string : String,startIndex : Int, stopIndex: Int)->String{
        
        if string != "" {
            let start = String.Index(encodedOffset: startIndex)
            let end = String.Index(encodedOffset: stopIndex)
            let substring = String(string[start..<end])
            print(substring)
            return substring
        }
        else{
            return ""
        }
    }
    
    //MARK:- tagImageInformation
    func  tagImageInformation()-> [[String : Any]]{
        
        var arrayTaggedUser = [[String : Any]]()
        
        for i in 0..<self.personTagInfo.count {
         
            for j in 0..<self.personTagInfo[i].tagPerson.count{
                
                  var dict = [String : Any]()
                  dict.updateValue(self.personTagInfo[i].imageId, forKey: "image_id")
                    dict.updateValue("\(self.personTagInfo[i].tagPerson[j].personId)", forKey: "user_id")
                 dict.updateValue("\(self.personTagInfo[i].tagPerson[j].xPos)", forKey: "xpos")
                 dict.updateValue("\(self.personTagInfo[i].tagPerson[j].yPos)", forKey: "ypos")
                 dict.updateValue("\(self.personTagInfo[i].tagPerson[j].name)", forKey: "name")
                 dict.updateValue("\(self.personTagInfo[i].tagPerson[j].status)", forKey: "status")
           arrayTaggedUser.append(dict)
            }
            
        }
        return arrayTaggedUser
        
    }
    
    
    //MARK:- Update Api Functions
    func updateUserProfileApi(){
        
        print(getSubstring(string: UserInfo.sharedInstance.height ?? "", startIndex: 6, stopIndex: 9))
        
        let partnerHeightInCM = "\(getSubstring(string: UserInfo.sharedInstance.partnerHeight , startIndex: 6, stopIndex: 9)) to \(getSubstring(string: UserInfo.sharedInstance.partnerHeight, startIndex: 24, stopIndex: 27))"
        
        
        print(self.tagImageInformation())
        
        let parameters : Parameters  = ["user_id":UserInfo.sharedInstance.userId ?? 1,"gender":UserInfo.sharedInstance.gender ?? "","user_type":(UserInfo.sharedInstance.relationshipStatus == "single") ? 0 : 1,"education":UserInfo.sharedInstance.education ?? "","occupation" : UserInfo.sharedInstance.occupation ?? "","university":"","current_work" : UserInfo.sharedInstance.workPlace ?? "","aboutme": (UserInfo.sharedInstance.aboutMe ?? "").base64Encoded() ?? "","height":UserInfo.sharedInstance.height?.base64Encoded() ?? "","height_in_cm":getSubstring(string: UserInfo.sharedInstance.height ?? "", startIndex: 6, stopIndex: 9),"ethnicity":UserInfo.sharedInstance.ethnicity ?? "","religion":UserInfo.sharedInstance.religion ?? "","current_location":UserInfo.sharedInstance.currentLocation ?? "","current_longitude":UserInfo.sharedInstance.currentLocationLong ?? 0.0,"current_latitude":UserInfo.sharedInstance.currentLocationLat ?? 0.0,"tag_info":self.tagImageInformation(),
                                        "gender_preference" : UserInfo.sharedInstance.wantToDate,
                                        
                                        
                                        "partner_preferences":["age" : UserInfo.sharedInstance.partnerAge, "height" :UserInfo.sharedInstance.partnerHeight.base64Encoded(), "distance" :UserInfo.sharedInstance.partnerDistance, "ethnicity":UserInfo.sharedInstance.partnerEthnicity, "religion" : UserInfo.sharedInstance.partnerReligion,"height_in_cm" : partnerHeightInCM,"distance_in_km" : getSubstring(string: UserInfo.sharedInstance.partnerDistance, startIndex: 0, stopIndex: 3),
            
                                            "gcm_id": "\(standard.value(forKey: "gcm")!)",
                                            
                                            "device_id": deviceUUID
            ]
        ]
        
        print(parameters)
        
        self.signUpApiCall(parameters: parameters)
        
    }
    
    //MARK:- Sign Up API Call
    func signUpApiCall(parameters: Parameters){
        print(parameters)
        AccountApiInterface.sharedInstance.postRequestWithParam(fromViewController: self, actionName: "signUp", dictParam: parameters as [String : AnyObject], isShowIndicator: true, success: { (json) in
            print(json)
            
            let Profiledata  = UserData.init(json: json["data"])
            
            standard.set(Profiledata.dictionaryRepresentation(), forKey: "userData")
            standard.set(json["token"].stringValue, forKey: "token")
            standard.set(Profiledata.usertype ?? 0, forKey: "user_type")
            standard.set(Profiledata.image ?? "", forKey: "userImage")
            standard.set(Profiledata.name ?? "", forKey: "userName")
            standard.set(Profiledata.internalIdentifier ?? 0, forKey: "userid")
            standard.set(Profiledata.genderPreference ?? 1, forKey: "gender_preference")
            DispatchQueue.main.async {
                appDelegates.window?.removeFromSuperview()
                appDelegates.homeTabBar()
            }
        }) { (error) in
            self.showAlert(messageStr: error)
        }
        
    }
    
    
    //MARK:- Upload Image Api
    
    func uploadImageApi(img : UIImage, index : Int) {
        self.view.startIndicator()
        let paramerter : Parameters = ["user_id": String(UserInfo.sharedInstance.userId ?? 0)]
        
        ApiManager.sharedInstance.requestMultiPartURL(baseUrl + "upload_images", params: paramerter as [String : AnyObject], headers: nil, imagesArray: [img], imageName: ["image_"], success: { (json) in
            self.view.stopIndicator()
            print(json)
            
            guard(json["result"].intValue) == 1 else{
                
                
                return
            }
            if index == -1 {
                
                let p = PersonTag.init()
                
                p.imageId = json["data"][0]["image_id"].stringValue
                p.imageUrl = json["data"][0]["image_url"].stringValue
                self.personTagInfo.append(p)
            }
            else{
                let p = PersonTag.init()
                p.imageId = json["data"][0]["image_id"].stringValue
                p.imageUrl = json["data"][0]["image_url"].stringValue
                self.personTagInfo[index] = p
            }
            
               self.checkPersonTagged()
            DispatchQueue.main.async {
                self.collectionView.reloadData()
            }
            
        }) { (err) in
            self.view.stopIndicator()
            DispatchQueue.main.async {
                self.collectionView.reloadData()
            }
            
        }
    }
    //MARK:- Delete Photo Api Function
    func deleteApiCall(parameters: Parameters,index :Int){
        print(parameters)
        AccountApiInterface.sharedInstance.postRequestWithParam(fromViewController: self, actionName: "delete_image", dictParam: parameters as [String : AnyObject], isShowIndicator: true, success: { (json) in
            print(json)
            
            self.personTagInfo.remove(at: index)
               self.checkPersonTagged()
            DispatchQueue.main.async {
                self.collectionView.reloadData()
            }
            
        }) { (error) in
            self.showAlert(messageStr: error)
        }
        
    }
    
    //MARK:- Crop Image Function And Delegates
    func presentCropViewController(image: UIImage) {
        
        let cropViewController = CropViewController(image: image)
        cropViewController.delegate = self
        present(cropViewController, animated: true, completion: nil)
    }
    func cropViewController(_ cropViewController: CropViewController, didCropToImage image: UIImage, withRect cropRect: CGRect, angle: Int) {
        print(image)
        
        self.uploadImageApi(img: image, index: imageIndex)
        cropViewController.dismiss(animated: true, completion: nil)
    }
}

extension TagFriendsListViewController: okButtonDelegates ,alertButtonDelegates , FBSDKAppInviteDialogDelegate{
    
    
    func appInviteDialog(_ appInviteDialog: FBSDKAppInviteDialog!, didCompleteWithResults results: [AnyHashable : Any]!) {
        print(results)
    }
    
    func appInviteDialog(_ appInviteDialog: FBSDKAppInviteDialog!, didFailWithError error: Error!) {
        print(error.localizedDescription)
    }
    
    //MARK:- Custom Delegates
    
    func okTappedfunction(){
        
    }
    
    func alertButtonAction(index : Int, delegatesStr:String){
        
        if delegatesStr == "single"{
            
            if index == 1 {
                filterPersonTagInfo[selectedFriendIndex.row].status = "1"
                for i in 0..<personTagInfo.count {
                    for j in 0..<personTagInfo[i].tagPerson.count {
                        
                        if personTagInfo[i].tagPerson[j].personId == filterPersonTagInfo[selectedFriendIndex.row].personId {
                            
                             personTagInfo[i].tagPerson[j].status = "1"
                        }
                        
                    }
                   
                }
                

            }
            else{
                  filterPersonTagInfo[selectedFriendIndex.row].status = "0"
               
                for i in 0..<personTagInfo.count {
                    for j in 0..<personTagInfo[i].tagPerson.count {
                        
                        if personTagInfo[i].tagPerson[j].personId == filterPersonTagInfo[selectedFriendIndex.row].personId {
                            
                            personTagInfo[i].tagPerson[j].status = "0"
                        }
                        
                    }
                    
                }
                
            }
            
        }
        if delegatesStr == "taken"{
            
            if index == 1 {
                 filterPersonTagInfo[selectedFriendIndex.row].status = "0"
                for i in 0..<personTagInfo.count {
                    for j in 0..<personTagInfo[i].tagPerson.count {
                        
                        if personTagInfo[i].tagPerson[j].personId == filterPersonTagInfo[selectedFriendIndex.row].personId {
                            
                            personTagInfo[i].tagPerson[j].status = "0"
                        }
                        
                    }
                    
                }
                
            }
            else{
                 filterPersonTagInfo[selectedFriendIndex.row].status = "1"
                for i in 0..<personTagInfo.count {
                    for j in 0..<personTagInfo[i].tagPerson.count {
                        
                        if personTagInfo[i].tagPerson[j].personId == filterPersonTagInfo[selectedFriendIndex.row].personId {
                            
                            personTagInfo[i].tagPerson[j].status = "1"
                        }
                        
                    }
                    
                }
            }
            
        }
        
        
        if delegatesStr == "tag"{
            if index == 1 {
                self.navigationController?.popViewController(animated: true)
            }
            else{
                
                self.shareTextViaSocialApp(shareText: "\(UserInfo.sharedInstance.name ?? "") wants you on Rendezwoo. Install now! App link coming soon.  Use my referral code : \(UserInfo.sharedInstance.referralCode ?? "")")
                
            }
        }
        
        tableView.reloadData()
    }
    
    
    
}
