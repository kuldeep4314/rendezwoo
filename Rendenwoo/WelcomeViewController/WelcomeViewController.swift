//
//  WelcomeViewController.swift
//  Rendenwoo
//
//  Created by goyal on 27/02/18.
//  Copyright © 2018 ankita. All rights reserved.
//

import UIKit

class WelcomeViewController: UIViewController {

    @IBOutlet weak var heightBottom: NSLayoutConstraint!
    
    @IBOutlet weak var lblBottom: NSLayoutConstraint!
    @IBOutlet weak var btnContinueBottom: NSLayoutConstraint!
    @IBOutlet weak var imgBottom: NSLayoutConstraint!
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var btnContinue: UIButton!
 
    @IBOutlet weak var continueTop: NSLayoutConstraint!
    @IBOutlet weak var backViewImage: UIView!
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var userImageView: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()

        self.bottomView.backgroundColor = moreLight
         btnContinue.backgroundColor = appColor
        self.view.backgroundColor = moreLight
        
       self.lblUserName.text = "Welcome \(UserInfo.sharedInstance.name ?? "")"
        
        let imageURL =  "\(UserInfo.sharedInstance.image_url ?? "")"
     
      
        userImageView.sd_setImage(with: URL(string: imageURL), placeholderImage: #imageLiteral(resourceName: "imgProfile"), options: [], completed: nil)
        // Do any additional setup after loading the view.
        
        backViewImage.layer.borderColor = orange.cgColor
        backViewImage.layer.borderWidth = 5
        DispatchQueue.main.asyncAfter(deadline: .now() + 2) { 
            appDelegates.coreLocationFunction()
            appDelegates.setNotificationFunction(application: appDelegatesApplication!)
        }
        self.checkDevice()
    }
    func checkDevice(){
        if UIDevice().userInterfaceIdiom == .phone {
            switch UIScreen.main.nativeBounds.height {
            case 1136:
                print("iPhone 5 or 5S or 5C")
                heightBottom.constant = 140
                continueTop.constant = 25
                lblBottom.constant = 30
                btnContinueBottom.constant = 30
                imgBottom.constant = 30
                
            case 1334:
                print("iPhone 6/6S/7/8")
            case 2208:
                print("iPhone 6+/6S+/7+/8+")
            case 2436:
                print("iPhone X")
            default:
                print("unknown")
            }
        }
        
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
       
        
        
    }
    
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
     
         topView.roundCorners([.bottomRight, .bottomLeft], radius: self.topView.frame.height / 2)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func btnContinueTapped(_ sender: Any) {
        
        DispatchQueue.main.async {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "RelationshipViewController") as! RelationshipViewController
            self.navigationController?.pushViewController(vc, animated: true)
        }
        
    }
    

}
