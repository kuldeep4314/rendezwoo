//
//  WelcomeCollectionViewCell.swift
//  Rendenwoo
//
//  Created by goyal on 27/02/18.
//  Copyright © 2018 ankita. All rights reserved.
//

import UIKit

class WelcomeCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var lblTitte: UILabel!
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var pageControl: UIPageControl!

    @IBOutlet weak var bottomView: UIView!
   
    @IBOutlet weak var topView: RoundView!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
    
        self.pageControl.currentPageIndicatorTintColor = appColor
        
        self.pageControl.pageIndicatorTintColor = lightGray
    }
    
    override func layoutSubviews() {
          super.layoutSubviews()
       
    }
}
