//
//  ProfileCollectionViewCell.swift
//  Rendenwoo
//
//  Created by goyal on 27/02/18.
//  Copyright © 2018 ankita. All rights reserved.
//

import UIKit

class ProfileCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var lblCount: UILabel!
    @IBOutlet weak var btnSingle: UIButton!
    @IBOutlet weak var btnTaken: UIButton!
    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var btnError: UIButton!
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var workView: UIView!
    @IBOutlet weak var btnMen: UIButton!
    @IBOutlet weak var btnWomen: UIButton!
    
    @IBOutlet weak var txtWorkPlace: AETextFieldValidator!
    @IBOutlet weak var btnBoth: UIButton!
    
    @IBOutlet weak var txtOccupation: AETextFieldValidator!
    
    @IBOutlet weak var textViewError: AETextFieldValidator!
    
    @IBOutlet weak var lblConnectToInstagram: UILabel!
    @IBOutlet weak var textview: UITextView!
    @IBOutlet weak var subView: UIView!
    
    @IBOutlet weak var txtHeight: AETextFieldValidator!
    @IBOutlet weak var btnSelectPhoto1: UIButton!
    @IBOutlet weak var btnSelectPhoto2: UIButton!
     @IBOutlet weak var btnSelectPhoto3: UIButton!
     @IBOutlet weak var btnSelectPhoto4: UIButton!
     @IBOutlet weak var btnSelectPhoto5: UIButton!
    
    @IBOutlet weak var txtEducation: AETextFieldValidator!
    @IBOutlet weak var txtReligion: AETextFieldValidator!
    @IBOutlet weak var txtEthnicity: AETextFieldValidator!
    @IBOutlet weak var lblPartnerBottom: UILabel!
    @IBOutlet weak var bottomViewPartner: UIView!
    
    @IBOutlet weak var txtPartnerReligion: UITextField!
    @IBOutlet weak var txtPartnerEthnicity: UITextField!
    @IBOutlet weak var txtPartnerDistance: UITextField!
    @IBOutlet weak var txtPartnerHeight: UITextField!
    @IBOutlet weak var txtPartnerAge: UITextField!
    override func awakeFromNib() {
        super.awakeFromNib()
     
        if pageControl != nil {
            self.pageControl.currentPageIndicatorTintColor = appColor
            
            self.pageControl.pageIndicatorTintColor = lightGray
        }
        
       
        
    }
}
