//
//  OnboardingViewController.swift
//  Rendenwoo
//
//  Created by goyal on 27/02/18.
//  Copyright © 2018 ankita. All rights reserved.
//

import UIKit
import FBSDKCoreKit
import FBSDKLoginKit
class OnboardingViewController: UIViewController ,SKSplashDelegate,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,UIScrollViewDelegate{
    
    @IBOutlet weak var collectionView: UICollectionView!
    var userFbFriends = [JSON]()
    @IBOutlet weak var lblAgreement: UILabel!
    
    let agreementString = "By signing in, you agree to our Terms of Services and Privacy Policy."
    let headingArr = ["No more swiping. Just exploring!","See someone you like? \n Tap to find out who they are.","Get introduced by a mutual friend. \n Or use your daily FREE Intros.","If all else fails, request intros on the Matchmaker wall.","Date. Matchmake. Or do both","Tag single/taken friends in photos for the Explore Wall"]
    override func viewDidLoad() {
        super.viewDidLoad()
        self.animationLikeTwiiter()
        lblAgreement.textColor = DarkGray
        lblAgreement.attributedText  = self.setAttributeString()
         UIApplication.shared.statusBarStyle = .default
    
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func btnFacebookTrapped(_ sender: Any) {
        
   self.loginWithFacebook()
        
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
       
    }
    override func viewDidAppear(_ animated: Bool) {
         super.viewDidAppear(animated)
          setTimer()
    }
    
    
    override func viewDidDisappear(_ animated: Bool) {
         super.viewDidDisappear(animated)
         time.invalidate()
    }
    
    @IBAction func termsAndServiceTapped(_ sender: UITapGestureRecognizer) {
        var actionCases = ["Terms of Services", "Privacy Policy"]
        let tapLocation = sender.location(in: lblAgreement)
        let index = lblAgreement.indexOfAttributedTextCharacterAtPoint(point: tapLocation)
        

        for i in 0..<actionCases.count {
            let range = (self.agreementString as NSString).range(of: actionCases[i])
            let startPos = range.location

            print("Index of tapped \(index)\n startPos: \(startPos)\n")
            if index >= startPos && index <= range.location + (actionCases[i].count){
                self.pushInfoViewController(str: actionCases[i])
            }
        }
    

    }
    func pushInfoViewController(str :String){
        print(str)
        DispatchQueue.main.async {
            let story = UIStoryboard.init(name: "Home", bundle: nil)
            let vc = story.instantiateViewController(withIdentifier: "PrivacyAndTermsViewController") as! PrivacyAndTermsViewController
                 vc.strTitle = str
            self.navigationController?.pushViewController(vc, animated: true)
        }
        
        
    }
    //MARK:- Animation Like Twitter
    
    func animationLikeTwiiter(){
        let twitterSplashIcon = SKSplashIcon.init(image: UIImage.init(named: "imgLogo"), animationType: .bounce)
        let twitterColor  = appColor
        twitterSplashIcon?.iconSize = CGSize.init(width: 100, height: 100)
        let splashView = SKSplashView.init(splashIcon: twitterSplashIcon, animationType: .bounce)
        splashView?.delegate = self
        splashView?.delegate = self
        splashView?.backgroundColor = twitterColor
        splashView?.animationDuration = 1.5
        self.view.addSubview(splashView!)
        splashView?.startAnimation()
        
    }
    
    //MARK:- SetAttributeString
    func setAttributeString() -> NSMutableAttributedString{
        
        let main_string = agreementString
        let string_to_color = "Terms of Services"
        let string_to_color1 = "Privacy Policy"
        
        let range = (main_string as NSString).range(of: string_to_color)
        
        let range1 = (main_string as NSString).range(of: string_to_color1)
        
        let attribute = NSMutableAttributedString.init(string: main_string)
        attribute.addAttribute(NSAttributedStringKey.foregroundColor, value: UIColor.black , range: range)
        attribute.addAttribute(NSAttributedStringKey.underlineStyle, value: NSUnderlineStyle.styleSingle.rawValue , range: range)
        attribute.addAttribute(NSAttributedStringKey.foregroundColor, value: UIColor.black, range: range1)
        attribute.addAttribute(NSAttributedStringKey.underlineStyle, value: NSUnderlineStyle.styleSingle.rawValue , range: range1)
        attribute.addAttributes([NSAttributedStringKey.font: self.lblAgreement.font ], range: NSMakeRange(0, self.agreementString.count))
        return attribute
    }
    
    
    //MARK:- CollectionView Datasource
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 6
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! WelcomeCollectionViewCell
          cell.pageControl.currentPage = indexPath.row
        cell.lblTitte.text = headingArr [indexPath.row]
        cell.imgView.image = UIImage.init(named: "\(indexPath.row + 1)")
        // cell.topView.corner(demoView:  cell.topView)
       
        return cell
        
    }
    
    //MARK:- CollectionView Delegates
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        
        
        return CGSize(width: collectionView.frame.size.width, height: collectionView.frame.size.height )
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets.init(top: 0, left: 0, bottom: 0, right: 0 )
    }
    
    //MARK: - SplashView Delegates
    func splashView(_ splashView: SKSplashView!, didBeginAnimatingWithDuration duration: Float) {
     
    }
    
    func splashViewDidEndAnimating(_ splashView: SKSplashView!) {
        
    }
    
    //MARK: Timer
    
    var x = 1
    var time = Timer()
    
    
    func setTimer() {
        time = Timer.scheduledTimer(timeInterval: 10.0, target: self, selector: #selector(self.autoScroll), userInfo: nil, repeats: true)
    }
    
    
    @objc func autoScroll() {
        if self.collectionView != nil{
            let count = 6

            if self.x < count {
                let indexPath = IndexPath(item: x, section: 0)
                
                
                if indexPath.item <= count {
                    
                    self.collectionView.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
                    
                    self.x += 1
                    
                }
                
            } else {
                time.invalidate()
                
            }
        }
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        if scrollView == self.collectionView {
            
            let indexOfPage = scrollView.contentOffset.x / scrollView.frame.size.width
            
            self.x = Int(indexOfPage + 1)
            
        }
    }
    
    
    
    func loginWithFacebook () {
        let login = FBSDKLoginManager()
        login.logOut()
        login.loginBehavior = .native
        
        if FBSDKAccessToken.current() != nil{
            self.getFBUserData()
        }
        else{
            login.logIn(withReadPermissions: ["public_profile","email","user_location","user_birthday","user_friends" ,"user_photos"], from: self, handler: { (result, error) in
                
                if error != nil {
                    return
                }
                
                if result?.isCancelled  == true{
                    
                    self.showAlert(messageStr: "Log in cancelled")
                    
                }
                else{
                    if result?.grantedPermissions.contains("email") == true{
                        self.getFBUserData()
                    }
                    else{
                        self.showAlert(messageStr: "Facebook permissions error")
                        
                    }
                }
                
            })
        }
    }
    //MARK:- GetFACEBOOK DETAILS
    func getFBUserData()  {
        if FBSDKAccessToken.current() != nil{

                     FBSDKGraphRequest(graphPath: "me", parameters: ["fields": "id, name, first_name, last_name, picture.height(2000).width(2000), email,gender,age_range,location,work,birthday"]).start(completionHandler: { (conection, result, error) in

                guard error == nil else{
                    self.showAlert(messageStr: (error?.localizedDescription)!)
                    
                    return
                }
                
                        let resultJson = JSON(result ?? [String : Any]())
                        print(resultJson)
               self.fetchFacebookFriends(resultJson: resultJson)
                
            })
            
            
        }
        
    }
    func fetchFacebookFriends(resultJson:JSON){
        self.view.startIndicator()
        let params = ["fields": "id, name, first_name, last_name, picture.height(1000).width(1000), email,gender,age_range,location,work,birthday","limit": 100] as [String : Any]
        
        let graphRequest = FBSDKGraphRequest(graphPath: "/me/friends", parameters: params)
        let connection = FBSDKGraphRequestConnection()
        connection.add(graphRequest, completionHandler: { (connection, result, error) in
            self.view.stopIndicator()
            if error == nil {
                if let userData = result as? [String:Any] {
                    
                    
                    let friends = JSON(userData)
                   // print(friends)
                    
                    self.userFbFriends = friends["data"].arrayValue
                    
                    
                }
                 self.checkUserDetailsExits(resultJson: resultJson)
            } else {
                print("Error Getting Friends \(error?.localizedDescription ?? "")");
                 self.checkUserDetailsExits(resultJson: resultJson)
            }
            
        })
        
        connection.start()
        
    }
    
    
    //MARK:- Login Check Exits or Not
    
    func checkUserDetailsExits(resultJson:JSON){
        let userFriendsIds = userFbFriends.map({$0["id"]})
        
        var idsArray = [String]()
        for i in 0..<userFriendsIds.count{
            
            idsArray.append(userFriendsIds[i].stringValue)
            
        }

        
        
        let parameters :  Parameters =  [

            "social_id" : resultJson["id"].stringValue,
        
            "email" :resultJson["email"].stringValue,
            
            "name": resultJson["first_name"].stringValue,
            
            "age": self.fetchAgeOfUser(resultJson: resultJson),
            
            "gcm_id": "\(standard.value(forKey: "gcm")!)",
            
            "device_id": deviceUUID,
            
            "gender":(resultJson["gender"].stringValue == "male") ? "0" : "1",
            
            "image_url":resultJson["picture"]["data"]["url"].stringValue,
            
            "platform": "0",
            
            "friend_list":idsArray.joined(separator: ",")
        
        ]

        print(parameters)

        AccountApiInterface.sharedInstance.postRequestWithParam(fromViewController: self, actionName: "facebookLogin", dictParam: parameters as [String : AnyObject], isShowIndicator: true, success: { (json) in

            print(json)
            
            let userProfileData = UserProfileData.init(json: json)
        
            if userProfileData.signupRequired == 1{
                self.pushToAddUserData(user: userProfileData)
            }
            else{
                self.loginToHomeViewController(Profiledata: userProfileData)
                
            }

        }) { (error) in
            self.showAlert(messageStr: error)
        }

    }
    
    
    func loginToHomeViewController( Profiledata: UserProfileData ){
        
        standard.set(Profiledata.data?.dictionaryRepresentation(), forKey: "userData")
        standard.set(Profiledata.token ?? "", forKey: "token")
        standard.set(Profiledata.data?.usertype ?? 0, forKey: "user_type")
        standard.set(Profiledata.data?.image ?? "", forKey: "userImage")
        standard.set(Profiledata.data?.name ?? "", forKey: "userName")
        standard.set(Profiledata.data?.internalIdentifier ?? 0, forKey: "userid")
        standard.set(Profiledata.data?.genderPreference ?? 1, forKey: "gender_preference")
        
        DispatchQueue.main.async {
            appDelegates.window?.removeFromSuperview()
            appDelegates.homeTabBar()
        }
        
    }
    
    
    func pushToAddUserData(user:UserProfileData){
        DispatchQueue.main.async {
            let vc  = self.storyboard?.instantiateViewController(withIdentifier:"WelcomeViewController") as! WelcomeViewController
            
            UserInfo.sharedInstance.userId = user.data?.internalIdentifier ?? 0
            
            UserInfo.sharedInstance.social_id = user.data?.socialId ?? ""
            
            UserInfo.sharedInstance.email = user.data?.email ?? ""
            
            UserInfo.sharedInstance.name = user.data?.name ?? ""
            
            UserInfo.sharedInstance.age  = user.data?.age ?? ""
            
            UserInfo.sharedInstance.gender =  user.data?.gender ?? 0
            UserInfo.sharedInstance.image_url = user.data?.image ?? ""
            UserInfo.sharedInstance.referralCode = user.data?.referral ?? ""
            UserInfo.sharedInstance.userFriends  = user.friends ?? [ UserFriends]()
            
        
            self.navigationController?.pushViewController(vc, animated: true)
        }
        
        
    }
    
    
    
    func  fetchAgeOfUser(resultJson:JSON)-> String{
        print(resultJson["birthday"].stringValue)
        
        let arrayDate = resultJson["birthday"].stringValue.components(separatedBy: "/")
        
        var myAge = String()
        if arrayDate.count > 1 {
            let myDOB = Calendar.current.date(from: DateComponents(year: JSON(arrayDate[2]).intValue, month: JSON(arrayDate[0]).intValue, day: JSON(arrayDate[1]).intValue)) ??  Calendar.current.date(from: DateComponents(year: 1990, month: 9, day: 10))!
            
            myAge = "\(myDOB.age)"
            print(myAge)
        }
        return myAge
    }
    
    


}

