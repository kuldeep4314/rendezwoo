//
//  TagFriends.swift
//  Rendenwoo
//
//  Created by goyal on 28/02/18.
//  Copyright © 2018 ankita. All rights reserved.
//

import UIKit

 protocol tagFriendDelegates {
    func  setTagFriendsDelegates(friend: PersonTag , index : Int)
   
}


class TagFriends: UIView ,UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate{

    var searchArray = [UserFriends]()
     var mainArray = [UserFriends]()
    @IBOutlet weak var tableView: UITableView!
      var person = PersonTag()
      var delegates : tagFriendDelegates?
    var tagView = Int()
    var searchActive = false
     var tagViewPoint = CGPoint()
    @IBOutlet var view: UIView!
    
    var img = UIImageView()
    required init(coder aDecoder: NSCoder)  {
        super.init(coder: aDecoder)!
        
    }
    init(frame: CGRect , tag : Int ,personTag: PersonTag,superView : UIImageView,viewPoint : CGPoint)   {
        super.init(frame: frame)
       
        self.setup(tag: tag, personTag: personTag,superView:superView, viewPoint: viewPoint)
        
    }
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        tableView.tableFooterView = UIView()
        
    }
    func setup(tag : Int,personTag: PersonTag,superView : UIImageView,viewPoint : CGPoint) {
         view = Bundle.main.loadNibNamed("TagFriends", owner: self, options: nil)?[0] as? UIView
        view.frame = self.bounds
        tagView = tag
        img = superView
        person = personTag
        self.tagViewPoint = viewPoint
        self.tableView.register(UINib.init(nibName: "TagTableViewCell", bundle: nil), forCellReuseIdentifier: "cell")
    
        tableView.tableFooterView = UIView()
        self.addSubview(view)
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if searchActive == true {
            return self.searchArray.count
            
        }
        else{
            
            return mainArray.count
            
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! TagTableViewCell
       
         if searchActive == true {
            cell.lblTitle.text = searchArray[indexPath.row].name
            let imgStr   = searchArray[indexPath.row].imageUrl ?? ""
              cell.img.sd_setImage(with: URL(string: imgStr), placeholderImage: #imageLiteral(resourceName: "imgProfile"), options: [], completed: nil)
        }
         else{
             let imgStr   = mainArray[indexPath.row].imageUrl ?? ""
             cell.img.sd_setImage(with: URL(string: imgStr), placeholderImage: #imageLiteral(resourceName: "imgProfile"), options: [], completed: nil)
            cell.lblTitle.text = mainArray[indexPath.row].name
        }
        
    
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 35
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
          tableView.deselectRow(at: indexPath, animated: true)
        
       
 if searchActive == true {
    let p = Person.init()
         print(self.frame)
         
        p.xPos = (tagViewPoint.x/self.img.frame.width)*100
        p.yPos = (tagViewPoint.y/self.img.frame.height)*100
    
 
        p.personId = String(searchArray[indexPath.row].internalIdentifier ?? 0)
        p.name = searchArray[indexPath.row].name ?? ""
        p.imageIndex = tagView
    print(p.xPos)
    print(p.yPos)
    
        person.tagPerson.append(p)

      self.delegates?.setTagFriendsDelegates(friend: person, index: tagView)
     self.removeFromSuperview()
        }
 else{
    
    let p = Person.init()
    print(self.img)

    p.xPos = (tagViewPoint.x/self.img.frame.width)*100
    p.yPos = (tagViewPoint.y/self.img.frame.height)*100
     print(self.img.frame.size.width)
      print(self.img.frame.size.height)
    print(p.xPos)
    print(p.yPos)
    
    p.personId = String(mainArray[indexPath.row].internalIdentifier ?? 0)
    p.name = mainArray[indexPath.row].name ?? ""
    p.imageIndex = tagView
      person.tagPerson.append(p)
    
      self.delegates?.setTagFriendsDelegates(friend:person, index: tagView)
     self.removeFromSuperview()
        }
        
      
    
      
    }
    
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
       
            return 35
        
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let vc = UIView.init(frame: CGRect.init(x: 0, y: 0, width: tableView.frame.width, height: 35))
        vc.backgroundColor = UIColor.white
        let searchText = UITextField.init(frame: CGRect.init(x: 10, y: 1, width: tableView.frame.width, height: 33))
    
         let font = UIFont.init(name: "ProximaNova-Regular", size: 13)
        searchText.font = font
          searchText.placeholder = "Type any name"
        searchText.delegate = self
        searchText.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        vc.addSubview(searchText)
        return vc
        
        
        
    }
    
     //MARK:- UITEXTFIELD Function
    
    @objc func textFieldDidChange(_ textField: UITextField) {
        if textField.text?.isEmpty == true{
            searchActive = false
        } else{
            self.searchArray = mainArray.filter{
                
                let str =  ($0.name ?? "").lowercased()
                searchActive = true
                return (str.range(of: textField.text!.lowercased()) != nil)
            }
        }
        
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
    }
    
    
    //MARK:- UITEXTFIELD Delegates
    
    
    
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        searchActive = true
        self.tableView.reloadData()
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        searchActive = false
        self.tableView.reloadData()
    }
    
    

    
    
    
    
}
