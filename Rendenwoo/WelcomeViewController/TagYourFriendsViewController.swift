//
//  TagYourFriendsViewController.swift
//  Rendenwoo
//
//  Created by goyal on 06/03/18.
//  Copyright © 2018 ankita. All rights reserved.
//

import UIKit
import CoreImage
import FBSDKCoreKit
import FBSDKLoginKit

class TagYourFriendsViewController: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
 var  personTag = [PersonTag]()
    
    @IBOutlet weak var collectionViewTop: NSLayoutConstraint!
    @IBOutlet weak var collectionViewHeight: NSLayoutConstraint!
    @IBOutlet weak var scrollView: UIScrollView!
    var imgInfo = [[String : Any]]()
    @IBOutlet weak var topViewHeight: NSLayoutConstraint!
    @IBOutlet weak var btnCoutinue: UIButton!
    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var collectionView: UICollectionView!

    var heightArray : [CGFloat] = [0.0,0.0,0.0,0.0,0.0]
    override func viewDidLoad() {
        super.viewDidLoad()
        self.pageControl.currentPageIndicatorTintColor = appColor
        
        self.pageControl.pageIndicatorTintColor = lightGray
        self.btnCoutinue.backgroundColor = appColor
        
        self.view.backgroundColor = appColor
        if UIDevice.isIphoneX == true{
            topViewHeight.constant = 84
        }
        self.pageControl.numberOfPages = imgInfo.count

        for i in 0..<5 {
            let p = PersonTag.init()
            
               p.imageId = JSON(imgInfo[i])["image_id"].stringValue
               p.imageUrl = JSON(imgInfo[i])["image_url"].stringValue
               self.personTag.append(p)
        }
        self.downloadImages()
        // Do any additional setup after loading the view.
    }

    func downloadImages () {
        self.view.startIndicator()
        for i in 0..<self.personTag.count {
            let imgView = UIImageView()
        imgView.sd_setImage(with: URL(string:self.personTag[i].imageUrl), placeholderImage: #imageLiteral(resourceName: "imgUpload"), options: [.refreshCached]) { (img, error, type, url) in
            
            let widthInPixels = img?.size.width ?? 0
            let heightInPixels = img?.size.height ?? 0
            
            let height = self.view.frame.size.width * (heightInPixels/widthInPixels)
            
            if  URL.init(string: self.personTag[i].imageUrl) == url {
                print(height)
                self.heightArray[i] = height
                
            }
            
            let FilterArray =  self.heightArray.filter({$0 != 0.0})
            
            if FilterArray.count == 5 {
                  self.view.stopIndicator()
                self.collectionView.reloadData()
            }
            
           
        }
    }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
   
    func setTagInImages (img : UIImageView ,p : [Person]){
      
        print(p.count)
        for i in 10000..<10030{
            let vc =  img.viewWithTag(i) as? TagView
            vc?.removeFromSuperview()
        }
    
        for i in 0..<p.count{
            
            let x1 =  (p[i].xPos * img.frame.size.width ) / 100
            let y1 =  (p[i].yPos * img.frame.size.height ) / 100
            print(x1)
            print(y1)
            let faceBox = TagView.init(frame: CGRect.init(x: x1, y: y1 , width: 90, height: 100), name: p[i].name,color:  UIColor.black,ishowCrossbtn : false,hintText : "\(p[i].imageIndex),\(i)")
               faceBox.tag  = 10000 + i
            faceBox.delegates = self
            img.addSubview(faceBox)
         
        }
        
    }
    
    
    @IBAction func btnCoutinueTapped(_ sender: Any) {
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "TagFriendsListViewController") as! TagFriendsListViewController
          vc.personTagInfo = self.personTag

        self.navigationController?.pushViewController(vc, animated: true)
        
    }
      @objc func handleTap(_ sender: UITapGestureRecognizer) {

        let vcTag = self.view.viewWithTag(500) as? TagFriends
        
        if vcTag != nil {
            self.collectionView.isScrollEnabled = true
              vcTag?.removeFromSuperview()
        }
        else{
              self.collectionView.isScrollEnabled = false
               let touchPoint = sender.location(in: self.view)
              let viewPoint = sender.location(in: sender.view!)
            let img = sender.view as! UIImageView
        
            let vc = TagFriends.init(frame: CGRect.init(x: touchPoint.x, y: touchPoint.y, width: 150, height: 120), tag: sender.view!.tag,personTag : personTag[sender.view!.tag],superView : img,viewPoint : viewPoint)
            vc.delegates = self
              vc.mainArray = UserInfo.sharedInstance.userFriends
    
            for i in 0..<personTag[sender.view!.tag].tagPerson.count {
                
              vc.mainArray = vc.mainArray.filter({String($0.internalIdentifier ?? 0) != (personTag[sender.view!.tag].tagPerson[i].personId)})
            }
            
            vc.tag = 500
            self.view.addSubview(vc)
        }
        
    }

    //MARK:- CollectionView Datasource
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return imgInfo.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath)  as! TagFriendsCollectionViewCell
        
    
        cell.imgView.sd_setImage(with: URL(string:JSON(imgInfo[indexPath.row])["image_url"].stringValue), placeholderImage: #imageLiteral(resourceName: "imgUpload"), options: [.refreshCached], completed: nil)
        
        self.setTagInImages(img:  cell.imgView, p: personTag[indexPath.row].tagPerson)
    
       cell.height.constant = heightArray[indexPath.row]
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(_:)))
        
        cell.imgView.addGestureRecognizer(tap)
        cell.imgView.tag = indexPath.row
       
        cell.imgView.addGestureRecognizer(tap)
        
        
        return cell
        
    }
    
    //MARK:- CollectionView Delegates
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: collectionView.frame.size.width, height: self.heightArray[indexPath.row])
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets.init(top: 0, left: 0, bottom: 0, right: 0 )
    }

    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        
        if  heightArray[indexPath.row] > (self.scrollView.frame.size.height ) {
              self.collectionViewTop.constant = 0
        }
        else{
              self.collectionViewTop.constant = ((self.scrollView.frame.size.height ) - heightArray[indexPath.row]) / 2
        }
    
        self.collectionViewHeight.constant = heightArray[indexPath.row]
      
        
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        
        if scrollView == self.collectionView {
            let indexOfPage = scrollView.contentOffset.x / scrollView.frame.size.width
            
            pageControl.currentPage = Int(indexOfPage)
            
        
        }
        
    }
    
  
}



extension TagYourFriendsViewController : tagFriendDelegates,deleteTagDelegates {
    func setTagFriendsDelegates(friend: PersonTag, index: Int) {
        personTag[index] = friend
        self.collectionView.isScrollEnabled = true
        self.collectionView.reloadData()
    }
    func  deleteTagFunction(strIndex : String){
        let index = strIndex.components(separatedBy: ",")
        print(index)
        personTag[JSON(index[0]).intValue].tagPerson.remove(at: JSON(index[1]).intValue)
         self.collectionView.reloadData()
    }

    
}

