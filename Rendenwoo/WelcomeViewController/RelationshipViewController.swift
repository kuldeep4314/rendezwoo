//
//  RelationshipViewController.swift
//  Rendenwoo
//
//  Created by goyal on 27/02/18.
//  Copyright © 2018 ankita. All rights reserved.
//

import UIKit
import CoreLocation
import UserNotifications
import CropViewController
class RelationshipViewController: UIViewController ,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,UITextViewDelegate,UITextFieldDelegate, UIImagePickerControllerDelegate,
UINavigationControllerDelegate , UIScrollViewDelegate,CropViewControllerDelegate{
    
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var btnNext: UIButton!
    @IBOutlet weak var height: NSLayoutConstraint!
    @IBOutlet weak var backViewImage: UIView!
    let agreementString = "Change them any time from Settings > Preferences"
    
    var   personTagInfo = [PersonTag]()
    var isLocationPermissionEnable = Bool()
    var isLNotifiactionPermissionEnable = Bool()
    var tempText : String?
    @IBOutlet weak var imgUserImage: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    var  selectedTagPhoto = -1
    
    var arrImgInfo = [[String : Any]]()
    var x = 1
    
    let picker = UIImagePickerController()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
        btnNext.backgroundColor = appColor
        self.bottomView.backgroundColor = moreLight
        self.bottomView.backgroundColor = moreLight
        self.view.backgroundColor = moreLight
        collectionView.backgroundColor = moreLight
        collectionView.backgroundColor = moreLight
        self.view.backgroundColor = appColor
        self.collectionView.isScrollEnabled = false
        backViewImage.layer.borderColor = orange.cgColor
        backViewImage.layer.borderWidth = 5
        
        for _ in 0..<5 {
            arrImgInfo.append([String : Any]())
            
        }
        
        self.setupUserData()
        
        
        
        let nc = NotificationCenter.default
        
        nc.addObserver(self, selector: #selector(self.isPermissionGranted), name: Notification.Name("Permission"), object: nil)
    }
    
    func  setupUserData(){
      
        
        let imageURL =  "\(UserInfo.sharedInstance.image_url ?? "")"
        print(imageURL)
        self.lblName.text = "Welcome \(UserInfo.sharedInstance.name ?? "")"
        imgUserImage.sd_setImage(with: URL(string: imageURL), placeholderImage: #imageLiteral(resourceName: "imgProfile"), options: [], completed: nil)
        
        imgUserImage.sd_setImage(with: URL(string: imageURL), placeholderImage: #imageLiteral(resourceName: "imgProfile"), options: []) { (img, error,type , url) in
            
            if img != nil {
                self.uploadImageApi(img:  img ?? #imageLiteral(resourceName: "imgUpload"), index: 0)
            }
        }
        
        let vc = ReferalCode.init(frame: self.view.frame, strDelgates: "referal")
        vc.delegates = self
        self.view.addSubview(vc)
    }
    
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        height.constant =  UIScreen.main.bounds.height / 2
        topView.roundCorners([.bottomRight, .bottomLeft], radius: self.topView.frame.height / 2)
    }
    
    @objc func handleTap(_ sender: UITapGestureRecognizer) {
       
        if (standard.value(forKey: "instagram") as? String) != nil {
             let parameter : Parameters = ["user_id": String(UserInfo.sharedInstance.userId ?? 0)]
            
            self.instagaramDisconnectApiCall(parameters: parameter)
        }
        else{
            
            let story = UIStoryboard.init(name: "Home", bundle: nil)
            let vc = story.instantiateViewController(withIdentifier: "InstagramLoginVC") as! InstagramLoginVC
            
            self.navigationController?.pushViewController(vc, animated: true)
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.isPermissionGranted()
    }
    
    
    
    func validateTextView()-> Bool{
        
        let cell = self.collectionView.cellForItem(at: IndexPath.init(row: 1, section: 0)) as! ProfileCollectionViewCell
        
        if  cell.textview.text != nil ||  cell.textview.text != "" ||  cell.textview.text != "A little bit about me"{
            return true
        }
        else{
            return false
        }
    }
    
    
    
    func validateTextFields()-> Bool{
        
        let cell = self.collectionView.cellForItem(at: IndexPath.init(row: 2, section: 0)) as! ProfileCollectionViewCell
        
        if cell.textViewError.validate() && cell.txtHeight.validate() &&
            cell.txtEthnicity.validate()
            &&  cell.txtReligion.validate() && cell.txtEducation.validate() && cell.txtOccupation.validate() && cell.txtWorkPlace.validate() {
            return true
        }
        else{
            
            return false
        }
    }
    
    
    func validateTagImages()-> Bool{
        
        var count = 0
        
        for  i in 0..<arrImgInfo.count{
            
            if JSON(arrImgInfo[i])["image_id"].stringValue != ""{
                count = count + 1
            }
            
        }
        
        if count == 5 {
            
            return true
        }
        else{
         return false
        }
    }
    
    
    @IBAction func btnNextTapped(_ sender: UIButton) {
        
        print(self.x)
        
        
        if UserInfo.sharedInstance.relationshipStatus == "single" {
            
            self.validationForSingleUser()
        }
        else{
            self.validationForTakenUser()
        }
    }
    
    
    func checkViewControllerInNav(count:Int){
        if self.x < count {
            btnNext.setTitle("Next", for: .normal)
            
        }
        else  {
            if btnNext.titleLabel?.text == "Done!" {
                self.updateUserProfileApi()
               
            }
            else{
                
                btnNext.setTitle("Done!", for: .normal)
                
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "TagYourFriendsViewController") as! TagYourFriendsViewController
                
                vc.imgInfo = self.arrImgInfo
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }
        
        
    }
    
    //MARK:- ValidationForSingleUser Function
    func validationForSingleUser(){
        
        if self.x < 5 {
            
            if self.x == 3 {
                if  self.validateTextFields(){
                    
                    self.collectionView.scrollToItem(at: IndexPath(item: x, section: 0), at: .centeredHorizontally, animated: true)
                    
                    self.x += 1
                }
                
                
            }
            else if  self.x == 4 {
                
                if self.validateTagImages() == false {
                    let vc = AlertWithOneButton.init(frame: self.view.frame, title: "Rendezwoo is no fun without photos. Please upload 4 more photos to proceed.", doneTitle: "OK", bgColor: colorWithopacity)
                    
                    self.view.addSubview(vc)
                    
                }
                else{
                    self.collectionView.scrollToItem(at: IndexPath(item: x, section: 0), at: .centeredHorizontally, animated: true)
                    
                    self.x += 1
                    
                }
                
            }
                
                
            else{
                self.collectionView.scrollToItem(at: IndexPath(item: x, section: 0), at: .centeredHorizontally, animated: true)
                
                self.x += 1
            }
            
            
            
        }
        self.checkViewControllerInNav(count: 5)
        
    }
    
    //MARK:- ValidationForTakenUser Function
    func validationForTakenUser(){
        
        if self.x == 2 {
            if  self.validateTextView(){
                
                self.collectionView.scrollToItem(at: IndexPath(item: x, section: 0), at: .centeredHorizontally, animated: true)
                
                self.x += 1
            }
        }
        else if  self.x == 3 {
            
            if self.validateTagImages() == false {
                let vc = AlertWithOneButton.init(frame: self.view.frame, title: "Rendezwoo is no fun without photos. Please upload 4 more photos to proceed.", doneTitle: "OK", bgColor: colorWithopacity)
                
                self.view.addSubview(vc)
                
            }
            else{
                
                if btnNext.titleLabel?.text == "Done!" {
                    
                    appDelegates.window?.removeFromSuperview()
                    appDelegates.homeTabBar()
                    
                }
                else{
                    
                    btnNext.setTitle("Done!", for: .normal)
                    let vc = self.storyboard?.instantiateViewController(withIdentifier: "TagYourFriendsViewController") as! TagYourFriendsViewController
                    vc.imgInfo = self.arrImgInfo
                    self.navigationController?.pushViewController(vc, animated: true)
                }
                
                
                
            }
            
        }
            
        else{
            self.collectionView.scrollToItem(at: IndexPath(item: x, section: 0), at: .centeredHorizontally, animated: true)
            
            self.x += 1
        }
        
        
    }
    
    //MARK:- SetAttributeString
    func setAttributeString() -> NSMutableAttributedString{
        
        let main_string = agreementString
        let string_to_color = "Settings"
        let string_to_color1 = "Preferences"
        let string_to_color2 = ">"
        let range = (main_string as NSString).range(of: string_to_color)
        
        let range1 = (main_string as NSString).range(of: string_to_color1)
        
        let range2 = (main_string as NSString).range(of: string_to_color2)
        let attribute = NSMutableAttributedString.init(string: main_string)
        attribute.addAttribute(NSAttributedStringKey.foregroundColor, value: UIColor.black , range: range)
        
        attribute.addAttribute(NSAttributedStringKey.foregroundColor, value: UIColor.black, range: range1)
        attribute.addAttribute(NSAttributedStringKey.foregroundColor, value: red, range: range2)
        
        return attribute
    }
    
    @objc func selectPhotoTotag(sender : UIButton){
        self.selectedTagPhoto = sender.tag - 5000
        
        
        if  self.selectedTagPhoto == 0 {
            
            guard let img = imgUserImage.image else{
                 self.showActionSheet()
                return
            }
            
            self.presentCropViewController(image: img)
        }
        else{
              self.showActionSheet()
        }
      
    }
    
    @objc func selectNotificationTapped(sender : UIButton){
        
        let alert = AlertWithOneTitle.init(frame: self.view.frame, title: "To get the most out of Rendezwoo, please allow the app to access your location and send you notifications.", cancelTitle: "Later", doneTitle: "Allow",delegatesStr:"allow")
        alert.delegates = self
        self.view.addSubview(alert)
    }
    
    
    //MARK:- RelationshipStatus Button Function
    
    @objc func singleTakenBtnTapped(sender : UIButton){
        UserInfo.sharedInstance.relationshipStatus = sender.accessibilityHint!
        self.collectionView.reloadData()
    }
    
    @objc func wantToDateBtnTapped(sender : UIButton){
        UserInfo.sharedInstance.wantToDate = sender.accessibilityHint!
        self.collectionView.reloadData()
    }
    
    func setImages(arrBtn : [UIButton], arrStr: [String]){
        for i in 0..<arrBtn.count{
            let btn = arrBtn[i]
            btn.setImage(UIImage.init(named: arrStr[i]), for: .normal)
        }
    }
    
    
    func setTextFieldProperties(strText :  String?, textField : AETextFieldValidator){
        if strText == nil{
            textField.isMandatory = true
            
        }
        else{
            textField.isMandatory = false
            textField.validate()
        }
        
    }
    
    
    
    
    //MARK:- CollectionView Datasource
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if UserInfo.sharedInstance.relationshipStatus != "single" {
            return 3
        }
        else {
            return 5
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        
        
        if indexPath.row == 0 {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! ProfileCollectionViewCell
            cell.pageControl.currentPage = indexPath.row
            
            
            if UserInfo.sharedInstance.relationshipStatus != "single" {
                cell.pageControl.numberOfPages =  3
            }
            else {
                cell.pageControl.numberOfPages = 5
            }
            
            cell.btnError.addTarget(self, action: #selector(self.selectNotificationTapped(sender:)), for: .touchUpInside)
            
            
            
            cell.btnTaken.accessibilityHint = "taken"
            cell.btnSingle.accessibilityHint = "single"
            cell.btnSingle.addTarget(self, action: #selector(self.singleTakenBtnTapped(sender:)), for: .touchUpInside)
            cell.btnTaken.addTarget(self, action: #selector(self.singleTakenBtnTapped(sender:)), for: .touchUpInside)
            if UserInfo.sharedInstance.relationshipStatus == "single" {
                
                
                self.setImages(arrBtn: [cell.btnSingle,cell.btnTaken], arrStr: ["imgSingleSeleted","imgTakenUnslected"])
            }
            else{
                self.setImages(arrBtn: [cell.btnSingle,cell.btnTaken], arrStr: ["imgSingleUnseleted","imgTakenSelected"])
            }
            
            if isLocationPermissionEnable == true  && self.isLNotifiactionPermissionEnable == true{
                cell.btnError.isHidden = true
            }
            else{
                cell.btnError.isHidden = false
            }
            
            return cell
        }
            
        else  {
            
            
            if UserInfo.sharedInstance.relationshipStatus == "single" {
                if indexPath.row == 1 {
                    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cellDate", for: indexPath) as! ProfileCollectionViewCell
                    cell.pageControl.currentPage = indexPath.row
                    cell.btnError.addTarget(self, action: #selector(self.selectNotificationTapped(sender:)), for: .touchUpInside)
                    
                    if isLocationPermissionEnable == true  && self.isLNotifiactionPermissionEnable == true{
                        cell.btnError.isHidden = true
                    }
                    else{
                        cell.btnError.isHidden = false
                    }
                    
                    
                    cell.pageControl.numberOfPages = 5
                    
                    
                    cell.btnMen.accessibilityHint = "0"
                    cell.btnWomen.accessibilityHint = "1"
                    cell.btnBoth.accessibilityHint = "2"
                    
                    cell.btnMen.addTarget(self, action: #selector(self.wantToDateBtnTapped(sender:)), for: .touchUpInside)
                    cell.btnWomen.addTarget(self, action: #selector(self.wantToDateBtnTapped(sender:)), for: .touchUpInside)
                    cell.btnBoth.addTarget(self, action: #selector(self.wantToDateBtnTapped(sender:)), for: .touchUpInside)
                    
                    
                    if UserInfo.sharedInstance.wantToDate == "0" {
                        
                        cell.btnMen.setImage(UIImage.init(named: "imgMenSelected"), for: .normal)
                        cell.btnWomen.setImage(UIImage.init(named: "imgWomenUnselected"), for: .normal)
                        cell.btnBoth.setImage(UIImage.init(named: "imgBothUnseleted"), for: .normal)
                        
                        self.setImages(arrBtn: [cell.btnMen,cell.btnWomen,cell.btnBoth], arrStr: ["imgMenSelected","imgWomenUnselected","imgBothUnseleted"])
                        
                    }
                    else if UserInfo.sharedInstance.wantToDate == "1" {
                        
                        self.setImages(arrBtn: [cell.btnMen,cell.btnWomen,cell.btnBoth], arrStr: ["imgMenUnselected","imgWomenSelected","imgBothUnseleted"])
                        
                    }
                    else{
                        
                        self.setImages(arrBtn: [cell.btnMen,cell.btnWomen,cell.btnBoth], arrStr: ["imgMenUnselected","imgWomenUnselected","imgBothSelected"])
                    }
                    
                    
                    
                    return cell
                }
                    
                else if indexPath.row == 2 {
                    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cellAbout", for: indexPath) as! ProfileCollectionViewCell
                    cell.pageControl.currentPage = indexPath.row
                    cell.textview.addshodowToTextView()
                    cell.subView.addshodowToView()
                    cell.workView.addshodowToView()
                    cell.pageControl.numberOfPages = 5
                    
                    
                    if UserInfo.sharedInstance.aboutMe == nil {
                        cell.textview.text = "A little bit about me"
                        
                        cell.textview.textColor = lightGray
                        
                        cell.textViewError.text = nil
                    }
                    else{
                        cell.textview.text = UserInfo.sharedInstance.aboutMe
                        cell.textViewError.text = tempText
                        
                        cell.textview.textColor = UIColor.black
                    }
                    
                    
                    cell.textview.delegate = self
                    
                    
                    cell.txtHeight.delegate = self
                    cell.txtEthnicity.delegate = self
                    cell.txtReligion.delegate = self
                    cell.txtEducation .delegate = self
                    
                    cell.txtWorkPlace.delegate = self
                    cell.txtOccupation.delegate = self
                    cell.txtWorkPlace.accessibilityHint = "workPlace"
                    cell.txtOccupation.accessibilityHint = "occupation"
                    
                    cell.txtHeight.accessibilityHint = "height"
                    cell.txtEthnicity.accessibilityHint = "ethnicity"
                    cell.txtReligion.accessibilityHint = "religion"
                    cell.txtEducation.accessibilityHint = "education"
                    cell.textViewError.accessibilityHint = "temp"
                    
                    
                    cell.textViewError.addRegx(nameReg, withMsg: invalidName)
                    cell.txtHeight.addRegx(nameReg, withMsg: invalidName)
                    
                    cell.txtEthnicity.addRegx(nameReg, withMsg: invalidName)
                    cell.txtReligion.addRegx(nameReg, withMsg: invalidName)
                    cell.txtEducation.addRegx(nameReg, withMsg: invalidName)
                    
                    self.setTextFieldProperties(strText:UserInfo.sharedInstance.aboutMe , textField: cell.textViewError)
                    
                    self.setTextFieldProperties(strText:UserInfo.sharedInstance.height , textField: cell.txtHeight)
                    
                    self.setTextFieldProperties(strText:UserInfo.sharedInstance.religion , textField: cell.txtReligion)
                    
                    self.setTextFieldProperties(strText:UserInfo.sharedInstance.ethnicity , textField: cell.txtEthnicity)
                    
                    self.setTextFieldProperties(strText:UserInfo.sharedInstance.education , textField: cell.txtEducation)
                    
                    cell.txtHeight.text = UserInfo.sharedInstance.height
                    cell.txtEthnicity.text = UserInfo.sharedInstance.ethnicity
                    cell.txtReligion.text = UserInfo.sharedInstance.religion
                    
                    cell.txtEducation.text = UserInfo.sharedInstance.education
                    
                    cell.txtOccupation.text = UserInfo.sharedInstance.occupation?.capitalized
                    cell.txtWorkPlace.text = UserInfo.sharedInstance.workPlace?.capitalized
                    return cell
                }
                    
                    
                else if indexPath.row == 3 {
                    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cellTag", for: indexPath) as! ProfileCollectionViewCell
                    cell.pageControl.currentPage = indexPath.row
                    
                    cell.pageControl.numberOfPages = 5
                    
                    
                    
                    cell.btnSelectPhoto1.addTarget(self, action: #selector(self.selectPhotoTotag(sender:)), for: .touchUpInside)
                    cell.btnSelectPhoto2.addTarget(self, action: #selector(self.selectPhotoTotag(sender:)), for: .touchUpInside)
                    cell.btnSelectPhoto3.addTarget(self, action: #selector(self.selectPhotoTotag(sender:)), for: .touchUpInside)
                    cell.btnSelectPhoto4.addTarget(self, action: #selector(self.selectPhotoTotag(sender:)), for: .touchUpInside)
                    cell.btnSelectPhoto5.addTarget(self, action: #selector(self.selectPhotoTotag(sender:)), for: .touchUpInside)
                    cell.btnSelectPhoto1.tag = 5000
                    cell.btnSelectPhoto2.tag = 5001
                    cell.btnSelectPhoto3.tag = 5002
                    cell.btnSelectPhoto4.tag = 5003
                    cell.btnSelectPhoto5.tag = 5004
                    
                    let imageURL1 = JSON(self.arrImgInfo[0])["image_url"].stringValue
                    
                    cell.btnSelectPhoto1.sd_setImage(with: URL(string: imageURL1), for: .normal, placeholderImage: #imageLiteral(resourceName: "imgUpload"), options: [.refreshCached], completed: nil)
                    
                    let imageURL2 = JSON(self.arrImgInfo[1])["image_url"].stringValue
                    
                    cell.btnSelectPhoto2.sd_setImage(with: URL(string: imageURL2), for: .normal, placeholderImage: #imageLiteral(resourceName: "imgUpload"), options: [.refreshCached], completed: nil)
                    
                    let imageURL3 = JSON(self.arrImgInfo[2])["image_url"].stringValue
                    cell.btnSelectPhoto3.sd_setImage(with: URL(string: imageURL3), for: .normal, placeholderImage: #imageLiteral(resourceName: "imgUpload"), options: [.refreshCached], completed: nil)
                    
                    
                    let imageURL4 = JSON(self.arrImgInfo[3])["image_url"].stringValue
                    cell.btnSelectPhoto4.sd_setImage(with: URL(string: imageURL4), for: .normal, placeholderImage: #imageLiteral(resourceName: "imgUpload"), options: [.refreshCached], completed: nil)
                    
                    
                    let imageURL5 = JSON(self.arrImgInfo[4])["image_url"].stringValue
                    cell.btnSelectPhoto5.sd_setImage(with: URL(string: imageURL5), for: .normal, placeholderImage: #imageLiteral(resourceName: "imgUpload"), options: [.refreshCached], completed: nil)
                    
                    
                    cell.btnSelectPhoto1.addTarget(self, action: #selector(self.selectPhotoTotag(sender:)), for: .touchUpInside)
                    cell.btnSelectPhoto2.addTarget(self, action: #selector(self.selectPhotoTotag(sender:)), for: .touchUpInside)
                    cell.btnSelectPhoto3.addTarget(self, action: #selector(self.selectPhotoTotag(sender:)), for: .touchUpInside)
                    cell.btnSelectPhoto4.addTarget(self, action: #selector(self.selectPhotoTotag(sender:)), for: .touchUpInside)
                    cell.btnSelectPhoto5.addTarget(self, action: #selector(self.selectPhotoTotag(sender:)), for: .touchUpInside)
                    
                    
                    let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(_:)))
                    
                    cell.lblConnectToInstagram.addGestureRecognizer(tap)
                    
                    if ( standard.value(forKey: "instagram") as? String ) != nil {
                          cell.lblConnectToInstagram.text = "Disconnect Instagram"
                        
                    }
                        
                    else{
                          cell.lblConnectToInstagram.text = "Connect Instagram"
                    }
                    
                    return cell
                }
                else if indexPath.row == 4 {
                    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cellPartner", for: indexPath) as! ProfileCollectionViewCell
                    cell.pageControl.currentPage = indexPath.row
                    
                    cell.pageControl.numberOfPages =  5
                    
                    cell.bottomViewPartner.addshodowToView()
                    cell.lblPartnerBottom.attributedText = self.setAttributeString()
                    
                    cell.txtPartnerAge.text = UserInfo.sharedInstance.partnerAge
                    cell.txtPartnerHeight.text = UserInfo.sharedInstance.partnerHeight
                    cell.txtPartnerDistance.text = UserInfo.sharedInstance.partnerDistance
                    cell.txtPartnerEthnicity.text = UserInfo.sharedInstance.partnerEthnicity
                    cell.txtPartnerReligion.text = UserInfo.sharedInstance.partnerReligion
                    
                    if UserInfo.sharedInstance.partnerReligion == "No Preference" {
                        cell.txtPartnerReligion.textColor = DarkGray
                    }
                    else{
                        cell.txtPartnerReligion.textColor = UIColor.black
                    }
                    if UserInfo.sharedInstance.partnerEthnicity == "No Preference" {
                        cell.txtPartnerEthnicity.textColor = DarkGray
                    }
                    else{
                        cell.txtPartnerEthnicity.textColor = UIColor.black
                    }
                    
                    
                    
                    cell.txtPartnerAge.delegate = self
                    cell.txtPartnerHeight.delegate = self
                    cell.txtPartnerDistance.delegate = self
                    cell.txtPartnerEthnicity.delegate = self
                    cell.txtPartnerReligion.delegate = self
                    
                    
                    cell.txtPartnerAge.tag = 1
                    cell.txtPartnerHeight.tag = 2
                    cell.txtPartnerDistance.tag = 3
                    cell.txtPartnerEthnicity.tag = 4
                    cell.txtPartnerReligion.tag = 5
                    
                    return cell
                }
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! ProfileCollectionViewCell
                
                return cell
                
                
            }
            else {
                
                if indexPath.row == 1 {
                    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cellAbout", for: indexPath) as! ProfileCollectionViewCell
                    cell.pageControl.currentPage = indexPath.row
                    
                    cell.pageControl.numberOfPages =  3
                    
                    cell.textview.addshodowToTextView()
                    cell.subView.addshodowToView()
                    cell.workView.addshodowToView()
                    
                    if UserInfo.sharedInstance.aboutMe == nil {
                        cell.textview.text = "A little bit about me"
                        cell.textview.textColor = lightGray
                    }
                    else{
                        cell.textview.text = UserInfo.sharedInstance.aboutMe
                        cell.textview.textColor = UIColor.black
                    }
                    
                    
                    cell.textview.delegate = self
                    
                    cell.txtHeight.delegate = self
                    cell.txtEthnicity.delegate = self
                    cell.txtReligion.delegate = self
                    
                    cell.txtEducation.delegate = self
                    cell.txtWorkPlace.delegate = self
                    cell.txtOccupation.delegate = self
                    cell.txtWorkPlace.accessibilityHint = "workPlace"
                    cell.txtOccupation.accessibilityHint = "occupation"
                    
                    cell.txtHeight.accessibilityHint = "height"
                    cell.txtEthnicity.accessibilityHint = "ethnicity"
                    cell.txtReligion.accessibilityHint = "religion"
                    cell.txtEducation.accessibilityHint = "education"
                    
                    cell.txtHeight.isMandatory = false
                    
                    
                    self.setTextFieldProperties(strText:UserInfo.sharedInstance.height , textField: cell.txtHeight)
                    
                    self.setTextFieldProperties(strText:UserInfo.sharedInstance.religion , textField: cell.txtReligion)
                    
                    self.setTextFieldProperties(strText:UserInfo.sharedInstance.ethnicity , textField: cell.txtEthnicity)
                    
                    self.setTextFieldProperties(strText:UserInfo.sharedInstance.education , textField: cell.txtEducation)
                    
                    cell.txtHeight.text = UserInfo.sharedInstance.height
                    cell.txtEthnicity.text = UserInfo.sharedInstance.ethnicity
                    cell.txtReligion.text = UserInfo.sharedInstance.religion
                    cell.txtEducation.text = UserInfo.sharedInstance.education
                    cell.txtOccupation.text = UserInfo.sharedInstance.occupation?.capitalized
                    cell.txtWorkPlace.text = UserInfo.sharedInstance.workPlace?.capitalized
                    return cell
                }
                    
                    
                else if indexPath.row == 2 {
                    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cellTag", for: indexPath) as! ProfileCollectionViewCell
                    cell.pageControl.currentPage = indexPath.row
                    
                    cell.pageControl.numberOfPages =  3
                    
                    cell.btnSelectPhoto1.tag = 5000
                    cell.btnSelectPhoto2.tag = 5001
                    cell.btnSelectPhoto3.tag = 5002
                    cell.btnSelectPhoto4.tag = 5003
                    cell.btnSelectPhoto5.tag = 5004
                    
                    
                    cell.btnSelectPhoto1.addTarget(self, action: #selector(self.selectPhotoTotag(sender:)), for: .touchUpInside)
                    cell.btnSelectPhoto2.addTarget(self, action: #selector(self.selectPhotoTotag(sender:)), for: .touchUpInside)
                    cell.btnSelectPhoto3.addTarget(self, action: #selector(self.selectPhotoTotag(sender:)), for: .touchUpInside)
                    cell.btnSelectPhoto4.addTarget(self, action: #selector(self.selectPhotoTotag(sender:)), for: .touchUpInside)
                    cell.btnSelectPhoto5.addTarget(self, action: #selector(self.selectPhotoTotag(sender:)), for: .touchUpInside)
                    
                    
                    let imageURL1 = JSON(self.arrImgInfo[0])["image_url"].stringValue
                    
                    cell.btnSelectPhoto1.sd_setImage(with: URL(string: imageURL1), for: .normal, placeholderImage: #imageLiteral(resourceName: "imgUpload"), options: [.refreshCached], completed: nil)
                    
                    let imageURL2 = JSON(self.arrImgInfo[1])["image_url"].stringValue
                    
                    cell.btnSelectPhoto2.sd_setImage(with: URL(string: imageURL2), for: .normal, placeholderImage: #imageLiteral(resourceName: "imgUpload"), options: [.refreshCached], completed: nil)
                    
                    let imageURL3 = JSON(self.arrImgInfo[2])["image_url"].stringValue
                    cell.btnSelectPhoto3.sd_setImage(with: URL(string: imageURL3), for: .normal, placeholderImage: #imageLiteral(resourceName: "imgUpload"), options: [.refreshCached], completed: nil)
                    
                    
                    let imageURL4 = JSON(self.arrImgInfo[3])["image_url"].stringValue
                    cell.btnSelectPhoto4.sd_setImage(with: URL(string: imageURL4), for: .normal, placeholderImage: #imageLiteral(resourceName: "imgUpload"), options: [.refreshCached], completed: nil)
                    
                    
                    let imageURL5 = JSON(self.arrImgInfo[4])["image_url"].stringValue
                    cell.btnSelectPhoto5.sd_setImage(with: URL(string: imageURL5), for: .normal, placeholderImage: #imageLiteral(resourceName: "imgUpload"), options: [.refreshCached], completed: nil)
                    
                    
                    
                    
                    let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(_:)))
                    
                    cell.lblConnectToInstagram.addGestureRecognizer(tap)
                    
                    
                    
                    cell.lblConnectToInstagram.addGestureRecognizer(tap)
                    
                    if   UserInfo.sharedInstance.authToken != nil {
                        cell.lblConnectToInstagram.text = "Disconnect Instagram"
                    }
                    else{
                        cell.lblConnectToInstagram.text = "Connect Instagram"
                    }
                    
                    return cell
                }
                
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! ProfileCollectionViewCell
                
                return cell
                
                
            }
            
            
        }
        
    }
    
    //MARK:- CollectionView Delegates
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: collectionView.frame.size.width, height: collectionView.frame.size.height )
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets.init(top: 0, left: 0, bottom: 0, right: 0 )
    }
    
    
    //MARK:- UITextField Delegates
    func textFieldDidBeginEditing(_ textField: UITextField) {
        

        if textField.accessibilityHint != "textView"{
            
            if textField.accessibilityHint == "height" {
                textField.resignFirstResponder()
                
                var array = [String]()
                for i in 135..<199 {
                    
                    let  feet  =  Double(i) * 0.3937008
                    
                    let  f1 = Int(feet / 12 )
                    let f2 = Int(feet) % 12
                    array.append("\(f1)'\(f2)\" (\(i) cms)")
                }
                
                
                self.optionWithValues(tag: 1, array: array, strValue: UserInfo.sharedInstance.height ?? "")
                
            }
            else if textField.accessibilityHint == "ethnicity" {
                textField.resignFirstResponder()
                let array = ["American Indian"," Black/African Descent","East Indian","Hispanic/Latino","Middle Eastern","Pacific Islander","South Asian","White/Caucasian","Other","Prefer Not to Say"]
                
                self.showOptionsInTableView(array: array, tag: 2, strValue: UserInfo.sharedInstance.ethnicity ?? "", isStartingPreference: false)
            }
            else if textField.accessibilityHint == "religion" {
                textField.resignFirstResponder()
                let array = ["Buddhist","Christian","Hindu","Jewish","Muslim","Sikh","No Religion","Prefer Not to Say"]
                
                self.showOptionsInTableView(array: array, tag: 3, strValue: UserInfo.sharedInstance.religion ?? "", isStartingPreference: false)
            }
                
            else if textField.accessibilityHint == "education" {
                textField.resignFirstResponder()
                let array = ["Doctorate","Masters","Bachelors","Associates","Trade School","High School","No Education","Not Specified"]
                
                self.showOptionsInTableView(array: array, tag: 4, strValue: UserInfo.sharedInstance.education ?? "", isStartingPreference: false)
            }
            else if textField.accessibilityHint == "occupation" {
                 let index = (UserInfo.sharedInstance.relationshipStatus == "single") ? 2 : 1
                let cell = self.collectionView.cellForItem(at: IndexPath.init(row: index, section: 0)) as? ProfileCollectionViewCell
                
                cell?.scrollView.isScrollEnabled = false
                
                
            }
            else if textField.accessibilityHint == "workPlace" {
                
                let index = (UserInfo.sharedInstance.relationshipStatus == "single") ? 2 : 1
                
                let cell = self.collectionView.cellForItem(at: IndexPath.init(row: index, section: 0)) as? ProfileCollectionViewCell
                
                cell?.scrollView.isScrollEnabled = false
                
            }
                
            else {
                textField.resignFirstResponder()
                self.selectTextFieldsValues(textField: textField)
            }
        }
        
    }
    
  
    func textFieldDidEndEditing(_ textField: UITextField) {
        textField.resignFirstResponder()
        

        
        if textField.accessibilityHint == "occupation" {
            
            
              let index = (UserInfo.sharedInstance.relationshipStatus == "single") ? 2 : 1
            let cell = self.collectionView.cellForItem(at: IndexPath.init(row: index, section: 0)) as? ProfileCollectionViewCell
            
            cell?.scrollView.isScrollEnabled = true
            UserInfo.sharedInstance.occupation = textField.text ?? ""
            self.collectionView.reloadData()
            
        }
        else if textField.accessibilityHint == "workPlace" {
                let index = (UserInfo.sharedInstance.relationshipStatus == "single") ? 2 : 1
            let cell = self.collectionView.cellForItem(at: IndexPath.init(row: index, section: 0)) as? ProfileCollectionViewCell
            
            cell?.scrollView.isScrollEnabled = true
            UserInfo.sharedInstance.workPlace = textField.text ?? ""
              self.collectionView.reloadData()
        }
        
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField.accessibilityHint == "occupation" || textField.accessibilityHint == "workPlace"{
            let maxLength = 25
            let currentString: NSString = textField.text! as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            return newString.length <= maxLength
            
        }
        else{
            return true
        }
        
        
    }
    
    
    
    func showOptionsInTableView(array : [String] ,tag : Int,strValue:String ,isStartingPreference : Bool){
        let vc = OptionTable.init(frame: self.view.frame, dataArr:array , tableframe: CGRect.init(x: 10, y: Int(self.view.frame.size.height / 2)  - Int(array.count * 25) , width: Int(self.view.frame.size.width - 20), height:  Int(array.count * 50)), tag: tag,strValue : strValue ,isStartingPreference :isStartingPreference,titleString:"dummy not need")
        vc.delegates = self
        self.view.addSubview(vc)
    }
    
    
    
    //MARK:- OptionWithValues Functions
    func optionWithValues(tag: Int ,array : [String] ,strValue : String){
        
        
        let vc = PickerView.init(frame: self.view.frame, array: array, str: strValue, tag: tag , numberOfComponents : 1,isStartingPreference : false)
        
        vc.delegates = self
        self.view.addSubview(vc)
        
    }
    
    //MARK:- TextView Delegates
    func textViewDidBeginEditing(_ textView: UITextView) {
        
        let index = (UserInfo.sharedInstance.relationshipStatus == "single") ? 2 : 1
        
        
        
        let cell = self.collectionView.cellForItem(at: IndexPath.init(row: index, section: 0)) as? ProfileCollectionViewCell
        
        cell?.scrollView.isScrollEnabled = false
        
        if textView.textColor == lightGray
        {
            textView.text = nil
            textView.textColor = UIColor.black
            tempText = nil
            print("begin working")
        }
    }
    
    
    func textViewDidEndEditing(_ textView: UITextView) {
        
          let index = (UserInfo.sharedInstance.relationshipStatus == "single") ? 2 : 1
        
        let cell = self.collectionView.cellForItem(at: IndexPath.init(row: index, section: 0)) as? ProfileCollectionViewCell
        
        cell?.scrollView.isScrollEnabled = true
        
        if textView.text.isEmpty {
            textView.text = "A little bit about me"
            textView.textColor = lightGray
            tempText = nil
            UserInfo.sharedInstance.aboutMe = nil
            
            print("end working")
        }
        else{
            tempText = "temp"
            UserInfo.sharedInstance.aboutMe = textView.text ?? nil
        }
        self.collectionView.reloadData()
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        let index = (UserInfo.sharedInstance.relationshipStatus == "single") ? 2 : 1
        
         let newText = (textView.text as NSString).replacingCharacters(in: range, with: text)
    
            let cell = self.collectionView.cellForItem(at: IndexPath.init(row: index, section: 0 )) as? ProfileCollectionViewCell
            
            cell?.lblCount.text = "\(150 - newText.count)"
        
           print(newText.count)
        return newText.count < 150
    }
    
    func photoFromLibrary(){
        picker.allowsEditing = false
        picker.delegate = self
        picker.sourceType = .photoLibrary
        picker.mediaTypes = UIImagePickerController.availableMediaTypes(for: .photoLibrary)!
        picker.modalPresentationStyle = .popover
        present(picker, animated: true, completion: nil)
        picker.popoverPresentationController?.sourceView = self.view
        
    }
    func photoFromCamera(){
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            picker.allowsEditing = false
            picker.delegate = self
            picker.sourceType = UIImagePickerControllerSourceType.camera
            picker.cameraCaptureMode = .photo
            picker.modalPresentationStyle = .fullScreen
            present(picker,animated: true,completion: nil)
        } else {
            self.showAlert(messageStr: "Sorry, this device has no camera")
        }
        
    }
    
    //MARK:- SelectTextFieldsValues
    func selectTextFieldsValues(textField : UITextField){
        
        if textField.tag == 1 {
            var array = [String]()
            for i in 18..<100 {
                array.append(String(i))
            }
            
            
            self.setPartnerPreferencesTextFieldsValues(array: array, tag: textField.tag, strValue: textField.text ?? "", numberOfComponents: 3)
        }
            
            
            
        else  if textField.tag == 2 {
            var array = [String]()
            for i in 122..<273 {
                
                let  feet  =  Double(i) * 0.3937008
                
                let  f1 = Int(feet / 12 )
                let f2 = Int(feet) % 12
                print(f1)
                print(f2)
                array.append("\(f1)'\(f2)\" (\(i) cms)")
            }
            
            
            self.setPartnerPreferencesTextFieldsValues(array: array, tag: textField.tag, strValue: textField.text ?? "", numberOfComponents: 3)
            
        }
            
        else  if textField.tag == 3 {
            var array = [String]()
            for i in 10..<1005 {
                if i % 10 == 0 {
                    let miles =   Int(Float(i) / 1.609344)
                    
                    array.append("\(i) km / \(miles) Miles")
                }
                
            }
            
            self.setPartnerPreferencesTextFieldsValues(array: array, tag: textField.tag, strValue: textField.text ?? "", numberOfComponents: 1)
            
        }
            
            
        else  if textField.tag == 4 {
            let array = ["American Indian"," Black/African Descent","East Indian","Hispanic/Latino","Middle Eastern","Pacific Islander","South Asian","White/Caucasian","Other","No Preference"]
            
            self.showOptionsInTableView(array: array, tag: textField.tag, strValue: textField.text ?? "", isStartingPreference: true)
        }
        else  if textField.tag == 5 {
            let array = ["Buddhist","Christian","Hindu","Jewish","Muslim","Sikh","No Religion","No Preference"]
            
            self.showOptionsInTableView(array: array, tag: textField.tag, strValue: textField.text ?? "", isStartingPreference: true)
            
        }
        
        
    }
    func setPartnerPreferencesTextFieldsValues( array : [String], tag : Int , strValue : String ,numberOfComponents : Int) {
        
        let vc = PickerView.init(frame: self.view.frame, array: array, str: strValue, tag: tag , numberOfComponents : numberOfComponents,isStartingPreference : true)
        
        vc.delegates = self
        self.view.addSubview(vc)
        
    }
    
    //MARK: - PickerView Delegates
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        let img = info[UIImagePickerControllerOriginalImage] as! UIImage
        print(img)
        
        dismiss(animated:true, completion: nil)
         self.presentCropViewController(image: img)
      
        
        
    }
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated:true, completion: nil)
    }
    
    
    func showActionSheet(){
        
        let alertController = UIAlertController(title: "Import pictures from", message: nil, preferredStyle: .actionSheet)
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (action) in
            
        }
        alertController.addAction(cancelAction)
        
        let takePhoto = UIAlertAction(title: "Facebook", style: .default) { (action) in
           
            
//            let story = UIStoryboard.init(name: "Home", bundle: nil)
//            
//            let vc = story.instantiateViewController(withIdentifier: "FacebookPhotoViewController") as! FacebookPhotoViewController
//            
//            self.navigationController?.pushViewController(vc, animated: true)
            
            
        }
        alertController.addAction(takePhoto)
        
        let selectPhoto = UIAlertAction(title: "Photo Gallery", style: .default) { (action) in
            self.photoFromLibrary()
        }
        alertController.addAction(selectPhoto)
        
        
        
        alertController.popoverPresentationController?.sourceView = self.view
        self.present(alertController, animated: true) {
            
        }
    }
    
    
    //MARK:- ScrollView Delegates FUnction
    
    func scrollViewDidScroll(_ scrollView: UIScrollView){
        
        
    }
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        
        
        
    }
    
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        if scrollView == self.collectionView {
            let indexOfPage = scrollView.contentOffset.x / scrollView.frame.size.width
            
            self.x = Int(indexOfPage + 1)
            print(self.x)
            
            if UserInfo.sharedInstance.relationshipStatus == "single" {
                self.checkViewControllerInNav(count: 5)
            }
            else{
                self.checkViewControllerInNav(count: 3)
            }
            
        }
    }
    
    
    @objc func isPermissionGranted()
    {
        if CLLocationManager.locationServicesEnabled() == true {
            isLocationPermissionEnable = [.authorizedAlways, .authorizedWhenInUse].contains(CLLocationManager.authorizationStatus())
        }
        else{
            isLocationPermissionEnable = false
        }
        let notificationType = UIApplication.shared.currentUserNotificationSettings!.types
        if notificationType == [] {
            isLNotifiactionPermissionEnable = false
        } else {
            isLNotifiactionPermissionEnable = true
        }
        self.collectionView.reloadData()
    }
    
    //MARK:- Get Substring Function
    func getSubstring(string : String,startIndex : Int, stopIndex: Int)->String{
        
        let start = String.Index(encodedOffset: startIndex)
        let end = String.Index(encodedOffset: stopIndex)
        let substring = String(string[start..<end])
        print(substring)
        return substring
    }
    
    //MARK:- tagImageInformation
    func  tagImageInformation()-> [[String : Any]]{
        
        var arrayTaggedUser = [[String : Any]]()
        
        for i in 0..<self.personTagInfo.count {
            
            for j in 0..<self.personTagInfo[i].tagPerson.count{
                
                var dict = [String : Any]()
                dict.updateValue(self.personTagInfo[i].imageId, forKey: "image_id")
                dict.updateValue("\(self.personTagInfo[i].tagPerson[j].personId)", forKey: "user_id")
                dict.updateValue("\(self.personTagInfo[i].tagPerson[j].xPos)", forKey: "xpos")
                dict.updateValue("\(self.personTagInfo[i].tagPerson[j].yPos)", forKey: "ypos")
                dict.updateValue("\(self.personTagInfo[i].tagPerson[j].name)", forKey: "name")
                dict.updateValue("\(self.personTagInfo[i].tagPerson[j].status)", forKey: "status")
                arrayTaggedUser.append(dict)
            }
            
        }
        return arrayTaggedUser
        
    }
    
    
    //MARK:- Update Api Functions
    func updateUserProfileApi(){
        
        print(getSubstring(string: UserInfo.sharedInstance.height ?? "", startIndex: 6, stopIndex: 9))
        
        let partnerHeightInCM = "\(getSubstring(string: UserInfo.sharedInstance.partnerHeight , startIndex: 6, stopIndex: 9)) to \(getSubstring(string: UserInfo.sharedInstance.partnerHeight, startIndex: 24, stopIndex: 27))"
        
        
        print(self.tagImageInformation())
  
        let parameters : Parameters  = ["user_id":UserInfo.sharedInstance.userId ?? 1,"gender":UserInfo.sharedInstance.gender ?? "","user_type":(UserInfo.sharedInstance.relationshipStatus == "single") ? 0 : 1,"education":UserInfo.sharedInstance.education ?? "","occupation" : UserInfo.sharedInstance.occupation ?? "","university":"","current_work" : UserInfo.sharedInstance.workPlace ?? "","aboutme": (UserInfo.sharedInstance.aboutMe ?? "").base64Encoded() ?? "","height":UserInfo.sharedInstance.height?.base64Encoded() ?? "","height_in_cm":getSubstring(string: UserInfo.sharedInstance.height ?? "", startIndex: 6, stopIndex: 9),"ethnicity":UserInfo.sharedInstance.ethnicity ?? "","religion":UserInfo.sharedInstance.religion ?? "","current_location":UserInfo.sharedInstance.currentLocation ?? "","current_longitude":UserInfo.sharedInstance.currentLocationLong ?? 0.0,"current_latitude":UserInfo.sharedInstance.currentLocationLat ?? 0.0,"tag_info":self.tagImageInformation(),
             "gender_preference" : UserInfo.sharedInstance.wantToDate,
            
             "partner_preferences":["age" : UserInfo.sharedInstance.partnerAge , "height" :UserInfo.sharedInstance.partnerHeight.base64Encoded() ?? "", "distance" :UserInfo.sharedInstance.partnerDistance , "ethnicity":UserInfo.sharedInstance.partnerEthnicity , "religion" : UserInfo.sharedInstance.partnerReligion ,"height_in_cm" : partnerHeightInCM ,"distance_in_km" : getSubstring(string: UserInfo.sharedInstance.partnerDistance, startIndex: 0, stopIndex: 3) ,
                "gcm_id": "\(standard.value(forKey: "gcm") as? String ?? "")",
                
                "device_id": deviceUUID
            
            ]
            ]
        
        print(parameters)
        
        self.signUpApiCall(parameters: parameters)
        
    }
    
    //MARK:- Sign Up API Call
    func signUpApiCall(parameters: Parameters){
        print(parameters)
        AccountApiInterface.sharedInstance.postRequestWithParam(fromViewController: self, actionName: "signUp", dictParam: parameters as [String : AnyObject], isShowIndicator: true, success: { (json) in
            print(json)
            
            let Profiledata  = UserData.init(json: json["data"])
            
            standard.set(Profiledata.dictionaryRepresentation(), forKey: "userData")
            standard.set(json["token"].stringValue, forKey: "token")
            standard.set(Profiledata.usertype ?? 0, forKey: "user_type")
            standard.set(Profiledata.image ?? "", forKey: "userImage")
            standard.set(Profiledata.name ?? "", forKey: "userName")
            standard.set(Profiledata.internalIdentifier ?? 0, forKey: "userid")
            standard.set(Profiledata.genderPreference ?? 1, forKey: "gender_preference")
            
            DispatchQueue.main.async {
                appDelegates.window?.removeFromSuperview()
                appDelegates.homeTabBar()
            }
        }) { (error) in
            self.showAlert(messageStr: error)
        }
        
    }
    
    
    
    //MARK:- Upload Image Api
    
    func uploadImageApi(img : UIImage, index : Int) {
        self.view.startIndicator()
        let paramerter : Parameters = ["user_id": String(UserInfo.sharedInstance.userId ?? 0)]
        
        ApiManager.sharedInstance.requestMultiPartURL(baseUrl + "upload_images", params: paramerter as [String : AnyObject], headers: nil, imagesArray: [img], imageName: ["image_"], success: { (json) in
            self.view.stopIndicator()
            print(json)
            
            guard(json["result"].intValue) == 1 else{
                

                return
            }
            self.arrImgInfo[index] = json["data"][0].dictionaryObject ?? [String : Any]()
            print(self.arrImgInfo)
            
            DispatchQueue.main.async {
                self.collectionView.reloadData()
            }
            
        }) { (err) in
            self.view.stopIndicator()
            DispatchQueue.main.async {
               
                self.collectionView.reloadData()
                self.showAlert(messageStr: err.localizedDescription )
            }
           
        }
    }
    
    //MARK:- Crop Image Function And Delegates
    func presentCropViewController(image: UIImage) {
        
        let cropViewController = CropViewController(image: image)
        cropViewController.delegate = self
        present(cropViewController, animated: true, completion: nil)
    }
    func cropViewController(_ cropViewController: CropViewController, didCropToImage image: UIImage, withRect cropRect: CGRect, angle: Int) {
       print(image)
        
            self.uploadImageApi(img: image, index: self.selectedTagPhoto)
          cropViewController.dismiss(animated: true, completion: nil)
    }
    
    func instagaramDisconnectApiCall(parameters: Parameters){
        AccountApiInterface.sharedInstance.postRequestWithParam(fromViewController: self, actionName: "disconnect_insta", dictParam: parameters as [String : AnyObject], isShowIndicator: true, success: { (json) in
            
            print(json)
            
            DispatchQueue.main.async {
                standard.set(nil, forKey: "instagram")
                standard.set(nil, forKey: "insta_name")
                self.collectionView.reloadData()
                let cookieJar : HTTPCookieStorage = HTTPCookieStorage.shared
                for cookie in cookieJar.cookies! as [HTTPCookie]{
                    NSLog("cookie.domain = %@", cookie.domain)
                    
                    if cookie.domain == "www.instagram.com" ||
                        cookie.domain == "api.instagram.com"{
                        
                        cookieJar.deleteCookie(cookie)
                    }
                }
            }
        }) { (error) in
            self.showAlert(messageStr: error)
        }
        
    }

    
}



extension RelationshipViewController : setValuesDelegates,alertButtonDelegates{
    
    func  setTextfieldsvalues (str : String , tag: Int){
        if tag == 1 {
            UserInfo.sharedInstance.height = str
            
        }
        else if tag == 2 {
            UserInfo.sharedInstance.ethnicity = str
            
        }
        else if tag == 3{
            UserInfo.sharedInstance.religion = str
            
        }
        else if tag == 4{
            UserInfo.sharedInstance.education = str
            
        }
        self.collectionView.reloadData()
    }
    
    
    func  setPreferenceValues(str : String , tag: Int) {
        if tag == 3{
            UserInfo.sharedInstance.partnerDistance = str
        }
            
        else if tag == 4{
            UserInfo.sharedInstance.partnerEthnicity = str
        }
        else if tag == 5{
            UserInfo.sharedInstance.partnerReligion = str
        }
        self.collectionView.reloadData()
    }
    func  setPreferenceValuesWithTwoValues (str : String , str1 : String , tag: Int) {
        
        if tag == 1 {
            UserInfo.sharedInstance.partnerAge = "\(str) to \(str1)"
            
        }
            
        else  if tag == 2 {
            UserInfo.sharedInstance.partnerHeight = "\(str) to \(str1)"
        }
        
        self.collectionView.reloadData()
    }
    
    func alertButtonAction(index : Int, delegatesStr:String){
        
        if delegatesStr == "referal"{
            
            
        }
        else if delegatesStr == "allow"{
            if index == 2 {
                
                let url = URL(string: UIApplicationOpenSettingsURLString)!
                
                if #available(iOS 10.0, *) {
                    UIApplication.shared.open(url, options: [:], completionHandler: { (granted) in
                        
                    })
                } else {
                    UIApplication.shared.openURL(url)
                }
                
            }
            else{
                collectionView.reloadData()
            }
        }
        
        
        
    }
    
}
