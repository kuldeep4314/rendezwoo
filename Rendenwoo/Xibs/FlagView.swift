//
//  QCView.swift
//  MallshopeeVendor
//
//  Created by goyal on 13/03/18.
//  Copyright © 2018 ankita. All rights reserved.
//
@objc protocol  FlagViewDelegates{
    @objc optional  func flagViewDelegatesFunction(view : FlagView,index : Int)
    
}

import UIKit

class FlagView: UIView {
 
    @IBOutlet var view: UIView!
    
    @IBOutlet weak var lblBlockTitle: UILabel!
    var delegaes : FlagViewDelegates?
    required init(coder aDecoder: NSCoder)  {
        super.init(coder: aDecoder)!
    }
    init(frame: CGRect,blockName : String)   {
        super.init(frame: frame)
        self.setup(blockName : blockName)
        
    }
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    func setup(blockName : String) {
        view = Bundle.main.loadNibNamed("FlagView", owner: self, options: nil)?[0] as? UIView
        
        view.frame = self.bounds
        view.layer.cornerRadius = 5
         self.lblBlockTitle.text = blockName
        view.addshodowToView()
        self.addSubview(view)
    }
    @IBAction func btnQCOptionTapped(_ sender: UIButton) {
        
        if sender.tag != 3 {
            self.delegaes?.flagViewDelegatesFunction!(view: self, index: sender.tag)
        }
        else{
            self.removeFromSuperview()
        }
        
    }
    
    
}
