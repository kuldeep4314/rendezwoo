//
//  TagView.swift
//  Rendenwoo
//
//  Created by goyal on 19/03/18.
//  Copyright © 2018 ankita. All rights reserved.
//

import UIKit

  @objc protocol deleteTagDelegates {
    @objc optional  func  deleteTagFunction(strIndex : String)
     @objc optional  func  nameActionDelegates()
    @objc optional  func  topViewActionDelegates()
}

@objc protocol tapDelegates {
   
    @objc optional  func  nameActionDelegates(tag : Int)
    @objc optional  func  topViewActionDelegates(tag : Int)
}

class TagView: UIView {

    @IBOutlet var gestureName: UITapGestureRecognizer!
    @IBOutlet weak var topView: UIView!
    @IBOutlet var view: UIView!
  
    var nameTag = Int()
     var topTag = Int()
    @IBOutlet weak var btnCross: UIButton!
    @IBOutlet weak var subView: UIView!
    @IBOutlet weak var lblName: UILabel!
    var delegates : deleteTagDelegates?
     var delegatesTap : tapDelegates?
    required init(coder aDecoder: NSCoder)  {
        super.init(coder: aDecoder)!
    }
    init(frame: CGRect,name: String , color : UIColor,ishowCrossbtn : Bool,hintText : String)   {
        super.init(frame: frame)
        self.setup(name: name, color: color,ishowCrossbtn : ishowCrossbtn, hintText: hintText)
        
    }
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    func setup(name: String,color : UIColor,ishowCrossbtn : Bool,hintText : String) {
        view = Bundle.main.loadNibNamed("TagView", owner: self, options: nil)?[0] as? UIView
        
        view.frame = self.bounds
        subView.layer.borderColor = color.cgColor
        subView.layer.borderWidth = 1.0
        self.btnCross.isHidden = ishowCrossbtn
        lblName.text = name
        btnCross.accessibilityHint = hintText
        lblName.backgroundColor = color
        self.addSubview(view)
    }
   
    @IBAction func btnCrossAction(_ sender: UIButton) {
        delegates?.deleteTagFunction!(strIndex: sender.accessibilityHint ?? "0,0")
    }
    @IBAction func gestureNameAction(_ sender: UITapGestureRecognizer) {
        delegatesTap?.nameActionDelegates!(tag: nameTag)
    }
    @IBAction func gestureTapViewAction(_ sender: UITapGestureRecognizer) {
        delegatesTap?.topViewActionDelegates!(tag: topTag)
    }
    
    
    
    
}
