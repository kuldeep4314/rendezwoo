//
//  PurchaseIntros.swift
//  Rendenwoo
//
//  Created by goyal on 07/03/18.
//  Copyright © 2018 ankita. All rights reserved.
//

import UIKit

class Introduce: UIView {

    @IBOutlet var view: UIView!
    
    @IBOutlet weak var lblTitle: UILabel!
    var delegates:   alertButtonDelegates?
    
    var  delegatesStr = String()
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
    }
    init(frame: CGRect, strTile : String,delegatesStr: String)   {
        super.init(frame: frame)
        self.setup(strTile: strTile,delegatesStr:delegatesStr)
        
    }
    @IBAction func btnMybelaterTapped(_ sender: UIButton) {
        self.removeFromSuperview()
        
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    func setup( strTile : String,delegatesStr: String) {
        view = Bundle.main.loadNibNamed("Introduce", owner: self, options: nil)?[0] as? UIView
        
        self.delegatesStr = delegatesStr
        view.frame = self.bounds
        view.layer.cornerRadius = 10
        view.clipsToBounds = true
        lblTitle.text = strTile
        self.addSubview(view)
    }
    @IBAction func btnGetMeIntrosTapped(_ sender: Any) {
        
               self.removeFromSuperview()
       
        delegates?.alertButtonAction!(index: 1, delegatesStr: self.delegatesStr)
               
    }
    
}

