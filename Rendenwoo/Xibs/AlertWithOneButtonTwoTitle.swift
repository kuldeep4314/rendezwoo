//
//  AlertWithOneTitle.swift
//  Rendenwoo
//
//  Created by goyal on 28/02/18.
//  Copyright © 2018 ankita. All rights reserved.
//

import UIKit

class AlertWithOneButtonTwoTitle: UIView {

    @IBOutlet weak var lblMainTitle: UILabel!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var btnDone: UIButton!

    var delegateStr = String()
    
    var delegates : alertButtonDelegates?
    @IBOutlet weak var alertView: UIView!
    @IBOutlet var view: UIView!
    
    required init(coder aDecoder: NSCoder)  {
        super.init(coder: aDecoder)!
        
    }
    init(frame: CGRect ,mainTitle : String, title : String , doneTitle : String,delegateStr: String)   {
        super.init(frame: frame)
        self.setup(mainTitle: mainTitle, title: title, doneTitle: doneTitle, delegateStr: delegateStr)
        
    }
    
    @IBAction func getureTapped(_ sender: Any) {
        self.removeFromSuperview()
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    func setup(mainTitle : String, title : String, doneTitle : String,delegateStr: String) {
        view = Bundle.main.loadNibNamed("AlertWithOneButtonTwoTitle", owner: self, options: nil)?[0] as? UIView
        self.delegateStr = delegateStr
        view.frame = self.bounds
        btnDone.setTitle(doneTitle, for: .normal)
         lblMainTitle.text = mainTitle
        lblTitle.text = title
        self.addSubview(view)
    }
    @IBAction func btnDoneAction(_ sender: Any) {
      self.removeFromSuperview()
        
        delegates?.alertButtonAction!(index: 1, delegatesStr: self.delegateStr)
    }
   
}
