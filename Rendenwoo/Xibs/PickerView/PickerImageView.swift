//
//  PickerImageView.swift
//  GrumpNow
//
//  Created by CodeYeti on 10/6/17.
//  Copyright © 2017 Avatar Singh. All rights reserved.
//

import UIKit

class PickerImageView: UIView {

  
    @IBOutlet weak var lblHours: UILabel!
   

    
    @IBOutlet var view: UIView!
    required init(coder aDecoder: NSCoder)  {
        super.init(coder: aDecoder)!
        self.setup()
    }
     override init(frame: CGRect)   {
        super.init(frame: frame)
        self.setup()
       
    }
    func setup() {
        
       view = Bundle.main.loadNibNamed("PickerImageView", owner: self, options: nil)?[0] as? UIView
        view.frame = self.bounds
        
        self.addSubview(view)
    }

}
