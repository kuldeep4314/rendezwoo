//
//  PickerView.swift
//  Rendenwoo
//
//  Created by goyal on 09/03/18.
//  Copyright © 2018 ankita. All rights reserved.
//

import UIKit
 @objc protocol setValuesDelegates {
   @objc optional   func  setTextfieldsvalues (str : String , tag: Int)
   @objc optional   func  setTextFieldsWithTwoValues (str : String , str1 : String , tag: Int)
    
    @objc optional func  setPreferenceValues(str : String , tag: Int)
    @objc optional   func  setPreferenceValuesWithTwoValues (str : String , str1 : String , tag: Int)
    
    
      @objc optional func  setIntrosFriendsValues(str : String , tag: Int)
    
}
class PickerView: UIView , UIPickerViewDelegate, UIPickerViewDataSource{

    @IBOutlet weak var btnDone: UIButton!
    @IBOutlet weak var pickerView: UIPickerView!
    @IBOutlet weak var btnCancel: UIButton!
       var previousValue = String()
      var  array = [String]()
      var numberOfComponents = Int()
      var delegates : setValuesDelegates?
    
       var tagText = Int()
       var selectedValues = String()
       var isStartingPreference = Bool()
      var selectedValues1 = String()
    
    @IBOutlet var view: UIView!
    
    
    required init(coder aDecoder: NSCoder)  {
        super.init(coder: aDecoder)!
    }
    init(frame: CGRect,  array : [String],str: String ,  tag : Int,numberOfComponents : Int ,isStartingPreference : Bool)   {
        super.init(frame: frame)
        self.setup(array: array, str: str, tag: tag, numberOfComponents: numberOfComponents, isStartingPreference: isStartingPreference)
        
    }
    @IBAction func btnCancelTapped(_ sender: UIButton) {
        self.removeFromSuperview()
        
        
        
        if numberOfComponents == 1 {
            delegates?.setTextfieldsvalues!(str: previousValue, tag: tagText)
        }
        
    }

    @IBAction func btnDoneTapped(_ sender: Any) {
          self.removeFromSuperview()
        
        if self.isStartingPreference == true {
            if numberOfComponents == 1 {
               delegates?.setPreferenceValues!(str: selectedValues, tag: tagText)
            }
            else{
                delegates?.setPreferenceValuesWithTwoValues!(str: selectedValues, str1: selectedValues1, tag: tagText)
               
            }
        }
        else{
            if numberOfComponents == 1 {
                delegates?.setTextfieldsvalues!(str: selectedValues, tag: tagText)
            }
            else{
                delegates?.setTextFieldsWithTwoValues!(str: selectedValues, str1: selectedValues1, tag: tagText)
            }
        }
    
    }
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    func setup( array : [String],str: String ,  tag : Int, numberOfComponents : Int,isStartingPreference : Bool) {
        view = Bundle.main.loadNibNamed("PickerView", owner: self, options: nil)?[0] as? UIView
        
        view.frame = self.bounds
        view.layer.cornerRadius = 10
        view.clipsToBounds = true
        pickerView.dataSource  = self
        pickerView.delegate = self
        tagText = tag
        previousValue = str
        selectedValues = str
        self.isStartingPreference  = isStartingPreference
        self.array = array
        self.numberOfComponents = numberOfComponents
        
       if self.numberOfComponents == 1 {
        if str != "" && str != "No Preference" {
            let index = array.index(of:str )
            
            self.pickerView.selectRow(index ?? 0, inComponent: 0, animated: false)
            
            self.pickerView.delegate?.pickerView!(pickerView, didSelectRow: index ?? 0, inComponent: 0)
        }
        else{
        
            self.pickerView.selectRow(array.count / 2, inComponent: 0, animated: false)
            
            self.pickerView.delegate?.pickerView!(pickerView, didSelectRow: array.count / 2, inComponent: 0)
        }
            
        }
       else{
        
        if str != "" {
            
            let arr = str.components(separatedBy: " to ")
            
            if arr.count > 0{
            let firstIndex = array.index(of:arr[0] )
               let secondIndex = array.index(of:arr[1] )
                
            self.pickerView.selectRow(firstIndex ?? 0, inComponent: 0, animated: false)
            self.pickerView.selectRow(secondIndex ?? 0, inComponent: 2, animated: false)
            self.pickerView.delegate?.pickerView!(pickerView, didSelectRow: firstIndex ?? 0, inComponent: 0)
             self.pickerView.delegate?.pickerView!(pickerView, didSelectRow: secondIndex ?? 0, inComponent: 2)
        }
        }
        else{
            
            self.pickerView.selectRow(array.count / 2, inComponent: 0, animated: false)
             self.pickerView.selectRow(array.count / 2, inComponent: 2, animated: false)
            self.pickerView.delegate?.pickerView!(pickerView, didSelectRow: array.count / 2, inComponent: 0)
             self.pickerView.delegate?.pickerView!(pickerView, didSelectRow: array.count / 2, inComponent: 2)
        }
        
    
        }
        
       
        
        pickerView.reloadAllComponents()
        self.addSubview(view)
    }
    
    
    
    //MARK:- UIPickerView DataSource
    
    public func numberOfComponents(in pickerView: UIPickerView) -> Int{
        
        return numberOfComponents
        
    }
    
    
    public func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int{
        if numberOfComponents == 1 {
             return array.count
        }
        else {
            if component == 0 {
                return array.count
            }
            else if component == 1 {
                
                return 1
            }
            else{
                return array.count
            }

        }
        
    
    }
    

    //MARK:- PickerView Delegates
    public func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView{
        
        
           if numberOfComponents == 1 {
            
            let view = PickerImageView.init(frame: CGRect.init(x: 0, y: 0, width: pickerView.frame.width , height: 46))
            
            view.lblHours.text = array[row]
            
            return view
            
            
        }
           else{
            
            if component == 0 {
                let view = PickerImageView.init(frame: CGRect.init(x: 0, y: 0, width: pickerView.frame.width/3 , height: 46))
                
                view.lblHours.text = array[row]
                
                return view
            }
            else if component == 1 {
               
                
                let view = PickerImageView.init(frame: CGRect.init(x: 0, y: 0, width: pickerView.frame.width/3 , height: 46))
                
                view.lblHours.text = "to"
                
                return view
             }
                
            else{
                let view = PickerImageView.init(frame: CGRect.init(x: 0, y: 0, width: pickerView.frame.width/3 , height: 46))
                
                view.lblHours.text = array[row]
                
                return view
            }
            
            
        }
        
    }
    
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
          if numberOfComponents == 1 {
            
             self.selectedValues = array[row]
        }
          else {
            
            if component == 0 {
                 self.selectedValues = array[row]
            }
                else if component == 1 {
                
                print("to")
                
            }
            else if component == 2 {
                
                 self.selectedValues1 = array[row]
                
            }
        }
       
        
        
        
    }
    public func pickerView(_ pickerView: UIPickerView, rowHeightForComponent component: Int) -> CGFloat{
        return 44
    }
    
    
}
