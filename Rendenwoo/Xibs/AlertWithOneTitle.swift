//
//  AlertWithOneTitle.swift
//  Rendenwoo
//
//  Created by goyal on 28/02/18.
//  Copyright © 2018 ankita. All rights reserved.
//

import UIKit

class AlertWithOneTitle: UIView {

    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var btnDone: UIButton!
    @IBOutlet weak var btnCancel: UIButton!
     var delegates : alertButtonDelegates?
     var delegatesStr = String()
    @IBOutlet weak var alertView: UIView!
    @IBOutlet var view: UIView!
    required init(coder aDecoder: NSCoder)  {
        super.init(coder: aDecoder)!
        
    }
    init(frame: CGRect , title : String, cancelTitle: String , doneTitle : String,delegatesStr: String)   {
        super.init(frame: frame)
        self.setup(title: title, cancelTitle: cancelTitle, doneTitle: doneTitle,delegatesStr:delegatesStr)
        
    }
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    func setup( title : String, cancelTitle: String , doneTitle : String,delegatesStr: String) {
        view = Bundle.main.loadNibNamed("AlertWithOneTitle", owner: self, options: nil)?[0] as? UIView
        
        view.frame = self.bounds
         self.delegatesStr = delegatesStr
        btnDone.setTitle(doneTitle, for: .normal)
          btnCancel.setTitle(cancelTitle, for: .normal)
        lblTitle.text = title
        self.addSubview(view)
    }
    @IBAction func btnDoneAction(_ sender: Any) {
        
        self.delegates?.alertButtonAction!(index: 2, delegatesStr: self.delegatesStr)
          self.removeFromSuperview()
    }
    @IBAction func btnCancelActrion(_ sender: Any) {
         self.removeFromSuperview()
        self.delegates?.alertButtonAction!(index: 1, delegatesStr: self.delegatesStr)
    }
}
