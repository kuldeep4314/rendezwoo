//
//  CrushView.swift
//  Rendenwoo
//
//  Created by goyal on 07/03/18.
//  Copyright © 2018 ankita. All rights reserved.
//

import UIKit

class CrushView: UIView {

    @IBOutlet var view: UIView!
    
    
    required init(coder aDecoder: NSCoder)  {
        super.init(coder: aDecoder)!
    }
    override init(frame: CGRect)   {
        super.init(frame: frame)
        self.setup()
        
    }
    @IBAction func btnMybelaterTapped(_ sender: UIButton) {
        self.removeFromSuperview()
        
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    func setup() {
        view = Bundle.main.loadNibNamed("CrushView", owner: self, options: nil)?[0] as? UIView
        
        view.frame = self.bounds
        view.layer.cornerRadius = 10
        view.clipsToBounds = true
        self.addSubview(view)
    }
    @IBAction func btnGetMeIntrosTapped(_ sender: Any) {
        
        
    }
    
}
