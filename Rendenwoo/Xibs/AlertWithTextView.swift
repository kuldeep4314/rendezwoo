//
//  AlertWithOneTitle.swift
//  Rendenwoo
//
//  Created by goyal on 28/02/18.
//  Copyright © 2018 ankita. All rights reserved.
//

import UIKit
protocol friendwithMessageDelegates {
    
    func  friendwithMessageFunction(text : String,commanUserid : String, introduceToId : String,relationShipStatus : String)
    
}
class AlertWithTextView: UIView ,UITextViewDelegate{
    
      var commanUserId = String()
      var introduceToId = String()
      var relationShipStatus = String()
    @IBOutlet weak var btnDone: UIButton!
    @IBOutlet weak var txtHeight: NSLayoutConstraint!
    @IBOutlet weak var txtView: UITextView!
    @IBOutlet weak var btnCancel: UIButton!
    var delegates : friendwithMessageDelegates?
    @IBOutlet weak var alertView: UIView!
    @IBOutlet var view: UIView!
    @IBOutlet weak var lblCount: UILabel!
    var placeHolder = String()
    required init(coder aDecoder: NSCoder)  {
        super.init(coder: aDecoder)!
        
    }
    init(frame: CGRect , title : String, cancelTitle: String , doneTitle : String,commanUserId: String,introduceToId: String,relationShipStatus : String)   {
        super.init(frame: frame)
        self.setup(title: title, cancelTitle: cancelTitle, doneTitle: doneTitle,commanUserId: commanUserId,introduceToId: introduceToId,relationShipStatus : relationShipStatus)
        
    }
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    func setup( title : String, cancelTitle: String , doneTitle : String,commanUserId: String,introduceToId: String,relationShipStatus : String) {
        view = Bundle.main.loadNibNamed("AlertWithTextView", owner: self, options: nil)?[0] as? UIView
        
        view.frame = self.bounds
        placeHolder = title
        self.relationShipStatus = relationShipStatus
        self.commanUserId = commanUserId
        self.introduceToId = introduceToId
        self.relationShipStatus = relationShipStatus
        btnDone.setTitle(doneTitle, for: .normal)
        btnCancel.setTitle(cancelTitle, for: .normal)
        txtView.isUserInteractionEnabled = false
        lblCount.isHidden = true
        txtView.text = placeHolder
        
        
       
        self.addSubview(view)
    }
    @IBAction func btnDoneAction(_ sender: Any) {
        
  delegates?.friendwithMessageFunction(text: self.txtView.text, commanUserid: self.commanUserId, introduceToId:  self.introduceToId, relationShipStatus:  self.relationShipStatus)
        self.removeFromSuperview()
    }
    @IBAction func btnCancelActrion(_ sender: Any) {
   
        txtView.isUserInteractionEnabled = true
        lblCount.isHidden = false
        txtView.textColor = lightGray
        txtView.becomeFirstResponder()
        
    }
    
    
    func backSpaceTapped(string: String)-> Bool{
        
        
        if string.cString(using: String.Encoding.utf8) != nil {
            return true
        } else if string.isEmpty {
            return true
        } else {
            return false
        }
        
    }
    
    //MARK:- TextView Delegates
    func textViewDidBeginEditing(_ textView: UITextView) {
        
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        
        
        if textView.text.isEmpty {
            textView.text = placeHolder
    
        }
        txtView.isUserInteractionEnabled = false
        lblCount.isHidden = true
       txtView.textColor = UIColor.black
      
    }
    
    func textViewDidChange(_ textView: UITextView) {
        
        if self.backSpaceTapped(string: textView.text) == true{
            let lastChar = placeHolder.last
            let text = textView.text
            let text2 =   "\(text!)\(lastChar!)"
          
            if placeHolder == text2{
                textView.text = nil
                textView.textColor = UIColor.black
            }
        }
        
        if textView.text.contains(placeHolder) == true{
             textView.text = nil
             textView.textColor = UIColor.black
            
        }
        
        if textView.contentSize.height > 60{
            let height = textView.sizeThatFits(textView.contentSize).height
            
            self.txtHeight.constant = height
        } else{
            self.txtHeight.constant = 60
        }
        
    }
    @IBAction func tabGesture(_ sender: UITapGestureRecognizer) {
        self.removeFromSuperview()
    }
    
    
    
}

