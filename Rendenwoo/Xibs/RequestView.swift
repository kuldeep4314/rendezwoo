//
//  RequestView.swift
//  Rendenwoo
//
//  Created by goyal on 05/03/18.
//  Copyright © 2018 ankita. All rights reserved.
//

import UIKit

class RequestView: UIView {
 @IBOutlet var view: UIView!
   
    
    @IBOutlet weak var Title: UILabel!
    required init(coder aDecoder: NSCoder)  {
        super.init(coder: aDecoder)!
    }
    init(frame: CGRect,titleString: String)   {
        super.init(frame: frame)
        self.setup(titleString: titleString)
        
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    func setup(titleString: String) {
        view = Bundle.main.loadNibNamed("RequestView", owner: self, options: nil)?[0] as? UIView
        
        view.frame = self.bounds
        self.Title.text = titleString
        self.addSubview(view)
    }
    

}
