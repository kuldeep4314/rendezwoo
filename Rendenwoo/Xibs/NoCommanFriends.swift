//
//  NoCommanFriends.swift
//  Rendenwoo
//
//  Created by goyal on 07/03/18.
//  Copyright © 2018 ankita. All rights reserved.
//

import UIKit

class NoCommanFriends: UIView {

    @IBOutlet var view: UIView!
     var delegates:   alertButtonDelegates?
    @IBOutlet weak var lblDetails: UILabel!
      var  delegatesStr = String()
    required init(coder aDecoder: NSCoder)  {
        super.init(coder: aDecoder)!
    }
     init(frame: CGRect,strTile : String,delegatesStr: String)   {
        super.init(frame: frame)
        self.setup(strTile: strTile, delegatesStr: delegatesStr)
        
    }
    @IBAction func btnMybelaterTapped(_ sender: UIButton) {
        self.removeFromSuperview()
        
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    func setup(strTile : String,delegatesStr: String) {
        view = Bundle.main.loadNibNamed("NoCommanFriends", owner: self, options: nil)?[0] as? UIView
        
        
        view.frame = self.bounds
        view.layer.cornerRadius = 10
        view.clipsToBounds = true
        self.lblDetails.text = strTile
         self.delegatesStr = delegatesStr
        self.addSubview(view)
    }
    @IBAction func btnGetMeIntroduced(_ sender: Any) {
           self.removeFromSuperview()
        self.delegates?.alertButtonAction!(index: 1, delegatesStr:  self.delegatesStr)
     
       
    }
    @IBAction func takeMematchMakerAction(_ sender: UIButton) {
        self.removeFromSuperview()
        self.delegates?.alertButtonAction!(index: 2, delegatesStr:  self.delegatesStr)
    }
    
}
