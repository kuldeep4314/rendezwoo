//
//  AlertWithOneTitle.swift
//  Rendenwoo
//
//  Created by goyal on 28/02/18.
//  Copyright © 2018 ankita. All rights reserved.
//

import UIKit
@objc  protocol menuButtonDelegates{
    @objc  optional func menuButtonAction(view:Menu,index : Int,selectedIndex : Int,delegateStr : String)
    
}
class Menu: UIView {

   
    @IBOutlet weak var img3: UIImageView!
    @IBOutlet weak var img2: UIImageView!
    @IBOutlet weak var img1: UIImageView!
    @IBOutlet weak var lbl3: UILabel!
    @IBOutlet weak var lbl2: UILabel!
    @IBOutlet weak var lbl1: UILabel!
    @IBOutlet var view: UIView!
    var delegateStr  = String()

    var selectedRow = Int()
    var delegates: menuButtonDelegates?
    required init(coder aDecoder: NSCoder)  {
        super.init(coder: aDecoder)!
        
    }
    init(frame: CGRect, array : [String],imgArray : [String],selectedIndex : Int,delegateStr : String)   {
        super.init(frame: frame)
        self.setup(array: array,imgArray: imgArray, selectedIndex: selectedIndex, delegateStr: delegateStr)
        
    }
    @IBAction func tapOnScreenAction(_ sender: Any) {
        self.removeFromSuperview()
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    func setup( array : [String] , imgArray : [String],selectedIndex: Int,delegateStr : String) {
        view = Bundle.main.loadNibNamed("Menu", owner: self, options: nil)?[0] as? UIView
        
        view.frame = self.bounds
        lbl1.text = array[0]
        lbl2.text = array[1]
        lbl3.text = array[2]
        
         img1.image = UIImage.init(named: imgArray[0])
         img2.image = UIImage.init(named: imgArray[1])
         img3.image = UIImage.init(named: imgArray[2])
         self.delegateStr = delegateStr
        img1.tintColor = UIColor.black
        img2.tintColor = UIColor.black
        img3.tintColor = UIColor.black
        selectedRow = selectedIndex
        self.addSubview(view)
    }
    @IBAction func btnOptionTapped(_ sender: UIButton) {
        self.removeFromSuperview()
        delegates?.menuButtonAction!(view: self, index: sender.tag, selectedIndex: selectedRow, delegateStr: self.delegateStr)

    }
    
}
