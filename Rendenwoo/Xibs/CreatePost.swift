//
//  CreatePost.swift
//  Rendenwoo
//
//  Created by goyal on 23/03/18.
//  Copyright © 2018 ankita. All rights reserved.
//

import UIKit

@objc protocol editPostDelegates {
    @objc optional func editPostDelegatesFunction(view : CreatePost,index : Int , text : String)
    @objc optional func createPostDelegatesFunction(view : CreatePost, text : String)
    
    @objc optional  func uploadPostDelegates(view : CreatePost,image_id : String ,caption : String )
}

class CreatePost: UIView ,UITextViewDelegate{


    @IBOutlet weak var conHeight: NSLayoutConstraint!
    @IBOutlet weak var subView: UIView!
    
    @IBOutlet weak var txtHeight: NSLayoutConstraint!
    var isEditPost = Bool()
    var imageId = String()
    var  delegates : editPostDelegates?
    @IBOutlet weak var userImage: UIImageView!
    @IBOutlet weak var lblPost: UILabel!
    @IBOutlet var view: UIView!
    
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var btnCancel: UIButton!
    @IBOutlet weak var lblName: UILabel!
  
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var textView: UITextView!
    let placeHolder = "What's happening in your world?"
    var  delegatesStr = String()
    var selectedIndex = Int()
    var isSharedPostMatchMaker = Bool()
    @IBOutlet weak var btnPost: UIButton!
    
  
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
    }
    override init(frame: CGRect)   {
        super.init(frame: frame)
        self.setup(title: "Create Post")
        
    }
   
    init(frame: CGRect,post : RZMatchMaker,index : Int,isEditPost : Bool) {
        super.init(frame: frame)
        
        self.setup(title: "Edit Post",post: post, index : index , isEditPost: isEditPost)
        
    }
    
    init(frame: CGRect, imageId : String , isSharedPostMatchMaker : Bool,imageUrl : String) {
        super.init(frame: frame)
        
      self.setupSharePost(imageId: imageId, isSharedPostMatchMaker: isSharedPostMatchMaker, imageUrl: imageUrl)
        
    }
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func setupSharePost(imageId : String,isSharedPostMatchMaker : Bool,imageUrl : String){
        view = Bundle.main.loadNibNamed("CreatePost", owner: self, options: nil)?[0] as? UIView
        view.frame = self.bounds
        
        topView.backgroundColor = moreLight
       
          conHeight.constant = 180
        self.isSharedPostMatchMaker = isSharedPostMatchMaker
        self.imageId = imageId
        subView.layer.cornerRadius = 5
        subView.clipsToBounds = true
        
        self.btnCancel.setTitleColor(appColor, for: .normal)
        self.btnPost.setTitleColor(appColor, for: .normal)
        
        let userData = UserData.init(json: JSON(standard.value(forKey: "userData") as? [String : Any] ?? [String : Any]()))
        let imageURL = userData.image ?? ""
        
        lblName.text = userData.name
        
        
        userImage.sd_setImage(with: URL(string: imageURL), placeholderImage: #imageLiteral(resourceName: "imgProfile"), options: [], completed: nil)
        
        imageView.sd_setImage(with: URL(string: imageUrl), placeholderImage:#imageLiteral(resourceName: "imgPlaceHolder"), options: [], completed: nil)
   
            textView.text = placeHolder
            textView.textColor = lightGray
        
        self.addSubview(view)
        
        
    }
    
    
    
    func setup(title : String,post : RZMatchMaker, index : Int ,isEditPost : Bool) {
        view = Bundle.main.loadNibNamed("CreatePost", owner: self, options: nil)?[0] as? UIView
        view.frame = self.bounds
      
        topView.backgroundColor = moreLight
        
        if post.imageUrl == nil || post.imageUrl == "" {
            conHeight.constant = 0
            
        }
        else{
            conHeight.constant = 180
        }
        
        subView.layer.cornerRadius = 5
        subView.clipsToBounds = true
        selectedIndex = index
        self.isEditPost = isEditPost
        self.lblPost.text = title
        self.btnCancel.setTitleColor(appColor, for: .normal)
        self.btnPost.setTitleColor(appColor, for: .normal)
        
          let userData = UserData.init(json: JSON(standard.value(forKey: "userData") as? [String : Any] ?? [String : Any]()))
        let imageURL = userData.image ?? ""
        
        lblName.text = userData.name
       
        
        userImage.sd_setImage(with: URL(string: imageURL), placeholderImage: #imageLiteral(resourceName: "imgProfile"), options: [], completed: nil)
        
         imageView.sd_setImage(with: URL(string: post.imageUrl ?? ""), placeholderImage:#imageLiteral(resourceName: "imgPlaceHolder"), options: [], completed: nil)
        
        if post.caption?.base64Decoded() == "" {
              textView.text = placeHolder
            textView.textColor = lightGray
        }
        else{
              textView.text = post.caption?.base64Decoded()
             textView.textColor = UIColor.black
        }
       
        self.addSubview(view)
    }
    
    

    func setup(title : String) {
        view = Bundle.main.loadNibNamed("CreatePost", owner: self, options: nil)?[0] as? UIView
          view.frame = self.bounds
        textView.text = placeHolder
        textView.textColor = lightGray
        self.lblPost.text = title
        topView.backgroundColor = moreLight
        conHeight.constant = 0
        subView.layer.cornerRadius = 5
        subView.clipsToBounds = true
        self.btnCancel.setTitleColor(appColor, for: .normal)
         self.btnPost.setTitleColor(appColor, for: .normal)
      
        let userData = UserData.init(json: JSON(standard.value(forKey: "userData") as? [String : Any] ?? [String : Any]()))
         let imageURL = userData.image ?? ""
         userImage.sd_setImage(with: URL(string: imageURL), placeholderImage: #imageLiteral(resourceName: "imgProfile"), options: [], completed: nil)
        lblName.text = userData.name
       
        self.addSubview(view)
    }
    @IBAction func btnCancelTapped(_ sender: Any) {
        self.removeFromSuperview()
    }
    @IBAction func btnPostTapped(_ sender: UIButton) {
        
        if isSharedPostMatchMaker == true {
            if (textView.text != "" ||  textView.text != nil) && (textView.text != placeHolder){
                delegates?.uploadPostDelegates!(view: self,image_id: self.imageId, caption: textView.text.base64Encoded() ?? "")
            }
            else{
                textView.shake(count: 2, for: 0.2, withTranslation: 10)
            }
            
        }
        else{
            if isEditPost == true {
                if (textView.text != "" ||  textView.text != nil) && (textView.text != placeHolder){
                    delegates?.editPostDelegatesFunction!(view: self,index: self.selectedIndex, text: textView.text.base64Encoded() ?? "")
                }
                else{
                    textView.shake(count: 2, for: 0.2, withTranslation: 10)
                }
            }
            else{
                if (textView.text != "" ||  textView.text != nil) && (textView.text != placeHolder){
                    delegates?.createPostDelegatesFunction!(view: self, text: textView.text.base64Encoded() ?? "")
                }
                else{
                    textView.shake(count: 2, for: 0.2, withTranslation: 10)
                }
            }
        }
        
    }
    
    //MARK:- TextView Delegates
    func textViewDidBeginEditing(_ textView: UITextView) {
        
        
        if textView.textColor == lightGray
        {
            textView.text = nil
            textView.textColor = UIColor.black
            print("begin working")
        }
    }
    
    
    func textViewDidEndEditing(_ textView: UITextView) {
        
        
        if textView.text.isEmpty {
            textView.text = placeHolder
            textView.textColor = lightGray
           
            print("end working")
        }
        
    }
    
   
    func textViewDidChange(_ textView: UITextView) {
        
    
        if textView.contentSize.height > 40{
            let height = textView.sizeThatFits(textView.contentSize).height
            
            self.txtHeight.constant = height
        } else{
            self.txtHeight.constant = 40
        }
        
    }
    

}
