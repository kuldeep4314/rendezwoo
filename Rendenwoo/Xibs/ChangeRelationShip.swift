//
//  PurchaseIntros.swift
//  Rendenwoo
//
//  Created by goyal on 07/03/18.
//  Copyright © 2018 ankita. All rights reserved.
//

import UIKit

class ChangeRelationShip: UIView {

    @IBOutlet var view: UIView!
    
    @IBOutlet weak var btnNotIntertested: UIButton!
    @IBOutlet weak var btnChange: UIButton!
    @IBOutlet weak var lblTitle: UILabel!
    var delegates: alertButtonDelegates?
    required init(coder aDecoder: NSCoder)  {
        super.init(coder: aDecoder)!
    }
    var delegatesStr = String()
    init(frame: CGRect ,title : String, btn1Title : String, btn2Title : String,delegatesStr: String)   {
        super.init(frame: frame)
        self.setup( title: title,btn1Title:btn1Title,btn2Title:btn2Title,delegatesStr: delegatesStr)
        
    }
    @IBAction func gestureTapped(_ sender: Any) {
        self.removeFromSuperview()
    }
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    func setup(title : String, btn1Title : String, btn2Title : String,delegatesStr: String) {
        view = Bundle.main.loadNibNamed("ChangeRelationShip", owner: self, options: nil)?[0] as? UIView
        self.delegatesStr = delegatesStr
        view.frame = self.bounds
        view.layer.cornerRadius = 10
        view.clipsToBounds = true
        lblTitle.text = title
          btnChange.setTitle(btn1Title, for: .normal)
         btnNotIntertested.setTitle(btn2Title, for: .normal)
        self.addSubview(view)
    }
    @IBAction func btnChangeRelationShip(_ sender: Any) {
        self.removeFromSuperview()
        self.delegates?.alertButtonAction!(index: 1, delegatesStr: delegatesStr)
    }
    @IBAction func btnNotInterestedTapped(_ sender: Any) {
         self.removeFromSuperview()
        self.delegates?.alertButtonAction!(index: 2, delegatesStr: delegatesStr)
    }
    
}
