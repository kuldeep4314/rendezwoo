//
//  RoundView.swift
//  Rendenwoo
//
//  Created by goyal on 01/03/18.
//  Copyright © 2018 ankita. All rights reserved.
//

import UIKit

class RoundView: UIView {

    override func layoutSubviews() {
        super.layoutSubviews()
        
        self.roundCorners([.bottomRight, .bottomLeft], radius: self.frame.width / 2)
    }

}
