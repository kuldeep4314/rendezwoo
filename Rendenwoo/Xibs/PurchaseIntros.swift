//
//  PurchaseIntros.swift
//  Rendenwoo
//
//  Created by goyal on 07/03/18.
//  Copyright © 2018 ankita. All rights reserved.
//

import UIKit

class PurchaseIntros: UIView {

    var string = """
You need to be introduced to Singer12 before you can send him a message directly. Looks like you don't have a common friend and you have used all 3 of your FREE Intros.
No worries!
Go to matchmaker wall to ask about Singer12 for free 🤞

OR

Choose a paid option to get introduced your crush instantly! ❤️
"""
    
    @IBOutlet var view: UIView!
    
    @IBOutlet weak var lblPurchaseDetails: UILabel!
    var delegates: alertButtonDelegates?
    
    
     var  delegateStr = String()
    required init(coder aDecoder: NSCoder)  {
        super.init(coder: aDecoder)!
    }
    init(frame: CGRect,name : String,delegateStr : String)   {
        super.init(frame: frame)
        self.setup(name:name,delegateStr: delegateStr)
        
    }
    @IBAction func btnMybelaterTapped(_ sender: UIButton) {
        self.removeFromSuperview()
        
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    func setup(name : String,delegateStr : String) {
        view = Bundle.main.loadNibNamed("PurchaseIntros", owner: self, options: nil)?[0] as? UIView
         self.delegateStr = delegateStr
        
        let description = string.replacingOccurrences(of: "Singer12", with: name)
        lblPurchaseDetails.text = description
        view.frame = self.bounds
        view.layer.cornerRadius = 10
        view.clipsToBounds = true
        self.addSubview(view)
    }
    @IBAction func btnOptionTapped(_ sender: UIButton) {
        
        delegates?.alertButtonAction!(index: sender.tag, delegatesStr: self.delegateStr)
    }
    
    
}
