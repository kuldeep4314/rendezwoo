//
//  AlertWithOneTitle.swift
//  Rendenwoo
//
//  Created by goyal on 28/02/18.
//  Copyright © 2018 ankita. All rights reserved.
//

import UIKit
@objc protocol okButtonDelegates{
    @objc optional func okTappedfunction()
}

class AlertWithOneButton: UIView {

    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var btnDone: UIButton!

    var delegates: okButtonDelegates?
    @IBOutlet weak var alertView: UIView!
    @IBOutlet var view: UIView!
    required init(coder aDecoder: NSCoder)  {
        super.init(coder: aDecoder)!
        
    }
    init(frame: CGRect , title : String , doneTitle : String, bgColor : UIColor)   {
        super.init(frame: frame)
        self.setup(title: title, doneTitle: doneTitle,bgColor: bgColor)
        
    }
    
    @IBAction func getureTapped(_ sender: Any) {
        self.removeFromSuperview()
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    func setup( title : String, doneTitle : String, bgColor : UIColor) {
        view = Bundle.main.loadNibNamed("AlertWithOneButton", owner: self, options: nil)?[0] as? UIView
        
        view.frame = self.bounds
        btnDone.setTitle(doneTitle, for: .normal)
       
        lblTitle.text = title
       
        
        if bgColor == colorWithopacity {
           self.backgroundColor = colorWithopacity.withAlphaComponent(0.5)
            
        }
        else{
             self.backgroundColor = bgColor
        }
        self.addSubview(view)
    }
    @IBAction func btnDoneAction(_ sender: Any) {
        self.removeFromSuperview()
        delegates?.okTappedfunction!()
        
    }
    @IBAction func btnCancelActrion(_ sender: Any) {
        self.removeFromSuperview()
    }
}
