//
//  AlertWithOneTitle.swift
//  Rendenwoo
//
//  Created by goyal on 28/02/18.
//  Copyright © 2018 ankita. All rights reserved.
//

import UIKit

@objc  protocol alertButtonDelegates{
    @objc  optional func alertButtonAction(index : Int, delegatesStr:String)

}



class AlertWithTwoTitle: UIView {

    @IBOutlet weak var iconHeight: NSLayoutConstraint!
    @IBOutlet weak var bottomCons: NSLayoutConstraint!
    @IBOutlet weak var lblSubTitle: UILabel!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var btnDone: UIButton!
    @IBOutlet weak var btnCancel: UIButton!
    var delegates : alertButtonDelegates?
    @IBOutlet weak var firstImage: UIImageView!
    @IBOutlet weak var secondImage: UIImageView!
    @IBOutlet weak var alertView: UIView!
    @IBOutlet var view: UIView!
    var delegatesStr = String()
    required init(coder aDecoder: NSCoder)  {
        super.init(coder: aDecoder)!
        
    }
    init(frame: CGRect , title : String, Subtitle : String, cancelTitle: String , doneTitle : String , titleFont : UIFont, isShowImage : Bool , secondImage : UIImage,delegatesStr: String)   {
        super.init(frame: frame)
        self.setup(title: title, Subtitle: Subtitle, cancelTitle: cancelTitle, doneTitle: doneTitle, titleFont: titleFont, isShowImage: isShowImage, secondImage:secondImage, delegatesStr: delegatesStr )
        
    }
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    func setup( title : String, Subtitle : String, cancelTitle: String , doneTitle : String , titleFont : UIFont, isShowImage : Bool, secondImage : UIImage,delegatesStr: String) {
        view = Bundle.main.loadNibNamed("AlertWithTwoTitle", owner: self, options: nil)?[0] as? UIView
        
        view.frame = self.bounds
        self.delegatesStr = delegatesStr
        btnDone.setTitle(doneTitle, for: .normal)
        btnCancel.setTitle(cancelTitle, for: .normal)
        lblTitle.text = title
        lblTitle.font = titleFont
        lblSubTitle.text = Subtitle
        self.secondImage.image = secondImage
        if isShowImage == false {
            iconHeight.constant = 0
            bottomCons.constant = 5
        }
        
        self.addSubview(view)
    }
    @IBAction func btnDoneAction(_ sender: Any) {
        self.delegates?.alertButtonAction!(index: 2, delegatesStr: self.delegatesStr)
        self.removeFromSuperview()
        
    }
    @IBAction func btnCancelActrion(_ sender: Any) {
        self.delegates?.alertButtonAction!(index: 1, delegatesStr: self.delegatesStr)
        self.removeFromSuperview()
    }
}
