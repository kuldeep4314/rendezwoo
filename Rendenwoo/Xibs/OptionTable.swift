//
//  OptionTable.swift
//  Rendenwoo
//
//  Created by goyal on 28/02/18.
//  Copyright © 2018 ankita. All rights reserved.
//

import UIKit

class OptionTable: UIView ,UITableViewDelegate,UITableViewDataSource{
   var data = [String]()
    @IBOutlet weak var tableView: UITableView!
   var isStartingPreference  = Bool()
    var delegates : setValuesDelegates?
    var tagText = Int()
    var titleString = String()
    var selectedValues = String()
    @IBOutlet var view: UIView!
    required init(coder aDecoder: NSCoder)  {
        super.init(coder: aDecoder)!
        
    }
    init(frame: CGRect , dataArr : [String], tableframe: CGRect , tag : Int  ,strValue : String,isStartingPreference: Bool,titleString : String)   {
        super.init(frame: frame)
        self.setup(dataArr: dataArr, tableframe: tableframe, tag: tag, strValue: strValue,isStartingPreference:isStartingPreference, titleString: titleString)
        
    }
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    func setup(dataArr : [String], tableframe: CGRect, tag : Int,strValue : String,isStartingPreference: Bool,titleString : String) {
         view = Bundle.main.loadNibNamed("OptionTable", owner: self, options: nil)?[0] as? UIView
        view.frame = self.bounds
        data = dataArr
        tagText = tag
        self.titleString = titleString
        self.isStartingPreference = isStartingPreference
        selectedValues = strValue
        self.tableView.register(UINib.init(nibName: "OptionTableViewCell", bundle: nil), forCellReuseIdentifier: "cell")
        self.tableView.frame = tableframe
       
        tableView.tableFooterView = UIView()
        self.addSubview(view)
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! OptionTableViewCell
        cell.lblTitle.text = data[indexPath.row]
        
        if data[indexPath.row] == selectedValues {
           cell.imgTick.isHidden = false
              cell.lblTitle.textColor = red
        }
        else{
             cell.imgTick.isHidden = true
            cell.lblTitle.textColor = UIColor.black
        }
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
          tableView.deselectRow(at: indexPath, animated: true)
        
            if  self.isStartingPreference == true {
                delegates?.setPreferenceValues!(str: data[indexPath.row], tag: tagText)
            }
            else {
                  delegates?.setTextfieldsvalues!(str: data[indexPath.row], tag: tagText)
            }
            
          
        
        self.tableView.reloadData()
        self.removeFromSuperview()
      
    }
    
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
       
            return 0
        
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let vc = RequestView.init(frame: CGRect.init(x: 0, y: 0, width: tableView.frame.width, height: 100),titleString : titleString)
        return vc
    }
    
}
