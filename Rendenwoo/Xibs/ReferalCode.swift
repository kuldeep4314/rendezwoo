//
//  InviteFriends.swift
//  Rendenwoo
//
//  Created by goyal on 05/04/18.
//  Copyright © 2018 ankita. All rights reserved.
//

import UIKit

class ReferalCode: UIView {

  
    @IBOutlet weak var txtRefCode: AETextFieldValidator!
   
    @IBOutlet var view: UIView!
     var delegateStr = String()
      var delegates : alertButtonDelegates?
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
    }
    init(frame: CGRect , strDelgates : String)   {
        super.init(frame: frame)
        self.setup(strDelgates : strDelgates)
        
    }
    @IBAction func gestureTapped(_ sender: UITapGestureRecognizer) {
        self.removeFromSuperview()
    }
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    func setup(strDelgates : String) {
        view = Bundle.main.loadNibNamed("ReferalCode", owner: self, options: nil)?[0] as? UIView
        view.frame = self.bounds
        self.delegateStr = strDelgates
       
         txtRefCode.addRegx(referalReg, withMsg: invalidReferal)
      
        self.addSubview(view)
    }
   
    
    @IBAction func btnCountinueTapped(_ sender: UIButton) {
       
        
        if txtRefCode.validate() == true {
        let parameters : Parameters = [
            
            "user_id": UserInfo.sharedInstance.userId ?? 0,
            
            "referral_code": txtRefCode.text ?? "" ]
        self.referalApiCall(parameters: parameters)
        
        }
    }
    @IBAction func btnCancelTapped(_ sender: UIButton) {
        self.removeFromSuperview()
        
    }
    
  
    //MARK:- ReferalApiCall API Call
    func referalApiCall(parameters: Parameters){
        AccountApiInterface.sharedInstance.postRequestWithParam(fromViewController: (appDelegates.window?.rootViewController)!, actionName: "referral", dictParam: parameters as [String : AnyObject], isShowIndicator: true, success: { (json) in
            print(json)
//             self.delegates?.alertButtonAction!(index: 1, delegatesStr: self.delegateStr)
            self.removeFromSuperview()
            
        }) { (error) in
            appDelegates.window?.rootViewController?.showAlert(messageStr: error)
        }
        
    }
   
}
