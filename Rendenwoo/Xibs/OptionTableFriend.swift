//
//  OptionTable.swift
//  Rendenwoo
//
//  Created by goyal on 28/02/18.
//  Copyright © 2018 ankita. All rights reserved.
//

import UIKit
 protocol setFriendsDelegates {
    
    func  setFriendsValues(friend : JSON)
    
}
class OptionTableFriend: UIView ,UITableViewDelegate,UITableViewDataSource{
   var data = [JSON]()
    @IBOutlet weak var tableView: UITableView!

    var delegates : setFriendsDelegates?
  var selectedValues = String()
    @IBOutlet var view: UIView!
    required init(coder aDecoder: NSCoder)  {
        super.init(coder: aDecoder)!
        
    }
    init(frame: CGRect , dataArr : [JSON], tableframe: CGRect)   {
        super.init(frame: frame)
        
        self.setup(dataArr: dataArr, tableframe: tableframe)
    }
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    func setup(dataArr : [JSON], tableframe: CGRect) {
         view = Bundle.main.loadNibNamed("OptionTableFriend", owner: self, options: nil)?[0] as? UIView
        view.frame = self.bounds
        self.tableView.register(UINib.init(nibName: "OptionTableViewCell", bundle: nil), forCellReuseIdentifier: "cell")
        self.tableView.frame = tableframe
          data = dataArr
        tableView.tableFooterView = UIView()
        self.addSubview(view)
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! OptionTableViewCell
        cell.lblTitle.text = data[indexPath.row]["name"].stringValue
        
//        if data[indexPath.row].stringValue == selectedValues {
//           cell.imgTick.isHidden = false
//              cell.lblTitle.textColor = red
//        }
//        else{
//             cell.imgTick.isHidden = true
//            cell.lblTitle.textColor = UIColor.black
//        }
           cell.imgTick.isHidden = true
        cell.lblTitle.textColor = UIColor.black
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
          tableView.deselectRow(at: indexPath, animated: true)
        
        
     delegates?.setFriendsValues(friend: data[indexPath.row])
        self.tableView.reloadData()
        self.removeFromSuperview()
      
    }
    
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
    
            return 100
        
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let vc = RequestView.init(frame: CGRect.init(x: 0, y: 0, width: tableView.frame.width, height: 100),titleString : "Request a common friend below to introduce you to your crush!")
        return vc
    }
    
}
