//
//  AppDelegate.swift
//  Rendenwoo
//
//  Created by goyal on 23/02/18.
//  Copyright © 2018 ankita. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import EFInternetIndicator
import NUI
import NUIParse
import CoreLocation
import FBSDKCoreKit
import FBSDKLoginKit
import SwiftyStoreKit
import GoogleMaps
import GooglePlaces
import UserNotifications
import Firebase
import Fabric
import Crashlytics
@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate , CLLocationManagerDelegate {
var locationManager = CLLocationManager()
    var window: UIWindow?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
          FirebaseApp.configure()
          NUISettings.initWithStylesheet("Theme")
    
        GMSServices.provideAPIKey(GMapKey)
        GMSPlacesClient.provideAPIKey(GMapKey)
         IQKeyboardManager.sharedManager().enable = true
        UIApplication.shared.statusBarStyle = .lightContent
        
        appDelegatesApplication = application
       
        self.inAppPurchaseComplete()

       self.autoLoginFunction()
        standard.setValue("123456", forKey: "gcm")
    
           SocketBase.sharedInstance
              Fabric.with([Crashlytics.self])
        
        return FBSDKApplicationDelegate.sharedInstance().application(application, didFinishLaunchingWithOptions: launchOptions)
    }

    func applicationWillResignActive(_ application: UIApplication) {
        
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
       
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        let nc = NotificationCenter.default
        nc.post(name: Notification.Name("Permission"), object: nil)
        FBSDKAppEvents.activateApp()
    }

    func applicationWillTerminate(_ application: UIApplication) {
       
    }
    func application(_ application: UIApplication, supportedInterfaceOrientationsFor window: UIWindow?) -> UIInterfaceOrientationMask {
        return UIInterfaceOrientationMask.portrait
    }
    
    
    
    func autoLoginFunction(){
        
        if  standard.value(forKey: "userid") as? Int != nil {
            self.setNotificationFunction(application: appDelegatesApplication!)
            self.homeTabBar()
        }
        else{
            self.welcomeViewConroller()
        }
    }
    
    
    
    func inAppPurchaseComplete(){
        SwiftyStoreKit.completeTransactions(atomically: true) { purchases in
            for purchase in purchases {
                if purchase.transaction.transactionState == .purchased || purchase.transaction.transactionState == .restored {
                    if purchase.needsFinishTransaction {
                        SwiftyStoreKit.finishTransaction(purchase.transaction)
                    }
                    print("purchased: \(purchase)")
                }
            }
        }
    }
    
    
    //MARK:- Accept Permission Functions
    func coreLocationFunction(){
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
            locationManager.requestWhenInUseAuthorization()
            locationManager.startUpdatingLocation()
        }
        
        
    }
    func setNotificationFunction(application: UIApplication){
        if #available(iOS 10, *) {
            UNUserNotificationCenter.current().requestAuthorization(options:[.badge, .alert, .sound]){ (granted, error) in }
            application.registerForRemoteNotifications()
        }
        else  {
            UIApplication.shared.registerUserNotificationSettings(UIUserNotificationSettings(types: [.badge, .sound, .alert], categories: nil))
            UIApplication.shared.registerForRemoteNotifications()
        }
        
        
    }
    
    
    //MARK:-Custom Function
    func homeTabBar()
    {
        let vc =  UIStoryboard.init(name: "Home", bundle: nil).instantiateInitialViewController()
       
        self.window?.rootViewController = vc
        self.window?.makeKeyAndVisible()
        
    }
    func welcomeViewConroller()
    {
        let vc =  UIStoryboard.init(name: "Welcome", bundle: nil).instantiateInitialViewController()
        self.window?.rootViewController = vc
        self.window?.makeKeyAndVisible()
        
    }
   
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {

        if status == .authorizedWhenInUse {
            print("User allowed us to access location")
            //do whatever init activities here.
        }
    }
    
    
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        print("Did location updates is called")
        let userLocation:CLLocation = locations[0] as CLLocation
        UserInfo.sharedInstance.currentLocationLat = userLocation.coordinate.latitude
        UserInfo.sharedInstance.currentLocationLong = userLocation.coordinate.longitude
        
        self.myCountryAndCity(location: userLocation)
        manager.stopUpdatingLocation()
        
        
    }
    
    
    
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("Did location updates is called but failed getting location \(error)")
    }
    
    
    func isAuthorizedtoGetUserLocation() {
        
        if CLLocationManager.authorizationStatus() != .authorizedWhenInUse     {
            locationManager.requestWhenInUseAuthorization()
        }
    }
    
    
    
    func myCountryAndCity(location:CLLocation){
        if CLLocationManager.locationServicesEnabled()
        {
            switch(CLLocationManager.authorizationStatus())
            {
            case .authorizedAlways, .authorizedWhenInUse:
                print("Authorize.")
               
                CLGeocoder().reverseGeocodeLocation(location, completionHandler: {(placemarks, error) -> Void in
                    if error != nil {
                        return
                    }else if let country = placemarks?.first?.country,
                        let city = placemarks?.first?.locality {
                        print(city)
                           UserInfo.sharedInstance.currentLocation = city
                    }
                    else {
                          
                    }
                })
                break
                
            case .notDetermined:
                print("Not determined.")
            
                break
                
            case .restricted:
                print("Restricted.")
               
                break
                
            case .denied:
                print("Denied.")
            }
        }
        
        
    }
    //MARK:- Push Notification Functions
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        
        let deviceTokenString = deviceToken.reduce("", {$0 + String(format: "%02X", $1)})
        
        standard.setValue(deviceTokenString, forKey: "gcm")
        print(standard.value(forKey: "gcm") as! String)
        
    }
    func application(_ application: UIApplication, didRegister notificationSettings: UIUserNotificationSettings) {
        if notificationSettings.types != .none {
            application.registerForRemoteNotifications()
        }
        
    }
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        
        standard.setValue("11111", forKey: "gcm")
        
    }
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any]) {
        if application.applicationState == .active {
            let  notificationAlert =  AFDropdownNotification()
            notificationAlert.image = UIImage.init(named: "logo")
            notificationAlert.isImageRounded = true
            
            notificationAlert.bottomButtonText = "View"
            notificationAlert.titleText = "Rendezwoo"
            notificationAlert.subtitleText = JSON(userInfo)["aps"]["alert"].stringValue
            
            notificationAlert.present(in: self.window?.rootViewController?.view, withGravityAnimation: false)
            
            
            notificationAlert.listenEvents { (event) in
                notificationAlert.dismiss(withGravityAnimation: false)
                
               // self.homeTabBar(index: 3)
            }
            
        }
        else{
           // self.homeTabBar(index: 3)
        }
    }
    //MARK:- Facebook and Google Login
    @available(iOS 9.0, *)
    func application(_ app: UIApplication, open url: URL, options: [UIApplicationOpenURLOptionsKey : Any] = [:]) -> Bool {
        
        
        let fb = FBSDKApplicationDelegate.sharedInstance().application(app, open: url, options: options)
        
        return fb
        
        
    }
    func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool {
        
        let fb :Bool = FBSDKApplicationDelegate.sharedInstance().application(application,open: url as URL!,sourceApplication: sourceApplication,annotation: annotation)
        
       
        return fb
        
        
    }
    
}


