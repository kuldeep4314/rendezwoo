//
//  AccountApiInterface.swift

//  Rendenwoo
//
//  Created by goyal on 23/02/18.
//  Copyright © 2018 ankita. All rights reserved.
//
import UIKit

class AccountApiInterface: NSObject {

    static let sharedInstance = AccountApiInterface()
    
    func postRequestWithParam(fromViewController: UIViewController,actionName : String , dictParam : [String : AnyObject]?,isShowIndicator : Bool, success:@escaping (JSON) -> Void, failure:@escaping (String) -> Void){
       
    
        if isShowIndicator == true {
             fromViewController.view.startIndicator()
        }
        print("Token:- \(self.createApiHeaders())" )
        ApiManager.sharedInstance.requestPOSTURL(baseUrl + actionName, params: dictParam , headers: self.createApiHeaders(), success: { (result) in
            
            if isShowIndicator == true {
               fromViewController.view.stopIndicator()
            }
            
           
            print(result)
            guard ((result["result"].intValue) != 0) else{
                
                print(result["msg"].stringValue)
                
                failure(result["msg"].stringValue)
                return
            }
          
            success(result)
            

        }) { (error) in
            if isShowIndicator == true {
                fromViewController.view.stopIndicator()
            }
            
            print(error.localizedDescription)
            failure(error.localizedDescription)
        }
        
    }

    func getRequestWithParam(fromViewController: UIViewController,actionName : String , success:@escaping (JSON) -> Void, failure:@escaping (String) -> Void){
        
           fromViewController.view.startIndicator()
         print(baseUrl + actionName)
        
       
        ApiManager.sharedInstance.requestGetURL(baseUrl + actionName, success: { (result) in
            print(result)
            fromViewController.view.stopIndicator()
            guard ((result["result"].intValue) != 0) else{
                
                print(result["msg"].stringValue)
                
                failure(result["msg"].stringValue)
                return
            }
            
            success(result)
        }) { (error) in
              fromViewController.view.stopIndicator()
            print(error.localizedDescription)
            failure(error.localizedDescription)
        }
        
        
    }
    
    func createApiHeaders()-> [String:String]{
        return ["Authorization":"Bearer \(standard.value(forKey: "token")as? String ?? "")"]
    }

}
