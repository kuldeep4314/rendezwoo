 //
 //  ApiManager.swift
 //  PhotoSpot
 //
 //  Created by Omeesh on 2017-07-21.
 //  Copyright © 2017 Avatar Singh. All rights reserved.
 //
 
 import UIKit
 import Alamofire
 import SwiftyJSON
 
 class ApiManager: NSObject {
    
    static let sharedInstance = ApiManager.init()
    
    func requestPOSTURL(_ strURL : String, params : [String : AnyObject]?, headers : [String : String]?, success:@escaping (JSON) -> Void, failure:@escaping (Error) -> Void){
        
        Alamofire.request(strURL, method: .post, parameters: params, encoding: JSONEncoding.default , headers: headers).responseJSON { (responseObject) -> Void in
            
            if responseObject.result.isSuccess {
                let resJson = JSON(responseObject.result.value!)
                success(resJson)
            }
            if responseObject.result.isFailure {
                let error : Error = responseObject.result.error!
                failure(error)
            }
        }
    }
    
   
    
    
     func requestGetURL(_ strURL: String, success:@escaping (JSON) -> Void, failure:@escaping (Error) -> Void) {
        Alamofire.request(strURL).responseJSON { (responseObject) -> Void in
            
            print(responseObject)
            
            if responseObject.result.isSuccess {
                let resJson = JSON(responseObject.result.value!)
                success(resJson)
            }
            if responseObject.result.isFailure {
                let error : Error = responseObject.result.error!
                failure(error)
            }
        }
    }
    
    func requestMultiPartURL(_ strURL : String, params : [String : AnyObject]?, headers : [String : String]? , imagesArray : [UIImage], imageName: [String],  success:@escaping (JSON) -> Void, failure:@escaping (Error) -> Void){
        
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            if imagesArray.count != 0 {
                
                for i in 0..<imagesArray.count{
                    
                    let imageData = self.compressImage(image: imagesArray[i])
                    
                    multipartFormData.append(imageData , withName: imageName[i] + "\(i)", fileName: "swift_file\(arc4random_uniform(1051604)).jpeg", mimeType: "image/jpg")
                }
            }
            
            for (key, value ) in params! {
                multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
            }
        }, usingThreshold:UInt64.init(),
           to: strURL,
           method: .post,
           headers:headers,
           encodingCompletion: { encodingResult in
            switch encodingResult {
            case .success(let upload, _ ,_ ):
                
                upload.uploadProgress(closure: { (progress) in
                    UILabel().text = "\((progress.fractionCompleted * 100)) %"
                    print (progress.fractionCompleted * 100)
                    
                    
                })
                  upload.responseJSON { response in
                    
                    guard ((response.result.value) != nil) else{
                        
                        failure(response.result.error!)
                        return
                    }
                    
                    let resJson = JSON(response.result.value!)
                    
                    success(resJson)
                    
                }
                
            case .failure(let encodingError):
                
                
                failure(encodingError.localizedDescription as! Error)
                
            }
            
        })
    }
    
    func compressImage(image:UIImage) -> Data {
        // Reducing file size to a 10th
        
        var actualHeight : CGFloat = image.size.height
        var actualWidth : CGFloat = image.size.width
        let maxHeight : CGFloat = 1136.0
        let maxWidth : CGFloat = 640.0
        var imgRatio : CGFloat = actualWidth/actualHeight
        let maxRatio : CGFloat = maxWidth/maxHeight
        var compressionQuality : CGFloat = 0.5
        
        if (actualHeight > maxHeight || actualWidth > maxWidth){
            if(imgRatio < maxRatio){
                //adjust width according to maxHeight
                imgRatio = maxHeight / actualHeight;
                actualWidth = imgRatio * actualWidth;
                actualHeight = maxHeight;
            }
            else if(imgRatio > maxRatio){
                //adjust height according to maxWidth
                imgRatio = maxWidth / actualWidth;
                actualHeight = imgRatio * actualHeight;
                actualWidth = maxWidth;
            }
            else{
                actualHeight = maxHeight;
                actualWidth = maxWidth;
                compressionQuality = 1;
            }
        }
        

        let rect = CGRect.init(x: 0.0, y: 0.0, width: actualWidth, height: actualHeight)
        
        UIGraphicsBeginImageContext(rect.size);
      
        
        image.draw(in: rect)
        let img = UIGraphicsGetImageFromCurrentImageContext();
        
        let imageData = UIImageJPEGRepresentation(img ?? UIImage.init(named: "imgPlaceHolder")!, compressionQuality);
        
        UIGraphicsEndImageContext();
        
        return imageData ?? Data.init();
    }
    
 }
