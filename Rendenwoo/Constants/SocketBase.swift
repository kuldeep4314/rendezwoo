//
//  SocketBase.swift
//  CleanyDriver
//
//  Created by Apple on 3/22/18.
//  Copyright © 2018 Relinns. All rights reserved.
//

import UIKit
import SocketIO


protocol SocketDelegate: class {
    func listenedData(data: JSON)
}

class SocketBase: NSObject {
    
    //MARK: variables
    static let sharedInstance = SocketBase()
    var currentState = SocketIOStatus.notConnected
   
    private let manager = SocketManager(socketURL: URL(string: socketURL)!, config: [.log(true)])
    private var socket : SocketIOClient?
    weak var delegate: SocketDelegate?
    
    //MARK: constructor
    override init() {
        super.init()
        socket = manager.defaultSocket
        addHandlers()
        socket?.connect()
        
    }
    
    private func addHandlers() {
        socket?.on("responseFromServer", callback: { (Data, emitter) in
            
            self.delegate?.listenedData(data: JSON(Data))
        })

        
        socket?.on("connect") {data, ack in
            print("socket connected")
            
        }
        socket?.on("error") {data, ack in
            print("This block can be used to capture different errors and not only the error to connect, you can always see the error description 'data' ")
        }
        socket?.onAny({ (event) in
            self.currentState = self.socket!.status
        })
        
        socket?.on(clientEvent: .disconnect, callback: { (dataArr, Ack) in
            self.socket?.connect(timeoutAfter: 1, withHandler: {
                
            })
        })
    }
    
    
    
    func socketEmit(emitString: String, data:[String:Any]) {
        socket?.emit(emitString, data)
    }
    
    func getStatus() -> SocketIOStatus? {
        return self.socket?.status
    }
}
