//
//  UpdateAvailable.swift
//  bonobo
//
//  Created by Relinns iOS on 19/01/18.
//  Copyright © 2018 Relinns. All rights reserved.
//

import UIKit



class UpdateAvailable: NSObject {

    static let sharedInstance = UpdateAvailable()
    
    func appInformation() {
        
        let info = Bundle.main.infoDictionary
        let currentVersion = info?["CFBundleShortVersionString"] as? String
        let identifier =   "com.relinns.GrumpNow" // info?["CFBundleIdentifier"] as? String
        var url = URL(string: "http://itunes.apple.com/lookup?bundleId=\(identifier ?? "")")
        if identifier != nil{
            url = URL(string: "http://itunes.apple.com/lookup?bundleId=\(identifier)")
        }
        
        print(url)
        
        Alamofire.request(url!, method: .post, parameters: nil, encoding: URLEncoding.httpBody, headers: nil).responseJSON { (response) in
            guard ((response.result.value) != nil) else{
                print(response.result.error!.localizedDescription)
                return
            }
            var json = JSON(response.result.value!)
            print(json)
            
            guard (json["resultCount"].intValue) != 0 else {
               
                print("No data available")
                return
            }
            print("success")
            print(json)
            let newVersion = json["results"].arrayValue.map({$0["version"] .stringValue})[0]
                        
           
            
        }
        
    }
}
