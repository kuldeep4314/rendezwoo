//
//  EmptyDataSet.swift
//  Rendenwoo
//
//  Created by Avatar Singh on 2018-05-06.
//  Copyright © 2018 ankita. All rights reserved.
//

import UIKit
import DZNEmptyDataSet
@objc  protocol emptyDataSetDelegatesOnTab {
    
    @objc optional func didTappedFunction()
}
class EmptyDataSet: NSObject , DZNEmptyDataSetSource , DZNEmptyDataSetDelegate{

    var titleString  = String()
    var img = UIImage()
    static let sharedInstance = EmptyDataSet()
    var delegates : emptyDataSetDelegatesOnTab?
     func showEmptyData(title : String, image : UIImage,tableView : UITableView){
        
        tableView.emptyDataSetSource = self
        tableView.emptyDataSetDelegate = self
        img = image
        titleString = title
    }
    func showEmptyDataCollectionView(title : String, image : UIImage,collectionView : UICollectionView){
        
        collectionView.emptyDataSetSource = self
        collectionView.emptyDataSetDelegate = self
        img = image
        titleString = title
    }
    func description(forEmptyDataSet scrollView: UIScrollView!) -> NSAttributedString! {
        
        let str = NSAttributedString.init(string: titleString)
        
        return str
    }
    
    func image(forEmptyDataSet scrollView: UIScrollView!) -> UIImage! {
    return img
    }
    
    func emptyDataSet(_ scrollView: UIScrollView!, didTap view: UIView!) {
        delegates?.didTappedFunction!()
    }
    func emptyDataSet(_ scrollView: UIScrollView!, didTap button: UIButton!) {
        print("")
    }
}
