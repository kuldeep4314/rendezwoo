//
//  Constants.swift
//  Rendenwoo
//
//  Created by goyal on 27/02/18.
//  Copyright © 2018 ankita. All rights reserved.
//

import UIKit
let appDelegates = UIApplication.shared.delegate as! AppDelegate

let baseUrl = "http://relinns.co.in:7020/"
let imgBaseUrl = "http://relinns.co.in:7020/uploads/images/"
let standard = UserDefaults.standard

let nameReg = "^.{1,600}$"
let invalidName = "Invaid Data"

let emailReg = "[A-Z0-9a-z._%+-]{3,}+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}"
let invalidEmail = "Please Enter valid Email"

let referalReg = "^.{4,20}$"
let invalidReferal = "Please Enter valid Referal code"

let AppStorelink = "https://itunes.apple.com/us/app/photospotter/id1268381611?ls=1&mt=8"
let GMapKey = "AIzaSyBC1BU5rHHKwzXvTmTgkMeZjGDo_S9AzmU"

let clientID = "312864329fe84ac3a733dde1f4d2f6b4"
let deviceUUID : String = (UIDevice.current.identifierForVendor?.uuidString)!
  let intro1 = "com.iap.1intro"

//let socketURL = "http://relinns.co.in:7020"
let socketURL = "http://192.168.1.201:7020"
var appDelegatesApplication : UIApplication?

let appColor = UIColor.init(red: 0.0, green: 205.0/255.0, blue: 210.0/255.0, alpha: 1.0)
let DarkGray = UIColor.init(red: 115.0/255.0, green: 115.0/255.0, blue: 115.0/255.0, alpha: 1.0)
let lightGray = UIColor.init(red: 158.0/255.0, green: 158.0/255.0, blue: 158.0/255.0, alpha: 1.0)

let moreLight = UIColor.init(red: 248.0/255.0, green: 248.0/255.0, blue: 248.0/255.0, alpha: 1.0)

let orange = UIColor.init(red: 255.0/255.0, green: 154.0/255.0, blue: 140.0/255.0, alpha: 1.0)

let red = UIColor.init(red: 242.0/255.0, green: 114.0/255.0, blue: 115.0/255.0, alpha: 1.0)

let selectionRow = UIColor.init(red: 233.0/255.0, green: 251.0/255.0, blue: 251.0/255.0, alpha: 1.0)


let colorWithopacity = UIColor.init(red: 170.0/255.0, green: 170.0/255.0, blue: 170.0/255.0, alpha: 1.0)

let redDark = UIColor.init(red: 223.0/255.0, green: 51.0/255.0, blue: 50.0/255.0, alpha: 1.0)




struct INSTAGRAM_IDS {
    
    static let INSTAGRAM_AUTHURL = "https://api.instagram.com/oauth/authorize/"
    
    static let INSTAGRAM_APIURl  = "https://api.instagram.com/v1/users/"
    
    static let INSTAGRAM_CLIENT_ID  = "312864329fe84ac3a733dde1f4d2f6b4"
    
    static let INSTAGRAM_CLIENTSERCRET = "ad2dca41b8884275b2f4b69b6a7c28b7"
    
    static let INSTAGRAM_REDIRECT_URI = "http://localhost:3000/"
    
    static let INSTAGRAM_ACCESS_TOKEN =  "access_token"
    
    static let INSTAGRAM_SCOPE = "likes+comments+relationships"
    
}
