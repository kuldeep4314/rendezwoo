//
//  Helper.swift
//  Rendenwoo
//
//  Created by goyal on 22/03/18.
//  Copyright © 2018 ankita. All rights reserved.
//

import UIKit

class Helper: NSObject {

  class func rateApp(appId: String, completion: @escaping ((_ success: Bool)->())) {
        guard let url = URL(string : appId) else {
            completion(false)
            return
        }
        guard #available(iOS 10, *) else {
            completion(UIApplication.shared.openURL(url))
            return
        }
        UIApplication.shared.open(url, options: [:], completionHandler: completion)
    }
 
    
    //MARK: - Get Day And Time Function
 class   func getDayAndTime(date:NSDate)-> String{
        
        let dayPeriodFormatter = DateFormatter()
        dayPeriodFormatter.dateFormat = "MMM dd"
        
        let timePeriodFormatter = DateFormatter()
        timePeriodFormatter.dateFormat = "hh:mm a"
        
        let dateString1 = dayPeriodFormatter.string(from: date as Date)
        let dateString2 = timePeriodFormatter.string(from: date as Date)
        
        return "\(dateString1) at \(dateString2)"
    }
    
    
    
  class  func timeAgoSinceDate(date:NSDate, numericDates:Bool) -> String {
        let calendar = NSCalendar.current
        let unitFlags: Set<Calendar.Component> = [.minute, .hour, .day, .weekOfYear, .month, .year, .second]
        let now = NSDate()
        let earliest = now.earlierDate(date as Date)
        let latest = (earliest == now as Date) ? date : now
        let components = calendar.dateComponents(unitFlags, from: earliest as Date,  to: latest as Date)
        
        if (components.year! >= 2) {
            return getDayAndTime(date: date)
        } else if (components.year! >= 1){
            if (numericDates){
                return getDayAndTime(date: date)
            } else {
                return getDayAndTime(date: date)
            }
        } else if (components.month! >= 2) {
            return getDayAndTime(date: date)
        } else if (components.month! >= 1){
            if (numericDates){
                return getDayAndTime(date: date)
            } else {
                return getDayAndTime(date: date)
            }
        } else if (components.weekOfYear! >= 2) {
            return getDayAndTime(date: date)
        } else if (components.weekOfYear! >= 1){
            if (numericDates){
                return getDayAndTime(date: date)
            } else {
                return getDayAndTime(date: date)
            }
        } else if (components.day! >= 2) {
            return getDayAndTime(date: date)
        } else if (components.day! >= 1){
            if (numericDates){
                return getDayAndTime(date: date)
            } else {
                return getDayAndTime(date: date)
            }
        } else if (components.hour! >= 2) {
            return "\(components.hour!)" + NSLocalizedString(" hours ago", comment: " hours ago")
        } else if (components.hour! >= 1){
            if (numericDates){
                return NSLocalizedString("1 hour ago", comment: "1 hour ago")
            } else {
                return NSLocalizedString("An hour ago", comment: "An hour ago")
            }
        } else if (components.minute! >= 2) {
            return "\(components.minute!)" + NSLocalizedString(" min ago", comment: " min ago")
        } else if (components.minute! >= 1){
            if (numericDates){
                return NSLocalizedString("1 min ago", comment: "1 min ago")
            } else {
                return NSLocalizedString("1 min ago", comment: "1 min ago")
            }
        } else if (components.second! >= 3) {
            return "\(components.second!)" + NSLocalizedString(" sec ago", comment: " sec ago")
        } else {
            return NSLocalizedString("Just Now", comment: "Just Now")
        }
        
    }
 class   func differenceBetweenTwoDates (dateStr : String)->Int {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SS'Z'"
        dateFormatter.locale = NSLocale.current
        let convDate = dateFormatter.date(from: dateStr)
        
        if convDate != nil{
            let diffInDays = Calendar.current.dateComponents([.day], from: convDate ?? Date(), to: Date()).day
            print(diffInDays ?? 0)
            return diffInDays ?? 0
        }
        else{
            return 0
        }
    
    }
    
    
  class  func convertStringTodate(strDate : String)-> Date{
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SS'Z'"
        dateFormatter.locale = NSLocale.current
        let convDate = dateFormatter.date(from: strDate)
        if convDate != nil{
            return convDate ?? Date()
        }else{
            return Date()
        }
    }
    
  class  func setBoldString(nameArray : [String] , fullString : String)-> NSMutableAttributedString{
        
        if nameArray.count == 1 {
            let range = (fullString as NSString).range(of: nameArray[0])
            
            let  myMutableString = NSMutableAttributedString(string: fullString, attributes: [NSAttributedStringKey.font:UIFont.init(name: "ProximaNova-Regular", size: 16)])
            
            let attrs = [NSAttributedStringKey.font: UIFont.init(name: "ProximaNova-Semibold", size: 16)]
            
            
            myMutableString.setAttributes(attrs, range: NSRange(location:range.location,length:range.length))
            
            return myMutableString
        }
        else if nameArray.count == 2 {
            
            let range = (fullString as NSString).range(of: nameArray[0])
            let range1 = (fullString as NSString).range(of: nameArray[1])
            
            let  myMutableString = NSMutableAttributedString(string: fullString, attributes: [NSAttributedStringKey.font:UIFont.init(name: "ProximaNova-Regular", size: 16)])
            
            let attrs = [NSAttributedStringKey.font: UIFont.init(name: "ProximaNova-Semibold", size: 16)]
            let attrs1 = [NSAttributedStringKey.font: UIFont.init(name: "ProximaNova-Semibold", size: 16)]
            
            
            myMutableString.setAttributes(attrs, range: NSRange(location:range.location,length:range.length))
            myMutableString.setAttributes(attrs1, range: NSRange(location:range1.location,length:range1.length))
            return myMutableString
            
            
        }
        else    if nameArray.count == 3 {
            let range = (fullString as NSString).range(of: nameArray[0])
            let range1 = (fullString as NSString).range(of: nameArray[1])
            let range2 = (fullString as NSString).range(of: nameArray[2])
            let  myMutableString = NSMutableAttributedString(string: fullString, attributes: [NSAttributedStringKey.font:UIFont.init(name: "ProximaNova-Regular", size: 16)])
            
            let attrs = [NSAttributedStringKey.font: UIFont.init(name: "ProximaNova-Semibold", size: 16)]
            let attrs1 = [NSAttributedStringKey.font: UIFont.init(name: "ProximaNova-Semibold", size: 16)]
            let attrs2 = [NSAttributedStringKey.font: UIFont.init(name: "ProximaNova-Semibold", size: 16)]
            
            myMutableString.setAttributes(attrs, range: NSRange(location:range.location,length:range.length))
            myMutableString.setAttributes(attrs1, range: NSRange(location:range1.location,length:range1.length))
            myMutableString.setAttributes(attrs2, range: NSRange(location:range2.location,length:range2.length))
            return myMutableString
        }
        else {
            let  myMutableString = NSMutableAttributedString(string: fullString, attributes: [NSAttributedStringKey.font:UIFont.init(name: "ProximaNova-Regular", size: 16)])
            return myMutableString
        }
        
        
    }
    
    class func convertUTCToLocal(timeString: String) -> Date {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SS'Z'"
        formatter.timeZone = TimeZone(abbreviation: "UTC")
        let oldDate = formatter.date(from: timeString)
        
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SS'Z'"
        
        formatter.timeZone = TimeZone.current
        
        formatter.locale = Locale.current
        if oldDate != nil{
            return oldDate!
        }
        
         return Date()
        
        
    }

    
    
}

