//
//  ProgressIndicator.swift
//  Certa
//
//  Created by Skycap on 10/9/17.
//  Copyright © 2017 Skycap. All rights reserved.
//

import UIKit

class ProgressIndicator: UIView {
    
    @IBOutlet weak var vewContent: UIButton!
    @IBOutlet weak var imgProgres: UIImageView!
    @IBOutlet weak var lblText: UILabel!
    
    class func initWith(frame:CGRect) -> ProgressIndicator {
        let indicator  = Bundle.main.loadNibNamed("ProgressIndicator", owner:self, options:nil)?.first as! ProgressIndicator
        indicator.frame = frame
        return indicator
    }
    
    static var shared: ProgressIndicator = {
        return ProgressIndicator.initWith(frame:UIScreen.main.bounds)
    }()
    
    func show(msg:String, at superView:UIView) {
        lblText.text = msg
        self.frame = superView.bounds
        addTo(superView: superView)
        animateRotation()
    }
    
    func updateText(msg:String) {
        lblText.text = msg
    }
    
    func hide() {
        imgProgres.layer.removeAllAnimations()
        self.removeFromSuperview()
    }
    
    func animateRotation() {
        let animation = CABasicAnimation(keyPath: "transform.rotation.z")
        animation.fromValue = 0
        animation.toValue = 2.0 * .pi
        animation.duration = 1.5
        animation.repeatCount = Float(INT_MAX)
        animation.isRemovedOnCompletion = false
        imgProgres.layer.add(animation, forKey:"rotation")
    }
    
    func addTo(superView:UIView) {
        superView.addSubview(self)
        
        let top = NSLayoutConstraint.init(item:superView, attribute:.top, relatedBy:.equal, toItem:self, attribute:.top, multiplier: 1.0, constant:0)
        let right = NSLayoutConstraint.init(item:superView, attribute:.leading, relatedBy:.equal, toItem:self, attribute:.leading, multiplier: 1.0, constant:0)
        let botom = NSLayoutConstraint.init(item:superView, attribute:.bottom, relatedBy:.equal, toItem:self, attribute:.bottom, multiplier: 1.0, constant:0)
        let left = NSLayoutConstraint.init(item:superView, attribute:.trailing, relatedBy:.equal, toItem:self, attribute:.trailing, multiplier: 1.0, constant:0)
        
        superView.addConstraints([top,right,botom,left])
    }
}
