//
//  UserFriends.swift
//
//  Created by goyal on 18/05/18
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

public class UserFriends: NSCoding {
    
    // MARK: Declaration for string constants to be used to decode and also serialize.
    private let kUserFriendsDeviceIdKey: String = "device_id"
    private let kUserFriendsNameKey: String = "name"
    private let kUserFriendsEmailKey: String = "email"
    private let kUserFriendsPlatformKey: String = "platform"
    private let kUserFriendsGenderKey: String = "gender"
    private let kUserFriendsAgeKey: String = "age"
    private let kUserFriendsGcmIdKey: String = "gcm_id"
    private let kUserFriendsReligionKey: String = "religion"
    private let kUserFriendsSocialIdKey: String = "social_id"
    private let kUserFriendsCurrentWorkKey: String = "current_work"
    private let kUserFriendsLatitudeKey: String = "latitude"
    private let kUserFriendsHeightKey: String = "height"
    private let kUserFriendsAboutmeKey: String = "aboutme"
    private let kUserFriendsOccupationKey: String = "occupation"
    private let kUserFriendsInternalIdentifierKey: String = "id"
    private let kUserFriendsEthnicityKey: String = "ethnicity"
    private let kUserFriendsUserTypeKey: String = "user_type"
    private let kUserFriendsImageUrlKey: String = "image_url"
    private let kUserFriendsCurrentLocationKey: String = "current_location"
    private let kUserFriendsLongitudeKey: String = "longitude"
    private let kUserFriendsReferralKey: String = "referral"
    private let kUserFriendsEducationKey: String = "education"
    
    // MARK: Properties
    public var deviceId: String?
    public var name: String?
    public var email: String?
    public var platform: Int?
    public var gender: Int?
    public var age: String?
    public var gcmId: String?
    public var religion: String?
    public var socialId: String?
    public var currentWork: String?
    public var latitude: String?
    public var height: String?
    public var aboutme: String?
    public var occupation: String?
    public var internalIdentifier: Int?
    public var ethnicity: String?
    public var userType: Int?
    public var imageUrl: String?
    public var currentLocation: String?
    public var longitude: String?
    public var referral: String?
    public var education: String?
    
    // MARK: SwiftyJSON Initalizers
    /**
     Initates the instance based on the object
     - parameter object: The object of either Dictionary or Array kind that was passed.
     - returns: An initalized instance of the class.
     */
    convenience public init(object: Any) {
        self.init(json: JSON(object))
    }
    
    /**
     Initates the instance based on the JSON that was passed.
     - parameter json: JSON object from SwiftyJSON.
     - returns: An initalized instance of the class.
     */
    public init(json: JSON) {
        deviceId = json[kUserFriendsDeviceIdKey].string
        name = json[kUserFriendsNameKey].string
        email = json[kUserFriendsEmailKey].string
        platform = json[kUserFriendsPlatformKey].int
        gender = json[kUserFriendsGenderKey].int
        age = json[kUserFriendsAgeKey].string
        gcmId = json[kUserFriendsGcmIdKey].string
        religion = json[kUserFriendsReligionKey].string
        socialId = json[kUserFriendsSocialIdKey].string
        currentWork = json[kUserFriendsCurrentWorkKey].string
        latitude = json[kUserFriendsLatitudeKey].string
        height = json[kUserFriendsHeightKey].string
        aboutme = json[kUserFriendsAboutmeKey].string
        occupation = json[kUserFriendsOccupationKey].string
        internalIdentifier = json[kUserFriendsInternalIdentifierKey].int
        ethnicity = json[kUserFriendsEthnicityKey].string
        userType = json[kUserFriendsUserTypeKey].int
        imageUrl = json[kUserFriendsImageUrlKey].string
        currentLocation = json[kUserFriendsCurrentLocationKey].string
        longitude = json[kUserFriendsLongitudeKey].string
        referral = json[kUserFriendsReferralKey].string
        education = json[kUserFriendsEducationKey].string
    }
    
    /**
     Generates description of the object in the form of a NSDictionary.
     - returns: A Key value pair containing all valid values in the object.
     */
    public func dictionaryRepresentation() -> [String: Any] {
        var dictionary: [String: Any] = [:]
        if let value = deviceId { dictionary[kUserFriendsDeviceIdKey] = value }
        if let value = name { dictionary[kUserFriendsNameKey] = value }
        if let value = email { dictionary[kUserFriendsEmailKey] = value }
        if let value = platform { dictionary[kUserFriendsPlatformKey] = value }
        if let value = gender { dictionary[kUserFriendsGenderKey] = value }
        if let value = age { dictionary[kUserFriendsAgeKey] = value }
        if let value = gcmId { dictionary[kUserFriendsGcmIdKey] = value }
        if let value = religion { dictionary[kUserFriendsReligionKey] = value }
        if let value = socialId { dictionary[kUserFriendsSocialIdKey] = value }
        if let value = currentWork { dictionary[kUserFriendsCurrentWorkKey] = value }
        if let value = latitude { dictionary[kUserFriendsLatitudeKey] = value }
        if let value = height { dictionary[kUserFriendsHeightKey] = value }
        if let value = aboutme { dictionary[kUserFriendsAboutmeKey] = value }
        if let value = occupation { dictionary[kUserFriendsOccupationKey] = value }
        if let value = internalIdentifier { dictionary[kUserFriendsInternalIdentifierKey] = value }
        if let value = ethnicity { dictionary[kUserFriendsEthnicityKey] = value }
        if let value = userType { dictionary[kUserFriendsUserTypeKey] = value }
        if let value = imageUrl { dictionary[kUserFriendsImageUrlKey] = value }
        if let value = currentLocation { dictionary[kUserFriendsCurrentLocationKey] = value }
        if let value = longitude { dictionary[kUserFriendsLongitudeKey] = value }
        if let value = referral { dictionary[kUserFriendsReferralKey] = value }
        if let value = education { dictionary[kUserFriendsEducationKey] = value }
        return dictionary
    }
    
    // MARK: NSCoding Protocol
    required public init(coder aDecoder: NSCoder) {
        self.deviceId = aDecoder.decodeObject(forKey: kUserFriendsDeviceIdKey) as? String
        self.name = aDecoder.decodeObject(forKey: kUserFriendsNameKey) as? String
        self.email = aDecoder.decodeObject(forKey: kUserFriendsEmailKey) as? String
        self.platform = aDecoder.decodeObject(forKey: kUserFriendsPlatformKey) as? Int
        self.gender = aDecoder.decodeObject(forKey: kUserFriendsGenderKey) as? Int
        self.age = aDecoder.decodeObject(forKey: kUserFriendsAgeKey) as? String
        self.gcmId = aDecoder.decodeObject(forKey: kUserFriendsGcmIdKey) as? String
        self.religion = aDecoder.decodeObject(forKey: kUserFriendsReligionKey) as? String
        self.socialId = aDecoder.decodeObject(forKey: kUserFriendsSocialIdKey) as? String
        self.currentWork = aDecoder.decodeObject(forKey: kUserFriendsCurrentWorkKey) as? String
        self.latitude = aDecoder.decodeObject(forKey: kUserFriendsLatitudeKey) as? String
        self.height = aDecoder.decodeObject(forKey: kUserFriendsHeightKey) as? String
        self.aboutme = aDecoder.decodeObject(forKey: kUserFriendsAboutmeKey) as? String
        self.occupation = aDecoder.decodeObject(forKey: kUserFriendsOccupationKey) as? String
        self.internalIdentifier = aDecoder.decodeObject(forKey: kUserFriendsInternalIdentifierKey) as? Int
        self.ethnicity = aDecoder.decodeObject(forKey: kUserFriendsEthnicityKey) as? String
        self.userType = aDecoder.decodeObject(forKey: kUserFriendsUserTypeKey) as? Int
        self.imageUrl = aDecoder.decodeObject(forKey: kUserFriendsImageUrlKey) as? String
        self.currentLocation = aDecoder.decodeObject(forKey: kUserFriendsCurrentLocationKey) as? String
        self.longitude = aDecoder.decodeObject(forKey: kUserFriendsLongitudeKey) as? String
        self.referral = aDecoder.decodeObject(forKey: kUserFriendsReferralKey) as? String
        self.education = aDecoder.decodeObject(forKey: kUserFriendsEducationKey) as? String
    }
    
    public func encode(with aCoder: NSCoder) {
        aCoder.encode(deviceId, forKey: kUserFriendsDeviceIdKey)
        aCoder.encode(name, forKey: kUserFriendsNameKey)
        aCoder.encode(email, forKey: kUserFriendsEmailKey)
        aCoder.encode(platform, forKey: kUserFriendsPlatformKey)
        aCoder.encode(gender, forKey: kUserFriendsGenderKey)
        aCoder.encode(age, forKey: kUserFriendsAgeKey)
        aCoder.encode(gcmId, forKey: kUserFriendsGcmIdKey)
        aCoder.encode(religion, forKey: kUserFriendsReligionKey)
        aCoder.encode(socialId, forKey: kUserFriendsSocialIdKey)
        aCoder.encode(currentWork, forKey: kUserFriendsCurrentWorkKey)
        aCoder.encode(latitude, forKey: kUserFriendsLatitudeKey)
        aCoder.encode(height, forKey: kUserFriendsHeightKey)
        aCoder.encode(aboutme, forKey: kUserFriendsAboutmeKey)
        aCoder.encode(occupation, forKey: kUserFriendsOccupationKey)
        aCoder.encode(internalIdentifier, forKey: kUserFriendsInternalIdentifierKey)
        aCoder.encode(ethnicity, forKey: kUserFriendsEthnicityKey)
        aCoder.encode(userType, forKey: kUserFriendsUserTypeKey)
        aCoder.encode(imageUrl, forKey: kUserFriendsImageUrlKey)
        aCoder.encode(currentLocation, forKey: kUserFriendsCurrentLocationKey)
        aCoder.encode(longitude, forKey: kUserFriendsLongitudeKey)
        aCoder.encode(referral, forKey: kUserFriendsReferralKey)
        aCoder.encode(education, forKey: kUserFriendsEducationKey)
    }
    
}
