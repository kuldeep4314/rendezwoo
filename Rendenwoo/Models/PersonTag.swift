//
//  PersonTag.swift
//  Rendenwoo
//
//  Created by goyal on 09/04/18.
//  Copyright © 2018 ankita. All rights reserved.
//

import UIKit

class PersonTag: NSObject {
      var imageId = String()
     var imageUrl = String()
     var tagPerson = [Person]()
   
}


class Person: NSObject {
    
    var personId = String()
    
    var name  = String()
    var xPos = CGFloat()
    var yPos = CGFloat()
    var status  = "0"
    var imageIndex = Int()
    var commanConection = Bool()
    var isFriend = Bool()
    var isLike = Bool()
    var numberofLikes = Int()
    var numberOfIntros = Int()
}



