//
//  UserData.swift
//
//  Created by goyal on 08/05/18
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

public class UserData: NSCoding {
    
    // MARK: Declaration for string constants to be used to decode and also serialize.
    private let kUserDataNameKey: String = "name"
    private let kUserDataEmailKey: String = "email"
    private let kUserDataPlatformKey: String = "platform"
    private let kUserDataGenderPreferenceKey: String = "gender_preference"
    private let kUserDataGenderKey: String = "gender"
    private let kUserDataAgeKey: String = "age"
    private let kUserDataGcmIdKey: String = "gcm_id"
    private let kUserDataReligionKey: String = "religion"
    private let kUserDataCurrentWorkKey: String = "current_work"
    private let kUserDataSocialIdKey: String = "social_id"
    private let kUserDataLatitudeKey: String = "latitude"
    private let kUserDataUniversityKey: String = "university"
    private let kUserDataHeightKey: String = "height"
    private let kUserDataOccupationKey: String = "occupation"
    private let kUserDataUsertypeKey: String = "usertype"
    private let kUserDataImageKey: String = "image"
    private let kUserDataInternalIdentifierKey: String = "id"
    private let kUserDataAboutmeKey: String = "aboutme"
    private let kUserDataEthnicityKey: String = "ethnicity"
    private let kUserDataReferralKey: String = "referral"
    private let kUserDataEducationKey: String = "education"
    private let kUserDataDeviceIdKey: String = "device_id"
    private let kUserDataCurrentLocationKey: String = "current_location"
    private let kUserDataLongitudeKey: String = "longitude"
    
    // MARK: Properties
    public var name: String?
    public var email: String?
    public var platform: Int?
    public var genderPreference: Int?
    public var gender: Int?
    public var age: String?
    public var gcmId: String?
    public var religion: String?
    public var currentWork: String?
    public var socialId: String?
    public var latitude: String?
    public var university: String?
    public var height: String?
    public var occupation: String?
    public var usertype: Int?
    public var image: String?
    public var internalIdentifier: Int?
    public var aboutme: String?
    public var ethnicity: String?
    public var referral: String?
    public var education: String?
    public var deviceId: String?
    public var currentLocation: String?
    public var longitude: String?
    
    // MARK: SwiftyJSON Initalizers
    /**
     Initates the instance based on the object
     - parameter object: The object of either Dictionary or Array kind that was passed.
     - returns: An initalized instance of the class.
     */
    convenience public init(object: Any) {
        self.init(json: JSON(object))
    }
    
    /**
     Initates the instance based on the JSON that was passed.
     - parameter json: JSON object from SwiftyJSON.
     - returns: An initalized instance of the class.
     */
    public init(json: JSON) {
        name = json[kUserDataNameKey].string
        email = json[kUserDataEmailKey].string
        platform = json[kUserDataPlatformKey].int
        genderPreference = json[kUserDataGenderPreferenceKey].int
        gender = json[kUserDataGenderKey].int
        age = json[kUserDataAgeKey].string
        gcmId = json[kUserDataGcmIdKey].string
        religion = json[kUserDataReligionKey].string
        currentWork = json[kUserDataCurrentWorkKey].string
        socialId = json[kUserDataSocialIdKey].string
        latitude = json[kUserDataLatitudeKey].string
        university = json[kUserDataUniversityKey].string
        height = json[kUserDataHeightKey].string
        occupation = json[kUserDataOccupationKey].string
        usertype = json[kUserDataUsertypeKey].int
        image = json[kUserDataImageKey].string
        internalIdentifier = json[kUserDataInternalIdentifierKey].int
        aboutme = json[kUserDataAboutmeKey].string
        ethnicity = json[kUserDataEthnicityKey].string
        referral = json[kUserDataReferralKey].string
        education = json[kUserDataEducationKey].string
        deviceId = json[kUserDataDeviceIdKey].string
        currentLocation = json[kUserDataCurrentLocationKey].string
        longitude = json[kUserDataLongitudeKey].string
    }
    
    /**
     Generates description of the object in the form of a NSDictionary.
     - returns: A Key value pair containing all valid values in the object.
     */
    public func dictionaryRepresentation() -> [String: Any] {
        var dictionary: [String: Any] = [:]
        if let value = name { dictionary[kUserDataNameKey] = value }
        if let value = email { dictionary[kUserDataEmailKey] = value }
        if let value = platform { dictionary[kUserDataPlatformKey] = value }
        if let value = genderPreference { dictionary[kUserDataGenderPreferenceKey] = value }
        if let value = gender { dictionary[kUserDataGenderKey] = value }
        if let value = age { dictionary[kUserDataAgeKey] = value }
        if let value = gcmId { dictionary[kUserDataGcmIdKey] = value }
        if let value = religion { dictionary[kUserDataReligionKey] = value }
        if let value = currentWork { dictionary[kUserDataCurrentWorkKey] = value }
        if let value = socialId { dictionary[kUserDataSocialIdKey] = value }
        if let value = latitude { dictionary[kUserDataLatitudeKey] = value }
        if let value = university { dictionary[kUserDataUniversityKey] = value }
        if let value = height { dictionary[kUserDataHeightKey] = value }
        if let value = occupation { dictionary[kUserDataOccupationKey] = value }
        if let value = usertype { dictionary[kUserDataUsertypeKey] = value }
        if let value = image { dictionary[kUserDataImageKey] = value }
        if let value = internalIdentifier { dictionary[kUserDataInternalIdentifierKey] = value }
        if let value = aboutme { dictionary[kUserDataAboutmeKey] = value }
        if let value = ethnicity { dictionary[kUserDataEthnicityKey] = value }
        if let value = referral { dictionary[kUserDataReferralKey] = value }
        if let value = education { dictionary[kUserDataEducationKey] = value }
        if let value = deviceId { dictionary[kUserDataDeviceIdKey] = value }
        if let value = currentLocation { dictionary[kUserDataCurrentLocationKey] = value }
        if let value = longitude { dictionary[kUserDataLongitudeKey] = value }
        return dictionary
    }
    
    // MARK: NSCoding Protocol
    required public init(coder aDecoder: NSCoder) {
        self.name = aDecoder.decodeObject(forKey: kUserDataNameKey) as? String
        self.email = aDecoder.decodeObject(forKey: kUserDataEmailKey) as? String
        self.platform = aDecoder.decodeObject(forKey: kUserDataPlatformKey) as? Int
        self.genderPreference = aDecoder.decodeObject(forKey: kUserDataGenderPreferenceKey) as? Int
        self.gender = aDecoder.decodeObject(forKey: kUserDataGenderKey) as? Int
        self.age = aDecoder.decodeObject(forKey: kUserDataAgeKey) as? String
        self.gcmId = aDecoder.decodeObject(forKey: kUserDataGcmIdKey) as? String
        self.religion = aDecoder.decodeObject(forKey: kUserDataReligionKey) as? String
        self.currentWork = aDecoder.decodeObject(forKey: kUserDataCurrentWorkKey) as? String
        self.socialId = aDecoder.decodeObject(forKey: kUserDataSocialIdKey) as? String
        self.latitude = aDecoder.decodeObject(forKey: kUserDataLatitudeKey) as? String
        self.university = aDecoder.decodeObject(forKey: kUserDataUniversityKey) as? String
        self.height = aDecoder.decodeObject(forKey: kUserDataHeightKey) as? String
        self.occupation = aDecoder.decodeObject(forKey: kUserDataOccupationKey) as? String
        self.usertype = aDecoder.decodeObject(forKey: kUserDataUsertypeKey) as? Int
        self.image = aDecoder.decodeObject(forKey: kUserDataImageKey) as? String
        self.internalIdentifier = aDecoder.decodeObject(forKey: kUserDataInternalIdentifierKey) as? Int
        self.aboutme = aDecoder.decodeObject(forKey: kUserDataAboutmeKey) as? String
        self.ethnicity = aDecoder.decodeObject(forKey: kUserDataEthnicityKey) as? String
        self.referral = aDecoder.decodeObject(forKey: kUserDataReferralKey) as? String
        self.education = aDecoder.decodeObject(forKey: kUserDataEducationKey) as? String
        self.deviceId = aDecoder.decodeObject(forKey: kUserDataDeviceIdKey) as? String
        self.currentLocation = aDecoder.decodeObject(forKey: kUserDataCurrentLocationKey) as? String
        self.longitude = aDecoder.decodeObject(forKey: kUserDataLongitudeKey) as? String
    }
    
    public func encode(with aCoder: NSCoder) {
        aCoder.encode(name, forKey: kUserDataNameKey)
        aCoder.encode(email, forKey: kUserDataEmailKey)
        aCoder.encode(platform, forKey: kUserDataPlatformKey)
        aCoder.encode(genderPreference, forKey: kUserDataGenderPreferenceKey)
        aCoder.encode(gender, forKey: kUserDataGenderKey)
        aCoder.encode(age, forKey: kUserDataAgeKey)
        aCoder.encode(gcmId, forKey: kUserDataGcmIdKey)
        aCoder.encode(religion, forKey: kUserDataReligionKey)
        aCoder.encode(currentWork, forKey: kUserDataCurrentWorkKey)
        aCoder.encode(socialId, forKey: kUserDataSocialIdKey)
        aCoder.encode(latitude, forKey: kUserDataLatitudeKey)
        aCoder.encode(university, forKey: kUserDataUniversityKey)
        aCoder.encode(height, forKey: kUserDataHeightKey)
        aCoder.encode(occupation, forKey: kUserDataOccupationKey)
        aCoder.encode(usertype, forKey: kUserDataUsertypeKey)
        aCoder.encode(image, forKey: kUserDataImageKey)
        aCoder.encode(internalIdentifier, forKey: kUserDataInternalIdentifierKey)
        aCoder.encode(aboutme, forKey: kUserDataAboutmeKey)
        aCoder.encode(ethnicity, forKey: kUserDataEthnicityKey)
        aCoder.encode(referral, forKey: kUserDataReferralKey)
        aCoder.encode(education, forKey: kUserDataEducationKey)
        aCoder.encode(deviceId, forKey: kUserDataDeviceIdKey)
        aCoder.encode(currentLocation, forKey: kUserDataCurrentLocationKey)
        aCoder.encode(longitude, forKey: kUserDataLongitudeKey)
    }
    
}
