//
//  UserInfo.swift
//  Rendenwoo
//
//  Created by goyal on 09/03/18.
//  Copyright © 2018 ankita. All rights reserved.
//

import UIKit

class UserInfo: NSObject {
    
    static var sharedInstance = UserInfo()
    
    var relationshipStatus = "single"
    var wantToDate = "0"
    var partnerAge = "29 to 34"
    var partnerHeight = "5'8\" (175 cms) to 6'2\" (188 cms)"
    var partnerDistance = "100 km / 62 Miles"
    var partnerEthnicity = "No Preference"
    var partnerReligion  = "No Preference"
    
    var userFriends = [UserFriends]()
    var friendsInfoArray = [[String : Any]]()
    var referralCode : String?
    var social_id: String?
    var userId: Int?
    var email: String?
    var name: String?
    var  age : String?
    var gender : Int?
    
    var image_url : String?
    
    var education: String?
    
    var workPlace: String?
    var occupation: String?
    
    var aboutMe : String?
    var height : String?
    
    var ethnicity : String?
    var religion : String?
    
    var currentLocation : String?
    var currentLocationLat : Double?
    var currentLocationLong : Double?
    
    var selectedLocation : String?
    var selectedLocationLat : Double?
    var selectedLocationLong : Double?
    
    var tagggedFriendCount = 0
    var isNotifiactionEnable = 0
    var authToken :  String?
    
    
    
    
    func deintSharedInstace(){
        UserInfo.sharedInstance.social_id = nil
        UserInfo.sharedInstance.email = nil
        UserInfo.sharedInstance.name = nil
        UserInfo.sharedInstance.age = nil
        UserInfo.sharedInstance.gender = nil
        
        UserInfo.sharedInstance.image_url = nil
        UserInfo.sharedInstance.relationshipStatus = ""
        
        UserInfo.sharedInstance.relationshipStatus = "single"
        UserInfo.sharedInstance.wantToDate = "0"
        UserInfo.sharedInstance.partnerAge = "29 to 34"
        UserInfo.sharedInstance.partnerHeight = "5'8\" (175 cms) to 6'2\" (188 cms)"
        UserInfo.sharedInstance.partnerDistance = "100 km / 62 Miles"
        UserInfo.sharedInstance.partnerEthnicity = "No Preference"
        UserInfo.sharedInstance.partnerReligion  = "No Preference"
        UserInfo.sharedInstance.education = nil
        UserInfo.sharedInstance.workPlace = nil
        UserInfo.sharedInstance.occupation = nil
        
        UserInfo.sharedInstance.aboutMe = nil
        UserInfo.sharedInstance.height = nil
        UserInfo.sharedInstance.ethnicity = nil
        UserInfo.sharedInstance.religion = nil
        
        UserInfo.sharedInstance.currentLocation = nil
        UserInfo.sharedInstance.currentLocationLat = nil
        UserInfo.sharedInstance.currentLocationLong = nil
        
        UserInfo.sharedInstance.selectedLocation = nil
        UserInfo.sharedInstance.selectedLocationLat = nil
        UserInfo.sharedInstance.selectedLocationLong = nil
        UserInfo.sharedInstance.tagggedFriendCount = 0
        UserInfo.sharedInstance.isNotifiactionEnable = 0
        UserInfo.sharedInstance.authToken = nil
        
        
        
    }
    
    
}
