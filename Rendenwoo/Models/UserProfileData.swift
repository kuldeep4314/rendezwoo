//
//  UserProfileData.swift
//
//  Created by goyal on 08/05/18
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

public class UserProfileData: NSCoding {
    
    // MARK: Declaration for string constants to be used to decode and also serialize.
    private let kUserProfileDataSignupRequiredKey: String = "signup_required"
    private let kUserProfileDataDataKey: String = "data"
    private let kUserProfileDataFriendsKey: String = "friends"
    private let kUserProfileDataResultKey: String = "result"
    private let kUserProfileDataTokenKey: String = "token"
    private let kUserProfileDataMsgKey: String = "msg"
    
    // MARK: Properties
    public var signupRequired: Int?
    public var data: UserData?
    public var friends: [UserFriends]?
    public var result: Int?
    public var token: String?
    public var msg: String?
    
    // MARK: SwiftyJSON Initalizers
    /**
     Initates the instance based on the object
     - parameter object: The object of either Dictionary or Array kind that was passed.
     - returns: An initalized instance of the class.
     */
    convenience public init(object: Any) {
        self.init(json: JSON(object))
    }
    
    /**
     Initates the instance based on the JSON that was passed.
     - parameter json: JSON object from SwiftyJSON.
     - returns: An initalized instance of the class.
     */
    public init(json: JSON) {
        signupRequired = json[kUserProfileDataSignupRequiredKey].int
        data = UserData(json: json[kUserProfileDataDataKey])
        if let items = json[kUserProfileDataFriendsKey].array { friends = items.map { UserFriends(json: $0) } }
        result = json[kUserProfileDataResultKey].int
        token = json[kUserProfileDataTokenKey].string
        msg = json[kUserProfileDataMsgKey].string
    }
    
    /**
     Generates description of the object in the form of a NSDictionary.
     - returns: A Key value pair containing all valid values in the object.
     */
    public func dictionaryRepresentation() -> [String: Any] {
        var dictionary: [String: Any] = [:]
        if let value = signupRequired { dictionary[kUserProfileDataSignupRequiredKey] = value }
        if let value = data { dictionary[kUserProfileDataDataKey] = value.dictionaryRepresentation() }
        if let value = friends { dictionary[kUserProfileDataFriendsKey] = value.map { $0.dictionaryRepresentation() } }
        if let value = result { dictionary[kUserProfileDataResultKey] = value }
        if let value = token { dictionary[kUserProfileDataTokenKey] = value }
        if let value = msg { dictionary[kUserProfileDataMsgKey] = value }
        return dictionary
    }
    
    // MARK: NSCoding Protocol
    required public init(coder aDecoder: NSCoder) {
        self.signupRequired = aDecoder.decodeObject(forKey: kUserProfileDataSignupRequiredKey) as? Int
        self.data = aDecoder.decodeObject(forKey: kUserProfileDataDataKey) as? UserData
        self.friends = aDecoder.decodeObject(forKey: kUserProfileDataFriendsKey) as? [UserFriends]
        self.result = aDecoder.decodeObject(forKey: kUserProfileDataResultKey) as? Int
        self.token = aDecoder.decodeObject(forKey: kUserProfileDataTokenKey) as? String
        self.msg = aDecoder.decodeObject(forKey: kUserProfileDataMsgKey) as? String
    }
    
    public func encode(with aCoder: NSCoder) {
        aCoder.encode(signupRequired, forKey: kUserProfileDataSignupRequiredKey)
        aCoder.encode(data, forKey: kUserProfileDataDataKey)
        aCoder.encode(friends, forKey: kUserProfileDataFriendsKey)
        aCoder.encode(result, forKey: kUserProfileDataResultKey)
        aCoder.encode(token, forKey: kUserProfileDataTokenKey)
        aCoder.encode(msg, forKey: kUserProfileDataMsgKey)
    }
    
}
