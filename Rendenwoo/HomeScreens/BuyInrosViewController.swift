//
//  BuyInrosViewController.swift
//  Rendenwoo
//
//  Created by goyal on 19/03/18.
//  Copyright © 2018 ankita. All rights reserved.
//

import UIKit
import SwiftyStoreKit
class BuyInrosViewController:UIViewController ,UITableViewDataSource,UITableViewDelegate{
    @IBOutlet weak var tableView: UITableView!
    
    let arrayIntros = ["1 Intro","5 Intros", "10 Intros" ,"25 Intros","50 Intros","Unlimited Intros"]
    let arrayIntrosCount = ["$0.99","$1.99", "$3.99" ,"$6.99","$9.99","$29.99/mth"]
    
    
    var typePurchase = ["1","5","10","25","50","UNLIMITED"]
    
    var index = 3
    @IBOutlet weak var topViewHeight: NSLayoutConstraint!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.tableFooterView = UIView()
        self.tableView.rowHeight = UITableViewAutomaticDimension
        if UIDevice.isIphoneX == true
        {
            topViewHeight.constant = 84
        }
        
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func btnBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnProceedToCheckOut(_ sender: Any) {
        
//        let dateFormatter = DateFormatter()
//        dateFormatter.dateFormat = "yyyy-MM-dd"
//
//     let date = dateFormatter.string(from: Date())
//
//        let params : Parameters = ["user_id": "\(standard.value(forKey: "userid")!)",
//
//                                    "intro_count" : typePurchase[index],
//
//            "start_date": date ]
//
//       print(params)
//        self.purchaseIntroApiCall(parameters: params)
        self.inAppPurchaseDataFetch()
    }
    //MARK: - UITABLEVIEW DATA SOURCE
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 9
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.row == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cellHeader", for:
                indexPath)
            let lblTitle = cell.viewWithTag(1) as! UILabel
           
            lblTitle.text = "Buy intros and never wait for an introduction again!"
            
            lblTitle.text = "Buy intros and never wait for an introduction again! Intros never expire. Use them anytime!"
           
           
            return cell
        }
        else if indexPath.row == 1 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cellEmpty", for: indexPath)
            return cell
        }
        else if indexPath.row == 2 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cellHeader", for: indexPath)
            let lblTitle = cell.viewWithTag(1) as! UILabel
            lblTitle.text = "Select an item to add to cart"
            return cell
        }
            
        else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
            let lblTitle = cell.viewWithTag(2) as! UILabel
            let lblCount = cell.viewWithTag(3) as! UILabel
            let lblBottom = cell.viewWithTag(4) as! UILabel
            lblBottom.backgroundColor = lightGray
            
            lblTitle.text = arrayIntros[indexPath.row - 3]
            lblCount.text = arrayIntrosCount[indexPath.row - 3]
            
            if index == indexPath.row{
                cell.backgroundColor = selectionRow
            }
            else{
                cell.backgroundColor = moreLight
            }
            return cell
        }
        
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        if indexPath.row > 2 && indexPath.row < 9 {
        index = indexPath.row
        self.tableView.reloadData()
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0 {
            return UITableViewAutomaticDimension
        }
      else  if indexPath.row == 1 {
            return 30
        }
        else{
             return 44
        }
   
        
    }
    
    
    func inAppPurchaseDataFetch(){
        
        SwiftyStoreKit.retrieveProductsInfo([intro1]) { result in
            
            print(result)
            if let product = result.retrievedProducts.first {
                let priceString = product.localizedPrice!
                print("Product: \(product.localizedDescription), price: \(priceString)")
                
                SwiftyStoreKit.purchaseProduct(intro1, quantity: 1, atomically: true) { result in
                   
                    switch result {
                    case .success(let purchase):
                        print("Purchase Success: \(purchase.productId)")
                        
                        SwiftyStoreKit.finishTransaction(purchase.transaction)
                        
                
                    case .error(let error):
                        switch error.code {
                        case .unknown: print("Unknown error. Please contact support")
                        case .clientInvalid: print("Not allowed to make the payment")
                        case .paymentCancelled: break
                        case .paymentInvalid: print("The purchase identifier was invalid")
                        case .paymentNotAllowed: print("The device is not allowed to make the payment")
                        case .storeProductNotAvailable: print("The product is not available in the current storefront")
                        case .cloudServicePermissionDenied: print("Access to cloud service information is not allowed")
                        case .cloudServiceNetworkConnectionFailed: print("Could not connect to the network")
                        case .cloudServiceRevoked: print("User has revoked permission to use this cloud service")
                        }
                    }
                }
            }
            else if result.invalidProductIDs.first != nil {
            
                return (appDelegates.window?.rootViewController?.showAlert(messageStr: "Could not retrieve Any intro information yet."))!
                
                
            }
            else {
                print("Error: \(result.error?.localizedDescription ?? "")")
            }
        }
        
    }
    
    //MARK:- Purchase Intro API Call
    func purchaseIntroApiCall(parameters: Parameters){
        AccountApiInterface.sharedInstance.postRequestWithParam(fromViewController: self, actionName: "purchase_intro", dictParam: parameters as [String : AnyObject], isShowIndicator: true, success: { (json) in
            
        
            DispatchQueue.main.async {
                self.tableView.reloadData()
                  self.navigationController?.popViewController(animated: true)
            }
        }) { (error) in
            self.showAlert(messageStr: error)
        }
        
    }
    
    
}

