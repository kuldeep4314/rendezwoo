//
//  InstagramLoginVC.swift
//  InstagramLogin-Swift
//
//  Created by Aman Aggarwal on 2/7/17.
//  Copyright © 2017 ClickApps. All rights reserved.
//

import UIKit

class InstagramLoginVC: UIViewController, UIWebViewDelegate {

    @IBOutlet weak var height: NSLayoutConstraint!
    @IBOutlet weak var loginWebView: UIWebView!
    
    
    let fetchPostUrl = "https://api.instagram.com/v1/users/self/media/recent/?access_token="
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        if UIDevice.isIphoneX == true{
            
            height.constant = 84
        }
        // Do any additional setup after loading the view.
        loginWebView.delegate = self
        unSignedRequest()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func btnBackTapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK: - unSignedRequest
    
    
    
    func unSignedRequest () {
        let authURL = String(format: "%@?client_id=%@&redirect_uri=%@&response_type=token&scope=%@&DEBUG=True", arguments: [INSTAGRAM_IDS.INSTAGRAM_AUTHURL,INSTAGRAM_IDS.INSTAGRAM_CLIENT_ID,INSTAGRAM_IDS.INSTAGRAM_REDIRECT_URI, INSTAGRAM_IDS.INSTAGRAM_SCOPE ])
        let urlRequest =  URLRequest.init(url: URL.init(string: authURL)!)
        loginWebView.loadRequest(urlRequest)
    }

    func checkRequestForCallbackURL(request: URLRequest) -> Bool {
        
        let requestURLString = (request.url?.absoluteString)! as String
        
        if requestURLString.hasPrefix(INSTAGRAM_IDS.INSTAGRAM_REDIRECT_URI) {
            let range: Range<String.Index> = requestURLString.range(of: "#access_token=")!
            handleAuth(authToken: requestURLString.substring(from: range.upperBound))
            return false;
        }
        return true
    }
    
    func handleAuth(authToken: String)  {
        standard.set("authToken", forKey: "instagram")
        
        print("Instagram authentication token ==", authToken)
      
         self.fetchPostFromInstagram(token: authToken)
      
    }

    
    // MARK: - UIWebViewDelegate
    
    func webView(_ webView: UIWebView, shouldStartLoadWith request: URLRequest, navigationType: UIWebViewNavigationType) -> Bool {
        return checkRequestForCallbackURL(request: request)
    }
    
    func webViewDidStartLoad(_ webView: UIWebView) {
         self.view.startIndicator()
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
           self.view.stopIndicator()
    }
    
    func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
        webViewDidFinishLoad(webView)
    }

    //MARK: - Fetch Post From instagram
    func  fetchPostFromInstagram (token : String){
        ApiManager.sharedInstance.requestGetURL(fetchPostUrl + token, success: { (json) in
            print(json)
            
            var arrayDict = [[String : Any]]()
            
            let nameArray = json["data"].arrayValue.map({$0["user"]["username"].stringValue})
           
            
            for item in json["data"].arrayValue {
                
                let image = item["images"]["standard_resolution"]["url"].stringValue
                  let id = item["id"].stringValue
                
                arrayDict.append(["id": id,"image_url": image])
                print(arrayDict)
            }
           
            var name = ""
            if nameArray.count > 0 {
                name = nameArray[0]
            }
             print(name)
            let paras : Parameters = [
                "user_id":standard.value(forKey: "userid")!,
                "insta_name" : name
                ,
                
                "images" : arrayDict]
           self.uploadImagesToInstagramApiCall(parameters: paras)
        }) { (err) in
            print(err.localizedDescription)
        }
        
    }
    
    //MARK:- Upload images to instagram API Call
    func uploadImagesToInstagramApiCall(parameters: Parameters){
        print(parameters)
        AccountApiInterface.sharedInstance.postRequestWithParam(fromViewController: self, actionName: "instagram_upload", dictParam: parameters as [String : AnyObject], isShowIndicator: true, success: { (json) in

             print(json)
            standard.set(json["data"]["insta_name"].stringValue, forKey: "insta_name")
            DispatchQueue.main.async {
               self.navigationController?.popViewController(animated: true)
            }
        }) { (error) in
            self.showAlert(messageStr: error)
        }
        
    }
}
