//
//  NotificationsViewController.swift
//  Rendenwoo
//
//  Created by goyal on 28/02/18.
//  Copyright © 2018 ankita. All rights reserved.
//

import UIKit
//0-like, 1-share, 2-tag, 3-message, 4-intro

class NotificationsViewController: UIViewController ,UITableViewDelegate,UITableViewDataSource{
 @IBOutlet weak var topViewHeight: NSLayoutConstraint!
    @IBOutlet weak var tableView: UITableView!
      let refreshControl = UIRefreshControl()
      var newNotifications = [NotificationModel]()
      var earlierNotifications = [NotificationModel]()
      var notifications = [NotificationModel]()
    var currentPage = 1
    var isResultsAviablleToLoad = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
      tableView.tableFooterView = UIView()
        self.view.backgroundColor = moreLight
        self.tableView.backgroundColor = moreLight
        if UIDevice.isIphoneX == true{
            topViewHeight.constant = 84
        }
        if #available(iOS 10.0, *) {
            tableView.refreshControl = refreshControl
        } else {
            tableView.addSubview(refreshControl)
        }
        refreshControl.tintColor = appColor
        refreshControl.addTarget(self, action: #selector(refreshData(_:)), for: .valueChanged)
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
   
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    
           currentPage = 1
           isResultsAviablleToLoad = true
        let parameter : Parameters = ["user_id":"\(standard.value(forKey: "userid")!)" ,"page" : currentPage]
        
          self.fetchNotificationApiCall(parameters: parameter)
    }
    
    
    
    func  updateBadge(){
        
        let  parent = self.parent as! Tabbar
        
        parent.tabBar.items![3].badgeValue = nil
        
        
    }
    @objc func refreshData(_ sender: Any) {
        currentPage = 1
        isResultsAviablleToLoad = true
        let parameter : Parameters = ["user_id":"\(standard.value(forKey: "userid")!)" ,"page" : currentPage]
        
        self.fetchNotificationApiCall(parameters: parameter)
    }

  
    
    
    //MARK:- TableView DataSource
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if section == 0 {
            return newNotifications.count
        }
        else{
             return earlierNotifications.count
        }
       
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! NotificationTableViewCell
       
        if indexPath.section == 0 {
            
            if (newNotifications[indexPath.row].names?.count ?? 0) > 0 {
                    cell.lblNotificationMsg.attributedText = Helper.setBoldString(nameArray: newNotifications[indexPath.row].names ?? [String](), fullString: newNotifications[indexPath.row].text ?? "")
            }
            else{
                    cell.lblNotificationMsg.text = newNotifications[indexPath.row].text
            }
            
              let typeUrl = newNotifications[indexPath.row].status ?? ""
            
              cell.imgNotType.sd_setImage(with: URL(string: typeUrl), placeholderImage: UIImage.init(named: "11"), options: [], completed: nil)
            
            let imageURL = newNotifications[indexPath.row].imageUrl ?? ""
            
            cell.imgUser.sd_setImage(with: URL(string: imageURL), placeholderImage: #imageLiteral(resourceName: "imgProfile"), options: [], completed: nil)
            
            
            let date = NSDate(timeIntervalSince1970:  TimeInterval(newNotifications[indexPath.row].timestamp ?? 0))
            cell.lblTime.text = Helper.timeAgoSinceDate(date: date, numericDates: true)
            
            
        }
        else{
            if (earlierNotifications[indexPath.row].names?.count ?? 0) > 0 {
                cell.lblNotificationMsg.attributedText = Helper.setBoldString(nameArray: earlierNotifications[indexPath.row].names ?? [String](), fullString: earlierNotifications[indexPath.row].text ?? "")
            }
            else{
                cell.lblNotificationMsg.text = earlierNotifications[indexPath.row].text
            }
            
            let typeUrl = earlierNotifications[indexPath.row].status ?? ""
            
            cell.imgNotType.sd_setImage(with: URL(string: typeUrl), placeholderImage: UIImage.init(named: "11"), options: [], completed: nil)
            
            
            
            
            let imageURL = earlierNotifications[indexPath.row].imageUrl ?? ""
            
            cell.imgUser.sd_setImage(with: URL(string: imageURL), placeholderImage: #imageLiteral(resourceName: "imgProfile"), options: [], completed: nil)
            
            
            let date = NSDate(timeIntervalSince1970:  TimeInterval(earlierNotifications[indexPath.row].timestamp ?? 0))
            cell.lblTime.text = Helper.timeAgoSinceDate(date: date, numericDates: true)
        }
        
         return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellHeader") as! NotificationTableViewCell
        
        if section == 0 {
            cell.lblHeader.text = "New"
        }
        else{
              cell.lblHeader.text = "Earlier"
        }
        
        return cell.contentView
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        if section == 0 {
            return  ( newNotifications.count  > 0) ? 30 : 0
        }
        else{
            return (earlierNotifications.count > 0) ? 30 : 0
        }
        
    }
   
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
  
        if indexPath.section == 0 {
            if indexPath.row ==  self.newNotifications.count - 1 && isResultsAviablleToLoad == true{
                currentPage = currentPage + 1
                let parameter : Parameters = ["user_id":"\(standard.value(forKey: "userid")!)" ,"page" : currentPage]
               self.fetchNotificationApiCall(parameters: parameter)
            }
            
        }
        else{
            if indexPath.row ==  self.earlierNotifications.count - 1 && isResultsAviablleToLoad == true{
                currentPage = currentPage + 1
                let parameter : Parameters = ["user_id":"\(standard.value(forKey: "userid")!)" ,"page" : currentPage]
                self.fetchNotificationApiCall(parameters: parameter)
            }
            
        }
        
        
       
    }
    
    
    //MARK:- Fetch Notification Api Call Function
    
    func fetchNotificationApiCall(parameters: Parameters){
        AccountApiInterface.sharedInstance.postRequestWithParam(fromViewController: self, actionName: "getNotification", dictParam: parameters as [String : AnyObject], isShowIndicator: (self.currentPage == 1) ? true : false, success: { (json) in
             self.refreshControl.endRefreshing()
            EmptyDataSet.sharedInstance.delegates = self
            EmptyDataSet.sharedInstance.showEmptyData(title: "No notifications found.", image: #imageLiteral(resourceName: "search"), tableView: self.tableView)
           print(json)
            self.refreshControl.endRefreshing()
            if self.currentPage == 1 {
                self.newNotifications.removeAll()
                self.earlierNotifications.removeAll()
                self.notifications.removeAll()
            }
            guard(json["data"].arrayValue.count) > 0 else {
                
                self.isResultsAviablleToLoad = false
                DispatchQueue.main.async {
                    self.tableView.reloadData()
                }
                return
            }
            
            for i in 0..<json["data"].arrayValue.count{
               let arrTemp = NotificationModel.init(json: json["data"][i])
                self.notifications.append(arrTemp)
            }
      
            let calendar = Calendar.current
           for i in 0..<self.notifications.count{
            
                if  calendar.isDateInToday(  Date.init(timeIntervalSince1970:   TimeInterval(self.notifications[i].timestamp ?? 0))) == true {
                    self.newNotifications.append(self.notifications[i])
                }
                else{
                   self.earlierNotifications.append(self.notifications[i])
            }
            
            }
         
            self.isResultsAviablleToLoad = true
            
            DispatchQueue.main.async {
                  self.updateBadge()
                self.tableView.reloadData()
            }
          
        }) { (error) in
             self.refreshControl.endRefreshing()
            self.isResultsAviablleToLoad = false
            EmptyDataSet.sharedInstance.delegates = self
            EmptyDataSet.sharedInstance.showEmptyData(title: "No notifications found.", image: #imageLiteral(resourceName: "search"), tableView: self.tableView)
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
           
        }
        
    }
    
   
    
}

extension NotificationsViewController : emptyDataSetDelegatesOnTab {
    func didTappedFunction(){
       
        currentPage = 1
        isResultsAviablleToLoad = true
        let parameter : Parameters = ["user_id":"\(standard.value(forKey: "userid")!)" ,"page" : currentPage]
        
        self.fetchNotificationApiCall(parameters: parameter)
        
    }

    
    
    
}

