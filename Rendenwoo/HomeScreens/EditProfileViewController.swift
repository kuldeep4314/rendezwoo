//
//  EditProfileViewController.swift
//  Rendenwoo
//
//  Created by goyal on 22/03/18.
//  Copyright © 2018 ankita. All rights reserved.
//

import UIKit

class EditProfileViewController: UIViewController ,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,UITextViewDelegate,UITextFieldDelegate, UIImagePickerControllerDelegate,
UINavigationControllerDelegate {
    @IBOutlet weak var backViewImage: UIView!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var topView: UIView!
     var tempText : String?
    
    var textCount = Int()
    var userType = Int()
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var btnSave: UIButton!
    @IBOutlet weak var height: NSLayoutConstraint!
    @IBOutlet weak var imgUserImage: UIImageView!
    var isRelationshipStatus = Bool()
        var userData = UserData.init(json: JSON(JSON.self))
    override func viewDidLoad() {
        super.viewDidLoad()
        btnSave.backgroundColor = appColor
        self.bottomView.backgroundColor = moreLight
        self.bottomView.backgroundColor = moreLight
        self.view.backgroundColor = moreLight
        collectionView.backgroundColor = moreLight
        collectionView.backgroundColor = moreLight
        self.view.backgroundColor = appColor
        backViewImage.layer.borderColor = orange.cgColor
        backViewImage.layer.borderWidth = 5
        self.setupUserData()
    }
    
    func  setupUserData(){
         userData = UserData.init(json: JSON(standard.value(forKey: "userData") as? [String : Any] ?? [String : Any]()))
        userType = userData.usertype ?? 0
        self.lblUserName.text = "\(standard.value(forKey: "userName") ?? "")"
        let imageURL =  "\(standard.value(forKey: "userImage") ?? "")"
        print(imageURL)
       
        imgUserImage.sd_setImage(with: URL(string: imageURL), placeholderImage: #imageLiteral(resourceName: "imgProfile"), options: [], completed: nil)
        
        textCount = userData.aboutme?.base64Decoded()?.count ?? 0
        
        
        
        
    }

    func validateTextFields()-> Bool{
        
        let cell = self.collectionView.cellForItem(at: IndexPath.init(row: 0, section: 0)) as! ProfileCollectionViewCell
        
        if cell.textViewError.validate() && cell.txtHeight.validate() &&
            cell.txtEthnicity.validate()
            &&  cell.txtReligion.validate() && cell.txtEducation.validate() && cell.txtOccupation.validate() && cell.txtWorkPlace.validate() {
            return true
        }
        else{
            
            return false
        }
    }
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        height.constant =  UIScreen.main.bounds.height / 2
        topView.roundCorners([.bottomRight, .bottomLeft], radius: self.topView.frame.height / 2)
    }
    
    //MARK:- Get Substring Function
    func getSubstring(string : String,startIndex : Int, stopIndex: Int)->String{
        
        if string != "" {
            let start = String.Index(encodedOffset: startIndex)
            let end = String.Index(encodedOffset: stopIndex)
            let substring = String(string[start..<end])
            print(substring)
            return substring
        }
        else{
             return string
        }
    }
    
    @IBAction func btnSaveTapped(_ sender: UIButton) {
        if isRelationshipStatus == true {
            
            let parameters : Parameters = ["user_id":"\(standard.value(forKey: "userid") ?? 0 )",
                                           
                                           "user_type":userType ]
            self.apiUpdateUserType(parameter: parameters)
        }
        else{
           let cell = self.collectionView.cellForItem(at: IndexPath.init(row: 0, section: 0)) as? ProfileCollectionViewCell
                
               let parameters : Parameters = [   "user_id" :"\(standard.value(forKey: "userid") ?? "" )",
                
                "education" : userData.education ?? "",
                
                "occupation" : userData.occupation ?? "",
                
                "current_work" : userData.currentWork ?? "",
                
                "aboutme":(cell?.textview.text ?? "").base64Encoded() ?? "",
                
                "height":cell?.txtHeight.text?.base64Encoded() ?? "",
                
                "height_in_cm":getSubstring(string: cell?.txtHeight.text?.base64Encoded() ?? "", startIndex: 6, stopIndex: 9),
                
                "ethnicity":userData.ethnicity ?? "",
                
                "religion":userData.religion ?? "",
                
                "university":userData.religion ?? "" ]
                
            
            if userType == 1 {
                 self.updateProfileApiCall(parameters: parameters)
            }
            else{
                if   self.validateTextFields() == true {
                    self.updateProfileApiCall(parameters: parameters)
                }
            }
            
            
            
            
        }
        
    }
    
    
    
    //MARK:- RelationshipStatus Button Function
    
    @objc func singleTakenBtnTapped(sender : UIButton){
       userType = JSON(sender.accessibilityHint ?? "0").intValue
        self.collectionView.reloadData()
    }
    
    @objc func wantToDateBtnTapped(sender : UIButton){
        UserInfo.sharedInstance.wantToDate = sender.accessibilityHint!
        self.collectionView.reloadData()
    }
    
    func setImages(arrBtn : [UIButton], arrStr: [String]){
        for i in 0..<arrBtn.count{
            let btn = arrBtn[i]
            btn.setImage(UIImage.init(named: arrStr[i]), for: .normal)
        }
    }
    func setTextFieldProperties(strText :  String?, textField : AETextFieldValidator){
        if strText == nil{
            textField.isMandatory = true
            
        }
        else{
            textField.isMandatory = false
            textField.validate()
        }
        
    }
    //MARK:- CollectionView Datasource
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if isRelationshipStatus == true {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! ProfileCollectionViewCell
           
            cell.btnTaken.accessibilityHint = "1"
            cell.btnSingle.accessibilityHint = "0"
            cell.btnSingle.addTarget(self, action: #selector(self.singleTakenBtnTapped(sender:)), for: .touchUpInside)
            cell.btnTaken.addTarget(self, action: #selector(self.singleTakenBtnTapped(sender:)), for: .touchUpInside)
            if userType == 0 {
                
                
                self.setImages(arrBtn: [cell.btnSingle,cell.btnTaken], arrStr: ["imgSingleSeleted","imgTakenUnslected"])
            }
            else{
                self.setImages(arrBtn: [cell.btnSingle,cell.btnTaken], arrStr: ["imgSingleUnseleted","imgTakenSelected"])
            }
            
            return cell
        }
        
        else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cellAbout", for: indexPath) as! ProfileCollectionViewCell
            
            cell.textview.addshodowToTextView()
            cell.subView.addshodowToView()
            cell.workView.addshodowToView()
          
            
            
            if  userData.aboutme == nil {
                cell.textview.text = "A little bit about me"
                
                cell.textview.textColor = lightGray
                
                cell.textViewError.text = nil
            }
            else{
                cell.textview.text = userData.aboutme?.base64Decoded()
                cell.textViewError.text = tempText
                
                cell.textview.textColor = UIColor.black
            }
            
            cell.textview.delegate = self
            
            
            cell.txtHeight.delegate = self
            cell.txtEthnicity.delegate = self
            cell.txtReligion.delegate = self
            cell.txtEducation .delegate = self
            
            cell.txtWorkPlace.delegate = self
            cell.txtOccupation.delegate = self
            cell.txtWorkPlace.accessibilityHint = "workPlace"
            cell.txtOccupation.accessibilityHint = "occupation"
            
            cell.txtHeight.accessibilityHint = "height"
            cell.txtEthnicity.accessibilityHint = "ethnicity"
            cell.txtReligion.accessibilityHint = "religion"
            cell.txtEducation.accessibilityHint = "education"
            cell.textViewError.accessibilityHint = "temp"
            
            cell.lblCount.text = "\(150 - textCount)"
            cell.textViewError.addRegx(nameReg, withMsg: invalidName)
            cell.txtHeight.addRegx(nameReg, withMsg: invalidName)
            
            cell.txtEthnicity.addRegx(nameReg, withMsg: invalidName)
            cell.txtReligion.addRegx(nameReg, withMsg: invalidName)
            cell.txtEducation.addRegx(nameReg, withMsg: invalidName)
            
            self.setTextFieldProperties(strText:userData.aboutme , textField: cell.textViewError)
            
            self.setTextFieldProperties(strText:userData.height?.base64Decoded() , textField: cell.txtHeight)
            
            self.setTextFieldProperties(strText:userData.religion , textField: cell.txtReligion)
            
            self.setTextFieldProperties(strText:userData.ethnicity , textField: cell.txtEthnicity)
            
            self.setTextFieldProperties(strText:userData.education , textField: cell.txtEducation)
            
            cell.txtHeight.text = userData.height?.base64Decoded()
            cell.txtEthnicity.text = userData.ethnicity
            cell.txtReligion.text = userData.religion
            
            cell.txtEducation.text = userData.education
            
            cell.txtOccupation.text = userData.occupation?.capitalized
            cell.txtWorkPlace.text = userData.currentWork?.capitalized
            return cell
        }
            
      
        
        
        
    }
    
    //MARK:- CollectionView Delegates
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        
        
        return CGSize(width: collectionView.frame.size.width, height: collectionView.frame.size.height )
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets.init(top: 0, left: 0, bottom: 0, right: 0 )
    }
    
    
    //MARK:- UITextField Delegates
    func textFieldDidBeginEditing(_ textField: UITextField) {
    
        
          if textField.accessibilityHint != "textView"{
        if textField.accessibilityHint == "height" {
                textField.resignFirstResponder()
            
            var array = [String]()
            for i in 135..<199 {
                
                let  feet  =  Double(i) * 0.3937008
                
                let  f1 = Int(feet / 12 )
                let f2 = Int(feet) % 12
                array.append("\(f1)'\(f2)\" (\(i) cms)")
            }
            self.optionWithValues(tag: 1, array: array, strValue: userData.height?.base64Decoded() ?? "")
            
        }
        else if textField.accessibilityHint == "ethnicity" {
                textField.resignFirstResponder()
            let array = ["American Indian"," Black/African Descent","East Indian","Hispanic/Latino","Middle Eastern","Pacific Islander","South Asian","White/Caucasian","Other","Prefer Not to Say"]
            
            self.showOptionsInTableView(array: array, tag: 2, strValue: userData.ethnicity ?? "", isStartingPreference: false)
        }
        else if textField.accessibilityHint == "religion" {
                textField.resignFirstResponder()
            let array = ["Buddhist","Christian","Hindu","Jewish","Muslim","Sikh","No Religion","Prefer Not to Say"]
            
            self.showOptionsInTableView(array: array, tag: 3, strValue: userData.religion ?? "", isStartingPreference: false)
        }
        else if textField.accessibilityHint == "education" {
            textField.resignFirstResponder()
            let array = ["Doctorate","Masters","Bachelors","Associates","Trade School","High School","No Education","Not Specified"]
            
            self.showOptionsInTableView(array: array, tag: 4, strValue: userData.education ?? "", isStartingPreference: false)
        }
        else if textField.accessibilityHint == "occupation" {
            let cell = self.collectionView.cellForItem(at: IndexPath.init(row: 0, section: 0)) as? ProfileCollectionViewCell
            
            cell?.scrollView.isScrollEnabled = false
            
        }
        else if textField.accessibilityHint == "workPlace" {
            let cell = self.collectionView.cellForItem(at: IndexPath.init(row: 0, section: 0)) as? ProfileCollectionViewCell
            
            cell?.scrollView.isScrollEnabled = false
            
        }
    }
        
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        textField.resignFirstResponder()
        
        if textField.accessibilityHint == "occupation" {
            let cell = self.collectionView.cellForItem(at: IndexPath.init(row: 0, section: 0)) as? ProfileCollectionViewCell
            
            cell?.scrollView.isScrollEnabled = true
           userData.occupation = textField.text ?? ""
              self.collectionView.reloadData()
        }
        else if textField.accessibilityHint == "workPlace" {
            let cell = self.collectionView.cellForItem(at: IndexPath.init(row: 0, section: 0)) as? ProfileCollectionViewCell
            
            cell?.scrollView.isScrollEnabled = true
            userData.currentWork = textField.text ?? ""
            self.collectionView.reloadData()
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField.accessibilityHint == "occupation" || textField.accessibilityHint == "workPlace"{
            let maxLength = 25
            let currentString: NSString = textField.text! as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            return newString.length <= maxLength
            
        }
        else{
            return true
        }
    }
    
    func showOptionsInTableView(array : [String] ,tag : Int,strValue:String ,isStartingPreference : Bool){
        let vc = OptionTable.init(frame: self.view.frame, dataArr:array , tableframe: CGRect.init(x: 10, y: Int(self.view.frame.size.height / 2)  - Int(array.count * 25) , width: Int(self.view.frame.size.width - 20), height:  Int(array.count * 50)), tag: tag,strValue : strValue ,isStartingPreference :isStartingPreference,titleString:"dummy not need")
        vc.delegates = self
        self.view.addSubview(vc)
    }
    
    
    
    //MARK:- OptionWithValues Functions
    func optionWithValues(tag: Int ,array : [String] ,strValue : String){
        
        let vc = PickerView.init(frame: self.view.frame, array: array, str: strValue, tag: tag , numberOfComponents : 1,isStartingPreference : false)
        
        vc.delegates = self
        self.view.addSubview(vc)
        
    }
    
    //MARK:- TextView Delegates
    func textViewDidBeginEditing(_ textView: UITextView) {
        let cell = self.collectionView.cellForItem(at: IndexPath.init(row: 0, section: 0)) as? ProfileCollectionViewCell
        
        cell?.scrollView.isScrollEnabled = false
        
        if textView.textColor == lightGray
        {
            textView.text = nil
            textView.textColor = UIColor.black
            print("begin working")
        }
    }
    
    
    func textViewDidEndEditing(_ textView: UITextView) {
        
        
        if textView.text.isEmpty {
            textView.text = "A little bit about me"
            textView.textColor = lightGray
           userData.aboutme = nil
            print("end working")
        }
        else{
              userData.aboutme = textView.text ?? nil
        }
        let cell = self.collectionView.cellForItem(at: IndexPath.init(row: 0, section: 0)) as? ProfileCollectionViewCell
        
        cell?.scrollView.isScrollEnabled = true
        self.collectionView.reloadData()
    }
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        let newText = (textView.text as NSString).replacingCharacters(in: range, with: text)
        
        
        let cell = self.collectionView.cellForItem(at: IndexPath.init(row: 0, section: 0 )) as! ProfileCollectionViewCell
        
        cell.lblCount.text = "\(150 - newText.count)"
        textCount = newText.count
        return newText.count < 150
    }
    
    //MARK:- Api Update Usertype Function
    func apiUpdateUserType(parameter : Parameters){
        
        AccountApiInterface.sharedInstance.postRequestWithParam(fromViewController: self, actionName: "confirmRelationship_status", dictParam: parameter as [String : AnyObject], isShowIndicator: true, success: { (json) in
            
            DispatchQueue.main.async {
            
                self.userData.usertype = self.userType
                
                 standard.set(self.userData.dictionaryRepresentation(), forKey: "userData")
                if   self.userData.usertype == 1{
                    
                    DispatchQueue.main.async {
                        self.navigationController?.popViewController(animated: true)
                    }
                }
                else{
                    let vc = self.storyboard?.instantiateViewController(withIdentifier: "PreferencesViewController") as! PreferencesViewController
                    
                    vc.isFromEditProfile = true
                    self.navigationController?.pushViewController(vc, animated: true)
                }
            }
            
        }) { (error) in
            self.showAlert(messageStr: error)
        }
    }
    
    
    //MARK:- Update Profile Call Function
    
    func updateProfileApiCall(parameters: Parameters){
        print(parameters)
        AccountApiInterface.sharedInstance.postRequestWithParam(fromViewController: self, actionName: "edit_profile", dictParam: parameters as [String : AnyObject], isShowIndicator: true, success: { (json) in
            
            print(json)
             standard.set(self.userData.dictionaryRepresentation(), forKey: "userData")
            DispatchQueue.main.async {
               self.navigationController?.popViewController(animated: true)
            }
            
        }) { (error) in
           self.showAlert(messageStr: error)
        }
        
    }
    
    
    
}



extension EditProfileViewController : setValuesDelegates,alertButtonDelegates{
    
    func  setTextfieldsvalues (str : String , tag: Int){
        if tag == 1 {
              userData.height = str
            
        }
        else if tag == 2 {
           userData.ethnicity = str
            
        }
        else if tag == 3{
          userData.religion = str
            
        }
        else if tag == 4{
           userData.education = str
            
        }
        self.collectionView.reloadData()
    }
    
    
    
    }

    


