//
//  ChatViewController.swift
//  chatapp
//
//  Created by Codeyeti iOS on 08/11/17.
//  Copyright © 2017 Relinns. All rights reserved.
//

import UIKit

class ChatViewController: UIViewController , UITableViewDelegate , UITableViewDataSource ,UITextViewDelegate{
   let placeHolder = "Type your message"
    @IBOutlet weak var navView: UIView!
     var arrChat = [ChatModel]()
    var currentPage = 1
   
    var isResultsAviablleToLoad = true
    
    var fromIntro = Bool()
    var friendId = Int()
    @IBOutlet weak var imgSend: UIImageView!
    @IBOutlet weak var heightCons: NSLayoutConstraint!
    @IBOutlet weak var table: UITableView!
    
    @IBOutlet weak var txtHeight: NSLayoutConstraint!
    @IBOutlet weak var imgSendIcon: UIImageView!
    
    @IBOutlet weak var messageBox: UITextView!
    
    let refreshControl = UIRefreshControl()
    @IBOutlet weak var bottomToolsView: UIView!
  
    //MARK: VARIABLES
    
    override func viewDidLoad() {
        super.viewDidLoad()
     
        self.table.delegate = self
        self.table.dataSource = self
        self.table.tableFooterView = UIView()
        
         messageBox.text = placeHolder
        messageBox.textColor = lightGray
        self.imgSendIcon.tintColor = appColor
        if UIDevice.isIphoneX == true {
            heightCons.constant = 84
        }
        currentPage = 1
        self.fetchChatApiCall(page: currentPage)
        
        if #available(iOS 10.0, *) {
            table.refreshControl = refreshControl
        } else {
            table.addSubview(refreshControl)
        }
        refreshControl.tintColor = appColor
        refreshControl.addTarget(self, action: #selector(refreshData(_:)), for: .valueChanged)
        
        // Do any additional setup after loading the view.
        
        
        
    }
    @objc func refreshData(_ sender: Any) {
        
        currentPage =  currentPage + 1
        self.isResultsAviablleToLoad = true
        self.fetchChatApiCall(page: currentPage)
      
    }
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
       
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    //MARK: BUTTONS ACTIONS
    
    @IBAction func backBtn(_ sender: UIButton) {
       
        if self.fromIntro == true {
                let viewControllers: [UIViewController] = self.navigationController!.viewControllers
                for aViewController in viewControllers {
                    if aViewController is Tabbar {
                        let vc  = aViewController as! Tabbar
                        
                        vc.setSelectIndex(from: 2, to: 4)
                        self.navigationController!.popToViewController(aViewController, animated: false)

                    }
                }
            }
            else{
                self.navigationController?.popViewController(animated: true)
            }
            
            
        
    }
    @IBAction func sendBtnTapped(_ sender: UIButton) {
        if messageBox.text != nil && messageBox.text != placeHolder && messageBox.text != ""{
        self.emitData()
        }
        
    }
    //MARK:- Emit Data To Server
    func emitData() {
         let userData = UserData.init(json: JSON(standard.value(forKey: "userData") as? [String : Any] ?? [String : Any]()))
        let dict = [
            
            "from_user":"\(standard.value(forKey: "userid")!)",
            
            "user_type": userData.usertype ?? 0,
            
            "to_user":friendId,
            
            "message":(messageBox.text).base64Encoded() ?? "",
            
            
            "methodName":"chat"
            ] as [String : Any]
        print(dict)
        messageBox.text = ""
         SocketBase.sharedInstance.delegate = self
        SocketBase.sharedInstance.socketEmit(emitString: "socketFromClient", data: dict)
    }
    
    //MARK: TABLE VIEW
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrChat.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        if standard.value(forKey: "userid") as? Int ==  arrChat[indexPath.row].fromUser{
            
             let myCell = tableView.dequeueReusableCell(withIdentifier: "MyMessageTableViewCell")as! MyMessageTableViewCell
             myCell.userMessage.text = arrChat[indexPath.row].text?.base64Decoded()
             myCell.userName.text = arrChat[indexPath.row].fromUserName
            
            let local = Helper.convertUTCToLocal(timeString: arrChat[indexPath.row].timestamp ?? "")
            
            myCell.userTime.text =  Helper.timeAgoSinceDate(date: local as NSDate, numericDates: true)
            
            
            
            let imageURL = arrChat[indexPath.row].fromUserImage ?? ""
            
            
            myCell.userImage.sd_setImage(with: URL(string: imageURL), placeholderImage: #imageLiteral(resourceName: "imgProfile"), options: [], completed: nil)
            return myCell
        }
        else{
             let cell = tableView.dequeueReusableCell(withIdentifier: "OtherMessageTableViewCell")as! OtherMessageTableViewCell
            cell.userMessage.text =  arrChat[indexPath.row].text?.base64Decoded()
            cell.userName.text = arrChat[indexPath.row].fromUserName
         
    
              let local = Helper.convertUTCToLocal(timeString: arrChat[indexPath.row].timestamp ?? "")
            
            cell.userTime.text =  Helper.timeAgoSinceDate(date: local as NSDate, numericDates: true)
            
            let imageURL = arrChat[indexPath.row].fromUserImage ?? ""
            
            
            cell.userImage.sd_setImage(with: URL(string: imageURL), placeholderImage: #imageLiteral(resourceName: "imgProfile"), options: [], completed: nil)
             return cell
        }
        
        
       
        
    }
    
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        tableView.deselectRow(at: indexPath, animated: true)
        
    }
   
    //MARK:- Fetch Chat API Call
    func fetchChatApiCall(page : Int){
        let parameter : Parameters = ["user_id":standard.value(forKey: "userid")!,
            "page": page,"from_user":friendId]
       
        print(parameter)
        AccountApiInterface.sharedInstance.postRequestWithParam(fromViewController: self, actionName: "fetch_chat", dictParam: parameter as [String : AnyObject], isShowIndicator: (page == 1) ? true : false , success: { (json) in
            
            print(json)
             self.refreshControl.endRefreshing()
            
            if page == 1 {
                self.arrChat.removeAll()
            }
            
            guard(json["data"].arrayValue.count) > 0 else {
                
                self.isResultsAviablleToLoad = false
                DispatchQueue.main.async {
                    self.table.reloadData()
                }
                
                return
            }
            print(json["data"].arrayValue.count)
            for i in 0..<json["data"].arrayValue.count{
                
                let arrTemp = ChatModel.init(json: json["data"][i])
                
                self.arrChat.append(arrTemp)
            }
            self.arrChat   =    self.arrChat.sorted() {return (Helper.convertStringTodate(strDate: $0.timestamp ?? "") ) < (Helper.convertStringTodate(strDate: $1.timestamp ?? "") )}
            self.isResultsAviablleToLoad = true
            
            
            DispatchQueue.main.async {
                self.table.reloadData()
                if self.currentPage == 1 {
                   
                        self.table.scrollToRow(at: IndexPath.init(row:  self.arrChat.count - 1, section: 0), at: .bottom, animated: false)
               }
                
                
            }
        }) { (error) in
            
            self.isResultsAviablleToLoad = false
            self.refreshControl.endRefreshing()
            DispatchQueue.main.async {
                self.table.reloadData()
            }
        }
        
    }
    
    //MARK:- TextView Delegates
    func textViewDidBeginEditing(_ textView: UITextView) {
        
        
        if textView.textColor == lightGray
        {
            textView.text = nil
            textView.textColor = UIColor.black
            print("begin working")
        }
    }
    
    
    func textViewDidEndEditing(_ textView: UITextView) {
        
        
        if textView.text.isEmpty {
            textView.text = placeHolder
            textView.textColor = lightGray
            
            print("end working")
        }
        
    }
    
    
    func textViewDidChange(_ textView: UITextView) {
        
        
        if textView.contentSize.height > 40 {
            let height = textView.sizeThatFits(textView.contentSize).height
            
            self.txtHeight.constant = height
        } else{
            self.txtHeight.constant = 40
        }
        
    }
    
}
extension ChatViewController: SocketDelegate {
    
    func listenedData(data: JSON) {
        print(data)
          let temp = ChatModel.init(json: data[0]["message"])
        self.arrChat.append(temp)
        self.table.reloadData()
         self.table.scrollToRow(at: IndexPath.init(row:  self.arrChat.count - 1, section: 0), at: .bottom, animated: false)
        
    }
    
}
