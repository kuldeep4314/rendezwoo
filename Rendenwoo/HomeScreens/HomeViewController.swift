//
//  HomeViewController.swift
//  Rendenwoo
//
//  Created by goyal on 28/02/18.
//  Copyright © 2018 ankita. All rights reserved.
//

import UIKit
var introsInfo = JSON(JSON.self)
class HomeViewController: UIViewController {

    @IBOutlet weak var lblGender: UILabel!
    @IBOutlet weak var lblLocation: UILabel!
    @IBOutlet weak var lblOccupation: UILabel!
    @IBOutlet weak var lblNameAge: UILabel!
    @IBOutlet weak var imgUserImage: UIImageView!
    @IBOutlet weak var lblReligion: UILabel!
    @IBOutlet weak var lblEthnicity: UILabel!
    @IBOutlet weak var lblHeight: UILabel!
    @IBOutlet weak var lblAboutMe: UILabel!
    @IBOutlet weak var lblRelationShipStatus: UILabel!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var topView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
      self.topView.addshodowToView()
    
        self.view.backgroundColor = moreLight
        UIApplication.shared.statusBarStyle = .lightContent
           self.setProfileData()
        appDelegates.coreLocationFunction()
    
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    

    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        let parameter : Parameters = ["user_id":"\(standard.value(forKey: "userid")!)"]
        
        self.fetchIntrosApiCall(parameters: parameter)
        
        self.fetchUnReadNotification(parameters: parameter)
        
        self.setProfileData()
    }
    
    func setProfileData (){
          let userData = UserData.init(json: JSON(standard.value(forKey: "userData") as? [String : Any] ?? [String : Any]()))
        
                 self.lblNameAge.text  = userData.name
             self.lblGender.text = (userData.gender == 0) ? "Male" : "Female"
             self.lblRelationShipStatus.text =  (userData.usertype == 0) ? "Single" : "Taken"
     
            self.lblHeight.text =  userData.height?.base64Decoded()
        
            self.lblReligion.text = userData.religion
        
            self.lblEthnicity.text = userData.ethnicity
        
            self.lblAboutMe.text = userData.aboutme?.base64Decoded()
        
            self.lblLocation.text = userData.currentLocation ?? ""
           let imageURL = userData.image ?? ""
        
        
        imgUserImage.sd_setImage(with: URL(string: imageURL), placeholderImage: #imageLiteral(resourceName: "imgProfile"), options: [], completed: nil)
        guard let value1 = userData.occupation, let value2 = userData.currentWork,  userData.occupation != "" , userData.currentWork != ""
            else
        {
                self.lblOccupation.attributedText = NSMutableAttributedString.init(string: "")
               print(userData.occupation ?? "")
                print(userData.currentWork ?? "")
            return
        }
        
        let attrs1 =  [NSAttributedStringKey.foregroundColor : DarkGray]
        
        let myMutableString = NSMutableAttributedString(
            string: "\(value1) at \(value2)",
            attributes: attrs1)
        
        myMutableString.addAttributes([NSAttributedStringKey.foregroundColor : UIColor.black], range: NSRange(
            location:"\(value1) at ".count,
            length:value2.count))
        self.lblOccupation.attributedText = myMutableString
        
       
        
        
    }
    
    
    @IBAction func btnEditProfileTapped(_ sender: UIButton) {
        let vc  = self.storyboard?.instantiateViewController(withIdentifier: "EditProfileViewController") as! EditProfileViewController
        
        if sender.tag == 100 {
            vc.isRelationshipStatus = true
            
        }
        self.navigationController?.pushViewController(vc, animated: true)
       
        
    }
    
    
    @IBAction func btnMyPhotoTapped(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "PhotosViewController") as! PhotosViewController
        
        self.navigationController?.pushViewController(vc, animated: true)
    }
    @IBAction func gestureSettingTapped(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "SettingsViewController") as! SettingsViewController
        
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    @IBAction func gestureIntrosTapped(_ sender: UITapGestureRecognizer) {
        
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "IntrosViewController") as! IntrosViewController
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    @IBAction func gestureTaggedFriendTapped(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "TaggedFriendsViewController") as! TaggedFriendsViewController
        
        self.navigationController?.pushViewController(vc, animated: true)
       
    }
    @IBAction func gesturePreferencesTapped(_ sender: Any) {
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "PreferencesViewController") as! PreferencesViewController
        
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    //MARK:- Fetch Intros API Call
    func fetchIntrosApiCall(parameters: Parameters){
        AccountApiInterface.sharedInstance.postRequestWithParam(fromViewController: self, actionName: "all_intro_info", dictParam: parameters as [String : AnyObject], isShowIndicator: true, success: { (json) in
            
            
            introsInfo = json["data"]
            
        }) { (error) in
            self.showAlert(messageStr: error)
        }
        
    }
    
    //MARK:- Fetch UnReadNotification API Call
    func fetchUnReadNotification(parameters: Parameters){
        AccountApiInterface.sharedInstance.postRequestWithParam(fromViewController: self, actionName: "unreadNotification", dictParam: parameters as [String : AnyObject], isShowIndicator: false, success: { (json) in
            print(json)
            DispatchQueue.main.async {
                
                if json["data"]["unread_notification"].intValue > 0 {
                    let  parent = self.parent as! Tabbar
                    
                    parent.tabBar.items![3].badgeValue = "\(json["data"]["unread_notification"].intValue)"
                }
             
                
            }
        }) { (error) in
            print(error)
           
        }
        
    }
}
