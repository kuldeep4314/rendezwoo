//
//  MessageViewController.swift
//  Rendenwoo
//
//  Created by goyal on 28/02/18.
//  Copyright © 2018 ankita. All rights reserved.
//

import UIKit

class MessageViewController: UIViewController ,UITableViewDataSource,UITableViewDelegate{

    @IBOutlet weak var topViewHeight: NSLayoutConstraint!
    @IBOutlet weak var btnCrushes: UIButton!
    @IBOutlet weak var btnFriends: UIButton!
    @IBOutlet weak var topSubView: UIView!
    @IBOutlet weak var tableView: UITableView!
    let refreshControl = UIRefreshControl()
    var jsonFriends = JSON(JSON.self)
    var isFriendsMsg = true
    var todayMessage = [MessageFriends]()
    var yesterdayMessage = [MessageFriends]()
    var earilerMessage = [MessageFriends]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
    tableView.tableFooterView = UIView()
        
        topSubView.layer.borderColor = UIColor.white.cgColor
        topSubView.layer.borderWidth = 1.0
        self.view.backgroundColor = moreLight
        self.tableView.backgroundColor = moreLight
        
        btnCrushes.backgroundColor = appColor
        btnCrushes.setTitleColor(UIColor.white, for: .normal)
        btnFriends.backgroundColor = UIColor.white
        btnFriends.setTitleColor(appColor, for: .normal)
        
        if UIDevice.isIphoneX == true{
            topViewHeight.constant = 120
        }
        if #available(iOS 10.0, *) {
            tableView.refreshControl = refreshControl
        } else {
            tableView.addSubview(refreshControl)
        }
        refreshControl.tintColor = appColor
        refreshControl.addTarget(self, action: #selector(refreshData(_:)), for: .valueChanged)
       
        // Do any additional setup after loading the view.
    }
    
    func setDataOfUser(){
         let userData = UserData.init(json: JSON(standard.value(forKey: "userData") as? [String : Any] ?? [String : Any]()))
        
        if  (userData.usertype ?? 0)  == 0{
            btnFriends.setTitle("FRIENDS", for: .normal)
            btnCrushes.setTitle("CRUSHES", for: .normal)
            
        }
        else{
            btnFriends.setTitle("FRIENDS", for: .normal)
            btnCrushes.setTitle("MATCH-SEEKERS", for: .normal)
        }
        let param  : Parameters = ["user_id" : "\(standard.value(forKey: "userid")!)"]
        
        self.fetchMessageListApi(parameters: param)
        
    }
    @objc func refreshData(_ sender: Any) {
        let param  : Parameters = ["user_id" : "\(standard.value(forKey: "userid")!)"]
        
        self.fetchMessageListApi(parameters: param)
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
         self.setDataOfUser()
       
    }
   
    func showAlertView (){
        let vc =  AlertWithOneButtonTwoTitle.init(frame: self.view.frame, mainTitle: "Congratulations!", title: "Jasmine introduced you to Garry. \n Garry fancies you too!", doneTitle: "Chat with Garry",delegateStr: "chat")
        vc.delegates = self
        self.view.addSubview(vc)
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func btnFriendTapped(_ sender: Any) {
        btnCrushes.backgroundColor = appColor
        btnCrushes.setTitleColor(UIColor.white, for: .normal)
        btnFriends.backgroundColor = UIColor.white
        btnFriends.setTitleColor(appColor, for: .normal)
        isFriendsMsg = true
        
            self.filterData(jsonArray: self.jsonFriends["friends"].arrayValue)
        
        
    }
    @IBAction func btnCrushesTapped(_ sender: Any) {
        btnFriends.backgroundColor = appColor
        btnFriends.setTitleColor(UIColor.white, for: .normal)
        btnCrushes.backgroundColor = UIColor.white
        btnCrushes.setTitleColor(appColor, for: .normal)
          isFriendsMsg = false
       
        self.filterData(jsonArray: self.jsonFriends["other_friends"].arrayValue)
        
        
        
//           if  UserInfo.sharedInstance.relationshipStatus == "single" {
//                    let vc = CrushView.init(frame: self.view.frame)
//                    self.view.addSubview(vc)
//        }

        
    }
    
    
    //MARK:- TableView DataSource
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

        if section == 0 {
            return  self.todayMessage.count
        }
        else if section == 1{
            return self.yesterdayMessage.count
        }
        else{
            return self.earilerMessage.count
        }

    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! MessageTableViewCell
        
        if indexPath.section == 0{
            let imageURL = self.todayMessage[indexPath.row].imageUrl ?? ""
            
            
            cell.imgView.sd_setImage(with: URL(string: imageURL), placeholderImage: #imageLiteral(resourceName: "imgProfile"), options: [], completed: nil)
            
            cell.lblName.text = self.todayMessage[indexPath.row].name
            cell.lblMessage.text = self.todayMessage[indexPath.row].text?.base64Decoded()
            
            let local = Helper.convertUTCToLocal(timeString: self.todayMessage[indexPath.row].timestamp ?? "")
            
            cell.lblTime.text =  Helper.timeAgoSinceDate(date: local as NSDate, numericDates: true)
            
        }
        else if indexPath.section == 1 {
            let imageURL = self.yesterdayMessage[indexPath.row].imageUrl ?? ""
            
            
            cell.imgView.sd_setImage(with: URL(string: imageURL), placeholderImage: #imageLiteral(resourceName: "imgProfile"), options: [], completed: nil)
            
            cell.lblName.text = self.yesterdayMessage[indexPath.row].name
            cell.lblMessage.text = self.yesterdayMessage[indexPath.row].text?.base64Decoded()
          
            let local = Helper.convertUTCToLocal(timeString: self.yesterdayMessage[indexPath.row].timestamp ?? "")
            
            cell.lblTime.text =  Helper.timeAgoSinceDate(date: local as NSDate, numericDates: true)
        }
        else{
            let imageURL = self.earilerMessage[indexPath.row].imageUrl ?? ""
            
            
            cell.imgView.sd_setImage(with: URL(string: imageURL), placeholderImage: #imageLiteral(resourceName: "imgProfile"), options: [], completed: nil)
            
            cell.lblName.text = self.earilerMessage[indexPath.row].name
            cell.lblMessage.text = self.earilerMessage[indexPath.row].text?.base64Decoded()
           
            let local = Helper.convertUTCToLocal(timeString: self.earilerMessage[indexPath.row].timestamp ?? "")
            
            cell.lblTime.text =  Helper.timeAgoSinceDate(date: local as NSDate, numericDates: true)
            
            
        }
        
        
           let viewBg = UIView()
        viewBg.backgroundColor = selectionRow
         cell.selectedBackgroundView = viewBg
    return cell
    }
   
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 90
    }

    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
          let cell = tableView.dequeueReusableCell(withIdentifier: "cellHeader")
        
        let lbl = cell?.viewWithTag(333) as? UILabel
     
        if section == 0{
            lbl?.text = "Today"
        }
        else if section == 1 {
               lbl?.text = "Yesterday"
        }
        else{
              lbl?.text = "Earlier"
        }
        
        
         return cell?.contentView
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        if section == 0{
            return  (todayMessage.count > 0) ? 30 : 0
        }
        else if section == 1 {
             return  (yesterdayMessage.count > 0) ? 30 : 0
        }
        else{
            return  (earilerMessage.count > 0) ? 30 : 0
        }
        

    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
         tableView.deselectRow(at: indexPath, animated: true)
        
        let vc  = self.storyboard?.instantiateViewController(withIdentifier: "ChatViewController") as! ChatViewController
        
        if indexPath.section == 0{
           vc.friendId = todayMessage[indexPath.row].fromUser ?? 0
        }
        else if indexPath.section == 1 {
            vc.friendId = yesterdayMessage[indexPath.row].fromUser ?? 0
        }
        else{
           vc.friendId = earilerMessage[indexPath.row].fromUser ?? 0
        }
        
        self.navigationController?.pushViewController(vc, animated: true)
        
        
    }
    
   
    
    
    //MARK:- FetchMessageList Call Function
    
    func fetchMessageListApi(parameters: Parameters){
        print(parameters)
        AccountApiInterface.sharedInstance.postRequestWithParam(fromViewController: self, actionName: "get_messages", dictParam: parameters as [String : AnyObject], isShowIndicator: true, success: { (json) in
              self.refreshControl.endRefreshing()
            print(json)
            self.jsonFriends =  json["data"]
            
            if self.isFriendsMsg == true {
                   self.filterData(jsonArray: self.jsonFriends["friends"].arrayValue)
            }
            else{
                   self.filterData(jsonArray: self.jsonFriends["other_friends"].arrayValue)
            }
            
         
            
        }) { (error) in
              self.refreshControl.endRefreshing()
            EmptyDataSet.sharedInstance.delegates = self
            EmptyDataSet.sharedInstance.showEmptyData(title: "No Messages found.", image: #imageLiteral(resourceName: "search"), tableView: self.tableView)
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
        }
        
    }
    
    
    func filterData(jsonArray : [JSON]){
        
        self.todayMessage.removeAll()
        self.yesterdayMessage.removeAll()
        self.earilerMessage.removeAll()
        
            for i in 0..<jsonArray.count{
                
                let count = Helper.differenceBetweenTwoDates(dateStr: jsonArray[i]["timestamp"].stringValue)
                
                if count == 0 {
                    self.todayMessage.append( MessageFriends.init(json: jsonArray[i]))
                    
                }
                else if count == 1{
                    self.yesterdayMessage.append( MessageFriends.init(json: jsonArray[i]))
                }
                else{
                     self.earilerMessage.append( MessageFriends.init(json: jsonArray[i]))
                }
                
                
            }
            
        if   self.todayMessage.count == 0 && self.yesterdayMessage.count == 0 && self.earilerMessage.count == 0 {
            EmptyDataSet.sharedInstance.delegates = self
            EmptyDataSet.sharedInstance.showEmptyData(title: "No Messages found.", image: #imageLiteral(resourceName: "search"), tableView: self.tableView)
        }
    
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
        
    }
}


extension MessageViewController : alertButtonDelegates ,emptyDataSetDelegatesOnTab{
    func alertButtonAction(index : Int, delegatesStr:String){
        
        let vc  = self.storyboard?.instantiateViewController(withIdentifier: "ChatViewController") as! ChatViewController
        
        self.navigationController?.pushViewController(vc, animated: true)
    
    }
      func didTappedFunction(){
      let   userData = UserData.init(json: JSON(standard.value(forKey: "userData") as? [String : Any] ?? [String : Any]()))
        let param  : Parameters = ["user_id" : "\(standard.value(forKey: "userid")!)","user_type": (userData.usertype ?? 0)]
        
        self.fetchMessageListApi(parameters: param)
    }
}

