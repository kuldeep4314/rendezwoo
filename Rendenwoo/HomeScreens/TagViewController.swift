//
//  TagViewController.swift
//  Rendenwoo
//
//  Created by goyal on 16/05/18.
//  Copyright © 2018 ankita. All rights reserved.
//



import UIKit

class TagViewController: UIViewController,UIScrollViewDelegate {
    @IBOutlet weak var scrollView: UIScrollView!
    var userFriends = [UserFriends]()

    @IBOutlet weak var topCons: NSLayoutConstraint!
    @IBOutlet weak var imageView: UIImageView!
   
    @IBOutlet weak var imageHeight: NSLayoutConstraint!
    
    @IBOutlet weak var topSpace: NSLayoutConstraint!
    @IBOutlet weak var btnCoutinue: UIButton!
    var  personTag : PersonTag?
  
    var imgInfo = [[String : Any]]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if UIDevice.isIphoneX == true{
            topSpace.constant = 84
        }
        
        self.btnCoutinue.backgroundColor = appColor
        
        
        imageView.sd_setImage(with: URL(string:personTag?.imageUrl ?? ""), placeholderImage: #imageLiteral(resourceName: "imgUpload"), options: [.refreshCached]) { (img, error, type, url) in
            
            let widthInPixels = img?.size.width ?? 0
            let heightInPixels = img?.size.height ?? 0
            
            let height = self.view.frame.size.width * (heightInPixels/widthInPixels)
            
           self.imageHeight.constant =  height
             
        }
        
    
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(_:)))
        
        imageView.addGestureRecognizer(tap)
       
      
        let param : Parameters = ["user_id": "\(standard.value(forKey: "userid") ?? 0)"]
        self.fetchFriendsApiCall(parameters : param)

        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if  self.imageHeight.constant > scrollView.frame.size.height{
            topCons.constant = 0
        }
        
        scrollView.contentSize = CGSize(width: self.view.frame.width, height: scrollView.frame.size.height)
        
          self.setTagInImages(img: imageView, p: personTag?.tagPerson ?? [Person]())
        
    }
    @IBAction func imgBackTapped(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
 
    
    func setTagInImages (img : UIImageView ,p : [Person]){
        
        print(p.count)
        for i in 10000..<10030{
            let vc =  img.viewWithTag(i) as? TagView
            vc?.removeFromSuperview()
        }
        
        for i in 0..<p.count{
            
            let x1 =  (p[i].xPos * img.frame.size.width ) / 100
            let y1 =  (p[i].yPos * img.frame.size.height ) / 100
          
            print(img.frame.size.width)
            print(img.frame.size.height)
            print(p[i].xPos)
            print(p[i].yPos)
            print(x1)
            print(y1)
            let faceBox = TagView.init(frame: CGRect.init(x: x1, y: y1 , width: 90, height: 100), name: p[i].name,color:  UIColor.black,ishowCrossbtn : false,hintText : "\(p[i].imageIndex),\(i)")
            faceBox.tag  = 10000 + i
            faceBox.delegates = self
            img.addSubview(faceBox)
            
        }
        
    }
    
    
    
    @objc func handleTap(_ sender: UITapGestureRecognizer) {
        
        let vcTag = self.view.viewWithTag(500) as? TagFriends
        
        if vcTag != nil {
            self.scrollView.isScrollEnabled = true
            vcTag?.removeFromSuperview()
        }
        else{
            self.scrollView.isScrollEnabled = false
            let touchPoint = sender.location(in: self.view)
            let viewPoint = sender.location(in: sender.view!)
            let img = sender.view as! UIImageView
            
            let vc = TagFriends.init(frame: CGRect.init(x: touchPoint.x, y: touchPoint.y, width: 150, height: 120), tag: sender.view!.tag ,personTag : personTag! ,superView : img,viewPoint : viewPoint)
            vc.delegates = self
            vc.mainArray = userFriends
            
            for i in 0..<(personTag?.tagPerson.count ?? 0) {
                
                vc.mainArray = vc.mainArray.filter({String($0.internalIdentifier ?? 0) != (personTag?.tagPerson[i].personId)})
            }
            
            vc.tag = 500
            self.view.addSubview(vc)
        }
        
    }
    
    
    @IBAction func btnContinue(_ sender: Any) {

        self.navigationController?.popViewController(animated: true)
    }
    
    
    //MARK:- Fetch Friends API Call
    func fetchFriendsApiCall(parameters: Parameters){
        AccountApiInterface.sharedInstance.postRequestWithParam(fromViewController: self, actionName: "fetch_friend", dictParam: parameters as [String : AnyObject], isShowIndicator: true, success: { (json) in
            
            print(json)
            self.userFriends.removeAll()
            for i in 0..<json["data"].arrayValue.count{
                
                let arrTemp = UserFriends.init(json: json["data"][i])
                
                self.userFriends.append(arrTemp)
            }
            
        }) { (error) in
            print(error)
        }
        
    }

    
    
    
    //MARK:- Fetch Location API Call
    func untagFromPicture(parameters: Parameters,indexs : [String]){
        print(parameters)
        AccountApiInterface.sharedInstance.postRequestWithParam(fromViewController: self, actionName: "untag_friend_from_one", dictParam: parameters as [String : AnyObject], isShowIndicator: true, success: { (json) in
            print(json)
            self.personTag?.tagPerson.remove(at: JSON(indexs[1]).intValue)
            print(json)
            DispatchQueue.main.async {
                self.setTagInImages(img: self.imageView, p: self.personTag?.tagPerson ?? [Person]())
            }
        }) { (error) in
            
            self.showAlert(messageStr: error)
        }
        
    }
    //MARK:- Tag Friends API Call
    func tagFriendsApiCall(parameters: Parameters,friend: PersonTag, index: Int){
        print(parameters)
        AccountApiInterface.sharedInstance.postRequestWithParam(fromViewController: self, actionName: "tag_friends", dictParam: parameters as [String : AnyObject], isShowIndicator: true, success: { (json) in
            
            print(json)
            self.personTag = friend
            DispatchQueue.main.async {
                self.setTagInImages(img: self.imageView, p: self.personTag?.tagPerson ?? [Person]())
                
            }
        }) { (error) in
            self.showAlert(messageStr: error)
        }
        
    }
    //MARK:- Send Verify User Status
//    func sendVerificationApiCall(parameters: Parameters,usertype : String){
//        print(parameters)
//        AccountApiInterface.sharedInstance.postRequestWithParam(fromViewController: self, actionName: "verify_relationship_status", dictParam: parameters as [String : AnyObject], isShowIndicator: true, success: { (json) in
//            self.personTag[self.selectedFriendIndex.section].tagPerson[self.selectedFriendIndex.row].status = usertype
//            DispatchQueue.main.async {
//                self.tableView.reloadData()
//                self.showAlert(messageStr: json["msg"].stringValue)
//            }
//        }) { (error) in
//            self.showAlert(messageStr: error)
//        }
//
//    }

    
}
extension TagViewController : tagFriendDelegates,deleteTagDelegates {
    func setTagFriendsDelegates(friend: PersonTag, index: Int) {
       
        self.scrollView.isScrollEnabled = true
        
        let params : Parameters = ["image_id":friend.imageId,
                                   "user_id" :friend.tagPerson[friend.tagPerson.count - 1].personId, "xpos":"\(friend.tagPerson[friend.tagPerson.count - 1].xPos)", "ypos":"\(friend.tagPerson[friend.tagPerson.count - 1].yPos)","name": friend.tagPerson[friend.tagPerson.count - 1].name,"status":friend.tagPerson[friend.tagPerson.count - 1].status]

        print(params)
        self.tagFriendsApiCall(parameters: params, friend: friend, index: index)

        
        
        
    }
    func  deleteTagFunction(strIndex : String){
        let index = strIndex.components(separatedBy: ",")
        print(index)
        let fId = personTag?.tagPerson[JSON(index[1]).intValue].personId
        let parameter  : Parameters = ["user_id":"\(standard.value(forKey: "userid")!)","image_id": personTag?.imageId ?? "","friend_id" :fId ?? ""]
        
        self.untagFromPicture(parameters: parameter,indexs: index)

        
        
    }
    
    
}
