//
//  MatchMakerViewController.swift
//  Rendenwoo
//
//  Created by goyal on 28/02/18.
//  Copyright © 2018 ankita. All rights reserved.
//

import UIKit

class MatchMakerViewController: UIViewController ,UITableViewDelegate,UITableViewDataSource ,UITextFieldDelegate{
    
    @IBOutlet weak var imgProfilePic: UIImageView!
    @IBOutlet weak var tableView: UITableView!
     let refreshControl = UIRefreshControl()
      var currentPage = 1
      var isResultsAviablleToLoad = true
     var arrMatchMaker = [RZMatchMaker]()
    
  
    @IBOutlet weak var topViewHeight: NSLayoutConstraint!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.tableFooterView = UIView()
        self.tableView.rowHeight = UITableViewAutomaticDimension
        let userData = UserData.init(json: JSON(standard.value(forKey: "userData") as? [String : Any] ?? [String : Any]()))
        let imageURL = userData.image ?? ""
       imgProfilePic.sd_setImage(with: URL(string: imageURL), placeholderImage: #imageLiteral(resourceName: "imgProfile"), options: [], completed: nil)
        
        
        
        if UIDevice.isIphoneX == true{
            topViewHeight.constant = 84
        }
        if #available(iOS 10.0, *) {
            tableView.refreshControl = refreshControl
        } else {
            tableView.addSubview(refreshControl)
        }
        refreshControl.tintColor = appColor
        refreshControl.addTarget(self, action: #selector(refreshData(_:)), for: .valueChanged)
        
        
        
        // Do any additional setup after loading the view.
    }
    
  @objc func refreshData(_ sender: Any) {
        
        currentPage = 1
        self.isResultsAviablleToLoad = true
        self.fetchMatchMakerApiCall(page: currentPage)
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    
    
    @IBAction func btnWhathapeningTapped(_ sender: Any) {
        let vc = CreatePost.init(frame: self.view.frame)
        vc.delegates = self
        self.view.addSubview(vc)
    
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        currentPage = 1
          isResultsAviablleToLoad = true
        self.fetchMatchMakerApiCall(page: currentPage)
        
    }
    
    
    func uploadPostToMatcMakerProfile(image_id : String ,imageUrl : String ){
    
        let vc = CreatePost.init(frame:  self.view.frame, imageId: image_id, isSharedPostMatchMaker: true, imageUrl: imageUrl)
        vc.delegates = self
        self.view.addSubview(vc)
        
    }
    
    @objc func  moreBtnTapped(sender : UIButton){
        
        let indexPath = self.convertPointToIndexPath(sender: sender)
        
        print(indexPath.row)
        if standard.value(forKey: "userid") as? Int == arrMatchMaker[indexPath.row].userId {

            let array = ["Edit Post","Delete Post","Turn off notifications for this post"]
            let  imgArray = ["imgEditMachMaker","imgDelete","imgNotification"]
            let vc = Menu.init(frame: self.view.frame, array: array,imgArray:imgArray,selectedIndex : indexPath.row,delegateStr : "own")
            vc.delegates = self
            self.view.addSubview(vc)
        }
        else{
          
            let array = ["Hide Post","Turn off notifications for this post","Report this post"]
            
            let  imgArray = ["imgHide","imgNotification","imgRepostPost"]
            let vc = Menu.init(frame: self.view.frame, array: array,imgArray:imgArray,selectedIndex : indexPath.row,delegateStr : "other")
            vc.delegates = self
            self.view.addSubview(vc)
            
            
            
        }
        
    }
    
    @objc func btnlikeTapped(sender : UIButton){
        
        let indexPath = self.convertPointToIndexPath(sender: sender)
        
        print(indexPath.row)
        
        let cell = self.tableView.cellForRow(at: indexPath) as? MatchmakerTableViewCell
        
        if sender.imageView?.image == #imageLiteral(resourceName: "imgUnlike") {
            sender.setImage(#imageLiteral(resourceName: "imgLike"), for: .normal)
            cell?.lblLike.text = "\( (arrMatchMaker[indexPath.row].likes ?? 0) + 1) LIKES"
            sender.transform = CGAffineTransform(scaleX: 0.1, y: 0.1)
            
            UIView.animate(withDuration: 1.0,
                           delay: 0,
                           usingSpringWithDamping: 0.2,
                           initialSpringVelocity: 5.0,
                           options: .curveEaseOut,
                           animations: {
                            
                            sender.transform = .identity
            },
                           completion: {
                            action in
                           
                            
            })
            let parameters : Parameters = ["user_id":"\(standard.value(forKey: "userid")!)", "post_id" :arrMatchMaker[indexPath.row].postId ?? 0, "post_user_id":arrMatchMaker[indexPath.row].userId ?? 0,"action" : "0"]
            
            self.likePostActionApiCall(parameters: parameters, indexPath: indexPath)
            
            
        }else{
              cell?.lblLike.text = "\( (arrMatchMaker[indexPath.row].likes ?? 0) - 1) LIKES"
            sender.setImage(#imageLiteral(resourceName: "imgUnlike"), for: .normal)
            let parameters : Parameters = ["user_id":"\(standard.value(forKey: "userid")!)", "post_id" :arrMatchMaker[indexPath.row].postId ?? 0, "post_user_id":arrMatchMaker[indexPath.row].userId ?? 0,"action" : "1"]
            
            self.likePostActionApiCall(parameters: parameters, indexPath: indexPath)
        }
       
    }
    
    
    //MARK:- Post Action Perform Function
    
    @objc func sharePostTapped(sender: UIButton){
        
        let indexPath = self.convertPointToIndexPath(sender: sender)
        
        print(indexPath.row)
        
        let array = ["Share Now (Public)","Send through Rendezwoo Messages","Copy link"]
        let  imgArray = ["imgShareNow","imgRendezwooMsg","imgCopy"]
        let vc = Menu.init(frame: self.view.frame, array: array,imgArray:imgArray,selectedIndex : indexPath.row,delegateStr : "share")
        vc.delegates = self
        self.view.addSubview(vc)
        
        
    }
    
    @objc func handleTap(_ sender: UITapGestureRecognizer) {
        let point = sender.view?.convert(CGPoint.zero, to: self.tableView)
        
        guard let indexPath = self.tableView.indexPathForRow(at: point!) else {
            return
        }
        
        print(indexPath.row)
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "PostCommentViewController") as! PostCommentViewController
        
        
         vc.postInfo = self.arrMatchMaker[indexPath.row]
        self.navigationController?.pushViewController(vc, animated: true)
        
        
    }
    
    func popViewToMainMeassge(){
        let viewControllers: [UIViewController] = self.navigationController!.viewControllers
        for aViewController in viewControllers {
            if aViewController is Tabbar {
                let vc  = aViewController as! Tabbar
                
                let getVC = vc.viewControllers![4] as! MessageViewController
                
                vc.setSelectIndex(from: 1, to: 4)
                
                self.navigationController!.popToViewController(aViewController, animated: false)
            }
        }
    }
    
    @objc func commentPostTapped(sender: UIButton){
        
         let indexPath = self.convertPointToIndexPath(sender: sender)
        
        print(indexPath.row)
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "PostCommentViewController") as! PostCommentViewController
         vc.postInfo = self.arrMatchMaker[indexPath.row]
        self.navigationController?.pushViewController(vc, animated: true)
        
        
        
    }
    
    func convertPointToIndexPath(sender : UIButton)-> IndexPath{
        
        let point = sender.convert(CGPoint.zero, to: self.tableView)
        
        guard let indexPath = self.tableView.indexPathForRow(at: point) else {
            return IndexPath.init(row: 0, section: 0)
        }
        
       return indexPath
        
    }
    
    
    //MARK: - UITABLEVIEW DATA SOURCE
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return  self.arrMatchMaker.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! MatchmakerTableViewCell
        cell.btnMore.addTarget(self, action: #selector(self.moreBtnTapped(sender:)), for: .touchUpInside)
        
        if arrMatchMaker[indexPath.row].repostUserName != "" {
            
            if arrMatchMaker[indexPath.row].userId == arrMatchMaker[indexPath.row].repostUserId {
                  cell.lblUserName.text =  "\(arrMatchMaker[indexPath.row].name ?? "") shared a post."
            }
            else{
                  cell.lblUserName.text =  "\(arrMatchMaker[indexPath.row].name ?? "") shared \(arrMatchMaker[indexPath.row].repostUserName ?? "")'s post."
            }
            
           
        }
        else{
               cell.lblUserName.text = arrMatchMaker[indexPath.row].name
        }
       
        cell.lblDescription.text = arrMatchMaker[indexPath.row].caption?.base64Decoded()
        
     
        let date = NSDate(timeIntervalSince1970:  TimeInterval(arrMatchMaker[indexPath.row].timestamp ?? 0))
        
         cell.lblDate.text = date.getDayAndTime()
        
        
        let imageUserURL =  arrMatchMaker[indexPath.row].userImage ?? ""
        cell.imgUserPic.sd_setImage(with: URL(string: imageUserURL), placeholderImage: #imageLiteral(resourceName: "imgProfile"), options: [], completed: nil)
        
           let imageURL =  arrMatchMaker[indexPath.row].imageUrl ?? ""
        
        if  imageURL == "" {
            cell.imageHeight.constant = 0
            
        }
        else{
         
             cell.imgPic.sd_setImage(with: URL(string:imageURL), placeholderImage: #imageLiteral(resourceName: "imgUpload"), options: [.refreshCached]) { (img, error, type, url) in
                
                let widthInPixels = img?.size.width ?? 0
                let heightInPixels = img?.size.height ?? 0
                
                let height = self.view.frame.size.width * (heightInPixels/widthInPixels)
                
                cell.imageHeight.constant =  height
                
            }
            
        }
        
        cell.lblLike.text = "\( arrMatchMaker[indexPath.row].likes ?? 0) LIKES"
        cell.lblComments.text = "\( arrMatchMaker[indexPath.row].comments ?? 0) COMMENTS"
        cell.lblShare.text = "\( arrMatchMaker[indexPath.row].share ?? 0) SHARES"
        
        
        if arrMatchMaker[indexPath.row].userLike == "TRUE" {
            cell.btnLike.setImage(#imageLiteral(resourceName: "imgLike"), for: .normal)
        }
        else{
            cell.btnLike.setImage(#imageLiteral(resourceName: "imgUnlike"), for: .normal)
        }
            
        cell.btnLike.addTarget(self, action: #selector(self.btnlikeTapped(sender:)), for: .touchUpInside)
        
        cell.btnShare.addTarget(self, action: #selector(self.sharePostTapped(sender:)), for: .touchUpInside)
        
         cell.btnComment.addTarget(self, action: #selector(self.commentPostTapped(sender:)), for: .touchUpInside)
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(_:)))

        cell.lblComments.addGestureRecognizer(tap)
      
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
         return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        if indexPath.row ==  self.arrMatchMaker.count - 1 && isResultsAviablleToLoad == true{
            currentPage = currentPage + 1
           self.fetchMatchMakerApiCall(page: currentPage)
        }
    }
    
    //MARK:- UITEXTFIELD DELEGATES
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        textField.resignFirstResponder()
       
        
        
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        textField.resignFirstResponder()
    }
    
    
    //MARK:- Fetch MatcMaker API Call
    func fetchMatchMakerApiCall(page : Int){
      let userData = UserData.init(json: JSON(standard.value(forKey: "userData") as? [String : Any] ?? [String : Any]()))
        let parameter : Parameters = ["user_id":"\(standard.value(forKey: "userid")!)",
            "page": page,"limit":"10","user_type" : userData.usertype ?? 0]

          print(parameter)
        AccountApiInterface.sharedInstance.postRequestWithParam(fromViewController: self, actionName: "matchmaker_wall", dictParam: parameter as [String : AnyObject], isShowIndicator: (page == 1) ? true : false , success: { (json) in
              self.refreshControl.endRefreshing()
                 print(json)
              EmptyDataSet.sharedInstance.delegates = self
            EmptyDataSet.sharedInstance.showEmptyData(title: "No post found.", image: #imageLiteral(resourceName: "search"), tableView: self.tableView)
         
            if page == 1 {
                self.arrMatchMaker.removeAll()
            }

            guard(json["data"].arrayValue.count) > 0 else {
          
                self.isResultsAviablleToLoad = false
                DispatchQueue.main.async {
                    self.tableView.reloadData()
                }

                return
            }
          print(json["data"].arrayValue.count)
            for i in 0..<json["data"].arrayValue.count{
                
                   let arrTemp = RZMatchMaker.init(json: json["data"][i])
                
                  self.arrMatchMaker.append(arrTemp)
            }
            self.arrMatchMaker   =    self.arrMatchMaker.sorted() {return ($0.timestamp ?? 0 ) > ($1.timestamp ?? 0 )}
              self.isResultsAviablleToLoad = true
            
            
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
        }) { (error) in
             self.refreshControl.endRefreshing()
             self.isResultsAviablleToLoad = false
            self.tableView.reloadData()
            self.showAlert(messageStr: error)
        }
        
    }
    
    //MARK:- Own Post Action
    func ownPostActionApiCall(parameters: Parameters , isDelete : Bool , index : Int){
        AccountApiInterface.sharedInstance.postRequestWithParam(fromViewController: self, actionName: "own_post_action", dictParam: parameters as [String : AnyObject], isShowIndicator: true, success: { (json) in
            print(json)
            
            
            if isDelete == true {
                self.arrMatchMaker.remove(at: index)
            }
           
            self.showAlert(messageStr: json["msg"].stringValue)
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
            
        }) { (error) in
            self.showAlert(messageStr: error)
        }
        
    }
    
    //MARK: - Create Post Action
    func createPost( text : String,view : CreatePost){
        
        let param  : Parameters = ["user_id":"\(standard.value(forKey: "userid")!)","text":text]
        print(param)
        AccountApiInterface.sharedInstance.postRequestWithParam(fromViewController: self, actionName: "create_post", dictParam: param as [String : AnyObject], isShowIndicator: true, success: { (json) in
            print(json)
            view.removeFromSuperview()
           
            self.arrMatchMaker.append(RZMatchMaker.init(json: json["data"]))
             self.arrMatchMaker   =    self.arrMatchMaker.sorted() {return ($0.timestamp ?? 0 ) > ($1.timestamp ?? 0 )}
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
            
        }) { (error) in
            self.showAlert(messageStr: error)
        }
    }

    
    
    
    //MARK: - Edit Own Post Action
    func editpostActionApiCall( index : Int , text : String,view : CreatePost){
        
        let param  : Parameters = ["user_id":"\(standard.value(forKey: "userid") ?? 0)" ,"post_id":arrMatchMaker[index].postId ?? 0,"caption":text]
        print(param)
        AccountApiInterface.sharedInstance.postRequestWithParam(fromViewController: self, actionName: "edit_own_post", dictParam: param as [String : AnyObject], isShowIndicator: true, success: { (json) in
            print(json)
            view.removeFromSuperview()
            self.arrMatchMaker[index].caption = text
            DispatchQueue.main.async {
                self.tableView.reloadRows(at: [IndexPath.init(row: index, section: 0)], with: .automatic)
            }
            
        }) { (error) in
            self.showAlert(messageStr: error)
        }
    }

    
    
    //MARK:- hide post from their wall, turn off notifications, report a post matchmaker
    func otherPostActionApiCall(parameters: Parameters , index : Int , isHide : Bool){
        AccountApiInterface.sharedInstance.postRequestWithParam(fromViewController: self, actionName: "post_action", dictParam: parameters as [String : AnyObject], isShowIndicator: true, success: { (json) in
            print(json)
            
            
            if isHide == true {
                
                self.arrMatchMaker.remove(at: index)
            }
            
            
            self.showAlert(messageStr: json["msg"].stringValue)
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
            
        }) { (error) in
            self.showAlert(messageStr: error)
        }
    }
    
    func likePostActionApiCall(parameters: Parameters ,indexPath : IndexPath){
        AccountApiInterface.sharedInstance.postRequestWithParam(fromViewController: self, actionName: "like_post", dictParam: parameters as [String : AnyObject], isShowIndicator: false, success: { (json) in
            print(json)
            
        
            self.arrMatchMaker[indexPath.row].userLike = ( self.arrMatchMaker[indexPath.row].userLike == "FALSE") ? "TRUE" : "FALSE"
            
            if self.arrMatchMaker[indexPath.row].userLike == "TRUE" {
                    self.arrMatchMaker[indexPath.row].likes =     (self.arrMatchMaker[indexPath.row].likes ?? 0) + 1
                
            }
            else{
                 self.arrMatchMaker[indexPath.row].likes =     (self.arrMatchMaker[indexPath.row].likes  ?? 0) - 1
            }
            
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
            
        }) { (error) in
            print(error)
            //self.showAlert(messageStr: error)
        }
        
    }
    
    //MARK: - Share Post Api Call
    func sharePostActionApiCall(parameters: Parameters,action : String){
        AccountApiInterface.sharedInstance.postRequestWithParam(fromViewController: self, actionName: action, dictParam: parameters as [String : AnyObject], isShowIndicator: false, success: { (json) in
            print(json)
            
            DispatchQueue.main.async {
                self.currentPage = 1
                self.isResultsAviablleToLoad = true
                self.fetchMatchMakerApiCall(page: self.currentPage)
            }
            
        }) { (error) in
            self.showAlert(messageStr: error)
        }
        
    }
    
    
    
}

extension MatchMakerViewController : menuButtonDelegates ,emptyDataSetDelegatesOnTab,editPostDelegates {
    
    func menuButtonAction(view:Menu,index : Int,selectedIndex : Int,delegateStr : String){
        print(index)
        
       if delegateStr == "own" {
        
        if index == 1 {
            let vc = CreatePost.init(frame: self.view.frame ,post : arrMatchMaker[selectedIndex] ,index : selectedIndex,isEditPost : true)
            vc.delegates = self
            self.view.addSubview(vc)
            
        }
        else{
          let array =   ["delete","turn_off"]
            let parameters:  Parameters = [ "user_id":"\(standard.value(forKey: "userid")!)","post_id":"\(arrMatchMaker[selectedIndex].postId ?? 0)", "action":array[index - 2]]
            
            print(parameters)
            self.ownPostActionApiCall(parameters: parameters, isDelete: (array[index - 2] == "delete"), index: selectedIndex)
        }
        
    
        }
       else if delegateStr == "other"{
        let parameters:  Parameters = [ "user_id":"\(standard.value(forKey: "userid")!)","post_id":"\(arrMatchMaker[selectedIndex].postId ?? 0)","post_user_id":"\(arrMatchMaker[selectedIndex].userId ?? 0)", "action":"\(index - 1)"]
        
        print(parameters)
        
        self.otherPostActionApiCall(parameters: parameters, index: selectedIndex, isHide: (index == 1))
        }
        
       else if delegateStr == "share"{
        
        if index == 1 {
            let parameters:  Parameters = [ "user_id":"\(standard.value(forKey: "userid")!)","post_id":"\(arrMatchMaker[selectedIndex].postId ?? 0)","post_user_id":"\(arrMatchMaker[selectedIndex].userId ?? 0)"]
            
            self.sharePostActionApiCall(parameters: parameters, action: "post_share")
        }
        else if index == 2 {
            self.popViewToMainMeassge()
        }
        
        
    }
   
    }
    
    func didTappedFunction(){
        
        currentPage = 1
        self.isResultsAviablleToLoad = true
        self.fetchMatchMakerApiCall(page: currentPage)
    }
    
    func editPostDelegatesFunction(view : CreatePost,index : Int , text : String){
        
        self.editpostActionApiCall(index: index, text: text, view: view)
        
        
        
    }
    func createPostDelegatesFunction(view : CreatePost,text : String){
        self.createPost(text: text, view: view)
    }
    
    func uploadPostDelegates(view : CreatePost,image_id : String ,caption : String ) {
              view.removeFromSuperview()
        let parameters:  Parameters = [ "user_id":"\(standard.value(forKey: "userid")!)","image_id":image_id,"caption":caption]
        
        self.sharePostActionApiCall(parameters: parameters, action: "share_on_matchmaker_wall")
        
    }

}


