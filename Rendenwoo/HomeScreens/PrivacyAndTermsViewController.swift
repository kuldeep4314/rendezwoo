//
//  PrivacyAndTermsViewController.swift
//  Rendenwoo
//
//  Created by goyal on 05/03/18.
//  Copyright © 2018 ankita. All rights reserved.
//

import UIKit

class PrivacyAndTermsViewController: UIViewController ,UIWebViewDelegate{

    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var topViewHeight: NSLayoutConstraint!
    @IBOutlet weak var webView: UIWebView!
    var strTitle = String()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        if UIDevice.isIphoneX == true
        {
            topViewHeight.constant = 84
        }
        self.lblTitle.text = strTitle
        if strTitle == "Privacy Policy" {
            self.apiCallFunctions(action: "privacyPolicy")
        }
        else{
              self.apiCallFunctions(action: "terms_of_service")
        }
      
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func btnBackTapped(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    

    //MARK:- WebView Delegates Methods
    public func webViewDidStartLoad(_ webView: UIWebView){
       self.view.startIndicator()
    }
    
    
    public func webViewDidFinishLoad(_ webView: UIWebView){
     self.view.stopIndicator()
    }
    
    
    public func webView(_ webView: UIWebView, didFailLoadWithError error: Error){
       self.view.stopIndicator()
    }

   
    //MARK:- Api Call Functions-
    func apiCallFunctions(action : String) {
        
        AccountApiInterface.sharedInstance.getRequestWithParam(fromViewController: self, actionName: action, success: { (json) in
            print(json)
            
            DispatchQueue.main.async {
                self.webView.loadHTMLString(json["data"].stringValue, baseURL: nil)
                
            }
            
        }) { (errorString) in
            
            self.showAlert(messageStr: errorString)
        }
        
    }
    
    
    
}
