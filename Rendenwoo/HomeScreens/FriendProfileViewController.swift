//
//  FriendProfileViewController.swift
//  Rendenwoo
//
//  Created by goyal on 01/03/18.
//  Copyright © 2018 ankita. All rights reserved.
//

import UIKit
import CoreLocation
class FriendProfileViewController: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    @IBOutlet weak var btnHeight: NSLayoutConstraint!
    var currentPage = 1
    var isResultsAviablleToLoad = true
      var imagesArr = [RZPhotos]()
    var imageId  = String()
      var userDetails = JSON(JSON.self)
    @IBOutlet weak var lblReligion: UILabel!
    @IBOutlet weak var lblEthnity: UILabel!
    @IBOutlet weak var lblHeight: UILabel!
    @IBOutlet weak var lblAboutMe: UILabel!
    @IBOutlet weak var lblRelationShipStatus: UILabel!
    @IBOutlet weak var lblGender: UILabel!
    @IBOutlet weak var lblDestination: UILabel!
    @IBOutlet weak var lblLocation: UILabel!
    @IBOutlet weak var lblName: UILabel!
    
    @IBOutlet weak var btnGetIntroduced: UIButton!
    @IBOutlet weak var lblInstaName: UILabel!
    @IBOutlet weak var btnInstaName: UIButton!
    @IBOutlet weak var topSpace: NSLayoutConstraint!
    var selectedIndex = 0
 @IBOutlet weak var topView: UIView!
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var btnFlag: UIButton!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var imgView: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()
         self.topView.addshodowToView()
        self.bottomView.addshodowToView()
        self.view.backgroundColor = appColor
        
        if UIDevice.isIphoneX == true {
            topSpace.constant = 30
        }
           self.imgView.image = #imageLiteral(resourceName: "imgPlaceHolder")
        self.setProfileData()
        print(userDetails)
     
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        currentPage = 1
        
        
        self.fetchApiCall(page: currentPage)
        
    }
    
    @IBAction func btnBackTapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    func setProfileData (){
           let currentUserData = UserData.init(json: JSON(standard.value(forKey: "userData") as? [String : Any] ?? [String : Any]()))
        
       let friendData = UserData.init(json: userDetails["data"])
        
        self.lblInstaName.text = userDetails["data"]["insta_name"].stringValue
        self.lblName.text  = friendData.name
        self.lblGender.text = (friendData.gender == 0) ? "Male" : "Female"
        self.lblRelationShipStatus.text =  (friendData.usertype == 0) ? "Single" : "Taken"
        
        self.lblHeight.text =  friendData.height?.base64Decoded()
        
        self.lblReligion.text = friendData.religion
        
        self.lblEthnity.text = friendData.ethnicity
        
        self.lblAboutMe.text = friendData.aboutme?.base64Decoded()
    
        let coordinate₀ = CLLocation(latitude: JSON(currentUserData.latitude ?? "0").doubleValue, longitude: JSON(currentUserData.longitude ?? "0").doubleValue)
        let coordinate₁ = CLLocation(latitude: JSON(friendData.latitude ?? "0").doubleValue, longitude: JSON(friendData.longitude ?? "0").doubleValue)

        let distanceInMeters = coordinate₀.distance(from: coordinate₁) / 1000

            self.lblLocation.text = String(format:"%.1f", distanceInMeters) + "km away"
        guard let value1 = friendData.occupation, let value2 = friendData.currentWork,  friendData.occupation != "" , friendData.currentWork != ""
            else
        {
            self.lblDestination.attributedText = NSMutableAttributedString.init(string: "")
            print(friendData.occupation ?? "")
            print(friendData.currentWork ?? "")
            return
        }
        
        let attrs1 =  [NSAttributedStringKey.foregroundColor : DarkGray]
        
        let myMutableString = NSMutableAttributedString(
            string: "\(value1) at \(value2)",
            attributes: attrs1)
        
        myMutableString.addAttributes([NSAttributedStringKey.foregroundColor : UIColor.black], range: NSRange(
            location:"\(value1) at ".count,
            length:value2.count))
        self.lblDestination.attributedText = myMutableString
        
        
     
        
    }
    @IBAction func btnFlagTapped(_ sender: UIButton) {
        
      self.view.viewWithTag(1000)?.removeFromSuperview()
        let friendData = UserData.init(json: userDetails["data"])
        let flag = FlagView.init(frame: CGRect.init(x: 0, y: self.view.frame.height - 160, width:self.view.frame.width, height: 160),blockName:  "Block " + (friendData.name ?? ""))
           flag.tag = 1000
        flag.delegaes = self
        self.view.addSubview(flag)
        
    }
    @IBAction func BtnGetIntroducedTapped(_ sender: UIButton) {
        let friendData = UserData.init(json: userDetails["data"])
           let vc  = Introduce.init(frame: self.view.frame,  strTile:"You need to be introduced to \(friendData.name ?? "") before you can send him a message directly.",delegatesStr:"get")
            vc.delegates  = self
            self.view.addSubview(vc)
       
    }
    
    
    func showAlert(userName : String){
        
        let vc = AlertWithOneButton.init(frame: self.view.frame, title: "We have sent an Intro message to \(userName). We will let you know as soon as \(userName) likes you back.", doneTitle: "Keep Exploring",bgColor : UIColor.clear)
        vc.delegates = self
        self.view.addSubview(vc)
        
    }
    
   
    //MARK:- CollectionView Datasource
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return   self.imagesArr.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath)
        
        let vc = cell.viewWithTag(999)
        if self.selectedIndex == indexPath.row {
            vc?.backgroundColor = UIColor.white
            
        }
        else{
            vc?.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        }
            return cell
        }
    
    //MARK:- CollectionView Delegates
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        selectedIndex = indexPath.row
        
        
        let imageURL = imagesArr[indexPath.row].imageUrl ?? ""
        imgView.sd_setImage(with: URL(string: imageURL), placeholderImage: #imageLiteral(resourceName: "imgPlaceHolder"), options: [], completed: nil)
           collectionView.reloadData()
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
            return CGSize(width:CGFloat(Int(collectionView.frame.size.width) / self.imagesArr.count), height: collectionView.frame.size.height )
        
        
        
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if indexPath.row ==  self.imagesArr.count - 1 && isResultsAviablleToLoad == true{
            currentPage = currentPage + 1
            self.fetchApiCall(page: currentPage)
        }
    }

    
    func listofCommanFriendsApiCall(parameters: Parameters,array:@escaping ([JSON]) -> Void) {
        AccountApiInterface.sharedInstance.postRequestWithParam(fromViewController: self, actionName: "common_friend_list", dictParam: parameters as [String : AnyObject], isShowIndicator: true, success: { (json) in
            print(json)
    
            array(json["data"].arrayValue)
            
            
        }) { (error) in
           
            DispatchQueue.main.async {
                array([JSON]())
               
            }
        }
        
    }
    
    
    
    //MARK:- Flag Report API Call
    func flagReportApiCall(parameters: Parameters){
        AccountApiInterface.sharedInstance.postRequestWithParam(fromViewController: self, actionName: "reportUser", dictParam: parameters as [String : AnyObject], isShowIndicator: true, success: { (json) in
            
            print(json)
        }) { (error) in
            self.showAlert(messageStr: error)
        }
        
    }
    
    
    //MARK:- Fetch Photo Api Function
    func fetchApiCall(page : Int){
         let friendData = UserData.init(json: userDetails["data"])
        let parameter : Parameters = ["user_id": friendData.internalIdentifier ?? 0 ,"page" : page]
        print(parameter)
        
        AccountApiInterface.sharedInstance.postRequestWithParam(fromViewController: self, actionName: "fetch_photos", dictParam: parameter as [String : AnyObject], isShowIndicator: (page == 1) ? true : false, success: { (json) in
            print(json)
            
            if page == 1 {
                self.imagesArr.removeAll()
            }
            
            guard(json["image_array"].arrayValue.count) > 0 else {
                
                self.isResultsAviablleToLoad = false
                DispatchQueue.main.async {
                    self.collectionView.reloadData()
                }
                
                return
            }
            self.isResultsAviablleToLoad = true
            for i in 0..<json["image_array"].arrayValue.count{
                let arrTemp = RZPhotos.init(json: json["image_array"][i])
                self.imagesArr .append(arrTemp)
            }
            let imageURL =  self.imagesArr[self.selectedIndex].imageUrl ?? ""
            self.imgView.sd_setImage(with: URL(string: imageURL), placeholderImage: #imageLiteral(resourceName: "imgPlaceHolder"), options: [], completed: nil)
            
            self.collectionView.reloadData()
            
            
        }) { (error) in
            self.isResultsAviablleToLoad = false
            self.showAlert(messageStr: error)
        }
        
    }
    //MARK:- Introduce ApiCall  Function
    
    func introduceApiCall(parameters: Parameters){
        AccountApiInterface.sharedInstance.postRequestWithParam(fromViewController: self, actionName: "introduce_friend", dictParam: parameters as [String : AnyObject], isShowIndicator: true, success: { (json) in
            print(json)
            DispatchQueue.main.async {
                let friendData = UserData.init(json: self.userDetails["data"])
                self.showAlert(userName: friendData.name ?? "")
            }
            
        }) { (error) in
            self.showAlert(messageStr: error)
        }
        
    }
    
    func showIntroAlert(name:String,id : String){
        let friendData = UserData.init(json: userDetails["data"])
        
        let vc = AlertWithTextView.init(frame:self.view.frame
            , title: "Hey \(name)! Could you introduce me to \(friendData.name ?? "")?", cancelTitle: "Edit", doneTitle: "Send",commanUserId: id,introduceToId: String(friendData.internalIdentifier ?? 0),relationShipStatus : "\(friendData.usertype ?? 0)")
        vc.delegates = self
        self.view.addSubview(vc)
    
    }
}
extension FriendProfileViewController : FlagViewDelegates,setFriendsDelegates,okButtonDelegates,friendwithMessageDelegates,alertButtonDelegates{
    
    func  friendwithMessageFunction(text : String,commanUserid : String, introduceToId : String,relationShipStatus : String){
        
                let param = ["image_id":imageId, "my_id":"\(standard.value(forKey: "userid")!)", "introduce_to_id" : introduceToId,"relationship_status":relationShipStatus,"common_user_id" : commanUserid,"text":text]
        
                self.introduceApiCall(parameters: param)
        
    }
    
    func flagViewDelegatesFunction(view : FlagView,index : Int){
        
        view.removeFromSuperview()
         let friendData = UserData.init(json: userDetails["data"])
        
        let paras : Parameters = [ "user_id":"\(standard.value(forKey: "userid")!)",
            
            "another_user_id" : friendData.internalIdentifier ?? 0,
            
            "action": (index == 1) ? 1 : 0
            
            ]
        
        self.flagReportApiCall(parameters: paras)
    }
    
    
    
    
    func  setFriendsValues(friend : JSON){

         self.showIntroAlert(name: friend["name"].stringValue, id: friend["id"].stringValue)


    }
    func okTappedfunction(){
        self.navigationController?.popToRootViewController(animated: true)
        
    }
    func alertButtonAction(index : Int, delegatesStr:String){
        
        if index == 1 && delegatesStr == "get"{
            let friendData = UserData.init(json: userDetails["data"])
            let param : Parameters = ["user_id":"\(standard.value(forKey: "userid")!)","another_user_id" : friendData.internalIdentifier ?? 0]
            
            
            self.listofCommanFriendsApiCall(parameters: param) { (array) in
                
                print(array)
                
                if array.count > 0 {
                    DispatchQueue.main.async {
                        let vc = OptionTableFriend.init(frame: self.view.frame, dataArr:array , tableframe: CGRect.init(x: 10, y: Int((self.view.frame.size.height) / 2)  - Int(array.count * 25) - 50 , width: Int((self.view.frame.size.width) - 20), height:  Int(array.count * 50) + 100))
                        vc.delegates = self
                        self.view.addSubview(vc)
                    }
                    
                    
                }
                
            }
            
        }
    }
    
}
