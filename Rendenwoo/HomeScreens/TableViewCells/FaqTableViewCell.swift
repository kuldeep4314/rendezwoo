//
//  FaqTableViewCell.swift
//  Rendenwoo
//
//  Created by goyal on 28/02/18.
//  Copyright © 2018 ankita. All rights reserved.
//

import UIKit

class FaqTableViewCell: UITableViewCell {

    @IBOutlet weak var lblQuestionTitle: UILabel!
    @IBOutlet weak var lblAns: UILabel!
    @IBOutlet weak var lblQuestion: UILabel!
    @IBOutlet weak var questionBgView: UIView!
    @IBOutlet weak var ansBgView: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
