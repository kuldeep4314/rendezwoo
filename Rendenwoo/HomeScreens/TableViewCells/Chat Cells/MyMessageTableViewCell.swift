//
//  MyMessageTableViewCell.swift
//  chatapp
//
//  Created by Codeyeti iOS on 08/11/17.
//  Copyright © 2017 Relinns. All rights reserved.
//

import UIKit

class MyMessageTableViewCell: UITableViewCell {

    
    //MARK: OUTLETS
    
    @IBOutlet weak var userImage: UIImageView!
    
    @IBOutlet weak var userName: UILabel!
    
    @IBOutlet weak var userMessage: UILabel!
    
    @IBOutlet weak var userTime: UILabel!
    
    @IBOutlet weak var statusImage: UIImageView!
    
    @IBOutlet weak var messageArrow: UIView!
    
    @IBOutlet weak var messageView: UIView!
    
    @IBOutlet weak var btnName: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.messageView.layer.cornerRadius = 8
        self.messageView.circularShadow()
    }
    
    
    override func prepareForReuse() {
        
//        self.messageView.layer.sublayers?.forEach { $0.removeFromSuperlayer()}
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
