//
//  NotificationTableViewCell.swift
//  Rendenwoo
//
//  Created by goyal on 10/04/18.
//  Copyright © 2018 ankita. All rights reserved.
//

import UIKit

class NotificationTableViewCell: UITableViewCell {

    @IBOutlet weak var lblNotificationMsg: UILabel!
    @IBOutlet weak var imgUser: UIImageView!
   
    @IBOutlet weak var lblHeader: UILabel!
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var imgNotType: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
