//
//  SettingsTableViewCell.swift
//  Rendenwoo
//
//  Created by goyal on 01/03/18.
//  Copyright © 2018 ankita. All rights reserved.
//

import UIKit

class SettingsTableViewCell: UITableViewCell {

    @IBOutlet weak var btnDeactiveAccount: UIButton!
    @IBOutlet weak var imgDivider3: UIImageView!
    @IBOutlet weak var imgDivider2: UIImageView!
    @IBOutlet weak var imgDivider1: UIImageView!
    @IBOutlet weak var btnArrow: UIButton!
    @IBOutlet weak var btnFirst: UIButton!
    @IBOutlet weak var btnSecond: UIButton!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblHeader: UILabel!
    @IBOutlet weak var lblNotificationTitle: UILabel!
    @IBOutlet weak var btnNotificationState: UIButton!
    @IBOutlet weak var lblInfo: UILabel!
    @IBOutlet weak var lblTitleInfo: UILabel!
    
    @IBOutlet weak var btnLogOut: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
