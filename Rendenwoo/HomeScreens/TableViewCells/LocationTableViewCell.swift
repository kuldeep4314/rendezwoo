//
//  LocationTableViewCell.swift
//  Rendenwoo
//
//  Created by goyal on 01/03/18.
//  Copyright © 2018 ankita. All rights reserved.
//

import UIKit

class LocationTableViewCell: UITableViewCell {

    @IBOutlet weak var lblLocation: UILabel!
    @IBOutlet weak var lblHeader: UILabel!
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var btnCheck: UIButton!
    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var btnAddLocation: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
