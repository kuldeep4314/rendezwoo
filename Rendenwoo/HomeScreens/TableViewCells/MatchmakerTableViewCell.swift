//
//  MatchmakerTableViewCell.swift
//  Rendenwoo
//
//  Created by goyal on 01/03/18.
//  Copyright © 2018 ankita. All rights reserved.
//

import UIKit

class MatchmakerTableViewCell: UITableViewCell {

    @IBOutlet weak var imageHeight: NSLayoutConstraint!
    @IBOutlet weak var lblComments: UILabel!
    @IBOutlet weak var lblLike: UILabel!
    @IBOutlet weak var lblShare: UILabel!
    @IBOutlet weak var btnShare: UIButton!
    @IBOutlet weak var btnComment: UIButton!
    @IBOutlet weak var btnLike: UIButton!
    @IBOutlet weak var imgPic: UIImageView!
    @IBOutlet weak var btnMore: UIButton!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var imgUserPic: UIImageView!
    @IBOutlet weak var subView: UIView!
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var lblDescription: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        if mainView != nil {
            mainView.backgroundColor = moreLight
            
        }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
