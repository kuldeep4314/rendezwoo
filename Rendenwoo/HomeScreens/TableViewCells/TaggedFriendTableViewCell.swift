//
//  TaggedFriendTableViewCell.swift
//  Rendenwoo
//
//  Created by goyal on 05/03/18.
//  Copyright © 2018 ankita. All rights reserved.
//

import UIKit
import UIKit
@objc protocol  checkTagStatusDelegate {
    @objc optional func  checkTagStats(status : Bool,section : Int)
}
import ADMozaicCollectionViewLayout
class TaggedFriendTableViewCell: UITableViewCell ,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout ,ADMozaikLayoutDelegate,alertButtonDelegates{
    
    var currentPage = 1
    var isResultsAviablleToLoad = true
    var imagesArr = [TaggedImages]()
    var name = String()
     var friend_id = String()
     var section = Int()
    var delegates : checkTagStatusDelegate?
    @IBOutlet weak var collectionViewHeight: NSLayoutConstraint!
    @IBOutlet weak var btnTaken: UIButton!
    @IBOutlet weak var btnSingle: UIButton!
    @IBOutlet weak var btnPhoto: UIButton!
    @IBOutlet weak var btnTag: UIButton!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var adlayout: ADMozaikLayout!
     var tagValue = 0
    @IBOutlet weak var collectionView: UICollectionView!
    var selectedIndex = -1
   
    override func awakeFromNib() {
        super.awakeFromNib()
        
        if collectionView != nil {
            collectionView.dataSource = self
            collectionView.delegate = self
              adlayout.delegate = self
            
        }
        // Initialization code
    }
    
    
    func checkTagStatus (){
        let checkTagArray = self.imagesArr.filter({$0.isUserTagged == true})
        
        if checkTagArray.count == self.imagesArr.count {
            self.delegates?.checkTagStats!(status: true, section: self.section)
        }
        else{
            self.delegates?.checkTagStats!(status: false, section: self.section)
        }
    }

    @objc func btnUntagTapped(sender : UIButton){
        
        if sender.imageView?.image == #imageLiteral(resourceName: "imgUntagBlue") {
            selectedIndex = sender.tag
               self.imagesArr[selectedIndex].isUserTagged = false
            
             self.checkTagStatus()
            
            self.collectionView.reloadData()
            
            let vc = AlertWithOneTitle.init(frame: (appDelegates.window?.rootViewController?.view.frame)!, title: "Are you sure you want to un-tag \(name) from this picture?", cancelTitle: "Nope", doneTitle: "Yes, un-tag",delegatesStr:"untag")
            vc.delegates = self
            appDelegates.window?.rootViewController?.view.addSubview(vc)
   
        }
        else{
          
            selectedIndex = sender.tag
              self.imagesArr[selectedIndex].isUserTagged = true
             self.checkTagStatus()
            self.collectionView.reloadData()
            
            let vc = AlertWithOneTitle.init(frame: (appDelegates.window?.rootViewController?.view.frame)!, title: "Would you like to re-tag \(name) in this picture?", cancelTitle: "Cancel", doneTitle: "Yes",delegatesStr:"retag")
            vc.delegates = self
            appDelegates.window?.rootViewController?.view.addSubview(vc)
            
        }
        
        
        
    }
    
    @objc func deletePhotoTapped(sender: UIButton){
          selectedIndex = sender.tag - 1000
        
        let vc = AlertWithOneTitle.init(frame: (appDelegates.window?.rootViewController?.view.frame)!, title: "Are you sure you want to delete this picture? All its tags will be lost.", cancelTitle: "No", doneTitle: "Yes", delegatesStr: "delete")
        
         vc.delegates = self
        appDelegates.window?.rootViewController?.view.addSubview(vc)


    }
    
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    //MARK:- CollectionView Datasource
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        
        if self.isResultsAviablleToLoad == true && imagesArr.count > 0{
        return imagesArr.count + 1
        }
        else{
             return imagesArr.count
        }
       
       
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        guard !(indexPath.row == self.imagesArr.count && isResultsAviablleToLoad == true ) else {
           let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "loader", for: indexPath)
            
            let activity = cell.viewWithTag(22) as? UIActivityIndicatorView
            
            activity?.startAnimating()
            activity?.tintColor = appColor
            return cell
        }
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! PhotoCollectionViewCell
        
        cell.btnUntag.addTarget(self, action: #selector(self.btnUntagTapped(sender:)), for: .touchUpInside)
        cell.btnUntag.tag  = indexPath.row
       
        //1 Tag 2 untag // 0 back to original
        if tagValue == 1 {
             cell.btnUntag.setImage(#imageLiteral(resourceName: "imgUntagBlue"), for: .normal)
        }
        else if tagValue == 2 {
             cell.btnUntag.setImage(#imageLiteral(resourceName: "imgUntag1"), for: .normal)
               }
        else{
            if  imagesArr[indexPath.row].isUserTagged  == true {
                
                cell.btnUntag.setImage(#imageLiteral(resourceName: "imgUntagBlue"), for: .normal)
            }
            else{
                cell.btnUntag.setImage(#imageLiteral(resourceName: "imgUntag1"), for: .normal)
                
            }
        }
        
        

        let imageURL = imagesArr[indexPath.row].imageUrl ?? ""


          cell.imageView.sd_setImage(with: URL(string: imageURL), placeholderImage: #imageLiteral(resourceName: "imgPlaceHolder"), options: [], completed: nil)

        cell.btnRmoveAdd.tag = indexPath.row + 1000
        cell.btnRmoveAdd.addTarget(self, action: #selector(self.deletePhotoTapped(sender:)), for: .touchUpInside)
        
       
        
        return cell
        
    }
    //MARK:- CollectionView Delegates
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        
        
        return CGSize(width: collectionView.frame.size.width, height: collectionView.frame.size.height)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    //MARK: - ADMozaikLayoutDelegate
    
    func collectonView(_ collectionView: UICollectionView, mozaik layoyt: ADMozaikLayout, geometryInfoFor section: ADMozaikLayoutSection) -> ADMozaikLayoutSectionGeometryInfo {
        
        var rowHeight: CGFloat = 20.0
        
        rowHeight = (self.collectionView.frame.size.height / 13)
        
        
        
        let width = ADMozaikLayoutColumn(width: collectionView.frame.size.width / 3 - 3)
        let columns = [width, width,width]
        
        
        let geometryInfo = ADMozaikLayoutSectionGeometryInfo(rowHeight: rowHeight,
                                                             columns: columns,
                                                             minimumInteritemSpacing: 0,
                                                             minimumLineSpacing: 0,
                                                             sectionInset: UIEdgeInsets(top: 5, left: 5, bottom: 5, right: 5),
                                                             headerHeight: 0, footerHeight: 0)
        
        return geometryInfo
    }
    
    func collectionView(_ collectionView: UICollectionView, mozaik layout: ADMozaikLayout, mozaikSizeForItemAt indexPath: IndexPath) -> ADMozaikLayoutSize {
        
        guard !(indexPath.row == self.imagesArr.count && isResultsAviablleToLoad == true ) else {
            return ADMozaikLayoutSize(numberOfColumns: 3, numberOfRows: 1)
        }
        
            if indexPath.row == 0 {
                return ADMozaikLayoutSize(numberOfColumns: 2, numberOfRows: 8)
            }
            else  if indexPath.row == 1 {
                return ADMozaikLayoutSize(numberOfColumns: 1, numberOfRows: 4)
            }
            else  if indexPath.row == 2 {
                return ADMozaikLayoutSize(numberOfColumns: 1, numberOfRows: 4)
            }
            else{
                return ADMozaikLayoutSize(numberOfColumns: 1, numberOfRows: 5)
            }
        
    }
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if indexPath.row ==  self.imagesArr.count - 1 && isResultsAviablleToLoad == true{
            currentPage = currentPage + 1
            self.fetchApiCall(page: currentPage, friend_id: self.friend_id)
        }
    }
    //MARK:- Fetch Photo Api Function
    func fetchApiCall(page : Int, friend_id : String){
        let parameter : Parameters = ["user_id": "\(standard.value(forKey: "userid")!)","page" : page,"friend_id" : friend_id]
        print(parameter)
        
        AccountApiInterface.sharedInstance.postRequestWithParam(fromViewController: appDelegates.window?.rootViewController ?? UIViewController(), actionName: "tagged_friend_images", dictParam: parameter as [String : AnyObject], isShowIndicator:  false, success: { (json) in
            print(json)
            
            if page == 1 {
                self.imagesArr.removeAll()
            }
            
            guard(json["image_array"].arrayValue.count) > 0 else {
                
                self.isResultsAviablleToLoad = false
                DispatchQueue.main.async {
                    self.collectionView.reloadData()
                }
                
                return
            }
            
             self.isResultsAviablleToLoad = true
            for i in 0..<json["image_array"].arrayValue.count{
                let arrTemp = TaggedImages.init(json: json["image_array"][i])
                self.imagesArr .append(arrTemp)
            }
            
            self.collectionView.reloadData()
            
            
        }) { (error) in
            
            self.isResultsAviablleToLoad = false
            appDelegates.window?.rootViewController?.showAlert(messageStr: error)
        }
        
    }
    
    
    
    //MARK:- Delete Photo Api Function
    func deleteApiCall(parameters: Parameters,index :Int){
        print(parameters)
        AccountApiInterface.sharedInstance.postRequestWithParam(fromViewController: appDelegates.window?.rootViewController ?? UIViewController(), actionName: "delete_image", dictParam: parameters as [String : AnyObject], isShowIndicator: true, success: { (json) in
            print(json)
            
                self.imagesArr.remove(at: index)
       
            DispatchQueue.main.async {
                 self.checkTagStatus()
                
                self.collectionView.reloadData()
            }
            
        }) { (error) in
             appDelegates.window?.rootViewController?.showAlert(messageStr: error)
        }
        
    }
    //MARK:- Fetch Location API Call
    func untagRetagFromPicture(parameters: Parameters, index  : Int,url : String , value : Bool){
        AccountApiInterface.sharedInstance.postRequestWithParam(fromViewController: appDelegates.window?.rootViewController ?? UIViewController(), actionName: url, dictParam: parameters as [String : AnyObject], isShowIndicator: true, success: { (json) in
            
            print(json)
            self.imagesArr[index].isUserTagged = value
            
            
            DispatchQueue.main.async {
                
                self.checkTagStatus()
                
                
                self.collectionView.reloadData()
            }
        }) { (error) in
            appDelegates.window?.rootViewController?.showAlert(messageStr: error)
        }
        
    }
    
    
    //MARK:- Delegates Function
    
    func alertButtonAction(index : Int, delegatesStr:String){
        print(index)
        if delegatesStr == "delete" {
            if index == 2{
                let imgId =  imagesArr[selectedIndex].imageId ?? 0
                let  parameters : Parameters =   [ "user_id":standard.value(forKey: "userid")!,"image_id":imgId]

                self.deleteApiCall(parameters: parameters, index: selectedIndex)
            }
            
        }
        else if delegatesStr == "untag"{
            if index == 2 {
                
               let imgId =  imagesArr[selectedIndex].imageId ?? 0
                 let parametre  : Parameters = ["user_id":"\(standard.value(forKey: "userid")!)","image_id":imgId,"friend_id" : friend_id]

                self.untagRetagFromPicture(parameters: parametre, index: selectedIndex, url: "untag_friend_from_one", value: false)
                
            }
            else{
                self.imagesArr[selectedIndex].isUserTagged = true
                self.checkTagStatus()
                
            }
            
        }
        
        else if delegatesStr == "retag"{
            if index == 2 {
                
                let imgId =  imagesArr[selectedIndex].imageId ?? 0
                let parametre  : Parameters = ["user_id":"\(standard.value(forKey: "userid")!)","image_id":imgId,"friend_id" : friend_id]
                
                self.untagRetagFromPicture(parameters: parametre, index: selectedIndex, url: "retag_friend_in_one", value: true)

            }
            else{
                self.imagesArr[selectedIndex].isUserTagged = false
                    self.checkTagStatus()
            }
            
        }
        
        self.collectionView.reloadData()
    }
    
    //MARK: TaggedImage Function
    func  taggedImagesfunction (friend_id : String,name : String,section : Int){
        self.friend_id = friend_id
         self.name = name
        self.section = section
        currentPage = 1
        self.fetchApiCall(page: currentPage, friend_id: friend_id)
        
    }
    
    
    //MARK: Untag From All Image
    
    func unTagRetagFromAllFunction(value : Int) {
        tagValue = value
        self.collectionView.reloadData()
    }
    
    
}
