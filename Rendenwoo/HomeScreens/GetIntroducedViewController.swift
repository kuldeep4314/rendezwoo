//
//  GetIntroducedViewController.swift
//  Rendenwoo
//
//  Created by goyal on 20/03/18.
//  Copyright © 2018 ankita. All rights reserved.
//

import UIKit
import CoreImage


class GetIntroducedViewController: UIViewController ,UIScrollViewDelegate {
 
    @IBOutlet weak var topCons: NSLayoutConstraint!
    
    @IBOutlet weak var btnLike: UIButton!
    @IBOutlet weak var btnTag: UIButton!
    @IBOutlet weak var topSpace: NSLayoutConstraint!
    @IBOutlet weak var scrollView: UIScrollView!
      var  personTag = PersonTag()
    @IBOutlet weak var lblIntro: UILabel!
    @IBOutlet weak var lblLikes: UILabel!
    @IBOutlet weak var btnHeight: NSLayoutConstraint!
    @IBOutlet weak var imgView: UIImageView!
  
    @IBOutlet weak var height: NSLayoutConstraint!
    var selectedPerson  = Person()
    
    var   exploreWallInfo : ExploreModel?
    @IBOutlet weak var btnGetIntroduce: UIButton!
    @IBOutlet weak var botttomView: UIView!
    
    var isHideGetIntroButton :Bool = false {
        didSet
        {
            btnHeight.constant = (isHideGetIntroButton == true) ? 0 : 50
            btnGetIntroduce.isHidden = isHideGetIntroButton
        }
        
    }

    public func viewForZooming(in scrollView: UIScrollView) -> UIView?{
        return imgView
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        scrollView.minimumZoomScale = 1.0;
        scrollView.maximumZoomScale = 5.0;
        scrollView.delegate = self
        self.view.backgroundColor = appColor
        if UIDevice.isIphoneX == true {
            topSpace.constant = 84
        }
          isHideGetIntroButton = true
        // Do any additional setup after loading the view.
        self.setUpView()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if  self.height.constant > scrollView.frame.size.height{
            topCons.constant = 0
        }
    }
    
    
    func setUpView() {
        
        personTag.imageId = String(exploreWallInfo?.imageId ?? 0)
        personTag.imageUrl = exploreWallInfo?.imageUrl ?? ""


        for i in 0..<(exploreWallInfo?.tagInfo?.count ?? 0) {

            let person = Person()

            person.personId = String(exploreWallInfo?.tagInfo?[i].userId ?? 0)
            person.name = exploreWallInfo?.tagInfo?[i].name ?? ""
            person.xPos =  CGFloat(JSON( exploreWallInfo?.tagInfo?[i].xpos ?? "0").floatValue)
            person.yPos = CGFloat(JSON( exploreWallInfo?.tagInfo?[i].ypos ?? "0").floatValue)
            person.status = String(exploreWallInfo?.tagInfo?[i].status ?? 0)
            person.commanConection = exploreWallInfo?.tagInfo?[i].commonConnection ?? false
             person.isFriend = exploreWallInfo?.tagInfo?[i].friend ?? false
              person.isLike = exploreWallInfo?.tagInfo?[i].isLiked ?? false
              person.numberofLikes = exploreWallInfo?.tagInfo?[i].likes ?? 0
            personTag.tagPerson.append(person)
          

        }

        let imageURL = exploreWallInfo?.imageUrl ?? ""
      imgView.sd_setImage(with: URL(string:imageURL), placeholderImage: #imageLiteral(resourceName: "imgUpload"), options: [.refreshCached]) { (img, error, type, url) in
            
            let widthInPixels = img?.size.width ?? 0
            let heightInPixels = img?.size.height ?? 0
            
            let height = self.view.frame.size.width * (heightInPixels/widthInPixels)
            
            
            self.height.constant =  height
            
            
            
        }

        self.setLikeAndCommentsFunction(total: true, id: 0)
    }
    
    func setLikeAndCommentsFunction(total : Bool, id : Int) {
        
        if total == true {
           
            lblIntro.text = String(exploreWallInfo?.totalIntros ?? 0)
            lblLikes.text = String(exploreWallInfo?.totalLikes ?? 0)
            
        }
        else{
        
            let likes =   exploreWallInfo?.tagInfo?.filter({$0.userId == id}).map({$0.likes})[0]
            let intro = exploreWallInfo?.tagInfo?.filter({$0.userId == id}).map({$0.intros})[0]
           lblLikes.text =   String(likes ?? 0)
           lblIntro.text =    String(intro ?? 0)
        }
    
    }
    @IBAction func btnLikeAction(_ sender: UIButton) {
        
         if isHideGetIntroButton == false {
    
            let param  : Parameters = ["liked_by":"\(standard.value(forKey: "userid")!)", "liked_user_id" : selectedPerson.personId,"image_id": personTag.imageId,"type" : (self.selectedPerson.isLike == true) ? 1: 0]
        
            self.likePersonApiCall(parameters: param, sender: sender, status: self.selectedPerson.isLike, id: Int(selectedPerson.personId) ?? 0 )
        }
    }
    
    
    @IBAction func btnTagTapped(_ sender: UIButton) {
    
        if   sender.imageView?.image == #imageLiteral(resourceName: "imgTagged1") {
        
            sender.setImage(#imageLiteral(resourceName: "imgTagged2"), for: .normal)
             self.setLikeAndCommentsFunction(total: true, id: 0)
            self.setTagInImages(img: self.imgView, p: personTag.tagPerson, selectedFriendId: "")
           
           
           }
        else{
            self.setLikeAndCommentsFunction(total: true, id: 0)
            sender.setImage(#imageLiteral(resourceName: "imgTagged1"), for: .normal)
           isHideGetIntroButton = true
            for i in 10000..<10030{
                let vc =  self.imgView.viewWithTag(i) as? TagView
                vc?.removeFromSuperview()
            }
            }
        
    }
    
    
    @IBAction func btnBackTapped(_ sender: Any) {
      self.navigationController?.popViewController(animated: true)
    }

    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    func setTagInImages (img : UIImageView ,p : [Person] , selectedFriendId : String){
        
        print(p.count)
      
        
        for i in 0..<p.count{
            
            let x1 =  (p[i].xPos * img.frame.size.width ) / 100
            let y1 =  (p[i].yPos * img.frame.size.height ) / 100
            
            
            if p[i].name != "" {
                let faceBox = TagView.init(frame: CGRect.init(x: x1, y: y1 , width: 90, height: 100), name: (p[i].name + ", " + ((p[i].status == "0") ? "Single" : "Taken")),color: (p[i].personId == selectedFriendId ) ? appColor :red,ishowCrossbtn : true,hintText : "\(p[i].imageIndex),\(i)")
                faceBox.delegatesTap = self
                
                faceBox.tag  = 10000 + i
                faceBox.nameTag = 2000 + i
                faceBox.topTag = 1000 + i
                
                img.addSubview(faceBox)
            }
            
        }
        
    }

    @IBAction func btnMessageTapped(_ sender: Any) {
        if isHideGetIntroButton == false {
            
            if selectedPerson.commanConection == true || selectedPerson.isFriend == true{
           
                let vchat  = self.storyboard?.instantiateViewController(withIdentifier: "ChatViewController") as! ChatViewController
                vchat.fromIntro = true
                vchat.friendId  = JSON(selectedPerson.personId).intValue
                self.navigationController?.pushViewController(vchat, animated: true)
                
            }
            else{
                self.btnGetIntroducedTapped(sender)
            }
    }
    }
    @IBAction func btnGetIntroducedTapped(_ sender: Any) {
        
        if selectedPerson.commanConection == true{
            
                        let vc  = Introduce.init(frame: self.view.frame,  strTile:"You need to be introduced to \(selectedPerson.name) before you can send him a message directly.",delegatesStr:"get")
                        vc.delegates  = self
                        self.view.addSubview(vc)
           
        }
        else{
             if exploreWallInfo?.introAvailable == true{
                let str = """
Looks like you don't have a comman friend with \(selectedPerson.name). No Worries!
Use one of your 3 FREE intros to be introduced to your crush. OR go to Matchmaker wall to ask about \(selectedPerson.name) fro FREE.🤞
"""
                let vc  = NoCommanFriends.init(frame: self.view.frame,  strTile:str,delegatesStr:"noComman")
                vc.delegates = self
                self.view.addSubview(vc)
                
                
            }
             else{
                let  vc = PurchaseIntros.init(frame: self.view.frame,name: selectedPerson.name,delegateStr :"purchase")
                vc.delegates = self
                self.view.addSubview(vc)
                
            }
            
        }
      
        
    }


    func showIntroAlert(name:String,id : String){
        let vc = AlertWithTextView.init(frame:self.view.frame
            , title: "Hey \(name)! Could you introduce me to \(selectedPerson.name)?", cancelTitle: "Edit", doneTitle: "Send",commanUserId: id,introduceToId: selectedPerson.personId,relationShipStatus : selectedPerson.status)
        vc.delegates = self
        self.view.addSubview(vc)
        
        
    }
    
    func showAlert(userName : String){
        
        let vc = AlertWithOneButton.init(frame: self.view.frame, title: "We have sent an Intro message to \(userName). We will let you know as soon as \(userName) likes you back.", doneTitle: "Keep Exploring",bgColor : UIColor.clear)
        vc.delegates = self
        self.view.addSubview(vc)
        
    }
    
    func popViewToMainViewController(){
        let viewControllers: [UIViewController] = self.navigationController!.viewControllers
        for aViewController in viewControllers {
            if aViewController is Tabbar {
                let vc  = aViewController as! Tabbar
               
                let match = vc.viewControllers![1] as! MatchMakerViewController
                match.uploadPostToMatcMakerProfile(image_id: personTag.imageId, imageUrl: personTag.imageUrl)
        
                vc.setSelectIndex(from: 2, to: 1)
         self.navigationController!.popToViewController(aViewController, animated: false)
            }
        }
    }
    
  
    
    
    //MARK:- Fetch UserDetails API Call
    func fetchUserDetailsApiCall(parameters: Parameters){
        AccountApiInterface.sharedInstance.postRequestWithParam(fromViewController: self, actionName: "another_user_profile", dictParam: parameters as [String : AnyObject], isShowIndicator: true, success: { (json) in
            
        
            print(json)
          
            
            DispatchQueue.main.async {
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "FriendProfileViewController") as! FriendProfileViewController
                   vc.userDetails = json
                vc.imageId = self.personTag.imageId
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }) { (error) in
            self.showAlert(messageStr: error)
        }
        
    }
    
    //MARK:- Comman Friends ApiCall  Function
    
    func listofCommanFriendsApiCall(parameters: Parameters,array:@escaping ([JSON]) -> Void) {
        AccountApiInterface.sharedInstance.postRequestWithParam(fromViewController: self, actionName: "common_friend_list", dictParam: parameters as [String : AnyObject], isShowIndicator: true, success: { (json) in
            print(json)
            
            array(json["data"].arrayValue)
            
            
        }) { (error) in
            
            DispatchQueue.main.async {
                array([JSON]())
                
            }
        }
        
    }

    
    
    //MARK:- Introduce ApiCall  Function
    
    func introduceApiCall(parameters: Parameters){
        AccountApiInterface.sharedInstance.postRequestWithParam(fromViewController: self, actionName: "introduce_friend", dictParam: parameters as [String : AnyObject], isShowIndicator: true, success: { (json) in
            print(json)
            DispatchQueue.main.async {
                self.showAlert(userName: self.selectedPerson.name)
            }
            
        }) { (error) in
            self.showAlert(messageStr: error)
        }
        
    }

    //MARK:- Like Person Api Call
    func likePersonApiCall(parameters: Parameters , sender : UIButton , status : Bool , id : Int){
        AccountApiInterface.sharedInstance.postRequestWithParam(fromViewController: self, actionName: "like_person", dictParam: parameters as [String : AnyObject], isShowIndicator: false, success: { (json) in
            print(json)
            DispatchQueue.main.async {
                if self.selectedPerson.isLike == true {
                    
                    self.selectedPerson.isLike = false
        
                    let likes = ( self.exploreWallInfo?.tagInfo?.filter({($0.userId ?? 0 )  == id})[0].likes  ?? 0) - 1
                    self.exploreWallInfo?.tagInfo?.filter({($0.userId ?? 0 ) == id})[0].likes = likes
                    
                    self.exploreWallInfo?.totalLikes =  ( self.exploreWallInfo?.totalLikes ?? 0) - 1
                    self.lblLikes.text =   String(likes)
                   
                }
                else{
                     self.selectedPerson.isLike = true
                    let likes = ( self.exploreWallInfo?.tagInfo?.filter({($0.userId ?? 0 )  == id})[0].likes  ?? 0) + 1
                    self.exploreWallInfo?.tagInfo?.filter({($0.userId ?? 0 ) == id})[0].likes = likes
                    
                    self.exploreWallInfo?.totalLikes =  ( self.exploreWallInfo?.totalLikes ?? 0) + 1
                    self.lblLikes.text =   String(likes)
                }
                
            }
            
        }) { (error) in
            print(error)
        }
        
    }

    
    
    
}

extension GetIntroducedViewController : setValuesDelegates,alertButtonDelegates , okButtonDelegates,setFriendsDelegates,friendwithMessageDelegates{
   
   
    
   func alertButtonAction(index : Int, delegatesStr:String){

    if index == 1 && delegatesStr == "get"{
        

        let userData = UserData.init(json: JSON(standard.value(forKey: "userData") as? [String : Any] ?? [String : Any]()))
        
        if  userData.usertype == 0 {
            
             let param : Parameters = ["user_id":"\(standard.value(forKey: "userid")!)","another_user_id" : selectedPerson.personId]
            
            print(param)
            self.listofCommanFriendsApiCall(parameters: param) { (array) in
                
                print(array)
                
                if array.count > 0 {
                    DispatchQueue.main.async {
                        let vc = OptionTableFriend.init(frame: self.view.frame, dataArr:array , tableframe: CGRect.init(x: 10, y: Int((self.view.frame.size.height) / 2)  - Int(array.count * 25) - 50 , width: Int((self.view.frame.size.width) - 20), height:  Int(array.count * 50) + 100))
                        vc.delegates = self
                        self.view.addSubview(vc)
                    }
                    
                    
                }
            }
            
            
            
        }
        
    
    }
    if  delegatesStr == "noComman"{
        
        if index == 1 {
        
        let param = ["image_id":personTag.imageId , "my_id":"\(standard.value(forKey: "userid")!)", "introduce_to_id" : selectedPerson.personId,"relationship_status":selectedPerson.status,"common_user_id" : "","text":""]
        
        self.introduceApiCall(parameters: param)
        }
        else if index == 2 {
             self.popViewToMainViewController()
        }
        
    }

   
    if delegatesStr == "purchase" {
        if index == 1 || index == 2 {
       let vc = self.storyboard?.instantiateViewController(withIdentifier: "BuyInrosViewController") as! BuyInrosViewController
        if index == 1{
            vc.index = 3
        }
        else if index == 2 {
            vc.index = 8
        }
        
        self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    if index == 3 {
        
       self.popViewToMainViewController()
        
    }
    
    }
    func  setFriendsValues(friend : JSON){
        
        self.showIntroAlert(name: friend["name"].stringValue, id: friend["id"].stringValue)
       
    }
    func okTappedfunction(){
        self.navigationController?.popViewController(animated: true)
        
    }
    func  friendwithMessageFunction(text : String,commanUserid : String, introduceToId : String,relationShipStatus : String){
        
        let param = ["image_id":personTag.imageId , "my_id":"\(standard.value(forKey: "userid")!)", "introduce_to_id" : introduceToId,"relationship_status":relationShipStatus,"common_user_id" : commanUserid,"text":text]
        
        self.introduceApiCall(parameters: param)
        
    }
    
    
}


extension GetIntroducedViewController : tapDelegates {
    
    func  nameActionDelegates(tag : Int) {
          if personTag.tagPerson[tag - 2000].commanConection == true {
        
                    let id =    personTag.tagPerson[tag - 2000].personId
        
                    let param : Parameters = ["user_id" : id]
        
        
                    self.fetchUserDetailsApiCall(parameters: param)
                }
        
          else{
            selectedPerson = personTag.tagPerson[tag - 2000]
            isHideGetIntroButton = false
            self.setLikeAndCommentsFunction(total: false, id: JSON(selectedPerson.personId).intValue)
        }
        
    }
    func  topViewActionDelegates(tag : Int) {
        selectedPerson = personTag.tagPerson[tag - 1000]
         isHideGetIntroButton = false
        for i in 10000..<10030{
            let vc =  self.imgView.viewWithTag(i) as? TagView
            vc?.removeFromSuperview()
        }
        self.setTagInImages(img: self.imgView, p: personTag.tagPerson, selectedFriendId: selectedPerson.personId)
       
        self.setLikeAndCommentsFunction(total: false, id: JSON(selectedPerson.personId).intValue)
        
    }
    
    
    
}

