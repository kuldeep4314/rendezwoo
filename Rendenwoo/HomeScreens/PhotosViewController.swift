//
//  PhotosViewController.swift
//  Rendenwoo
//
//  Created by goyal on 05/03/18.
//  Copyright © 2018 ankita. All rights reserved.
//

import UIKit
import ADMozaicCollectionViewLayout
import CropViewController
class PhotosViewController: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout , UIImagePickerControllerDelegate,
UINavigationControllerDelegate,ADMozaikLayoutDelegate,CropViewControllerDelegate{
    var currentPage = 1
    var isResultsAviablleToLoad = true

    var imagesArr = [RZPhotos]()
    @IBOutlet weak var lblConnectToInstagram: UILabel!
    @IBOutlet weak var topViewHeight: NSLayoutConstraint!
    @IBOutlet weak var adLayout: ADMozaikLayout!
    @IBOutlet weak var collectionView: UICollectionView!
    let picker = UIImagePickerController()
 var imageIndex = Int()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.adLayout.delegate = self
        if UIDevice.isIphoneX == true
        {
            topViewHeight.constant = 84
        }
        
    
       
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if ( standard.value(forKey: "instagram") as? String ) != nil {
            lblConnectToInstagram.text = "Disconnect Instagram"
            
        }
        
        else{
              lblConnectToInstagram.text = "Connect Instagram"
        }
         currentPage = 1
        
       
        self.fetchApiCall(page: currentPage)
    }
    @IBAction func btnBackTapped(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func connectToInstramTapped(_ sender: UITapGestureRecognizer) {
        
        if (standard.value(forKey: "instagram") as? String) != nil {
            
            if imagesArr.count < 6 {
                imageIndex = -1
                self.showActionSheet(title: "Upload a replacement from")
            }
            else{
                let parameter : Parameters = ["user_id":"\(standard.value(forKey: "userid")!)"]
                
                self.instagaramDisconnectApiCall(parameters: parameter)
            }
         
        }
        else{
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "InstagramLoginVC") as! InstagramLoginVC
            
            self.navigationController?.pushViewController(vc, animated: true)
        }
    
    }
    
    func photoFromLibrary(){
        picker.allowsEditing = false
        picker.sourceType = .photoLibrary
          picker.delegate = self
        picker.mediaTypes = UIImagePickerController.availableMediaTypes(for: .photoLibrary)!
        picker.modalPresentationStyle = .popover
        present(picker, animated: true, completion: nil)
        picker.popoverPresentationController?.sourceView = self.view
        
    }
    
    
    func showActionSheet(title : String){
        
        let alertController = UIAlertController(title: title, message: nil, preferredStyle: .actionSheet)
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (action) in
            
        }
        alertController.addAction(cancelAction)
        
        let takePhoto = UIAlertAction(title: "Facebook", style: .default) { (action) in
            
//           let vc = self.storyboard?.instantiateViewController(withIdentifier: "FacebookPhotoViewController") as! FacebookPhotoViewController
//  
//            self.navigationController?.pushViewController(vc, animated: true)
            
        }
        alertController.addAction(takePhoto)
        
        let selectPhoto = UIAlertAction(title: "Photo Gallery", style: .default) { (action) in
            self.photoFromLibrary()
        }
        alertController.addAction(selectPhoto)
        
        
        alertController.popoverPresentationController?.sourceView = self.view
        self.present(alertController, animated: true) {
            
        }
    }
    //MARK: - PickerView Delegates
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        let img = info[UIImagePickerControllerOriginalImage] as! UIImage
        print(img)
        dismiss(animated:true, completion: nil)
        self.presentCropViewController(image: img)
      
    }
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated:true, completion: nil)
    }
    
    @objc func addPhotoTapped(sender: UIButton){
           imageIndex = -1
        self.showActionSheet(title:"Import pictures from")
    }
    
    @objc func deletePhotoTapped(sender: UIButton){
       
        if imagesArr.count < 6 {
            imageIndex = sender.tag
            self.showActionSheet(title: "Upload a replacement from")
        }
        else{
            let  parameters : Parameters =   [ "user_id":standard.value(forKey: "userid")!,"image_id":imagesArr[sender.tag].imageId ?? 0]
    
              self.deleteApiCall(parameters: parameters, index: sender.tag)
        }
        
    }
    //MARK:- CollectionView Datasource
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
            return imagesArr.count + 1
        
        
        
      
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
          let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! PhotoCollectionViewCell
        
        if indexPath.row == imagesArr.count {
             cell.imageView.image = #imageLiteral(resourceName: "imgPlaceHolder")
             cell.btnRmoveAdd.setImage(#imageLiteral(resourceName: "imgAdd"), for: .normal)
             cell.btnRmoveAdd.removeTarget(self, action: nil, for: .allEvents)
             cell.btnRmoveAdd.addTarget(self, action: #selector(self.addPhotoTapped(sender:)), for: .touchUpInside)
          
        }
        else{
           
             cell.btnRmoveAdd.setImage(#imageLiteral(resourceName: "imgRemove"), for: .normal)
          
            let imageURL = imagesArr[indexPath.row].imageUrl ?? ""
            cell.imageView.sd_setImage(with: URL(string: imageURL), placeholderImage: #imageLiteral(resourceName: "imgPlaceHolder"), options: [], completed: nil)
            
            cell.btnRmoveAdd.tag = indexPath.row
            cell.btnRmoveAdd.removeTarget(self, action: nil, for: .allEvents)
            cell.btnRmoveAdd.addTarget(self, action: #selector(self.deletePhotoTapped(sender:)), for: .touchUpInside)
            
        }
        return cell
        
    }
    //MARK:- CollectionView Delegates
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        collectionView.deselectItem(at: indexPath, animated: true)
        
        if  indexPath.row < imagesArr.count {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "TagViewController") as! TagViewController

            let taggedInfo = imagesArr[indexPath.row]

            let imageInfo = PersonTag()
            imageInfo.imageId = String(taggedInfo.imageId ?? 0)
            imageInfo.imageUrl = taggedInfo.imageUrl ?? ""


            for i in 0..<(taggedInfo.tagInfo?.count ?? 0) {

                let person = Person()

                person.personId = String(taggedInfo.tagInfo?[i].userId ?? 0)
                person.name = taggedInfo.tagInfo?[i].name ?? ""
                person.xPos =  CGFloat(JSON(taggedInfo.tagInfo?[i].xpos ?? "0").floatValue)
                person.yPos = CGFloat(JSON(taggedInfo.tagInfo?[i].ypos ?? "0").floatValue)
                person.status = String(taggedInfo.tagInfo?[i].status ?? 0)
                imageInfo.tagPerson.append(person)

            }

            vc.personTag = imageInfo
             self.navigationController?.pushViewController(vc, animated: true)
        }
       
        
       
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        
        
        return CGSize(width: collectionView.frame.size.width, height: collectionView.frame.size.height )
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    //MARK: - ADMozaikLayoutDelegate
    
    func collectonView(_ collectionView: UICollectionView, mozaik layoyt: ADMozaikLayout, geometryInfoFor section: ADMozaikLayoutSection) -> ADMozaikLayoutSectionGeometryInfo {
        
        var rowHeight: CGFloat = 20.0
        
            rowHeight = (self.collectionView.frame.size.height / 19.5)
            
        
        
        let width = ADMozaikLayoutColumn(width: collectionView.frame.size.width / 3 - 3)
        let columns = [width, width,width]
        
    
        let geometryInfo = ADMozaikLayoutSectionGeometryInfo(rowHeight: rowHeight,
                                                             columns: columns,
                                                             minimumInteritemSpacing: 0,
                                                             minimumLineSpacing: 0,
                                                             sectionInset: UIEdgeInsets(top: 5, left: 5, bottom: 5, right: 5),
                                                             headerHeight: 0, footerHeight: 0)
        
        return geometryInfo
    }
    
    func collectionView(_ collectionView: UICollectionView, mozaik layout: ADMozaikLayout, mozaikSizeForItemAt indexPath: IndexPath) -> ADMozaikLayoutSize {
        
        if indexPath.row == 0 {
         return ADMozaikLayoutSize(numberOfColumns: 2, numberOfRows: 8)
        }
      else  if indexPath.row == 1 {
            return ADMozaikLayoutSize(numberOfColumns: 1, numberOfRows: 4)
        }
        else  if indexPath.row == 2 {
            return ADMozaikLayoutSize(numberOfColumns: 1, numberOfRows: 4)
        }
        else{
            return ADMozaikLayoutSize(numberOfColumns: 1, numberOfRows: 5)
        }
    
    }
    
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if indexPath.row ==  self.imagesArr.count - 1 && isResultsAviablleToLoad == true{
            currentPage = currentPage + 1
         self.fetchApiCall(page: currentPage)
        }
    }
    //MARK:- Crop Image Function And Delegates
    func presentCropViewController(image: UIImage) {
        
        let cropViewController = CropViewController(image: image)
        cropViewController.delegate = self
        present(cropViewController, animated: true, completion: nil)
    }
    func cropViewController(_ cropViewController: CropViewController, didCropToImage image: UIImage, withRect cropRect: CGRect, angle: Int) {
        print(image)
        
        if imageIndex > -1 {
            let  parameters : Parameters =   [ "user_id":standard.value(forKey: "userid")!,"image_id":imagesArr[imageIndex].imageId ?? 0]
            self.deleteAndUploadApiCall(parameters: parameters, index: imageIndex, img: image)
        }
        else{
               self.uploadImageApi(img: image, index: imageIndex)
        }
     
        cropViewController.dismiss(animated: true, completion: nil)
    }

    
    //MARK:- Upload Image Api
    
    func uploadImageApi(img : UIImage, index : Int) {
        self.view.startIndicator()
        let paramerter : Parameters = ["user_id": "\(standard.value(forKey: "userid") ?? 0)"]
        
        ApiManager.sharedInstance.requestMultiPartURL(baseUrl + "upload_images", params: paramerter as [String : AnyObject], headers: nil, imagesArray: [img], imageName: ["image_"], success: { (json) in
            self.view.stopIndicator()
             print(json)
            
            guard(json["result"].intValue) == 1 else{
                
                return
            }
            DispatchQueue.main.async {
                let taggedInfo = RZPhotos.init(json: json["data"][0])
             
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "TagViewController") as! TagViewController
            
                let imageInfo = PersonTag()
                imageInfo.imageId = String(taggedInfo.imageId ?? 0)
                imageInfo.imageUrl = taggedInfo.imageUrl ?? ""
                
                
                for i in 0..<(taggedInfo.tagInfo?.count ?? 0) {
                    
                    let person = Person()
                    
                    person.personId = String(taggedInfo.tagInfo?[i].userId ?? 0)
                    person.name = taggedInfo.tagInfo?[i].name ?? ""
                    person.xPos =  CGFloat(JSON(taggedInfo.tagInfo?[i].xpos ?? "0").floatValue)
                    person.yPos = CGFloat(JSON(taggedInfo.tagInfo?[i].ypos ?? "0").floatValue)
                    person.status = String(taggedInfo.tagInfo?[i].status ?? 0)
                    imageInfo.tagPerson.append(person)
                    
                }
                
                vc.personTag = imageInfo
                self.navigationController?.pushViewController(vc, animated: true)
            
            }
            
            
        }) { (err) in
            self.view.stopIndicator()
            
        }
    }
    func instagaramDisconnectApiCall(parameters: Parameters){
        AccountApiInterface.sharedInstance.postRequestWithParam(fromViewController: self, actionName: "disconnect_insta", dictParam: parameters as [String : AnyObject], isShowIndicator: true, success: { (json) in
            
            print(json)
            
            DispatchQueue.main.async {
                standard.set(nil, forKey: "instagram")
                standard.set(nil, forKey: "insta_name")
                self.lblConnectToInstagram.text = "Connect Instagram"
                self.currentPage = 1
                self.isResultsAviablleToLoad = true
                
                self.fetchApiCall(page:  self.currentPage)
                
                let cookieJar : HTTPCookieStorage = HTTPCookieStorage.shared
                for cookie in cookieJar.cookies! as [HTTPCookie]{
                    NSLog("cookie.domain = %@", cookie.domain)
                    
                    if cookie.domain == "www.instagram.com" ||
                        cookie.domain == "api.instagram.com"{
                        
                        cookieJar.deleteCookie(cookie)
                    }
                }
                
                
            }
        }) { (error) in
            self.showAlert(messageStr: error)
        }
        
    }
    
    //MARK:- Fetch Photo Api Function
    func fetchApiCall(page : Int){
        let parameter : Parameters = ["user_id": "\(standard.value(forKey: "userid")!)","page" : page]
        print(parameter)
       
        AccountApiInterface.sharedInstance.postRequestWithParam(fromViewController: self, actionName: "fetch_photos", dictParam: parameter as [String : AnyObject], isShowIndicator: false, success: { (json) in
            //print(json)
            
            if page == 1 {
                self.imagesArr.removeAll()
            }
            
            guard(json["image_array"].arrayValue.count) > 0 else {
                
                self.isResultsAviablleToLoad = false
                DispatchQueue.main.async {
                    self.collectionView.reloadData()
                }
                
                return
            }
             self.isResultsAviablleToLoad = true
            for i in 0..<json["image_array"].arrayValue.count{
                let arrTemp = RZPhotos.init(json: json["image_array"][i])
                  self.imagesArr .append(arrTemp)
            }
          
            self.collectionView.reloadData()
            
            
        }) { (error) in
               self.isResultsAviablleToLoad = false
            self.showAlert(messageStr: error)
        }
        
    }

    //MARK:- Delete Photo Api Function
    func deleteApiCall(parameters: Parameters,index :Int){
        print(parameters)
        AccountApiInterface.sharedInstance.postRequestWithParam(fromViewController: self, actionName: "delete_image", dictParam: parameters as [String : AnyObject], isShowIndicator: true, success: { (json) in
            print(json)
           
            self.imagesArr.remove(at: index)
            DispatchQueue.main.async {
                 self.collectionView.reloadData()
            }
            
        }) { (error) in
            self.showAlert(messageStr: error)
        }
        
    }
    //MARK:- Delete Photo Api Function
    func deleteAndUploadApiCall(parameters: Parameters,index :Int,img : UIImage){
        print(parameters)
        AccountApiInterface.sharedInstance.postRequestWithParam(fromViewController: self, actionName: "delete_image", dictParam: parameters as [String : AnyObject], isShowIndicator: true, success: { (json) in
            print(json)
            
            self.uploadImageApi(img: img, index: index)
        }) { (error) in
            self.showAlert(messageStr: error)
        }
        
    }
    
    
}
