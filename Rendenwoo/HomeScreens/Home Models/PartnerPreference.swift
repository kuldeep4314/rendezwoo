//
//  PartnerPreference.swift
//
//  Created by goyal on 23/04/18
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

public class PartnerPreference: NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private let kPartnerPreferenceDistanceInKmKey: String = "distance_in_km"
  private let kPartnerPreferenceHeightKey: String = "height"
  private let kPartnerPreferenceDistanceKey: String = "distance"
  private let kPartnerPreferenceGenderPreferenceKey: String = "gender_preference"
  private let kPartnerPreferenceAgeKey: String = "age"
  private let kPartnerPreferenceUserIdKey: String = "user_id"
  private let kPartnerPreferenceEthnicityKey: String = "ethnicity"
  private let kPartnerPreferenceHeightInCmKey: String = "height_in_cm"
  private let kPartnerPreferenceRelationshipStatusKey: String = "relationship_status"
  private let kPartnerPreferenceReligionKey: String = "religion"

  // MARK: Properties
  public var distanceInKm: String?
  public var height: String?
  public var distance: String?
  public var genderPreference: Int?
  public var age: String?
  public var userId: Int?
  public var ethnicity: String?
  public var heightInCm: String?
  public var relationshipStatus: String?
  public var religion: String?

  // MARK: SwiftyJSON Initalizers
  /**
   Initates the instance based on the object
   - parameter object: The object of either Dictionary or Array kind that was passed.
   - returns: An initalized instance of the class.
  */
  convenience public init(object: Any) {
    self.init(json: JSON(object))
  }

  /**
   Initates the instance based on the JSON that was passed.
   - parameter json: JSON object from SwiftyJSON.
   - returns: An initalized instance of the class.
  */
  public init(json: JSON) {
    distanceInKm = json[kPartnerPreferenceDistanceInKmKey].string
    height = json[kPartnerPreferenceHeightKey].string
    distance = json[kPartnerPreferenceDistanceKey].string
    genderPreference = json[kPartnerPreferenceGenderPreferenceKey].int
    age = json[kPartnerPreferenceAgeKey].string
    userId = json[kPartnerPreferenceUserIdKey].int
    ethnicity = json[kPartnerPreferenceEthnicityKey].string
    heightInCm = json[kPartnerPreferenceHeightInCmKey].string
    relationshipStatus = json[kPartnerPreferenceRelationshipStatusKey].string
    religion = json[kPartnerPreferenceReligionKey].string
  }

  /**
   Generates description of the object in the form of a NSDictionary.
   - returns: A Key value pair containing all valid values in the object.
  */
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = distanceInKm { dictionary[kPartnerPreferenceDistanceInKmKey] = value }
    if let value = height { dictionary[kPartnerPreferenceHeightKey] = value }
    if let value = distance { dictionary[kPartnerPreferenceDistanceKey] = value }
    if let value = genderPreference { dictionary[kPartnerPreferenceGenderPreferenceKey] = value }
    if let value = age { dictionary[kPartnerPreferenceAgeKey] = value }
    if let value = userId { dictionary[kPartnerPreferenceUserIdKey] = value }
    if let value = ethnicity { dictionary[kPartnerPreferenceEthnicityKey] = value }
    if let value = heightInCm { dictionary[kPartnerPreferenceHeightInCmKey] = value }
    if let value = relationshipStatus { dictionary[kPartnerPreferenceRelationshipStatusKey] = value }
    if let value = religion { dictionary[kPartnerPreferenceReligionKey] = value }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.distanceInKm = aDecoder.decodeObject(forKey: kPartnerPreferenceDistanceInKmKey) as? String
    self.height = aDecoder.decodeObject(forKey: kPartnerPreferenceHeightKey) as? String
    self.distance = aDecoder.decodeObject(forKey: kPartnerPreferenceDistanceKey) as? String
    self.genderPreference = aDecoder.decodeObject(forKey: kPartnerPreferenceGenderPreferenceKey) as? Int
    self.age = aDecoder.decodeObject(forKey: kPartnerPreferenceAgeKey) as? String
    self.userId = aDecoder.decodeObject(forKey: kPartnerPreferenceUserIdKey) as? Int
    self.ethnicity = aDecoder.decodeObject(forKey: kPartnerPreferenceEthnicityKey) as? String
    self.heightInCm = aDecoder.decodeObject(forKey: kPartnerPreferenceHeightInCmKey) as? String
    self.relationshipStatus = aDecoder.decodeObject(forKey: kPartnerPreferenceRelationshipStatusKey) as? String
    self.religion = aDecoder.decodeObject(forKey: kPartnerPreferenceReligionKey) as? String
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(distanceInKm, forKey: kPartnerPreferenceDistanceInKmKey)
    aCoder.encode(height, forKey: kPartnerPreferenceHeightKey)
    aCoder.encode(distance, forKey: kPartnerPreferenceDistanceKey)
    aCoder.encode(genderPreference, forKey: kPartnerPreferenceGenderPreferenceKey)
    aCoder.encode(age, forKey: kPartnerPreferenceAgeKey)
    aCoder.encode(userId, forKey: kPartnerPreferenceUserIdKey)
    aCoder.encode(ethnicity, forKey: kPartnerPreferenceEthnicityKey)
    aCoder.encode(heightInCm, forKey: kPartnerPreferenceHeightInCmKey)
    aCoder.encode(relationshipStatus, forKey: kPartnerPreferenceRelationshipStatusKey)
    aCoder.encode(religion, forKey: kPartnerPreferenceReligionKey)
  }

}
