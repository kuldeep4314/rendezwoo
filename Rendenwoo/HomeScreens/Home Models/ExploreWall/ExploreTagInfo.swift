//
//  ExploreTagInfo.swift
//
//  Created by goyal on 12/07/18
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

public class ExploreTagInfo: NSCoding {
    
    // MARK: Declaration for string constants to be used to decode and also serialize.
    private let kExploreTagInfoStatusKey: String = "status"
    private let kExploreTagInfoXposKey: String = "xpos"
    private let kExploreTagInfoNameKey: String = "name"
    private let kExploreTagInfoFriendKey: String = "friend"
    private let kExploreTagInfoLikesKey: String = "likes"
    private let kExploreTagInfoIsLikedKey: String = "is_liked"
    private let kExploreTagInfoCommonConnectionKey: String = "common_connection"
    private let kExploreTagInfoYposKey: String = "ypos"
    private let kExploreTagInfoUserIdKey: String = "user_id"
    private let kExploreTagInfoIntrosKey: String = "intros"
    
    // MARK: Properties
    public var status: Int?
    public var xpos: String?
    public var name: String?
    public var friend: Bool = false
    public var likes: Int?
    public var isLiked: Bool = false
    public var commonConnection: Bool = false
    public var ypos: String?
    public var userId: Int?
    public var intros: Int?
    
    // MARK: SwiftyJSON Initalizers
    /**
     Initates the instance based on the object
     - parameter object: The object of either Dictionary or Array kind that was passed.
     - returns: An initalized instance of the class.
     */
    convenience public init(object: Any) {
        self.init(json: JSON(object))
    }
    
    /**
     Initates the instance based on the JSON that was passed.
     - parameter json: JSON object from SwiftyJSON.
     - returns: An initalized instance of the class.
     */
    public init(json: JSON) {
        status = json[kExploreTagInfoStatusKey].int
        xpos = json[kExploreTagInfoXposKey].string
        name = json[kExploreTagInfoNameKey].string
        friend = json[kExploreTagInfoFriendKey].boolValue
        likes = json[kExploreTagInfoLikesKey].int
        isLiked = json[kExploreTagInfoIsLikedKey].boolValue
        commonConnection = json[kExploreTagInfoCommonConnectionKey].boolValue
        ypos = json[kExploreTagInfoYposKey].string
        userId = json[kExploreTagInfoUserIdKey].int
        intros = json[kExploreTagInfoIntrosKey].int
    }
    
    /**
     Generates description of the object in the form of a NSDictionary.
     - returns: A Key value pair containing all valid values in the object.
     */
    public func dictionaryRepresentation() -> [String: Any] {
        var dictionary: [String: Any] = [:]
        if let value = status { dictionary[kExploreTagInfoStatusKey] = value }
        if let value = xpos { dictionary[kExploreTagInfoXposKey] = value }
        if let value = name { dictionary[kExploreTagInfoNameKey] = value }
        dictionary[kExploreTagInfoFriendKey] = friend
        if let value = likes { dictionary[kExploreTagInfoLikesKey] = value }
        dictionary[kExploreTagInfoIsLikedKey] = isLiked
        dictionary[kExploreTagInfoCommonConnectionKey] = commonConnection
        if let value = ypos { dictionary[kExploreTagInfoYposKey] = value }
        if let value = userId { dictionary[kExploreTagInfoUserIdKey] = value }
        if let value = intros { dictionary[kExploreTagInfoIntrosKey] = value }
        return dictionary
    }
    
    // MARK: NSCoding Protocol
    required public init(coder aDecoder: NSCoder) {
        self.status = aDecoder.decodeObject(forKey: kExploreTagInfoStatusKey) as? Int
        self.xpos = aDecoder.decodeObject(forKey: kExploreTagInfoXposKey) as? String
        self.name = aDecoder.decodeObject(forKey: kExploreTagInfoNameKey) as? String
        self.friend = aDecoder.decodeBool(forKey: kExploreTagInfoFriendKey)
        self.likes = aDecoder.decodeObject(forKey: kExploreTagInfoLikesKey) as? Int
        self.isLiked = aDecoder.decodeBool(forKey: kExploreTagInfoIsLikedKey)
        self.commonConnection = aDecoder.decodeBool(forKey: kExploreTagInfoCommonConnectionKey)
        self.ypos = aDecoder.decodeObject(forKey: kExploreTagInfoYposKey) as? String
        self.userId = aDecoder.decodeObject(forKey: kExploreTagInfoUserIdKey) as? Int
        self.intros = aDecoder.decodeObject(forKey: kExploreTagInfoIntrosKey) as? Int
    }
    
    public func encode(with aCoder: NSCoder) {
        aCoder.encode(status, forKey: kExploreTagInfoStatusKey)
        aCoder.encode(xpos, forKey: kExploreTagInfoXposKey)
        aCoder.encode(name, forKey: kExploreTagInfoNameKey)
        aCoder.encode(friend, forKey: kExploreTagInfoFriendKey)
        aCoder.encode(likes, forKey: kExploreTagInfoLikesKey)
        aCoder.encode(isLiked, forKey: kExploreTagInfoIsLikedKey)
        aCoder.encode(commonConnection, forKey: kExploreTagInfoCommonConnectionKey)
        aCoder.encode(ypos, forKey: kExploreTagInfoYposKey)
        aCoder.encode(userId, forKey: kExploreTagInfoUserIdKey)
        aCoder.encode(intros, forKey: kExploreTagInfoIntrosKey)
    }
    
}
