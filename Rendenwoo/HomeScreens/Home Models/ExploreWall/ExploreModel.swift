//
//  ExploreModel.swift
//
//  Created by goyal on 12/07/18
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

public class ExploreModel: NSCoding {
    
    // MARK: Declaration for string constants to be used to decode and also serialize.
    private let kExploreModelTotalLikesKey: String = "total_likes"
    private let kExploreModelImageIdKey: String = "image_id"
    private let kExploreModelIntroAvailableKey: String = "intro_available"
    private let kExploreModelTotalMessagesKey: String = "total_messages"
    private let kExploreModelTotalIntrosKey: String = "total_intros"
    private let kExploreModelImageUrlKey: String = "image_url"
    private let kExploreModelCommonConnectionKey: String = "common_connection"
    private let kExploreModelUserIdKey: String = "user_id"
    private let kExploreModelTagInfoKey: String = "tag_info"
    private let kExploreModelTimestampKey: String = "timestamp"
    
    // MARK: Properties
    public var totalLikes: Int?
    public var imageId: Int?
    public var introAvailable: Bool = false
    public var totalMessages: Int?
    public var totalIntros: Int?
    public var imageUrl: String?
    public var commonConnection: Bool = false
    public var userId: Int?
    public var tagInfo: [ExploreTagInfo]?
    public var timestamp: String?
    
    // MARK: SwiftyJSON Initalizers
    /**
     Initates the instance based on the object
     - parameter object: The object of either Dictionary or Array kind that was passed.
     - returns: An initalized instance of the class.
     */
    convenience public init(object: Any) {
        self.init(json: JSON(object))
    }
    
    /**
     Initates the instance based on the JSON that was passed.
     - parameter json: JSON object from SwiftyJSON.
     - returns: An initalized instance of the class.
     */
    public init(json: JSON) {
        totalLikes = json[kExploreModelTotalLikesKey].int
        imageId = json[kExploreModelImageIdKey].int
        introAvailable = json[kExploreModelIntroAvailableKey].boolValue
        totalMessages = json[kExploreModelTotalMessagesKey].int
        totalIntros = json[kExploreModelTotalIntrosKey].int
        imageUrl = json[kExploreModelImageUrlKey].string
        commonConnection = json[kExploreModelCommonConnectionKey].boolValue
        userId = json[kExploreModelUserIdKey].int
        if let items = json[kExploreModelTagInfoKey].array { tagInfo = items.map { ExploreTagInfo(json: $0) } }
        timestamp = json[kExploreModelTimestampKey].string
    }
    
    /**
     Generates description of the object in the form of a NSDictionary.
     - returns: A Key value pair containing all valid values in the object.
     */
    public func dictionaryRepresentation() -> [String: Any] {
        var dictionary: [String: Any] = [:]
        if let value = totalLikes { dictionary[kExploreModelTotalLikesKey] = value }
        if let value = imageId { dictionary[kExploreModelImageIdKey] = value }
        dictionary[kExploreModelIntroAvailableKey] = introAvailable
        if let value = totalMessages { dictionary[kExploreModelTotalMessagesKey] = value }
        if let value = totalIntros { dictionary[kExploreModelTotalIntrosKey] = value }
        if let value = imageUrl { dictionary[kExploreModelImageUrlKey] = value }
        dictionary[kExploreModelCommonConnectionKey] = commonConnection
        if let value = userId { dictionary[kExploreModelUserIdKey] = value }
        if let value = tagInfo { dictionary[kExploreModelTagInfoKey] = value.map { $0.dictionaryRepresentation() } }
        if let value = timestamp { dictionary[kExploreModelTimestampKey] = value }
        return dictionary
    }
    
    // MARK: NSCoding Protocol
    required public init(coder aDecoder: NSCoder) {
        self.totalLikes = aDecoder.decodeObject(forKey: kExploreModelTotalLikesKey) as? Int
        self.imageId = aDecoder.decodeObject(forKey: kExploreModelImageIdKey) as? Int
        self.introAvailable = aDecoder.decodeBool(forKey: kExploreModelIntroAvailableKey)
        self.totalMessages = aDecoder.decodeObject(forKey: kExploreModelTotalMessagesKey) as? Int
        self.totalIntros = aDecoder.decodeObject(forKey: kExploreModelTotalIntrosKey) as? Int
        self.imageUrl = aDecoder.decodeObject(forKey: kExploreModelImageUrlKey) as? String
        self.commonConnection = aDecoder.decodeBool(forKey: kExploreModelCommonConnectionKey)
        self.userId = aDecoder.decodeObject(forKey: kExploreModelUserIdKey) as? Int
        self.tagInfo = aDecoder.decodeObject(forKey: kExploreModelTagInfoKey) as? [ExploreTagInfo]
        self.timestamp = aDecoder.decodeObject(forKey: kExploreModelTimestampKey) as? String
    }
    
    public func encode(with aCoder: NSCoder) {
        aCoder.encode(totalLikes, forKey: kExploreModelTotalLikesKey)
        aCoder.encode(imageId, forKey: kExploreModelImageIdKey)
        aCoder.encode(introAvailable, forKey: kExploreModelIntroAvailableKey)
        aCoder.encode(totalMessages, forKey: kExploreModelTotalMessagesKey)
        aCoder.encode(totalIntros, forKey: kExploreModelTotalIntrosKey)
        aCoder.encode(imageUrl, forKey: kExploreModelImageUrlKey)
        aCoder.encode(commonConnection, forKey: kExploreModelCommonConnectionKey)
        aCoder.encode(userId, forKey: kExploreModelUserIdKey)
        aCoder.encode(tagInfo, forKey: kExploreModelTagInfoKey)
        aCoder.encode(timestamp, forKey: kExploreModelTimestampKey)
    }
    
}
