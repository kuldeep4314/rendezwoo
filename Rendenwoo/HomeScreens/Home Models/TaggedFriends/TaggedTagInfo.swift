//
//  TaggedTagInfo.swift
//
//  Created by goyal on 24/05/18
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

public class TaggedTagInfo: NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private let kTaggedTagInfoStatusKey: String = "status"
  private let kTaggedTagInfoXposKey: String = "xpos"
  private let kTaggedTagInfoUserIdKey: String = "user_id"
  private let kTaggedTagInfoNameKey: String = "name"
  private let kTaggedTagInfoYposKey: String = "ypos"

  // MARK: Properties
  public var status: Int?
  public var xpos: String?
  public var userId: Int?
  public var name: String?
  public var ypos: String?

  // MARK: SwiftyJSON Initalizers
  /**
   Initates the instance based on the object
   - parameter object: The object of either Dictionary or Array kind that was passed.
   - returns: An initalized instance of the class.
  */
  convenience public init(object: Any) {
    self.init(json: JSON(object))
  }

  /**
   Initates the instance based on the JSON that was passed.
   - parameter json: JSON object from SwiftyJSON.
   - returns: An initalized instance of the class.
  */
  public init(json: JSON) {
    status = json[kTaggedTagInfoStatusKey].int
    xpos = json[kTaggedTagInfoXposKey].string
    userId = json[kTaggedTagInfoUserIdKey].int
    name = json[kTaggedTagInfoNameKey].string
    ypos = json[kTaggedTagInfoYposKey].string
  }

  /**
   Generates description of the object in the form of a NSDictionary.
   - returns: A Key value pair containing all valid values in the object.
  */
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = status { dictionary[kTaggedTagInfoStatusKey] = value }
    if let value = xpos { dictionary[kTaggedTagInfoXposKey] = value }
    if let value = userId { dictionary[kTaggedTagInfoUserIdKey] = value }
    if let value = name { dictionary[kTaggedTagInfoNameKey] = value }
    if let value = ypos { dictionary[kTaggedTagInfoYposKey] = value }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.status = aDecoder.decodeObject(forKey: kTaggedTagInfoStatusKey) as? Int
    self.xpos = aDecoder.decodeObject(forKey: kTaggedTagInfoXposKey) as? String
    self.userId = aDecoder.decodeObject(forKey: kTaggedTagInfoUserIdKey) as? Int
    self.name = aDecoder.decodeObject(forKey: kTaggedTagInfoNameKey) as? String
    self.ypos = aDecoder.decodeObject(forKey: kTaggedTagInfoYposKey) as? String
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(status, forKey: kTaggedTagInfoStatusKey)
    aCoder.encode(xpos, forKey: kTaggedTagInfoXposKey)
    aCoder.encode(userId, forKey: kTaggedTagInfoUserIdKey)
    aCoder.encode(name, forKey: kTaggedTagInfoNameKey)
    aCoder.encode(ypos, forKey: kTaggedTagInfoYposKey)
  }

}
