//
//  TaggedImages.swift
//
//  Created by goyal on 24/05/18
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

public class TaggedImages: NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private let kTaggedImagesImageIdKey: String = "image_id"
  private let kTaggedImagesUserIdKey: String = "user_id"
  private let kTaggedImagesTimestampKey: String = "timestamp"
  private let kTaggedImagesImageUrlKey: String = "image_url"
  private let kTaggedImagesIsUserTaggedKey: String = "is_user_tagged"
  private let kTaggedImagesTagInfoKey: String = "tag_info"

  // MARK: Properties
  public var imageId: Int?
  public var userId: Int?
  public var timestamp: String?
  public var imageUrl: String?
  public var isUserTagged: Bool = false
  public var tagInfo: [TaggedTagInfo]?

  // MARK: SwiftyJSON Initalizers
  /**
   Initates the instance based on the object
   - parameter object: The object of either Dictionary or Array kind that was passed.
   - returns: An initalized instance of the class.
  */
  convenience public init(object: Any) {
    self.init(json: JSON(object))
  }

  /**
   Initates the instance based on the JSON that was passed.
   - parameter json: JSON object from SwiftyJSON.
   - returns: An initalized instance of the class.
  */
  public init(json: JSON) {
    imageId = json[kTaggedImagesImageIdKey].int
    userId = json[kTaggedImagesUserIdKey].int
    timestamp = json[kTaggedImagesTimestampKey].string
    imageUrl = json[kTaggedImagesImageUrlKey].string
    isUserTagged = json[kTaggedImagesIsUserTaggedKey].boolValue
    if let items = json[kTaggedImagesTagInfoKey].array { tagInfo = items.map { TaggedTagInfo(json: $0) } }
  }

  /**
   Generates description of the object in the form of a NSDictionary.
   - returns: A Key value pair containing all valid values in the object.
  */
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = imageId { dictionary[kTaggedImagesImageIdKey] = value }
    if let value = userId { dictionary[kTaggedImagesUserIdKey] = value }
    if let value = timestamp { dictionary[kTaggedImagesTimestampKey] = value }
    if let value = imageUrl { dictionary[kTaggedImagesImageUrlKey] = value }
    dictionary[kTaggedImagesIsUserTaggedKey] = isUserTagged
    if let value = tagInfo { dictionary[kTaggedImagesTagInfoKey] = value.map { $0.dictionaryRepresentation() } }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.imageId = aDecoder.decodeObject(forKey: kTaggedImagesImageIdKey) as? Int
    self.userId = aDecoder.decodeObject(forKey: kTaggedImagesUserIdKey) as? Int
    self.timestamp = aDecoder.decodeObject(forKey: kTaggedImagesTimestampKey) as? String
    self.imageUrl = aDecoder.decodeObject(forKey: kTaggedImagesImageUrlKey) as? String
    self.isUserTagged = aDecoder.decodeBool(forKey: kTaggedImagesIsUserTaggedKey)
    self.tagInfo = aDecoder.decodeObject(forKey: kTaggedImagesTagInfoKey) as? [TaggedTagInfo]
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(imageId, forKey: kTaggedImagesImageIdKey)
    aCoder.encode(userId, forKey: kTaggedImagesUserIdKey)
    aCoder.encode(timestamp, forKey: kTaggedImagesTimestampKey)
    aCoder.encode(imageUrl, forKey: kTaggedImagesImageUrlKey)
    aCoder.encode(isUserTagged, forKey: kTaggedImagesIsUserTaggedKey)
    aCoder.encode(tagInfo, forKey: kTaggedImagesTagInfoKey)
  }

}
