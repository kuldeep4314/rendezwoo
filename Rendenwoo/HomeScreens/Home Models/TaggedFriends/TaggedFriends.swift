//
//  TaggedFriends.swift
//
//  Created by goyal on 27/06/18
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

public class TaggedFriends: NSCoding {
    
    // MARK: Declaration for string constants to be used to decode and also serialize.
    private let kTaggedFriendsNameKey: String = "name"
    private let kTaggedFriendsUserTypeKey: String = "user_type"
    private let kTaggedFriendsUserIdKey: String = "user_id"
    private let kTaggedFriendsTagKey: String = "tag"
    
    // MARK: Properties
    public var name: String?
    public var userType: Int?
    public var userId: Int?
    public var tag: Bool = false
    
    // MARK: SwiftyJSON Initalizers
    /**
     Initates the instance based on the object
     - parameter object: The object of either Dictionary or Array kind that was passed.
     - returns: An initalized instance of the class.
     */
    convenience public init(object: Any) {
        self.init(json: JSON(object))
    }
    
    /**
     Initates the instance based on the JSON that was passed.
     - parameter json: JSON object from SwiftyJSON.
     - returns: An initalized instance of the class.
     */
    public init(json: JSON) {
        name = json[kTaggedFriendsNameKey].string
        userType = json[kTaggedFriendsUserTypeKey].int
        userId = json[kTaggedFriendsUserIdKey].int
        tag = json[kTaggedFriendsTagKey].boolValue
    }
    
    /**
     Generates description of the object in the form of a NSDictionary.
     - returns: A Key value pair containing all valid values in the object.
     */
    public func dictionaryRepresentation() -> [String: Any] {
        var dictionary: [String: Any] = [:]
        if let value = name { dictionary[kTaggedFriendsNameKey] = value }
        if let value = userType { dictionary[kTaggedFriendsUserTypeKey] = value }
        if let value = userId { dictionary[kTaggedFriendsUserIdKey] = value }
        dictionary[kTaggedFriendsTagKey] = tag
        return dictionary
    }
    
    // MARK: NSCoding Protocol
    required public init(coder aDecoder: NSCoder) {
        self.name = aDecoder.decodeObject(forKey: kTaggedFriendsNameKey) as? String
        self.userType = aDecoder.decodeObject(forKey: kTaggedFriendsUserTypeKey) as? Int
        self.userId = aDecoder.decodeObject(forKey: kTaggedFriendsUserIdKey) as? Int
        self.tag = aDecoder.decodeBool(forKey: kTaggedFriendsTagKey)
    }
    
    public func encode(with aCoder: NSCoder) {
        aCoder.encode(name, forKey: kTaggedFriendsNameKey)
        aCoder.encode(userType, forKey: kTaggedFriendsUserTypeKey)
        aCoder.encode(userId, forKey: kTaggedFriendsUserIdKey)
        aCoder.encode(tag, forKey: kTaggedFriendsTagKey)
    }
    
}
