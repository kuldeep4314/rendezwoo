//
//  RZSettings.swift
//
//  Created by goyal on 11/04/18
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

public class RZSettings: NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private let kRZSettingsActiveStatusKey: String = "active_status"
  private let kRZSettingsLocationKey: String = "location"
  private let kRZSettingsNotificationSettingsKey: String = "notification_settings"
  private let kRZSettingsResultKey: String = "result"
  private let kRZSettingsPaymentMethodKey: String = "payment_method"
  private let kRZSettingsMsgKey: String = "msg"

  // MARK: Properties
  public var activeStatus: Int?
  public var location: String?
  public var notificationSettings: [RZNotificationSettings]?
  public var result: Int?
  public var paymentMethod: String?
  public var msg: String?

  // MARK: SwiftyJSON Initalizers
  /**
   Initates the instance based on the object
   - parameter object: The object of either Dictionary or Array kind that was passed.
   - returns: An initalized instance of the class.
  */
  convenience public init(object: Any) {
    self.init(json: JSON(object))
  }

  /**
   Initates the instance based on the JSON that was passed.
   - parameter json: JSON object from SwiftyJSON.
   - returns: An initalized instance of the class.
  */
  public init(json: JSON) {
    activeStatus = json[kRZSettingsActiveStatusKey].int
    location = json[kRZSettingsLocationKey].string
    if let items = json[kRZSettingsNotificationSettingsKey].array { notificationSettings = items.map { RZNotificationSettings(json: $0) } }
    result = json[kRZSettingsResultKey].int
    paymentMethod = json[kRZSettingsPaymentMethodKey].string
    msg = json[kRZSettingsMsgKey].string
  }

  /**
   Generates description of the object in the form of a NSDictionary.
   - returns: A Key value pair containing all valid values in the object.
  */
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = activeStatus { dictionary[kRZSettingsActiveStatusKey] = value }
    if let value = location { dictionary[kRZSettingsLocationKey] = value }
    if let value = notificationSettings { dictionary[kRZSettingsNotificationSettingsKey] = value.map { $0.dictionaryRepresentation() } }
    if let value = result { dictionary[kRZSettingsResultKey] = value }
    if let value = paymentMethod { dictionary[kRZSettingsPaymentMethodKey] = value }
    if let value = msg { dictionary[kRZSettingsMsgKey] = value }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.activeStatus = aDecoder.decodeObject(forKey: kRZSettingsActiveStatusKey) as? Int
    self.location = aDecoder.decodeObject(forKey: kRZSettingsLocationKey) as? String
    self.notificationSettings = aDecoder.decodeObject(forKey: kRZSettingsNotificationSettingsKey) as? [RZNotificationSettings]
    self.result = aDecoder.decodeObject(forKey: kRZSettingsResultKey) as? Int
    self.paymentMethod = aDecoder.decodeObject(forKey: kRZSettingsPaymentMethodKey) as? String
    self.msg = aDecoder.decodeObject(forKey: kRZSettingsMsgKey) as? String
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(activeStatus, forKey: kRZSettingsActiveStatusKey)
    aCoder.encode(location, forKey: kRZSettingsLocationKey)
    aCoder.encode(notificationSettings, forKey: kRZSettingsNotificationSettingsKey)
    aCoder.encode(result, forKey: kRZSettingsResultKey)
    aCoder.encode(paymentMethod, forKey: kRZSettingsPaymentMethodKey)
    aCoder.encode(msg, forKey: kRZSettingsMsgKey)
  }

}
