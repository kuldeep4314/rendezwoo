//
//  MessageFriends.swift
//
//  Created by goyal on 15/05/18
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

public class MessageFriends: NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private let kMessageFriendsReadStatusKey: String = "read_status"
  private let kMessageFriendsFromUserKey: String = "from_user"
  private let kMessageFriendsReportStatusKey: String = "report_status"
  private let kMessageFriendsToUserKey: String = "to_user"
  private let kMessageFriendsNameKey: String = "name"
  private let kMessageFriendsTextKey: String = "text"
  private let kMessageFriendsImageUrlKey: String = "image_url"
  private let kMessageFriendsTypeKey: String = "type"
  private let kMessageFriendsMessageIdKey: String = "message_id"
  private let kMessageFriendsTimestampKey: String = "timestamp"

  // MARK: Properties
  public var readStatus: Int?
  public var fromUser: Int?
  public var reportStatus: Int?
  public var toUser: Int?
  public var name: String?
  public var text: String?
  public var imageUrl: String?
  public var type: Int?
  public var messageId: Int?
  public var timestamp: String?

  // MARK: SwiftyJSON Initalizers
  /**
   Initates the instance based on the object
   - parameter object: The object of either Dictionary or Array kind that was passed.
   - returns: An initalized instance of the class.
  */
  convenience public init(object: Any) {
    self.init(json: JSON(object))
  }

  /**
   Initates the instance based on the JSON that was passed.
   - parameter json: JSON object from SwiftyJSON.
   - returns: An initalized instance of the class.
  */
  public init(json: JSON) {
    readStatus = json[kMessageFriendsReadStatusKey].int
    fromUser = json[kMessageFriendsFromUserKey].int
    reportStatus = json[kMessageFriendsReportStatusKey].int
    toUser = json[kMessageFriendsToUserKey].int
    name = json[kMessageFriendsNameKey].string
    text = json[kMessageFriendsTextKey].string
    imageUrl = json[kMessageFriendsImageUrlKey].string
    type = json[kMessageFriendsTypeKey].int
    messageId = json[kMessageFriendsMessageIdKey].int
    timestamp = json[kMessageFriendsTimestampKey].string
  }

  /**
   Generates description of the object in the form of a NSDictionary.
   - returns: A Key value pair containing all valid values in the object.
  */
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = readStatus { dictionary[kMessageFriendsReadStatusKey] = value }
    if let value = fromUser { dictionary[kMessageFriendsFromUserKey] = value }
    if let value = reportStatus { dictionary[kMessageFriendsReportStatusKey] = value }
    if let value = toUser { dictionary[kMessageFriendsToUserKey] = value }
    if let value = name { dictionary[kMessageFriendsNameKey] = value }
    if let value = text { dictionary[kMessageFriendsTextKey] = value }
    if let value = imageUrl { dictionary[kMessageFriendsImageUrlKey] = value }
    if let value = type { dictionary[kMessageFriendsTypeKey] = value }
    if let value = messageId { dictionary[kMessageFriendsMessageIdKey] = value }
    if let value = timestamp { dictionary[kMessageFriendsTimestampKey] = value }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.readStatus = aDecoder.decodeObject(forKey: kMessageFriendsReadStatusKey) as? Int
    self.fromUser = aDecoder.decodeObject(forKey: kMessageFriendsFromUserKey) as? Int
    self.reportStatus = aDecoder.decodeObject(forKey: kMessageFriendsReportStatusKey) as? Int
    self.toUser = aDecoder.decodeObject(forKey: kMessageFriendsToUserKey) as? Int
    self.name = aDecoder.decodeObject(forKey: kMessageFriendsNameKey) as? String
    self.text = aDecoder.decodeObject(forKey: kMessageFriendsTextKey) as? String
    self.imageUrl = aDecoder.decodeObject(forKey: kMessageFriendsImageUrlKey) as? String
    self.type = aDecoder.decodeObject(forKey: kMessageFriendsTypeKey) as? Int
    self.messageId = aDecoder.decodeObject(forKey: kMessageFriendsMessageIdKey) as? Int
    self.timestamp = aDecoder.decodeObject(forKey: kMessageFriendsTimestampKey) as? String
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(readStatus, forKey: kMessageFriendsReadStatusKey)
    aCoder.encode(fromUser, forKey: kMessageFriendsFromUserKey)
    aCoder.encode(reportStatus, forKey: kMessageFriendsReportStatusKey)
    aCoder.encode(toUser, forKey: kMessageFriendsToUserKey)
    aCoder.encode(name, forKey: kMessageFriendsNameKey)
    aCoder.encode(text, forKey: kMessageFriendsTextKey)
    aCoder.encode(imageUrl, forKey: kMessageFriendsImageUrlKey)
    aCoder.encode(type, forKey: kMessageFriendsTypeKey)
    aCoder.encode(messageId, forKey: kMessageFriendsMessageIdKey)
    aCoder.encode(timestamp, forKey: kMessageFriendsTimestampKey)
  }

}
