//
//  RZMatchMaker.swift
//
//  Created by goyal on 30/04/18
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

public class RZMatchMaker: NSCoding {
    
    // MARK: Declaration for string constants to be used to decode and also serialize.
    private let kRZMatchMakerNameKey: String = "name"
    private let kRZMatchMakerImageIdKey: String = "image_id"
    private let kRZMatchMakerLikesKey: String = "likes"
    private let kRZMatchMakerTimestampKey: String = "timestamp"
    private let kRZMatchMakerUserLikeKey: String = "user_like"
    private let kRZMatchMakerRepostUserIdKey: String = "repost_user_id"
    private let kRZMatchMakerPostIdKey: String = "post_id"
    private let kRZMatchMakerShareKey: String = "share"
    private let kRZMatchMakerCommentsKey: String = "comments"
    private let kRZMatchMakerImageUrlKey: String = "image_url"
    private let kRZMatchMakerUserIdKey: String = "user_id"
    private let kRZMatchMakerRepostUserNameKey: String = "repost_user_name"
    private let kRZMatchMakerUserImageKey: String = "user_image"
    private let kRZMatchMakerCaptionKey: String = "caption"
    
    // MARK: Properties
    public var name: String?
    public var imageId: Int?
    public var likes: Int?
    public var timestamp: Int?
    public var userLike: String?
    public var repostUserId: Int?
    public var postId: Int?
    public var share: Int?
    public var comments: Int?
    public var imageUrl: String?
    public var userId: Int?
    public var repostUserName: String?
    public var userImage: String?
    public var caption: String?
    
    // MARK: SwiftyJSON Initalizers
    /**
     Initates the instance based on the object
     - parameter object: The object of either Dictionary or Array kind that was passed.
     - returns: An initalized instance of the class.
     */
    convenience public init(object: Any) {
        self.init(json: JSON(object))
    }
    
    /**
     Initates the instance based on the JSON that was passed.
     - parameter json: JSON object from SwiftyJSON.
     - returns: An initalized instance of the class.
     */
    public init(json: JSON) {
        name = json[kRZMatchMakerNameKey].string
        imageId = json[kRZMatchMakerImageIdKey].int
        likes = json[kRZMatchMakerLikesKey].int
        timestamp = json[kRZMatchMakerTimestampKey].int
        userLike = json[kRZMatchMakerUserLikeKey].string
        repostUserId = json[kRZMatchMakerRepostUserIdKey].int
        postId = json[kRZMatchMakerPostIdKey].int
        share = json[kRZMatchMakerShareKey].int
        comments = json[kRZMatchMakerCommentsKey].int
        imageUrl = json[kRZMatchMakerImageUrlKey].string
        userId = json[kRZMatchMakerUserIdKey].int
        repostUserName = json[kRZMatchMakerRepostUserNameKey].string
        userImage = json[kRZMatchMakerUserImageKey].string
        caption = json[kRZMatchMakerCaptionKey].string
    }
    
    /**
     Generates description of the object in the form of a NSDictionary.
     - returns: A Key value pair containing all valid values in the object.
     */
    public func dictionaryRepresentation() -> [String: Any] {
        var dictionary: [String: Any] = [:]
        if let value = name { dictionary[kRZMatchMakerNameKey] = value }
        if let value = imageId { dictionary[kRZMatchMakerImageIdKey] = value }
        if let value = likes { dictionary[kRZMatchMakerLikesKey] = value }
        if let value = timestamp { dictionary[kRZMatchMakerTimestampKey] = value }
        if let value = userLike { dictionary[kRZMatchMakerUserLikeKey] = value }
        if let value = repostUserId { dictionary[kRZMatchMakerRepostUserIdKey] = value }
        if let value = postId { dictionary[kRZMatchMakerPostIdKey] = value }
        if let value = share { dictionary[kRZMatchMakerShareKey] = value }
        if let value = comments { dictionary[kRZMatchMakerCommentsKey] = value }
        if let value = imageUrl { dictionary[kRZMatchMakerImageUrlKey] = value }
        if let value = userId { dictionary[kRZMatchMakerUserIdKey] = value }
        if let value = repostUserName { dictionary[kRZMatchMakerRepostUserNameKey] = value }
        if let value = userImage { dictionary[kRZMatchMakerUserImageKey] = value }
        if let value = caption { dictionary[kRZMatchMakerCaptionKey] = value }
        return dictionary
    }
    
    // MARK: NSCoding Protocol
    required public init(coder aDecoder: NSCoder) {
        self.name = aDecoder.decodeObject(forKey: kRZMatchMakerNameKey) as? String
        self.imageId = aDecoder.decodeObject(forKey: kRZMatchMakerImageIdKey) as? Int
        self.likes = aDecoder.decodeObject(forKey: kRZMatchMakerLikesKey) as? Int
        self.timestamp = aDecoder.decodeObject(forKey: kRZMatchMakerTimestampKey) as? Int
        self.userLike = aDecoder.decodeObject(forKey: kRZMatchMakerUserLikeKey) as? String
        self.repostUserId = aDecoder.decodeObject(forKey: kRZMatchMakerRepostUserIdKey) as? Int
        self.postId = aDecoder.decodeObject(forKey: kRZMatchMakerPostIdKey) as? Int
        self.share = aDecoder.decodeObject(forKey: kRZMatchMakerShareKey) as? Int
        self.comments = aDecoder.decodeObject(forKey: kRZMatchMakerCommentsKey) as? Int
        self.imageUrl = aDecoder.decodeObject(forKey: kRZMatchMakerImageUrlKey) as? String
        self.userId = aDecoder.decodeObject(forKey: kRZMatchMakerUserIdKey) as? Int
        self.repostUserName = aDecoder.decodeObject(forKey: kRZMatchMakerRepostUserNameKey) as? String
        self.userImage = aDecoder.decodeObject(forKey: kRZMatchMakerUserImageKey) as? String
        self.caption = aDecoder.decodeObject(forKey: kRZMatchMakerCaptionKey) as? String
    }
    
    public func encode(with aCoder: NSCoder) {
        aCoder.encode(name, forKey: kRZMatchMakerNameKey)
        aCoder.encode(imageId, forKey: kRZMatchMakerImageIdKey)
        aCoder.encode(likes, forKey: kRZMatchMakerLikesKey)
        aCoder.encode(timestamp, forKey: kRZMatchMakerTimestampKey)
        aCoder.encode(userLike, forKey: kRZMatchMakerUserLikeKey)
        aCoder.encode(repostUserId, forKey: kRZMatchMakerRepostUserIdKey)
        aCoder.encode(postId, forKey: kRZMatchMakerPostIdKey)
        aCoder.encode(share, forKey: kRZMatchMakerShareKey)
        aCoder.encode(comments, forKey: kRZMatchMakerCommentsKey)
        aCoder.encode(imageUrl, forKey: kRZMatchMakerImageUrlKey)
        aCoder.encode(userId, forKey: kRZMatchMakerUserIdKey)
        aCoder.encode(repostUserName, forKey: kRZMatchMakerRepostUserNameKey)
        aCoder.encode(userImage, forKey: kRZMatchMakerUserImageKey)
        aCoder.encode(caption, forKey: kRZMatchMakerCaptionKey)
    }
    
}
