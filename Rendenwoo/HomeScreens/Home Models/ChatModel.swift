//
//  ChatModel.swift
//
//  Created by goyal on 11/07/18
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

public class ChatModel: NSCoding {
    
    // MARK: Declaration for string constants to be used to decode and also serialize.
    private let kChatModelToUserKey: String = "to_user"
    private let kChatModelFromUserNameKey: String = "from_user_name"
    private let kChatModelReportStatusKey: String = "report_status"
    private let kChatModelToUserNameKey: String = "to_user_name"
    private let kChatModelFromUserKey: String = "from_user"
    private let kChatModelTextKey: String = "text"
    private let kChatModelReadStatusKey: String = "read_status"
    private let kChatModelTypeKey: String = "type"
    private let kChatModelMessageIdKey: String = "message_id"
    private let kChatModelTimestampKey: String = "timestamp"
    private let kChatModelFromUserImageKey: String = "from_user_image"
    private let kChatModelToImageUrlKey: String = "to_image_url"
    
    // MARK: Properties
    public var toUser: Int?
    public var fromUserName: String?
    public var reportStatus: Int?
    public var toUserName: String?
    public var fromUser: Int?
    public var text: String?
    public var readStatus: Int?
    public var type: Int?
    public var messageId: Int?
    public var timestamp: String?
    public var fromUserImage: String?
    public var toImageUrl: String?
    
    // MARK: SwiftyJSON Initalizers
    /**
     Initates the instance based on the object
     - parameter object: The object of either Dictionary or Array kind that was passed.
     - returns: An initalized instance of the class.
     */
    convenience public init(object: Any) {
        self.init(json: JSON(object))
    }
    
    /**
     Initates the instance based on the JSON that was passed.
     - parameter json: JSON object from SwiftyJSON.
     - returns: An initalized instance of the class.
     */
    public init(json: JSON) {
        toUser = json[kChatModelToUserKey].int
        fromUserName = json[kChatModelFromUserNameKey].string
        reportStatus = json[kChatModelReportStatusKey].int
        toUserName = json[kChatModelToUserNameKey].string
        fromUser = json[kChatModelFromUserKey].int
        text = json[kChatModelTextKey].string
        readStatus = json[kChatModelReadStatusKey].int
        type = json[kChatModelTypeKey].int
        messageId = json[kChatModelMessageIdKey].int
        timestamp = json[kChatModelTimestampKey].string
        fromUserImage = json[kChatModelFromUserImageKey].string
        toImageUrl = json[kChatModelToImageUrlKey].string
    }
    
    /**
     Generates description of the object in the form of a NSDictionary.
     - returns: A Key value pair containing all valid values in the object.
     */
    public func dictionaryRepresentation() -> [String: Any] {
        var dictionary: [String: Any] = [:]
        if let value = toUser { dictionary[kChatModelToUserKey] = value }
        if let value = fromUserName { dictionary[kChatModelFromUserNameKey] = value }
        if let value = reportStatus { dictionary[kChatModelReportStatusKey] = value }
        if let value = toUserName { dictionary[kChatModelToUserNameKey] = value }
        if let value = fromUser { dictionary[kChatModelFromUserKey] = value }
        if let value = text { dictionary[kChatModelTextKey] = value }
        if let value = readStatus { dictionary[kChatModelReadStatusKey] = value }
        if let value = type { dictionary[kChatModelTypeKey] = value }
        if let value = messageId { dictionary[kChatModelMessageIdKey] = value }
        if let value = timestamp { dictionary[kChatModelTimestampKey] = value }
        if let value = fromUserImage { dictionary[kChatModelFromUserImageKey] = value }
        if let value = toImageUrl { dictionary[kChatModelToImageUrlKey] = value }
        return dictionary
    }
    
    // MARK: NSCoding Protocol
    required public init(coder aDecoder: NSCoder) {
        self.toUser = aDecoder.decodeObject(forKey: kChatModelToUserKey) as? Int
        self.fromUserName = aDecoder.decodeObject(forKey: kChatModelFromUserNameKey) as? String
        self.reportStatus = aDecoder.decodeObject(forKey: kChatModelReportStatusKey) as? Int
        self.toUserName = aDecoder.decodeObject(forKey: kChatModelToUserNameKey) as? String
        self.fromUser = aDecoder.decodeObject(forKey: kChatModelFromUserKey) as? Int
        self.text = aDecoder.decodeObject(forKey: kChatModelTextKey) as? String
        self.readStatus = aDecoder.decodeObject(forKey: kChatModelReadStatusKey) as? Int
        self.type = aDecoder.decodeObject(forKey: kChatModelTypeKey) as? Int
        self.messageId = aDecoder.decodeObject(forKey: kChatModelMessageIdKey) as? Int
        self.timestamp = aDecoder.decodeObject(forKey: kChatModelTimestampKey) as? String
        self.fromUserImage = aDecoder.decodeObject(forKey: kChatModelFromUserImageKey) as? String
        self.toImageUrl = aDecoder.decodeObject(forKey: kChatModelToImageUrlKey) as? String
    }
    
    public func encode(with aCoder: NSCoder) {
        aCoder.encode(toUser, forKey: kChatModelToUserKey)
        aCoder.encode(fromUserName, forKey: kChatModelFromUserNameKey)
        aCoder.encode(reportStatus, forKey: kChatModelReportStatusKey)
        aCoder.encode(toUserName, forKey: kChatModelToUserNameKey)
        aCoder.encode(fromUser, forKey: kChatModelFromUserKey)
        aCoder.encode(text, forKey: kChatModelTextKey)
        aCoder.encode(readStatus, forKey: kChatModelReadStatusKey)
        aCoder.encode(type, forKey: kChatModelTypeKey)
        aCoder.encode(messageId, forKey: kChatModelMessageIdKey)
        aCoder.encode(timestamp, forKey: kChatModelTimestampKey)
        aCoder.encode(fromUserImage, forKey: kChatModelFromUserImageKey)
        aCoder.encode(toImageUrl, forKey: kChatModelToImageUrlKey)
    }
    
}
