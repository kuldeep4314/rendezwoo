//
//  RZTagInfo.swift
//
//  Created by goyal on 24/05/18
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

public class RZTagInfo: NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private let kRZTagInfoStatusKey: String = "status"
  private let kRZTagInfoXposKey: String = "xpos"
  private let kRZTagInfoUserIdKey: String = "user_id"
  private let kRZTagInfoNameKey: String = "name"
  private let kRZTagInfoYposKey: String = "ypos"

  // MARK: Properties
  public var status: Int?
  public var xpos: String?
  public var userId: Int?
  public var name: String?
  public var ypos: String?

  // MARK: SwiftyJSON Initalizers
  /**
   Initates the instance based on the object
   - parameter object: The object of either Dictionary or Array kind that was passed.
   - returns: An initalized instance of the class.
  */
  convenience public init(object: Any) {
    self.init(json: JSON(object))
  }

  /**
   Initates the instance based on the JSON that was passed.
   - parameter json: JSON object from SwiftyJSON.
   - returns: An initalized instance of the class.
  */
  public init(json: JSON) {
    status = json[kRZTagInfoStatusKey].int
    xpos = json[kRZTagInfoXposKey].string
    userId = json[kRZTagInfoUserIdKey].int
    name = json[kRZTagInfoNameKey].string
    ypos = json[kRZTagInfoYposKey].string
  }

  /**
   Generates description of the object in the form of a NSDictionary.
   - returns: A Key value pair containing all valid values in the object.
  */
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = status { dictionary[kRZTagInfoStatusKey] = value }
    if let value = xpos { dictionary[kRZTagInfoXposKey] = value }
    if let value = userId { dictionary[kRZTagInfoUserIdKey] = value }
    if let value = name { dictionary[kRZTagInfoNameKey] = value }
    if let value = ypos { dictionary[kRZTagInfoYposKey] = value }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.status = aDecoder.decodeObject(forKey: kRZTagInfoStatusKey) as? Int
    self.xpos = aDecoder.decodeObject(forKey: kRZTagInfoXposKey) as? String
    self.userId = aDecoder.decodeObject(forKey: kRZTagInfoUserIdKey) as? Int
    self.name = aDecoder.decodeObject(forKey: kRZTagInfoNameKey) as? String
    self.ypos = aDecoder.decodeObject(forKey: kRZTagInfoYposKey) as? String
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(status, forKey: kRZTagInfoStatusKey)
    aCoder.encode(xpos, forKey: kRZTagInfoXposKey)
    aCoder.encode(userId, forKey: kRZTagInfoUserIdKey)
    aCoder.encode(name, forKey: kRZTagInfoNameKey)
    aCoder.encode(ypos, forKey: kRZTagInfoYposKey)
  }

}
