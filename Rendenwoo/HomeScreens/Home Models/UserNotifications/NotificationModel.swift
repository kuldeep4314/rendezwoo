//
//  NotificationModel.swift
//
//  Created by goyal on 11/07/18
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

public class NotificationModel: NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private let kNotificationModelReadStatusKey: String = "read_status"
  private let kNotificationModelStatusKey: String = "status"
  private let kNotificationModelInternalIdentifierKey: String = "id"
  private let kNotificationModelTextKey: String = "text"
  private let kNotificationModelNamesKey: String = "names"
  private let kNotificationModelToNameKey: String = "to_name"
  private let kNotificationModelImageUrlKey: String = "image_url"
  private let kNotificationModelByNameKey: String = "by_name"
  private let kNotificationModelTimestampKey: String = "timestamp"

  // MARK: Properties
  public var readStatus: Int?
  public var status: String?
  public var internalIdentifier: Int?
  public var text: String?
  public var names: [String]?
  public var toName: String?
  public var imageUrl: String?
  public var byName: String?
  public var timestamp: Int?

  // MARK: SwiftyJSON Initalizers
  /**
   Initates the instance based on the object
   - parameter object: The object of either Dictionary or Array kind that was passed.
   - returns: An initalized instance of the class.
  */
  convenience public init(object: Any) {
    self.init(json: JSON(object))
  }

  /**
   Initates the instance based on the JSON that was passed.
   - parameter json: JSON object from SwiftyJSON.
   - returns: An initalized instance of the class.
  */
  public init(json: JSON) {
    readStatus = json[kNotificationModelReadStatusKey].int
    status = json[kNotificationModelStatusKey].string
    internalIdentifier = json[kNotificationModelInternalIdentifierKey].int
    text = json[kNotificationModelTextKey].string
    if let items = json[kNotificationModelNamesKey].array { names = items.map { $0.stringValue } }
    toName = json[kNotificationModelToNameKey].string
    imageUrl = json[kNotificationModelImageUrlKey].string
    byName = json[kNotificationModelByNameKey].string
    timestamp = json[kNotificationModelTimestampKey].int
  }

  /**
   Generates description of the object in the form of a NSDictionary.
   - returns: A Key value pair containing all valid values in the object.
  */
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = readStatus { dictionary[kNotificationModelReadStatusKey] = value }
    if let value = status { dictionary[kNotificationModelStatusKey] = value }
    if let value = internalIdentifier { dictionary[kNotificationModelInternalIdentifierKey] = value }
    if let value = text { dictionary[kNotificationModelTextKey] = value }
    if let value = names { dictionary[kNotificationModelNamesKey] = value }
    if let value = toName { dictionary[kNotificationModelToNameKey] = value }
    if let value = imageUrl { dictionary[kNotificationModelImageUrlKey] = value }
    if let value = byName { dictionary[kNotificationModelByNameKey] = value }
    if let value = timestamp { dictionary[kNotificationModelTimestampKey] = value }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.readStatus = aDecoder.decodeObject(forKey: kNotificationModelReadStatusKey) as? Int
    self.status = aDecoder.decodeObject(forKey: kNotificationModelStatusKey) as? String
    self.internalIdentifier = aDecoder.decodeObject(forKey: kNotificationModelInternalIdentifierKey) as? Int
    self.text = aDecoder.decodeObject(forKey: kNotificationModelTextKey) as? String
    self.names = aDecoder.decodeObject(forKey: kNotificationModelNamesKey) as? [String]
    self.toName = aDecoder.decodeObject(forKey: kNotificationModelToNameKey) as? String
    self.imageUrl = aDecoder.decodeObject(forKey: kNotificationModelImageUrlKey) as? String
    self.byName = aDecoder.decodeObject(forKey: kNotificationModelByNameKey) as? String
    self.timestamp = aDecoder.decodeObject(forKey: kNotificationModelTimestampKey) as? Int
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(readStatus, forKey: kNotificationModelReadStatusKey)
    aCoder.encode(status, forKey: kNotificationModelStatusKey)
    aCoder.encode(internalIdentifier, forKey: kNotificationModelInternalIdentifierKey)
    aCoder.encode(text, forKey: kNotificationModelTextKey)
    aCoder.encode(names, forKey: kNotificationModelNamesKey)
    aCoder.encode(toName, forKey: kNotificationModelToNameKey)
    aCoder.encode(imageUrl, forKey: kNotificationModelImageUrlKey)
    aCoder.encode(byName, forKey: kNotificationModelByNameKey)
    aCoder.encode(timestamp, forKey: kNotificationModelTimestampKey)
  }

}
