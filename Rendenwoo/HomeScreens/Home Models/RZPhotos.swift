//
//  RZPhotos.swift
//
//  Created by goyal on 24/05/18
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

public class RZPhotos: NSCoding {
    
    // MARK: Declaration for string constants to be used to decode and also serialize.
    private let kRZPhotosImageIdKey: String = "image_id"
    private let kRZPhotosUserIdKey: String = "user_id"
    private let kRZPhotosTimestampKey: String = "timestamp"
    private let kRZPhotosImageUrlKey: String = "image_url"
    private let kRZPhotosTagInfoKey: String = "tag_info"
    
    // MARK: Properties
    public var imageId: Int?
    public var userId: Int?
    public var timestamp: String?
    public var imageUrl: String?
    public var tagInfo: [RZTagInfo]?
    
    // MARK: SwiftyJSON Initalizers
    /**
     Initates the instance based on the object
     - parameter object: The object of either Dictionary or Array kind that was passed.
     - returns: An initalized instance of the class.
     */
    convenience public init(object: Any) {
        self.init(json: JSON(object))
    }
    
    /**
     Initates the instance based on the JSON that was passed.
     - parameter json: JSON object from SwiftyJSON.
     - returns: An initalized instance of the class.
     */
    public init(json: JSON) {
        imageId = json[kRZPhotosImageIdKey].int
        userId = json[kRZPhotosUserIdKey].int
        timestamp = json[kRZPhotosTimestampKey].string
        imageUrl = json[kRZPhotosImageUrlKey].string
        if let items = json[kRZPhotosTagInfoKey].array { tagInfo = items.map { RZTagInfo(json: $0) } }
    }
    
    /**
     Generates description of the object in the form of a NSDictionary.
     - returns: A Key value pair containing all valid values in the object.
     */
    public func dictionaryRepresentation() -> [String: Any] {
        var dictionary: [String: Any] = [:]
        if let value = imageId { dictionary[kRZPhotosImageIdKey] = value }
        if let value = userId { dictionary[kRZPhotosUserIdKey] = value }
        if let value = timestamp { dictionary[kRZPhotosTimestampKey] = value }
        if let value = imageUrl { dictionary[kRZPhotosImageUrlKey] = value }
        if let value = tagInfo { dictionary[kRZPhotosTagInfoKey] = value.map { $0.dictionaryRepresentation() } }
        return dictionary
    }
    
    // MARK: NSCoding Protocol
    required public init(coder aDecoder: NSCoder) {
        self.imageId = aDecoder.decodeObject(forKey: kRZPhotosImageIdKey) as? Int
        self.userId = aDecoder.decodeObject(forKey: kRZPhotosUserIdKey) as? Int
        self.timestamp = aDecoder.decodeObject(forKey: kRZPhotosTimestampKey) as? String
        self.imageUrl = aDecoder.decodeObject(forKey: kRZPhotosImageUrlKey) as? String
        self.tagInfo = aDecoder.decodeObject(forKey: kRZPhotosTagInfoKey) as? [RZTagInfo]
    }
    
    public func encode(with aCoder: NSCoder) {
        aCoder.encode(imageId, forKey: kRZPhotosImageIdKey)
        aCoder.encode(userId, forKey: kRZPhotosUserIdKey)
        aCoder.encode(timestamp, forKey: kRZPhotosTimestampKey)
        aCoder.encode(imageUrl, forKey: kRZPhotosImageUrlKey)
        aCoder.encode(tagInfo, forKey: kRZPhotosTagInfoKey)
    }
    
}
