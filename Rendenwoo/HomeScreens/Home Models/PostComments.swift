//
//  PostComments.swift
//
//  Created by goyal on 30/04/18
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

public class PostComments: NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private let kPostCommentsNameKey: String = "name"
  private let kPostCommentsImageIdKey: String = "image_id"
  private let kPostCommentsCommentTimestampKey: String = "comment_timestamp"
  private let kPostCommentsPostIdKey: String = "post_id"
  private let kPostCommentsCommentIdKey: String = "comment_id"
  private let kPostCommentsCommentKey: String = "comment"
  private let kPostCommentsUserIdKey: String = "user_id"
  private let kPostCommentsCommentUserIdKey: String = "comment_user_id"
  private let kPostCommentsTimestampKey: String = "timestamp"
  private let kPostCommentsUserImageKey: String = "user_image"
  private let kPostCommentsCaptionKey: String = "caption"

  // MARK: Properties
  public var name: String?
  public var imageId: Int?
  public var commentTimestamp: Int?
  public var postId: Int?
  public var commentId: Int?
  public var comment: String?
  public var userId: Int?
  public var commentUserId: Int?
  public var timestamp: Int?
  public var userImage: String?
  public var caption: String?

  // MARK: SwiftyJSON Initalizers
  /**
   Initates the instance based on the object
   - parameter object: The object of either Dictionary or Array kind that was passed.
   - returns: An initalized instance of the class.
  */
  convenience public init(object: Any) {
    self.init(json: JSON(object))
  }

  /**
   Initates the instance based on the JSON that was passed.
   - parameter json: JSON object from SwiftyJSON.
   - returns: An initalized instance of the class.
  */
  public init(json: JSON) {
    name = json[kPostCommentsNameKey].string
    imageId = json[kPostCommentsImageIdKey].int
    commentTimestamp = json[kPostCommentsCommentTimestampKey].int
    postId = json[kPostCommentsPostIdKey].int
    commentId = json[kPostCommentsCommentIdKey].int
    comment = json[kPostCommentsCommentKey].string
    userId = json[kPostCommentsUserIdKey].int
    commentUserId = json[kPostCommentsCommentUserIdKey].int
    timestamp = json[kPostCommentsTimestampKey].int
    userImage = json[kPostCommentsUserImageKey].string
    caption = json[kPostCommentsCaptionKey].string
  }

  /**
   Generates description of the object in the form of a NSDictionary.
   - returns: A Key value pair containing all valid values in the object.
  */
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = name { dictionary[kPostCommentsNameKey] = value }
    if let value = imageId { dictionary[kPostCommentsImageIdKey] = value }
    if let value = commentTimestamp { dictionary[kPostCommentsCommentTimestampKey] = value }
    if let value = postId { dictionary[kPostCommentsPostIdKey] = value }
    if let value = commentId { dictionary[kPostCommentsCommentIdKey] = value }
    if let value = comment { dictionary[kPostCommentsCommentKey] = value }
    if let value = userId { dictionary[kPostCommentsUserIdKey] = value }
    if let value = commentUserId { dictionary[kPostCommentsCommentUserIdKey] = value }
    if let value = timestamp { dictionary[kPostCommentsTimestampKey] = value }
    if let value = userImage { dictionary[kPostCommentsUserImageKey] = value }
    if let value = caption { dictionary[kPostCommentsCaptionKey] = value }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.name = aDecoder.decodeObject(forKey: kPostCommentsNameKey) as? String
    self.imageId = aDecoder.decodeObject(forKey: kPostCommentsImageIdKey) as? Int
    self.commentTimestamp = aDecoder.decodeObject(forKey: kPostCommentsCommentTimestampKey) as? Int
    self.postId = aDecoder.decodeObject(forKey: kPostCommentsPostIdKey) as? Int
    self.commentId = aDecoder.decodeObject(forKey: kPostCommentsCommentIdKey) as? Int
    self.comment = aDecoder.decodeObject(forKey: kPostCommentsCommentKey) as? String
    self.userId = aDecoder.decodeObject(forKey: kPostCommentsUserIdKey) as? Int
    self.commentUserId = aDecoder.decodeObject(forKey: kPostCommentsCommentUserIdKey) as? Int
    self.timestamp = aDecoder.decodeObject(forKey: kPostCommentsTimestampKey) as? Int
    self.userImage = aDecoder.decodeObject(forKey: kPostCommentsUserImageKey) as? String
    self.caption = aDecoder.decodeObject(forKey: kPostCommentsCaptionKey) as? String
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(name, forKey: kPostCommentsNameKey)
    aCoder.encode(imageId, forKey: kPostCommentsImageIdKey)
    aCoder.encode(commentTimestamp, forKey: kPostCommentsCommentTimestampKey)
    aCoder.encode(postId, forKey: kPostCommentsPostIdKey)
    aCoder.encode(commentId, forKey: kPostCommentsCommentIdKey)
    aCoder.encode(comment, forKey: kPostCommentsCommentKey)
    aCoder.encode(userId, forKey: kPostCommentsUserIdKey)
    aCoder.encode(commentUserId, forKey: kPostCommentsCommentUserIdKey)
    aCoder.encode(timestamp, forKey: kPostCommentsTimestampKey)
    aCoder.encode(userImage, forKey: kPostCommentsUserImageKey)
    aCoder.encode(caption, forKey: kPostCommentsCaptionKey)
  }

}
