



//
//  LocationViewController.swift
//  Rendenwoo
//
//  Created by goyal on 01/03/18.
//  Copyright © 2018 ankita. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlaces

class LocationViewController: UIViewController ,UITableViewDataSource,UITableViewDelegate , CLLocationManagerDelegate{

    @IBOutlet weak var topViewHeight: NSLayoutConstraint!
    @IBOutlet weak var tableView: UITableView!
    
    var resultsViewController: GMSAutocompleteResultsViewController?
    var searchController: UISearchController?
    var locationDetails = JSON(JSON.self)
    override func viewDidLoad() {
        super.viewDidLoad()
    self.tableView.tableFooterView = UIView()
         UIApplication.shared.statusBarStyle = .default
        // Do any additional setup after loading the view.
        if UIDevice.isIphoneX == true
        {
            topViewHeight.constant = 84
        }
       
        self.tableView.rowHeight = UITableViewAutomaticDimension
        
        let parameters : Parameters = ["user_id" : standard.value(forKey: "userid")!]
        
        self.fetchLocationApiCall(parameters: parameters)
    }
    //MARK:- setupSearchView function
    func setupSearchView(){
        
        let autocompleteController = GMSAutocompleteViewController()
      
        UINavigationBar.appearance().tintColor = appColor
        autocompleteController.delegate = self
        
        
        let filter = GMSAutocompleteFilter()
       
            filter.type = .noFilter
       
        autocompleteController.autocompleteFilter = filter
        present(autocompleteController, animated: false, completion: nil)
    }
    
    @IBAction func btnAddLocationTapped(_ sender: Any) {
        self.setupSearchView()
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
       
    }
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func btnBackTapped(_ sender: Any) {
   self.navigationController?.popViewController(animated: true)
    }
    //MARK: - UITABLEVIEW DATA SOURCE
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if  self.locationDetails["added_location"].stringValue != ""{
           return 2
        }
        else{
            return 1
        }
      
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! LocationTableViewCell
        
        if indexPath.row == 0 {
            cell.lblLocation.text = self.locationDetails["current_location"].stringValue
            cell.btnCheck.setImage(UIImage.init(named: "imgSelected"), for: .normal)
        }
        else{
              cell.lblLocation.text = self.locationDetails["added_location"].stringValue
             cell.btnCheck.setImage(#imageLiteral(resourceName: "imgEdit"), for: .normal)
        }
        cell.bgView.addshodowToView()
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        if indexPath.row == 1 {
            
            self.setupSearchView()
        }
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellHeader") as! LocationTableViewCell
        
        
        return cell.contentView
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
          return 44
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellFooter") as! LocationTableViewCell
        
        cell.btnAddLocation.layer.cornerRadius = 5
        cell.btnAddLocation.backgroundColor = appColor
        return cell.contentView
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        if self.locationDetails["added_location"].stringValue != ""{
            return 0
        }
        else{
             return 70
        }
        
    }
    //MARK:- Fetch Location API Call
    func fetchLocationApiCall(parameters: Parameters){
        AccountApiInterface.sharedInstance.postRequestWithParam(fromViewController: self, actionName: "list_of_user_location", dictParam: parameters as [String : AnyObject], isShowIndicator: true, success: { (json) in
            
            
           self.locationDetails = json["data"]
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
        }) { (error) in
            self.showAlert(messageStr: error)
        }
        
    }
    
    
    
    
    //MARK:- SelectedLocationApiCall  Function
    
    func selectedLocationApiCall(parameters: Parameters){
        AccountApiInterface.sharedInstance.postRequestWithParam(fromViewController: self, actionName: "addLocation", dictParam: parameters as [String : AnyObject], isShowIndicator: true, success: { (json) in
             self.locationDetails = json["data"]
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
            
        print(json)
        }) { (error) in
            self.showAlert(messageStr: error)
        }
        
    }
    
    
}


extension LocationViewController: GMSAutocompleteViewControllerDelegate {
    
    // Handle the user's selection.
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        print("Place name: \(place.name)")
        

        let parameter : Parameters = ["user_id":"\(standard.value(forKey: "userid")!)",
            
            "location": place.name,"latitude" : place.coordinate.latitude,"longitude": place.coordinate.longitude,"action" : "1"]
        
        self.selectedLocationApiCall(parameters: parameter)
        
        
        dismiss(animated: true, completion: nil)
         self.tableView.reloadData()
            }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        // TODO: handle the error.
        print("Error: ", error.localizedDescription)
    }
    
    // User canceled the operation.
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        dismiss(animated: true, completion: nil)
    }
    
    // Turn the network activity indicator on and off again.
    func didRequestAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    func didUpdateAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
    
}




