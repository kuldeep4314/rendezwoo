//
//  PhotoCollectionViewCell.swift
//  Rendenwoo
//
//  Created by goyal on 05/03/18.
//  Copyright © 2018 ankita. All rights reserved.
//

import UIKit

class PhotoCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var btnRmoveAdd: UIButton!
    
    @IBOutlet weak var top: NSLayoutConstraint!
    @IBOutlet weak var trailing: NSLayoutConstraint!
    @IBOutlet weak var btnTag: UIButton!
    @IBOutlet weak var btnUntag: UIButton!
}
