//
//  PostCommentViewController.swift
//  Rendenwoo
//
//  Created by goyal on 31/03/18.
//  Copyright © 2018 ankita. All rights reserved.
//

import UIKit

class PostCommentViewController: UIViewController ,UITableViewDelegate,UITableViewDataSource ,UITextFieldDelegate{
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var txtComment: UITextField!
    
    
    var postInfo : RZMatchMaker?
    var commentArr = [PostComments]()
    @IBOutlet weak var lblTilte: UILabel!
    @IBOutlet weak var topViewHeight: NSLayoutConstraint!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.tableFooterView = UIView()
        self.tableView.rowHeight = UITableViewAutomaticDimension
        
        if UIDevice.isIphoneX == true{
            topViewHeight.constant = 84
        }

        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
   
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        let parameter : Parameters = ["post_id": postInfo?.postId ?? 0]
        print(parameter)
       self.fetchCommentsApiCall(parameters: parameter)
    }
    
    override func viewDidAppear(_ animated: Bool) {
         super.viewDidAppear(animated)
       
    }
    @IBAction func btnBackTapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnSendTapped(_ sender: UIButton) {
        
        if txtComment.text != "" || txtComment.text != nil{
        
            let parameters : Parameters =  ["user_id":standard.value(forKey: "userid")!,"post_user_id": postInfo?.userId ?? 0 ,"comment" :txtComment.text?.base64Encoded() ?? "","post_id"    :postInfo?.postId ?? 0]
            
            self.postCommentsApiCall(parameters: parameters)
            
        }
    }
    
    
    @objc func  moreBtnTapped(sender : UIButton){
        
        let point = sender.convert(CGPoint.zero, to: self.tableView)
        
        guard let indexPath = self.tableView.indexPathForRow(at: point) else {
            return
        }
    
          print(postInfo?.userId ?? 0)
        if standard.value(forKey: "userid") as? Int == postInfo?.userId {
            
            let array = ["Edit Post","Delete Post","Turn off notifications for this post"]
            let  imgArray = ["imgEditMachMaker","imgDelete","imgNotification"]
            let vc = Menu.init(frame: self.view.frame, array: array,imgArray:imgArray,selectedIndex : indexPath.row,delegateStr : "own")
            vc.delegates = self
            self.view.addSubview(vc)
        }
        else{
         
            let array = ["Hide Post","Turn off notifications for this post","Report this post"]
            
            let  imgArray = ["imgHide","imgNotification","imgRepostPost"]
            let vc = Menu.init(frame: self.view.frame, array: array,imgArray:imgArray,selectedIndex : indexPath.row,delegateStr : "other")
            vc.delegates = self
            self.view.addSubview(vc)
            
        }
        
    }
    
    //MARK:- Post Action Perform Function
    
    @objc func sharePostTapped(sender: UIButton){
        
        let point = sender.convert(CGPoint.zero, to: self.tableView)
        
        guard let indexPath = self.tableView.indexPathForRow(at: point) else {
            return
        }
        
        print(indexPath.row)
        
        let array = ["Share Now (Public)","Send through Rendezwoo Messages","Copy link"]
        let  imgArray = ["imgShareNow","imgRendezwooMsg","imgCopy"]
        let vc = Menu.init(frame: self.view.frame, array: array,imgArray:imgArray,selectedIndex : indexPath.row,delegateStr : "share")
        vc.delegates = self
        self.view.addSubview(vc)
        
        
    }
    func convertPointToIndexPath(sender : UIButton)-> IndexPath{
        
        let point = sender.convert(CGPoint.zero, to: self.tableView)
        
        guard let indexPath = self.tableView.indexPathForRow(at: point) else {
            return IndexPath.init(row: 0, section: 0)
        }
        
        return indexPath
        
    }
    @objc func btnlikeTapped(sender : UIButton){
        
        let indexPath = self.convertPointToIndexPath(sender: sender)
        
        print(indexPath.row)
        
        let cell = self.tableView.cellForRow(at: indexPath) as? MatchmakerTableViewCell
        
        if sender.imageView?.image == #imageLiteral(resourceName: "imgUnlike") {
            sender.setImage(#imageLiteral(resourceName: "imgLike"), for: .normal)
            cell?.lblLike.text = "\( (postInfo?.likes ?? 0) + 1) LIKES"
            sender.transform = CGAffineTransform(scaleX: 0.1, y: 0.1)
            
            UIView.animate(withDuration: 1.0,
                           delay: 0,
                           usingSpringWithDamping: 0.2,
                           initialSpringVelocity: 5.0,
                           options: .curveEaseOut,
                           animations: {
                            
                            sender.transform = .identity
            },
                           completion: {
                            action in
                            
                            
            })
            
            let parameters : Parameters = ["user_id":"\(standard.value(forKey: "userid")!)", "post_id" :postInfo?.postId ?? 0, "post_user_id":postInfo?.userId ?? 0,"action" : "0"]
            
            self.likePostActionApiCall(parameters: parameters, indexPath: indexPath)
            
        }else{
            cell?.lblLike.text = "\( (postInfo?.likes ?? 0) - 1) LIKES"
            sender.setImage(#imageLiteral(resourceName: "imgUnlike"), for: .normal)
            
            let parameters : Parameters = ["user_id":"\(standard.value(forKey: "userid")!)", "post_id" :postInfo?.postId ?? 0, "post_user_id":postInfo?.userId ?? 0,"action" : "1"]
            
            self.likePostActionApiCall(parameters: parameters, indexPath: indexPath)
        }
        
        
       
        
        
        
    }
  
    
    
    @objc func handleTap(_ sender: UITapGestureRecognizer) {
        
     self.txtComment.becomeFirstResponder()
    }
    
    //MARK: - UITABLEVIEW DATA SOURCE
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if section == 0 {
            return 1
        }
        else{
              return commentArr.count
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.section == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! MatchmakerTableViewCell
            
            if postInfo?.repostUserName != "" {
                
                if postInfo?.userId == postInfo?.repostUserId {
                    cell.lblUserName.text =  "\(postInfo?.name ?? "") shared a post."
                }
                else{
                   cell.lblUserName.text =  "\(postInfo?.name ?? "") shared \(postInfo?.repostUserName ?? "")'s post."
                }
             
            }
            else{
                cell.lblUserName.text = postInfo?.name
            }
            
            cell.lblDescription.text = postInfo?.caption?.base64Decoded()
            
            
            let date = NSDate(timeIntervalSince1970:  TimeInterval(postInfo?.timestamp ?? 0))
            
            cell.lblDate.text = date.getDayAndTime()
            
            
            let imageUserURL =  postInfo?.userImage ?? ""
            cell.imgUserPic.sd_setImage(with: URL(string: imageUserURL), placeholderImage: #imageLiteral(resourceName: "imgProfile"), options: [], completed: nil)
        
            let imageURL =   postInfo?.imageUrl ?? ""
            
            if  imageURL == "" {
                cell.imageHeight.constant = 0
                
            }
            else{
                cell.imageHeight.constant = 220
            }
            
            cell.imgPic.sd_setImage(with: URL(string: imageURL), placeholderImage: #imageLiteral(resourceName: "imgPlaceHolder"), options: [], completed: nil)
            
            cell.lblLike.text = "\( postInfo?.likes ?? 0) LIKES"
            cell.lblComments.text = "\( postInfo?.comments ?? 0) COMMENTS"
            cell.lblShare.text = "\( postInfo?.share ?? 0) SHARES"
            
            
            if postInfo?.userLike == "TRUE" {
                cell.btnLike.setImage(#imageLiteral(resourceName: "imgLike"), for: .normal)
            }
            else{
                cell.btnLike.setImage(#imageLiteral(resourceName: "imgUnlike"), for: .normal)
            }
            
            
            
              cell.btnLike.addTarget(self, action: #selector(self.btnlikeTapped(sender:)), for: .touchUpInside)
            
            
            cell.btnMore.addTarget(self, action: #selector(self.moreBtnTapped(sender:)), for: .touchUpInside)
            cell.btnShare.addTarget(self, action: #selector(self.sharePostTapped(sender:)), for: .touchUpInside)
            
            
            let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(_:)))
            
            cell.lblComments.addGestureRecognizer(tap)
            return cell
            
        }
        else{
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "comment", for: indexPath) as! OtherMessageTableViewCell
          
            
              cell.lessWidth.constant = cell.frame.width - 50
                cell.userName.text =   commentArr[indexPath.row].name
            cell.userMessage.text =  commentArr[indexPath.row].comment?.base64Decoded()
            
            let imageURL = commentArr[indexPath.row].userImage ?? ""
            
            
            cell.userImage.sd_setImage(with: URL(string: imageURL), placeholderImage: #imageLiteral(resourceName: "imgProfile"), options: [], completed: nil)

           
            return cell
            
        }
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    //MARK:- UITEXTFIELD DELEGATES
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        textField.resignFirstResponder()
    
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        textField.resignFirstResponder()
    }
    func popViewToMainMeassge(){
        let viewControllers: [UIViewController] = self.navigationController!.viewControllers
        for aViewController in viewControllers {
            if aViewController is Tabbar {
                let vc  = aViewController as! Tabbar
                
                 let getVC = vc.viewControllers![4] as! MessageViewController
                
                vc.setSelectIndex(from: 1, to: 4)
                
                self.navigationController!.popToViewController(aViewController, animated: false)
            }
        }
    }
   
    
    //MARK:- Own Post Action
    func ownPostActionApiCall(parameters: Parameters ,isDelete : Bool){
        AccountApiInterface.sharedInstance.postRequestWithParam(fromViewController: self, actionName: "own_post_action", dictParam: parameters as [String : AnyObject], isShowIndicator: true, success: { (json) in
            print(json)
            
          
            DispatchQueue.main.async {
                self.tableView.reloadData()
                if isDelete == true {
                    self.navigationController?.popViewController(animated: true)
                      appDelegates.window?.rootViewController?.showAlert(messageStr: json["msg"].stringValue)
                }
                
            }
            
        }) { (error) in
              appDelegates.window?.rootViewController?.showAlert(messageStr: error)
        }
        
    }
    
    
    
    //MARK:- hide post from their wall, turn off notifications, report a post matchmaker
    func otherPostActionApiCall(parameters: Parameters , isHide : Bool){
        AccountApiInterface.sharedInstance.postRequestWithParam(fromViewController: self, actionName: "post_action", dictParam: parameters as [String : AnyObject], isShowIndicator: true, success: { (json) in
            print(json)
            
            DispatchQueue.main.async {
                if isHide == true {
                    self.navigationController?.popViewController(animated: true)
                }
                appDelegates.window?.rootViewController?.showAlert(messageStr: json["msg"].stringValue)
               
            }
            
        }) { (error) in
                appDelegates.window?.rootViewController?.showAlert(messageStr: error)
        }
    }
    
    
    //MARK: - Like Post Action
    func likePostActionApiCall(parameters: Parameters ,indexPath : IndexPath){
        AccountApiInterface.sharedInstance.postRequestWithParam(fromViewController: self, actionName: "like_post", dictParam: parameters as [String : AnyObject], isShowIndicator: false, success: { (json) in
            print(json)
            
            
            self.postInfo?.userLike = (  self.postInfo?.userLike == "FALSE") ? "TRUE" : "FALSE"
            
            if   self.postInfo?.userLike == "TRUE" {
                 self.postInfo?.likes =     (  self.postInfo?.likes ?? 0) + 1
                
            }
            else{
                  self.postInfo?.likes =     (  self.postInfo?.likes  ?? 0) - 1
            }
            
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
            
        }) { (error) in
            print(error)
            //self.showAlert(messageStr: error)
        }
        
    }
    //MARK:- Fetch Comments ApiCall  Function
    
    func fetchCommentsApiCall(parameters: Parameters){
        AccountApiInterface.sharedInstance.postRequestWithParam(fromViewController: self, actionName: "fetch_comments", dictParam: parameters as [String : AnyObject], isShowIndicator: true, success: { (json) in
            print(json)
            
            self.postInfo?.comments = json["comment_count"].intValue
            self.commentArr.removeAll()
            for i in 0..<json["data"].arrayValue.count{
                
                let arrTemp = PostComments.init(json: json["data"][i])
                
                self.commentArr.append(arrTemp)
              
            }
              self.commentArr   =    self.commentArr.sorted() {return ($0.timestamp ?? 0 ) > ($1.timestamp ?? 0 )}
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
            
            print(json)
        }) { (error) in
            self.tableView.reloadData()
          //  self.showAlert(messageStr: error)
        }
        
    }
    //MARK:- Post Comments ApiCall  Function
    
    func postCommentsApiCall(parameters: Parameters){
        AccountApiInterface.sharedInstance.postRequestWithParam(fromViewController: self, actionName: "comment", dictParam: parameters as [String : AnyObject], isShowIndicator: true, success: { (json) in
            print(json)
            self.txtComment.text = ""
            self.commentArr.removeAll()
               self.postInfo?.comments = json["comment_count"].intValue
            for i in 0..<json["data"].arrayValue.count{
                
                let arrTemp = PostComments.init(json: json["data"][i])
                
                self.commentArr.append(arrTemp)
                
            }
      self.commentArr   =    self.commentArr.sorted() {return ($0.timestamp ?? 0 ) > ($1.timestamp ?? 0 )}

            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
            
            print(json)
        }) { (error) in
               self.txtComment.text = ""
          //  self.showAlert(messageStr: error)
        }
        
    }
    
    //MARK: - Share Post Api Call
    func sharePostActionApiCall(parameters: Parameters,index : Int){
        AccountApiInterface.sharedInstance.postRequestWithParam(fromViewController: self, actionName: "post_share", dictParam: parameters as [String : AnyObject], isShowIndicator: false, success: { (json) in
            print(json)
            
            self.postInfo?.share =  (self.postInfo?.share ?? 0) + 1
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
            
        }) { (error) in
            self.showAlert(messageStr: error)
        }
        
    }
    //MARK: - Edit Own Post Action
    func editpostActionApiCall(text : String,view : CreatePost){
        
        let param  : Parameters = ["user_id":"\(standard.value(forKey: "userid") ?? 0)" ,"post_id": postInfo!.postId ?? 0,"caption":text]
        print(param)
        AccountApiInterface.sharedInstance.postRequestWithParam(fromViewController: self, actionName: "edit_own_post", dictParam: param as [String : AnyObject], isShowIndicator: true, success: { (json) in
            print(json)
            view.removeFromSuperview()
          
            DispatchQueue.main.async {
                 self.postInfo?.caption? = text
             
                self.tableView.reloadData()
            }
            
        }) { (error) in
            self.showAlert(messageStr: error)
        }
    }
}

extension PostCommentViewController : menuButtonDelegates ,editPostDelegates {
    
    
    func menuButtonAction(view:Menu,index : Int,selectedIndex : Int,delegateStr : String){
        print(index)
        
        if delegateStr == "own" {
            
            if index == 1 {
                let vc = CreatePost.init(frame: self.view.frame ,post : postInfo ?? RZMatchMaker.init(json: JSON(JSON.self)) ,index : selectedIndex,isEditPost : true)
               vc.delegates = self
                self.view.addSubview(vc)
            }
            else{
                let array =   ["delete","turn_off"]
                let parameters:  Parameters = [ "user_id":"\(standard.value(forKey: "userid")!)","post_id":"\(postInfo?.postId ?? 0)", "action":array[index - 2]]
                
                print(parameters)
                self.ownPostActionApiCall(parameters: parameters, isDelete:  (array[index - 2] == "delete" ))
            }
            
            
        }
        else if delegateStr == "other"{
            let parameters:  Parameters = [ "user_id":"\(standard.value(forKey: "userid")!)","post_id":"\(postInfo?.postId ?? 0)","post_user_id":"\(postInfo?.userId ?? 0)", "action":"\(index - 1)"]
            
            print(parameters)
            
            self.otherPostActionApiCall(parameters: parameters, isHide: (index == 1))
        }
        else if delegateStr == "share"{
            if index == 1 {
                let parameters:  Parameters = [ "user_id":"\(standard.value(forKey: "userid")!)","post_id":"\(postInfo?.postId ?? 0)","post_user_id":"\(postInfo?.userId ?? 0)"]
                
                self.sharePostActionApiCall(parameters: parameters, index: selectedIndex)
            }
            else if index == 2 {
                self.popViewToMainMeassge()
            }
        }
    }
    func editPostDelegatesFunction(view : CreatePost,index : Int , text : String){
          self.editpostActionApiCall(text: text, view: view)
    }
}



