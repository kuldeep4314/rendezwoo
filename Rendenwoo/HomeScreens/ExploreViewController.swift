//
//  ExploreViewController.swift
//  Rendenwoo
//
//  Created by goyal on 28/02/18.
//  Copyright © 2018 ankita. All rights reserved.
//

import UIKit
import ADMozaicCollectionViewLayout
class ExploreViewController: UIViewController ,UITableViewDelegate,UITableViewDataSource,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout ,ADMozaikLayoutDelegate{
    var currentPage = 1
    var isResultsAviablleToLoad = true
    var arrayExploreWall = [ExploreModel]()
    let refreshControl = UIRefreshControl()
    @IBOutlet weak var topViewHeight: NSLayoutConstraint!
    @IBOutlet weak var btnGridList: UIButton!
    @IBOutlet weak var adLayout: ADMozaikLayout!
    @IBOutlet weak var tableView: UITableView!
    var isGridView = false
    @IBOutlet weak var collectionView: UICollectionView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.tableFooterView = UIView()
        adLayout.delegate = self
        self.btnGridAndListTapped(btnGridList)
        
        if UIDevice.isIphoneX == true{
            topViewHeight.constant = 84
        }
        if #available(iOS 10.0, *) {
            tableView.refreshControl = refreshControl
        } else {
            tableView.addSubview(refreshControl)
        }
        refreshControl.tintColor = appColor
        refreshControl.addTarget(self, action: #selector(refreshData(_:)), for: .valueChanged)
        
          self.tableView.rowHeight = UITableViewAutomaticDimension
        
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @objc func refreshData(_ sender: Any) {
        
        currentPage = 1
        self.isResultsAviablleToLoad = true
        self.fetchExploreWallApiCall(page: currentPage)
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        currentPage = 1
        self.isResultsAviablleToLoad = true
        self.fetchExploreWallApiCall(page: currentPage)
    }
    
    @IBAction func btnGridAndListTapped(_ sender: UIButton) {
        
        if isGridView == true {
            collectionView.isHidden = false
            tableView.isHidden = true
            sender.setImage(#imageLiteral(resourceName: "imgListIcon"), for: .normal)
        }
        else{
            collectionView.isHidden = true
            tableView.isHidden = false
            sender.setImage(#imageLiteral(resourceName: "imgGridicon"), for: .normal)
        }
        isGridView = !isGridView
        
    }
    //MARK: - UITABLEVIEW DATA SOURCE
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayExploreWall.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! ExploreTableViewCell
        let imageURL =  arrayExploreWall[indexPath.row].imageUrl ?? ""
    
        cell.lblLike.text = "\(arrayExploreWall[indexPath.row].totalLikes ?? 0) LIKES"
        cell.lblComments.text = "\(arrayExploreWall[indexPath.row].totalIntros ?? 0) INTRO REQUESTS"
        
        
         cell.imgPic.sd_setImage(with: URL(string:imageURL), placeholderImage: #imageLiteral(resourceName: "imgUpload"), options: [.refreshCached]) { (img, error, type, url) in
            
            let widthInPixels = img?.size.width ?? 0
            let heightInPixels = img?.size.height ?? 0
            
            let height = self.view.frame.size.width * (heightInPixels/widthInPixels)
        
              cell.imgHeight.constant =  height
          
        }
  
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "GetIntroducedViewController") as! GetIntroducedViewController
        vc.exploreWallInfo = arrayExploreWall[indexPath.row]
        self.navigationController?.pushViewController(vc, animated: true)
        
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        if indexPath.row ==  self.arrayExploreWall.count - 1 && isResultsAviablleToLoad == true{
            currentPage = currentPage + 1
            self.fetchExploreWallApiCall(page: currentPage)
        }
    }
    
    //MARK:- CollectionView Datasource
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrayExploreWall.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! PhotoCollectionViewCell
        let imageURL =  arrayExploreWall[indexPath.row].imageUrl ?? ""
        cell.imageView.sd_setImage(with: URL(string: imageURL), placeholderImage: #imageLiteral(resourceName: "imgPlaceHolder"), options: [], completed: nil)
        
        
        return cell
        
    }
    //MARK:- CollectionView Delegates
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "GetIntroducedViewController") as! GetIntroducedViewController
        vc.exploreWallInfo = arrayExploreWall[indexPath.row]
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        
        
        return CGSize(width: collectionView.frame.size.width, height: collectionView.frame.size.height )
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if indexPath.row ==  self.arrayExploreWall.count - 1 && isResultsAviablleToLoad == true{
            currentPage = currentPage + 1
            self.fetchExploreWallApiCall(page: currentPage)
        }
    }
    //MARK: - ADMozaikLayoutDelegate
    
    func collectonView(_ collectionView: UICollectionView, mozaik layoyt: ADMozaikLayout, geometryInfoFor section: ADMozaikLayoutSection) -> ADMozaikLayoutSectionGeometryInfo {
        
        var rowHeight: CGFloat = 20.0
        
        rowHeight = (self.collectionView.frame.size.height / 19.5)
        
        
        
        let width = ADMozaikLayoutColumn(width: collectionView.frame.size.width / 3 - 3)
        let columns = [width, width,width]
        
        
        let geometryInfo = ADMozaikLayoutSectionGeometryInfo(rowHeight: rowHeight,
                                                             columns: columns,
                                                             minimumInteritemSpacing: 0,
                                                             minimumLineSpacing: 0,
                                                             sectionInset: UIEdgeInsets(top: 5, left: 5, bottom: 5, right: 5),
                                                             headerHeight: 0, footerHeight: 0)
        
        return geometryInfo
    }
    
    func collectionView(_ collectionView: UICollectionView, mozaik layout: ADMozaikLayout, mozaikSizeForItemAt indexPath: IndexPath) -> ADMozaikLayoutSize {
        
        if indexPath.row == 0 {
            return ADMozaikLayoutSize(numberOfColumns: 2, numberOfRows: 8)
        }
        else  if indexPath.row == 1 {
            return ADMozaikLayoutSize(numberOfColumns: 2, numberOfRows: 6)
        }
            
        else{
            return ADMozaikLayoutSize(numberOfColumns: 1, numberOfRows: 8)
        }
        
    }
    
    
    //MARK:- Explore Wall  API Call
    
    func fetchExploreWallApiCall(page : Int){
        let userData = UserData.init(json: JSON(standard.value(forKey: "userData") as? [String : Any] ?? [String : Any]()))
        let parameter : Parameters = ["user_id":standard.value(forKey: "userid")!,
                                      "page": page,"limit":"10","user_type" : userData.usertype ?? 0]
        
        
        
        print(parameter)
        AccountApiInterface.sharedInstance.postRequestWithParam(fromViewController: self, actionName: "dating", dictParam: parameter as [String : AnyObject], isShowIndicator: (page == 1) ? true : false , success: { (json) in
            self.refreshControl.endRefreshing()
            print(json)
            EmptyDataSet.sharedInstance.delegates = self
            EmptyDataSet.sharedInstance.showEmptyData(title: "No post found.", image: #imageLiteral(resourceName: "search"), tableView: self.tableView)
            EmptyDataSet.sharedInstance.delegates = self
            EmptyDataSet.sharedInstance.showEmptyDataCollectionView(title: "No post found.", image: #imageLiteral(resourceName: "search"), collectionView: self.collectionView)
            
            
            if page == 1 {
                self.arrayExploreWall.removeAll()
            }
            
            guard(json["data"].arrayValue.count) > 0 else {
                
                self.isResultsAviablleToLoad = false
                DispatchQueue.main.async {
                    self.tableView.reloadData()
                    self.collectionView.reloadData()
                }
                
                return
            }
            print(json["data"].arrayValue.count)
            for i in 0..<json["data"].arrayValue.count{
                
                let arrTemp = ExploreModel.init(json: json["data"][i])
                
                self.arrayExploreWall.append(arrTemp)
            }
            
            self.isResultsAviablleToLoad = true
            
            
            DispatchQueue.main.async {
                self.collectionView.reloadData()
                self.tableView.reloadData()
            }
        }) { (error) in
            self.refreshControl.endRefreshing()
            EmptyDataSet.sharedInstance.delegates = self
            EmptyDataSet.sharedInstance.showEmptyData(title: "No post found.", image: #imageLiteral(resourceName: "search"), tableView: self.tableView)
            EmptyDataSet.sharedInstance.delegates = self
            EmptyDataSet.sharedInstance.showEmptyDataCollectionView(title: "No post found.", image: #imageLiteral(resourceName: "search"), collectionView: self.collectionView)
            self.isResultsAviablleToLoad = false
            self.collectionView.reloadData()
            self.tableView.reloadData()
            self.showAlert(messageStr: error)
        }
        
    }
    
}
extension ExploreViewController : emptyDataSetDelegatesOnTab {
    
    func didTappedFunction(){
        
        currentPage = 1
        self.isResultsAviablleToLoad = true
        self.fetchExploreWallApiCall(page: currentPage)
    }
    
    
}
