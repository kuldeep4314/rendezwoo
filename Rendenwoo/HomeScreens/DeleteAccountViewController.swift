//
//  DeleteAccountViewController.swift
//  Rendenwoo
//
//  Created by goyal on 28/02/18.
//  Copyright © 2018 ankita. All rights reserved.
//

import UIKit

class DeleteAccountViewController: UIViewController, UITextViewDelegate {

    @IBOutlet weak var topViewHeight: NSLayoutConstraint!
    @IBOutlet weak var txtView: UITextView!
    @IBOutlet weak var btnDeleteAccount: UIButton!
    @IBOutlet weak var descriptionView: UIView!
    @IBOutlet weak var view5: UIView!
    @IBOutlet weak var view4: UIView!
    @IBOutlet weak var view3: UIView!
    @IBOutlet weak var view2: UIView!
    @IBOutlet weak var view1: UIView!
    @IBOutlet weak var subView: UIView!
    
    @IBOutlet weak var img2: UIImageView!
    @IBOutlet weak var view4TopHeight: NSLayoutConstraint!
    @IBOutlet weak var view3Height: NSLayoutConstraint!
    @IBOutlet weak var lblTitle: UILabel!
    
    @IBOutlet weak var img1: UIImageView!
    @IBOutlet weak var lbl1: UILabel!
    @IBOutlet weak var lbl2: UILabel!
    @IBOutlet weak var lbl3: UILabel!
    @IBOutlet weak var lbl4: UILabel!
    @IBOutlet weak var lbl5: UILabel!
    var reasonForDeleteIndex = 1
    override func viewDidLoad() {
        super.viewDidLoad()
      self.view1.addshodowToView()
        self.view2.addshodowToView()
        self.view3.addshodowToView()
        self.view4.addshodowToView()
        self.view5.addshodowToView()
        self.descriptionView.isHidden = true
        txtView.text = "Description"
        txtView.textColor = lightGray
        subView.backgroundColor = moreLight
        subView.layer.cornerRadius = 5
       
        btnDeleteAccount.backgroundColor = redDark
        self.view.backgroundColor = redDark
        
        if UIDevice.isIphoneX == true
        {
            topViewHeight.constant = 84
        }
        
        self.setDataofUser()
         self.setViewAction(index: reasonForDeleteIndex)
        // Do any additional setup after loading the view.
    }

    @IBAction func hideFeedBackView(_ sender: UITapGestureRecognizer) {
          self.descriptionView.isHidden = true
    }
    
    
    func setDataofUser(){
       if UserInfo.sharedInstance.relationshipStatus == "single" {
            self.lblTitle.text = "All your matches, tags and messages will be deleted. This action cannot be undone. Why are you deleting your account?"
            
            self.lbl1.text = "Found my perfect match"
         self.lbl2.text = "Not looking anymore"
         self.lbl3.text = "Matches are not very good"
         self.lbl4.text = "Prefer not to use online dating"
         self.lbl5.text = "Other, Please specify"
        self.view3Height.constant = 50
          self.view4TopHeight.constant = 10
         self.view3.isHidden = false
        img1.image = UIImage.init(named: "imgIcn1")
         img2.image = UIImage.init(named: "imgIcn2")
       }else{
        
        self.lblTitle.text = "All your likes, tags, and messages will be deleted. This action cannot be undone. Why are you deleting your account?"
        img1.image = UIImage.init(named: "imgIcn3")
        img2.image = UIImage.init(named: "imgIcn1")
        self.lbl1.text = "I am done Matchmaking"
        self.lbl2.text = "All my friends are taken"
        self.lbl4.text = "Prefer not to use online dating"
        self.lbl5.text = "Other, please specify"
        self.view3Height.constant = 0
        self.view4TopHeight.constant = 0
         self.view3.isHidden = true
        
        }
        

    }
    
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func btnSubmitTapped(_ sender: Any) {
          self.descriptionView.isHidden = true
        
        let parameter : Parameters = ["user_id":"\(standard.value(forKey: "userid")!)",
            
            "reason": "\(reasonForDeleteIndex)","description": txtView.text ?? ""]
        
        self.deleteApiCall(parameters: parameter)
        
        
    }
    @IBAction func getureTapped(_ sender: UITapGestureRecognizer) {
        
        reasonForDeleteIndex = (sender.view?.tag)!
        print(reasonForDeleteIndex)
        if sender.view?.tag == 5{
            self.descriptionView.isHidden = false
        }
 
        
        self.setViewAction(index: reasonForDeleteIndex)
    }
    
    
    func setViewAction(index: Int){
        
        for i in 1..<6 {
            let vc = self.view.viewWithTag(i)
            if index == i {
                vc?.backgroundColor = selectionRow
            }
            else{
                vc?.backgroundColor = UIColor.white
            }
        }
    }
    
    @IBAction func btnBackTapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func deleteBtnClicked(_ sender: Any) {
        
        if UserInfo.sharedInstance.relationshipStatus == "single" {
            let vc = ChangeRelationShip.init(frame: self.view.frame,title : "If you are not single anymore, you can change your relationship status to TAKEN and still help your friends find matches. A little generosity goes a long way 🙂", btn1Title : "Change relationship status to Taken", btn2Title : "Not Interested!",delegatesStr: "change")
            vc.delegates = self
            self.view.addSubview(vc)
        }
        else{
             let vc = ChangeRelationShip.init(frame: self.view.frame,title : "If your relationship status has changed, you can still stay on and find eligible matches for yourself. 🙂", btn1Title : "Change relationship status to Single", btn2Title : "Not Interested!",delegatesStr: "change")
            vc.delegates = self
            self.view.addSubview(vc)
        }
        
    
    }
    
    //MARK:- TextView Delegates
    func textViewDidBeginEditing(_ textView: UITextView) {
        
        
        if textView.textColor == lightGray
        {
            textView.text = nil
            textView.textColor = UIColor.black
            print("begin working")
        }
    }
    
    
    func textViewDidEndEditing(_ textView: UITextView) {
        
        
        if textView.text.isEmpty {
            textView.text = "Description"
            textView.textColor = lightGray
            UserInfo.sharedInstance.aboutMe = nil
            print("end working")
        }
    
    }
    
    //MARK:- DeleteApi Call Function
    
    func deleteApiCall(parameters: Parameters){
        print(parameters)
        AccountApiInterface.sharedInstance.postRequestWithParam(fromViewController: self, actionName: "deleteAccount", dictParam: parameters as [String : AnyObject], isShowIndicator: true, success: { (json) in
            UserInfo.sharedInstance.deintSharedInstace()
            standard.set(nil, forKey: "userImage")
            standard.set(nil , forKey: "userName")
            standard.set(nil, forKey: "userid")
            standard.set(nil, forKey: "token")
            standard.set(nil, forKey: "userData")
            standard.set(nil, forKey: "user_type")
            standard.set(nil, forKey: "gender_preference")
            appDelegates.window?.rootViewController?.removeFromParentViewController()
            
            appDelegates.welcomeViewConroller()
        }) { (error) in
            self.showAlert(messageStr: error)
        }
        
    }
    
}
extension  DeleteAccountViewController : alertButtonDelegates {
    
    func alertButtonAction(index : Int, delegatesStr:String){
        if index == 1 {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "EditProfileViewController") as! EditProfileViewController
             vc.isRelationshipStatus = true
            self.navigationController?.pushViewController(vc, animated: true)
        }
        else{
            if UserInfo.sharedInstance.relationshipStatus == "single" {
                let parameter : Parameters = ["user_id":"\(standard.value(forKey: "userid")!)",
                    
                    "reason": "\(reasonForDeleteIndex)"]
                
                self.deleteApiCall(parameters: parameter)
            }
            else{
                if reasonForDeleteIndex < 3 {
                    reasonForDeleteIndex = reasonForDeleteIndex + 5
                }
                print(reasonForDeleteIndex)
                let parameter : Parameters = ["user_id":"\(standard.value(forKey: "userid")!)",
                    
                    "reason": "\(reasonForDeleteIndex)"]
                
                self.deleteApiCall(parameters: parameter)
            }
         
           
        }
        
        
        
    }
    
}
