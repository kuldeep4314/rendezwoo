//
//  FaqViewController.swift
//  Rendenwoo
//
//  Created by goyal on 28/02/18.
//  Copyright © 2018 ankita. All rights reserved.
//

import UIKit

class FaqViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {

    @IBOutlet weak var topViewHeight: NSLayoutConstraint!
    @IBOutlet weak var tableView: UITableView!
    var arrayFaq = [JSON]()
    var selectedIndex = -1
    override func viewDidLoad() {
        super.viewDidLoad()
          self.tableView.tableFooterView = UIView()
         self.tableView.estimatedRowHeight = UITableViewAutomaticDimension
        // Do any additional setup after loading the view.
        if UIDevice.isIphoneX == true
        {
            topViewHeight.constant = 84
        }
        
        let parameter : Parameters = ["user_id":"\(standard.value(forKey: "userid")!)"]
        self.fetchFaqApiCall(parameters: parameter)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func btnBackTapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
   
    
    //MARK: - UITABLEVIEW DATA SOURCE
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
      return arrayFaq.count
        
       
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.row == selectedIndex {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! FaqTableViewCell
            
            cell.ansBgView.layer.borderWidth = 0.7
            cell.ansBgView.layer.borderColor = red.cgColor
            cell.lblQuestion.text = arrayFaq[indexPath.row]["question"].stringValue
             cell.lblAns.text = arrayFaq[indexPath.row]["answer"].stringValue
            return cell
        }
        
        else{
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell1", for: indexPath) as! FaqTableViewCell
          cell.questionBgView.addshodowToView()
          cell.lblQuestionTitle.text = arrayFaq[indexPath.row]["question"].stringValue
        return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if selectedIndex  == indexPath.row {
            selectedIndex  = -1
        }
        else{
            selectedIndex  = indexPath.row
        }
        self.tableView.reloadData()
    }
 
    //MARK:- Fetch FAQ API Call
    func fetchFaqApiCall(parameters: Parameters){
        AccountApiInterface.sharedInstance.postRequestWithParam(fromViewController: self, actionName: "getFaq", dictParam: parameters as [String : AnyObject], isShowIndicator: true, success: { (json) in
            
            self.arrayFaq = json["data"].arrayValue
            
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
        }) { (error) in
            self.showAlert(messageStr: error)
        }
        
    }
    
}
