//
//  Tabbar.swift
//  Rendenwoo
//
//  Created by goyal on 16/03/18.
//  Copyright © 2018 ankita. All rights reserved.
//

import UIKit
import RAMAnimatedTabBarController
class Tabbar: RAMAnimatedTabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()
     
      
        
        self.tabBar.layer.shadowColor =  UIColor.lightGray.cgColor
        self.tabBar.layer.shadowOpacity = 1
        self.tabBar.layer.shadowOffset = CGSize.init(width: 0.5, height: 0.5)
        self.tabBar.layer.shadowRadius = 2
       self.tabBar.layer.masksToBounds = false
       
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    

}
