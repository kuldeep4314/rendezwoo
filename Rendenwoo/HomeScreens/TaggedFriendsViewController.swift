//
//  TaggedFriendsViewController.swift
//  Rendenwoo
//
//  Created by goyal on 05/03/18.
//  Copyright © 2018 ankita. All rights reserved.
//




class TaggedFriendsViewController: UIViewController , UITableViewDataSource,UITableViewDelegate {
    var selectedCell = -1
   
    var selectedFriendIndex = 0
    @IBOutlet weak var topViewHeight: NSLayoutConstraint!
    @IBOutlet weak var btnInviteMoreFriends: UIButton!
    @IBOutlet weak var tableView: UITableView!
 
    var taggedList = [TaggedFriends]()
 
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.tableFooterView = UIView()
        self.btnInviteMoreFriends.addshodowToButton()
        // Do any additional setup after loading the view.
        
        if UIDevice.isIphoneX == true
        {
            topViewHeight.constant = 84
        }
        self.tableView.rowHeight = UITableViewAutomaticDimension
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        let parameter : Parameters = ["user_id" : "\(standard.value(forKey: "userid") ?? 0)"]
        self.fetchTaggedFriendsApiCall(parameters: parameter)
    }
    
    @IBAction func btnInviteMoreTapped(_ sender: Any) {
        let text = "\(standard.value(forKey: "userName") ?? "") has invited you to join Rendezwoo. Click on the link to install. Link coming soon!"
        
        self.shareTextViaSocialApp(shareText: text)
    }
    
    
    
    func shareTextViaSocialApp(shareText: String){
        
        let vc = UIActivityViewController(activityItems: [shareText], applicationActivities: [])
        vc.popoverPresentationController?.sourceView = self.view
        present(vc, animated: true) {
        }
    }
    
    
    func alertWithOneTitleShow(){
        let vc = AlertWithOneTitle.init(frame: self.view.frame, title: "Your friend has not accepted their invitation yet.", cancelTitle: "Cancel", doneTitle: "Invite again", delegatesStr: "pending")
        vc.delegates = self
        self.view.addSubview(vc)
        
        
        
    }
    
    
    @objc func btnTagUnTagTapped(sender : UIButton){
        
        if sender.imageView?.image == #imageLiteral(resourceName: "imgUntagBlue"){
            selectedFriendIndex = sender.tag - 3000
          
            
            taggedList[selectedFriendIndex].tag = false
            self.tableView.reloadData()
            
            let cell = tableView.cellForRow(at: IndexPath.init(row: 0, section: selectedFriendIndex)) as?
            TaggedFriendTableViewCell
            
            cell?.unTagRetagFromAllFunction(value: 2)
            let vc = AlertWithOneTitle.init(frame: self.view.frame, title: "Are you sure you want to un-tag \(taggedList[selectedFriendIndex].name ?? "") from all your pictures?", cancelTitle: "Nope", doneTitle: "Yes, un-tag",delegatesStr:"untag")
            vc.delegates = self
            self.view.addSubview(vc)
            
        }
        else  if sender.imageView?.image ==  #imageLiteral(resourceName: "imgUntag2") {
                   //1 Tag 2 untag // 0 back to original
            
            selectedFriendIndex = sender.tag - 3000
            taggedList[selectedFriendIndex].tag = true
            self.tableView.reloadData()
            
            let cell = tableView.cellForRow(at: IndexPath.init(row: 0, section: selectedFriendIndex)) as?
            TaggedFriendTableViewCell
            
            cell?.unTagRetagFromAllFunction(value: 1)
            
            let vc = AlertWithOneTitle.init(frame: self.view.frame, title: "Would you like to re-tag \(taggedList[selectedFriendIndex].name ?? "") in these pictures?", cancelTitle: "Cancel", doneTitle: "Yes",delegatesStr:"retag")
            vc.delegates = self
            self.view.addSubview(vc)
        }
            
            
        else if sender.imageView?.image == #imageLiteral(resourceName: "imgInvitation"){
            let text = "\(standard.value(forKey: "userName") ?? "") has invited you to join Rendezwoo. Click on the link to install. Link coming soon!"
            
            self.shareTextViaSocialApp(shareText: text)
        }
        else if sender.imageView?.image == #imageLiteral(resourceName: "imgPending"){
            self.alertWithOneTitleShow()
            
        }
        
        
        
        
        
    }
    @objc func btnTakenRelationShip(sender : UIButton){
        selectedFriendIndex = sender.tag - 2000
      
        self.tableView.reloadData()
        self.showAlertWithTitle(strTitle: "Are you sure you want to change \(taggedList[selectedFriendIndex].name ?? "")'s relationship status to Taken?", subTitle: "We will send a verification alert to \(taggedList[selectedFriendIndex].name ?? "").", perform: "taken")
        
        
    }
    @objc func btnSingleRelationShip(sender : UIButton){
        selectedFriendIndex = sender.tag - 1000
        
       
        
        self.tableView.reloadData()
        self.showAlertWithTitle(strTitle: "Are you sure you want to change \(taggedList[selectedFriendIndex].name ?? "")'s relationship status to Single?", subTitle: "We will send a verification alert to \(taggedList[selectedFriendIndex].name ?? "").", perform: "single")
        
        
    }
    
    
    func showAlertWithTitle(strTitle: String,subTitle : String,perform : String){
        let font = UIFont.init(name: "ProximaNova-Semibold", size: 17)
        let vc = AlertWithTwoTitle.init(frame: self.view.frame, title: strTitle, Subtitle: subTitle, cancelTitle: "Don't Change", doneTitle: "Yes", titleFont: font!, isShowImage: false,secondImage : #imageLiteral(resourceName: "imgTime"),delegatesStr:perform)
        vc.delegates = self
        self.view.addSubview(vc)
        
    }
    
    
    
    
    
    @IBAction func btnBackTapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func handlePhotoTapped(_ sender: UIButton) {
        
        if selectedCell  == sender.tag {
            selectedCell  = -1
             self.tableView.reloadData()
        }
        else{
            selectedCell  = sender.tag
              self.tableView.reloadData()
             let cell = tableView.cellForRow(at: IndexPath.init(row: 0, section: selectedCell)) as?
            TaggedFriendTableViewCell
              cell?.delegates = self
            cell?.taggedImagesfunction(friend_id: String(taggedList[selectedCell].userId ?? 0), name: taggedList[selectedCell].name ?? "",section :selectedCell)
          
           
        }

    
        
       
        
    }
    //MARK: - UITABLEVIEW DATA SOURCE
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return  self.taggedList.count
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if selectedCell == section {
            return 1
        }
        else{
            return 0
        }
        
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! TaggedFriendTableViewCell
    

        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 300 
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 300
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let cell = tableView.dequeueReusableCell(withIdentifier: "header") as! TaggedFriendTableViewCell
        
        
        cell.btnTaken.addTarget(self, action: #selector(self.btnTakenRelationShip(sender:)), for: .touchUpInside)
        
        cell.btnSingle.addTarget(self, action: #selector(self.btnSingleRelationShip(sender:)), for: .touchUpInside)
        
        cell.btnPhoto.tag = section
        
        cell.btnSingle.tag = 1000 + section
        cell.btnTaken.tag = 2000 + section
        cell.btnTag.tag = 3000 + section
        cell.btnPhoto.addTarget(self, action: #selector(self.handlePhotoTapped(_:)), for: .touchUpInside)
        
        cell.btnTag.addTarget(self, action: #selector(self.btnTagUnTagTapped(sender:)), for: .touchUpInside)
        
         if self.taggedList[section].tag == true {
             cell.btnTag.setImage(#imageLiteral(resourceName: "imgUntagBlue"), for: .normal)
        }
        else{
            cell.btnTag.setImage(#imageLiteral(resourceName: "imgUntag2"), for: .normal)
        }
        
        
       
        if selectedCell == section {
            cell.btnPhoto.setImage(#imageLiteral(resourceName: "imgPhotoSelected"), for: .normal)
        }
        else{
            cell.btnPhoto.setImage(#imageLiteral(resourceName: "imgPhotoUnSelected"), for: .normal)
        }
        
          cell.lblName.text = self.taggedList[section].name ?? ""
        
        
        
        if self.taggedList[section].userType == 0 {
            cell.btnSingle.setImage(#imageLiteral(resourceName: "imgSingleSelected"), for: .normal)
            cell.btnTaken.setImage(#imageLiteral(resourceName: "imgTakenUnselected"), for: .normal)
        }
        else{
            cell.btnSingle.setImage(#imageLiteral(resourceName: "imgSingleUnselected"), for: .normal)
            cell.btnTaken.setImage(#imageLiteral(resourceName: "imgTakenSelect"), for: .normal)
        }
        
        return cell.contentView
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 60
    }
    
    
    //MARK:- Fetch Tagged Friends Api Call
    func fetchTaggedFriendsApiCall(parameters: Parameters){
        AccountApiInterface.sharedInstance.postRequestWithParam(fromViewController: self, actionName: "tagged_friend_list", dictParam: parameters as [String : AnyObject], isShowIndicator: true, success: { (json) in
            
            print(json)
         self.taggedList.removeAll()
           if  let friendTagged = json["data"].array {
                
            self.taggedList = friendTagged.map({TaggedFriends(json : $0)})
            }
            
           
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
        }) { (error) in
            self.showAlert(messageStr: error)
        }
        
    }
    
    //MARK:- Unatg  API Call
 
    func untagReTagApiCall(parameters: Parameters,url : String ,isTag : Bool){
        AccountApiInterface.sharedInstance.postRequestWithParam(fromViewController: self, actionName: url, dictParam: parameters as [String : AnyObject], isShowIndicator: true, success: { (json) in
            
            print(json)
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
        }) { (error) in
            self.showAlert(messageStr: error)
        }
        
    }
    
    
    
    //MARK:- Send Verify User Status
    func sendVerificationApiCall(parameters: Parameters){
        AccountApiInterface.sharedInstance.postRequestWithParam(fromViewController: self, actionName: "verify_relationship_status", dictParam: parameters as [String : AnyObject], isShowIndicator: true, success: { (json) in
           
            DispatchQueue.main.async {
                self.tableView.reloadData()
                 self.showAlert(messageStr: json["msg"].stringValue)
            }
        }) { (error) in
            self.showAlert(messageStr: error)
        }
        
    }
    
}

extension TaggedFriendsViewController : alertButtonDelegates {
    
    func alertButtonAction(index : Int, delegatesStr:String){
        
        if delegatesStr == "untag"{
            if index == 1 {
                
                let cell = tableView.cellForRow(at: IndexPath.init(row: 0, section: selectedFriendIndex)) as?
                TaggedFriendTableViewCell
                 cell?.unTagRetagFromAllFunction(value: 0)
                  taggedList[selectedFriendIndex].tag = true
                
                self.tableView.reloadData()
                
                
                 //1 Tag 2 untag // 0 back to original
                
            }
            else{
                let cell = tableView.cellForRow(at: IndexPath.init(row: 0, section: selectedFriendIndex)) as?
                TaggedFriendTableViewCell
                cell?.unTagRetagFromAllFunction(value: 2)
              
                let param : Parameters = ["user_id":standard.value(forKey: "userid")!,"friend_id" : taggedList[selectedFriendIndex].userId ?? 0]
                
                
                self.untagReTagApiCall(parameters: param, url: "untag_friend_from_all", isTag: false) // isTag false means Untag
                
            }
        }
        
        if delegatesStr == "retag"{
            if index == 1 {
                let cell = tableView.cellForRow(at: IndexPath.init(row: 0, section: selectedFriendIndex)) as?
                TaggedFriendTableViewCell
                cell?.unTagRetagFromAllFunction(value: 0)
               
                 taggedList[selectedFriendIndex].tag = false
                
                self.tableView.reloadData()
            }
            else{
                let cell = tableView.cellForRow(at: IndexPath.init(row: 0, section: selectedFriendIndex)) as?
                TaggedFriendTableViewCell
                cell?.unTagRetagFromAllFunction(value: 1)
                let param : Parameters = ["user_id":standard.value(forKey: "userid")!,"friend_id" : taggedList[selectedFriendIndex].userId ?? 0]
                self.untagReTagApiCall(parameters: param, url: "retag_friend_in_all", isTag: true)
            }
        }
        if delegatesStr == "single"{
            
            if index == 1 {
                print("1")
            }
            else{
                let param : Parameters = ["user_id":"\(standard.value(forKey: "userid") ?? 0 )",
                    
                    "friend_id":self.taggedList[selectedFriendIndex].userId ?? 0,
                    
                    "relationship_status":0]
                self.sendVerificationApiCall(parameters: param)
                
            }
            
        }
        if delegatesStr == "taken"{
            
            if index == 1 {
                 print("0")
            }
            else{
                let param : Parameters = ["user_id":"\(standard.value(forKey: "userid") ?? 0 )",
                    
                    "friend_id":self.taggedList[selectedFriendIndex].userId ?? 0,
                    
                    "relationship_status":1]
                self.sendVerificationApiCall(parameters: param)
            }
            
        }
        
        if delegatesStr == "pending"{
            if index == 2 {
                let text = "\(standard.value(forKey: "userName") ?? "") has invited you to join Rendezwoo. Click on the link to install. Link coming soon!"
                
                self.shareTextViaSocialApp(shareText: text)
                
            }
            
        }
        
        
        tableView.reloadData()
    }
    
}

extension  TaggedFriendsViewController : checkTagStatusDelegate {
    func  checkTagStats(status : Bool,section : Int){
        taggedList[section].tag = status
        self.tableView.reloadData()
    }
    
    
}
