//
//  FacebookPhotoViewController.swift
//  Rendenwoo
//
//  Created by goyal on 03/07/18.
//  Copyright © 2018 ankita. All rights reserved.
//

import UIKit
import ADMozaicCollectionViewLayout
import CropViewController
import FBSDKCoreKit

protocol PhotoDelegates {
    func  setPhotoFunction (Img : UIImage)
}



class FacebookPhotoViewController: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout , UIImagePickerControllerDelegate,
UINavigationControllerDelegate,ADMozaikLayoutDelegate,CropViewControllerDelegate{
   
    var imagesArr = [RZPhotos]()
  
    @IBOutlet weak var topViewHeight: NSLayoutConstraint!
    @IBOutlet weak var adLayout: ADMozaikLayout!
    @IBOutlet weak var collectionView: UICollectionView!
    let picker = UIImagePickerController()
    var imageIndex = Int()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.adLayout.delegate = self
        if UIDevice.isIphoneX == true
        {
            topViewHeight.constant = 84
        }
        if let _ = FBSDKAccessToken.current()
        {
            fetchListOfUserPhotos()
        }
        
        
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
       
    }
    @IBAction func btnBackTapped(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    func fetchListOfUserPhotos()
    {
        let graphPath = "me/photos"
        let parameters = [
            "fields":"picture"
        ]
        
        let graphRequest : FBSDKGraphRequest = FBSDKGraphRequest(graphPath: graphPath, parameters: parameters )


        graphRequest.start(completionHandler: { (connection, result, error) -> Void in

            if ((error) != nil)
            {
                // Process error
                print("Error: \(error)")
            }
            else
            {
                print("fetched user: \(result)")

                let fbResult:[String:AnyObject] = result as! [String : AnyObject]

                print( fbResult["data"])

                self.collectionView.reloadData()

            }
        })
        
        
        
    }

    

    
   
    //MARK:- CollectionView Datasource
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return imagesArr.count
        
        
        
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! PhotoCollectionViewCell
        
            let imageURL = imagesArr[indexPath.row].imageUrl ?? ""
            cell.imageView.sd_setImage(with: URL(string: imageURL), placeholderImage: #imageLiteral(resourceName: "imgPlaceHolder"), options: [], completed: nil)
    
        return cell
        
    }
    //MARK:- CollectionView Delegates
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        collectionView.deselectItem(at: indexPath, animated: true)
        
      
        
        
        
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        
        
        return CGSize(width: collectionView.frame.size.width, height: collectionView.frame.size.height )
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    //MARK: - ADMozaikLayoutDelegate
    
    func collectonView(_ collectionView: UICollectionView, mozaik layoyt: ADMozaikLayout, geometryInfoFor section: ADMozaikLayoutSection) -> ADMozaikLayoutSectionGeometryInfo {
        
        var rowHeight: CGFloat = 20.0
        
        rowHeight = (self.collectionView.frame.size.height / 19.5)
        
        
        
        let width = ADMozaikLayoutColumn(width: collectionView.frame.size.width / 3 - 3)
        let columns = [width, width,width]
        
        
        let geometryInfo = ADMozaikLayoutSectionGeometryInfo(rowHeight: rowHeight,
                                                             columns: columns,
                                                             minimumInteritemSpacing: 0,
                                                             minimumLineSpacing: 0,
                                                             sectionInset: UIEdgeInsets(top: 5, left: 5, bottom: 5, right: 5),
                                                             headerHeight: 0, footerHeight: 0)
        
        return geometryInfo
    }
    
    func collectionView(_ collectionView: UICollectionView, mozaik layout: ADMozaikLayout, mozaikSizeForItemAt indexPath: IndexPath) -> ADMozaikLayoutSize {
        
        if indexPath.row == 0 {
            return ADMozaikLayoutSize(numberOfColumns: 2, numberOfRows: 8)
        }
        else  if indexPath.row == 1 {
            return ADMozaikLayoutSize(numberOfColumns: 1, numberOfRows: 4)
        }
        else  if indexPath.row == 2 {
            return ADMozaikLayoutSize(numberOfColumns: 1, numberOfRows: 4)
        }
        else{
            return ADMozaikLayoutSize(numberOfColumns: 1, numberOfRows: 5)
        }
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        
    }
    //MARK:- Crop Image Function And Delegates
    func presentCropViewController(image: UIImage) {
        
        let cropViewController = CropViewController(image: image)
        cropViewController.delegate = self
        present(cropViewController, animated: true, completion: nil)
    }
    func cropViewController(_ cropViewController: CropViewController, didCropToImage image: UIImage, withRect cropRect: CGRect, angle: Int) {
        print(image)
        
        
        cropViewController.dismiss(animated: true, completion: nil)
    }
    
    
   
    
   
    

    
    
}
