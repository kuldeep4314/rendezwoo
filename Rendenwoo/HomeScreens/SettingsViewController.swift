//
//  SettingsViewController.swift
//  Rendenwoo
//
//  Created by goyal on 01/03/18.
//  Copyright © 2018 ankita. All rights reserved.
//

import UIKit

class SettingsViewController: UIViewController ,UITableViewDelegate,UITableViewDataSource{

    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var topViewHeight: NSLayoutConstraint!
    let array = ["ACCOUNTS","Active","My Current Location","Payment Methods","NOTIFICATIONS","Likes","Shares","Tag Notifications","Introductions","Messages","","Privacy & Terms of Services","FAQ's","Rate Us","Share","Contact us"]
    var selectedIndex = -1
    
    var settingModel = RZSettings.init(json: JSON(JSON.self))
    @IBOutlet weak var btnDeleteAccount: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
  self.tableView.tableFooterView = UIView()
        btnDeleteAccount.backgroundColor = redDark
        self.view.backgroundColor = redDark
        if UIDevice.isIphoneX == true
        {
            topViewHeight.constant = 84
        }
        
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        UIApplication.shared.statusBarStyle = .lightContent

        let parameter : Parameters = ["user_id":"\(standard.value(forKey: "userid")!)"]
        
        self.fetchSettingApi(parameters: parameter)
        
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func btnDeleteClicked(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "DeleteAccountViewController") as! DeleteAccountViewController
        
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @objc func btnLogOutAction(sender : UIButton){
      let parameter : Parameters = ["user_id":"\(standard.value(forKey: "userid")!)"]
        
        self.logoutApiCall(parameters: parameter)
    }
    @objc func btnDeactiveAccountAction(sender : UIButton){
        
        
    }
    
    func logoutApiCall(parameters: Parameters){
        AccountApiInterface.sharedInstance.postRequestWithParam(fromViewController: self, actionName: "logout", dictParam: parameters as [String : AnyObject], isShowIndicator: true, success: { (json) in
            
             print(json)
            
            DispatchQueue.main.async {
                self.unsetToUserDefaults()
                appDelegates.window?.rootViewController?.removeFromParentViewController()
                appDelegates.welcomeViewConroller()
            }
        }) { (error) in
            self.showAlert(messageStr: error)
        }
        
    }

    
    
    
    @IBAction func btnBackClicked(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func unsetToUserDefaults(){
    appDelegatesApplication?.applicationIconBadgeNumber = 0
         UserInfo.sharedInstance.deintSharedInstace()
         standard.set(nil, forKey: "userImage")
         standard.set(nil , forKey: "userName")
         standard.set(nil, forKey: "userid")
         standard.set(nil, forKey: "token")
        standard.set(nil, forKey: "userData")
        standard.set(nil, forKey: "user_type")
         standard.set(nil, forKey: "gender_preference")
       
    }
    
    
    func shareTextViaSocialApp(shareText: String,Subject : String){
      
            let vc = UIActivityViewController(activityItems: [shareText], applicationActivities: [])
         
                vc.setValue(Subject, forKey: "Subject")
            vc.popoverPresentationController?.sourceView = self.view
            present(vc, animated: true) {
            }
        }
    
    
    @objc func btnFirstAction(sender : UIButton){
      
         self.settingModel.activeStatus = 0
        self.tableView.reloadData()
       
        let alert = AlertWithOneTitle.init(frame: self.view.frame, title: "Do you want to activate your profile?", cancelTitle: "Nope", doneTitle: "Yes",delegatesStr:"unhide")
        alert.delegates = self
        self.view.addSubview(alert)
    }
    
    @objc func btnSecondAction(sender : UIButton){
      self.settingModel.activeStatus = 1
        
        self.tableView.reloadData()
        let font  = UIFont.init(name: "ProximaNova-Regular", size: 16)
        
        var meassge = ""
        
      let   userData = UserData.init(json: JSON(standard.value(forKey: "userData") as? [String : Any] ?? [String : Any]()))
        
        if userData.usertype == 0{
            meassge = "Hiding your profile will untag you from all pictures. New matches will not be able to find you. You can still chat with your existing matches."
        }
        else{
            meassge = "Hiding your profile will untag you from all your pictures. You will not be able to introduce your friends to potential matches. You can still chat with your friends."
        }
        
        let vc = AlertWithTwoTitle.init(frame: self.view.frame, title:meassge, Subtitle: "Do you want to continue?", cancelTitle: "Nope", doneTitle: "Yes, Hide Me", titleFont: font!,isShowImage : false,secondImage : #imageLiteral(resourceName: "imgTime"),delegatesStr:"hide")
          vc.delegates = self
        self.view.addSubview(vc)
        
        
    }
    
    
    @objc func btnNotificationAction(sender : UIButton){
      
        if   settingModel.notificationSettings?[sender.tag].status == 1{
             selectedIndex = sender.tag
            let font  = UIFont.init(name: "ProximaNova-Semibold", size: 16)
            let vc = AlertWithTwoTitle.init(frame: self.view.frame, title:"Turn off Notifications?", Subtitle: "In love as in life, timing is crucial! Are you sure you want to miss Likes, Shares, Introductions, Messages or Matches?", cancelTitle: "Yes, Please", doneTitle: "Keep them on", titleFont: font!,isShowImage : true,secondImage : #imageLiteral(resourceName: "imgTime"),delegatesStr:"notification")
              vc.delegates = self
            self.view.addSubview(vc)
            
        }
        else{
             selectedIndex = sender.tag
            let font  = UIFont.init(name: "ProximaNova-Semibold", size: 16)
            let vc = AlertWithTwoTitle.init(frame: self.view.frame, title:"Turn on Notifications?", Subtitle: "Turn on notifications and never miss another like, share, introduction, message, or a match!", cancelTitle: "Yes, Please", doneTitle: "Ask me Later", titleFont: font!,isShowImage : true,secondImage : #imageLiteral(resourceName: "imgSmile1"),delegatesStr:"notification")
             vc.delegates = self
            self.view.addSubview(vc)
        }
        
        
    }
    

    //MARK: - UITABLEVIEW DATA SOURCE
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return array.count + 1
        
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        
        if indexPath.row == 0 || indexPath.row == 4 || indexPath.row == 10{
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "header", for: indexPath) as! SettingsTableViewCell
            
             cell.lblHeader.text = array[indexPath.row]
            
         
            
            return cell
        }
       else if indexPath.row == 2 || indexPath.row == 3 {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! SettingsTableViewCell
            
            cell.lblTitleInfo.text = array[indexPath.row]
            cell.lblInfo.isHidden = false
          
            
             cell.imgDivider3.isHidden = true
           
            cell.lblInfo.text = settingModel.location
            
            return cell
        }
            
       else if indexPath.row == 1 || indexPath.row == 15 {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "cellWithButton", for: indexPath) as! SettingsTableViewCell
            
            cell.lblTitle.text = array[indexPath.row]
            if indexPath.row == 15 {
                cell.imgDivider1.isHidden = true
                cell.btnFirst.setImage(#imageLiteral(resourceName: "imgMessage"), for: .normal)
                   cell.btnSecond.setImage(#imageLiteral(resourceName: "imgEmail"), for: .normal)
            }
            else{
                cell.imgDivider1.isHidden = false
               
                 cell.btnSecond.addTarget(self, action: #selector(self.btnSecondAction(sender:)), for: .touchUpInside)
                 cell.btnFirst.addTarget(self, action: #selector(self.btnFirstAction(sender:)), for: .touchUpInside)
                
              if  self.settingModel.activeStatus == 1 {
                    cell.btnFirst.setImage(#imageLiteral(resourceName: "imgUnMute"), for: .normal)
                    cell.btnSecond.setImage(#imageLiteral(resourceName: "imgMute"), for: .normal)
                }
                
                else{
                    cell.btnFirst.setImage(#imageLiteral(resourceName: "imgActive_icn"), for: .normal)
                    cell.btnSecond.setImage(#imageLiteral(resourceName: "imgInactive"), for: .normal)
                }
               
            }
            
            return cell
        }
        else if indexPath.row > 4 && indexPath.row < 10 {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "cellNotification", for: indexPath) as! SettingsTableViewCell
        
              cell.lblNotificationTitle.text = array[indexPath.row]
              cell.btnNotificationState.tag = indexPath.row - 5
            
            
            if settingModel.notificationSettings?[indexPath.row - 5].status   == 1{
                  cell.btnNotificationState.setImage(#imageLiteral(resourceName: "imgSwitchOn"), for: .normal)
            }
            else{
                  cell.btnNotificationState.setImage(#imageLiteral(resourceName: "imgSwichOff"), for: .normal)
            }
            
          
            if indexPath.row == 9 {
                cell.imgDivider2.isHidden = true
            }
            else{
                cell.imgDivider2.isHidden = false
            }
            cell.btnNotificationState.addTarget(self, action: #selector(self.btnNotificationAction(sender:)), for: .touchUpInside)
            return cell
        }
          else if indexPath.row == array.count {
             let cell = tableView.dequeueReusableCell(withIdentifier: "cellBottom", for: indexPath) as! SettingsTableViewCell
            cell.btnLogOut.addTarget(self, action: #selector(self.btnLogOutAction(sender:)), for: .touchUpInside)
             cell.btnDeactiveAccount.addTarget(self, action: #selector(self.btnDeactiveAccountAction(sender:)), for: .touchUpInside)
            
            
            return cell
        }
        else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! SettingsTableViewCell
             cell.imgDivider3.isHidden = false
            cell.lblTitleInfo.text = array[indexPath.row]
            cell.lblInfo.isHidden = true
           
            return cell
        
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
     tableView.deselectRow(at: indexPath, animated: true)
        if indexPath.row == 2 {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "LocationViewController") as! LocationViewController
            
            self.navigationController?.pushViewController(vc, animated: true)
        }
        
        else   if indexPath.row == 11 {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "PrivacyAndTermsViewController") as! PrivacyAndTermsViewController
             vc.strTitle = "Privacy Policy"
            self.navigationController?.pushViewController(vc, animated: true)
        }
     else   if indexPath.row == 12 {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "FaqViewController") as! FaqViewController
            
            self.navigationController?.pushViewController(vc, animated: true)
        }
            
        else   if indexPath.row == 13 {
         
            Helper.rateApp(appId: AppStorelink, completion: { (sucess) in
                
                print(sucess)
                
            })
        }
            
        else   if indexPath.row == 14 {
        
             let userData = UserData.init(json: JSON(standard.value(forKey: "userData") as? [String : Any] ?? [String : Any]()))
            
          let text =    "\(userData.name ?? "") wants you on Rendezwoo. Install now! App link coming soon.  Use my referral code : \(userData.referral ?? "")"
            
            self.shareTextViaSocialApp(shareText: text, Subject: "\(userData.name ?? "") wants you on Rendezwoo.")
        }
        
}
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if indexPath.row == array.count {
            return 215
        }
        else if indexPath.row == 10 {
            return 20
        }
        else if indexPath.row == 3 {
            return 0
        }
        else{
            return 44
        }
    }
    
    //MARK:- FetchSettingApi Call Function
    
    func fetchSettingApi(parameters: Parameters){
        print(parameters)
        AccountApiInterface.sharedInstance.postRequestWithParam(fromViewController: self, actionName: "fetchSettings", dictParam: parameters as [String : AnyObject], isShowIndicator: true, success: { (json) in
            
            print(json)
          self.settingModel = RZSettings.init(json: json)
            self.tableView.reloadData()
        }) { (error) in
            self.showAlert(messageStr: error)
        }
        
    }
    
    //MARK:- UpdateSettingApi Call Function
    
    func updateSettingApi(){
    
        
        let param : Parameters = [
            
            "user_id" : standard.value(forKey: "userid")!,
            
            "active_status" : settingModel.activeStatus ?? 0,
            
            "my_location": settingModel.location ?? "",
            
            "payment_methods" : "11",
            
            "notification_setting" : [
                
                "likes_status" : settingModel.notificationSettings?[0].status,
                
                "shares_status" : settingModel.notificationSettings?[1].status,
                
                "tag_notification_status" :settingModel.notificationSettings?[2].status,
                
                "introductions_status" : settingModel.notificationSettings?[3].status,
                
                "messages_status" : settingModel.notificationSettings?[4].status
                
            ]
            
        ]
        
        
        AccountApiInterface.sharedInstance.postRequestWithParam(fromViewController: self, actionName: "updateSettings", dictParam: param as [String : AnyObject], isShowIndicator: true, success: { (json) in
            
            print(json)
            self.settingModel = RZSettings.init(json: json)
            self.tableView.reloadData()
        }) { (error) in
            self.showAlert(messageStr: error)
        }
        
    }
    
    
    
    
}

extension SettingsViewController : alertButtonDelegates {
    
  func alertButtonAction(index : Int, delegatesStr:String){
        
        if delegatesStr == "notification"{
            if index == 1 {
                if settingModel.notificationSettings?[selectedIndex].status == 1 {
                    
                   settingModel.notificationSettings?[selectedIndex].status = 0
                }
                else{
                   settingModel.notificationSettings?[selectedIndex].status = 1
                }
                
               
               
            }
        }else if delegatesStr == "hide"{
            
            if index == 2 {
              
                 self.settingModel.activeStatus = 1
            }
            else{
                 self.settingModel.activeStatus = 0
                
            }
            
        }
        
        else if delegatesStr == "unhide"{
            
            if index == 2 {
                 self.settingModel.activeStatus = 0
               
            }
            else{
                 self.settingModel.activeStatus = 1
               
            }
           
    }
    
         self.tableView.reloadData()
     self.updateSettingApi()
    }
}
