//
//  IntrosViewController.swift
//  Rendenwoo
//
//  Created by goyal on 15/03/18.
//  Copyright © 2018 ankita. All rights reserved.
//

import UIKit

class IntrosViewController: UIViewController ,UITableViewDataSource,UITableViewDelegate{
 @IBOutlet weak var tableView: UITableView!
    
  
    let arrayIntros = ["Free daily Intros","Purchased Intros", "Intros Used" ,"Intros Remaining"]
     let arrayIntrosCount = ["daily_free_intro","purchased_intro", "used_intro" ,"remaining_intro"]
    
    
    
    @IBOutlet weak var topViewHeight: NSLayoutConstraint!
    override func viewDidLoad() {
        super.viewDidLoad()
 self.tableView.tableFooterView = UIView()
        self.tableView.rowHeight = UITableViewAutomaticDimension
        
        if UIDevice.isIphoneX == true
        {
            topViewHeight.constant = 84
        }
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        
    }
    
    @IBAction func btnBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func btnMoreInrosTapped(_ sender: Any) {
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "BuyInrosViewController") as! BuyInrosViewController
        
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    //MARK: - UITABLEVIEW DATA SOURCE
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 6
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.row == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cellHeader", for: indexPath)
            return cell
        }
       else if indexPath.row == 1 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cellEmpty", for: indexPath)
            return cell
        }
        else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
            let lblTitle = cell.viewWithTag(2) as! UILabel
            let lblCount = cell.viewWithTag(3) as! UILabel
            let lblBottom = cell.viewWithTag(4) as! UILabel
            lblTitle.text = arrayIntros[indexPath.row - 2]
            lblCount.text = introsInfo[arrayIntrosCount[indexPath.row - 2]].stringValue
            
            if indexPath.row == 4 {
               lblBottom.backgroundColor = DarkGray
            }else
            {
               lblBottom.backgroundColor = UIColor.clear
            }
            return cell
        }
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0 {
            return UITableViewAutomaticDimension
        }
      else  if indexPath.row == 1 {
            return 70
        }
        else{
            return 44
        }
    }
    
    
    
    
}
