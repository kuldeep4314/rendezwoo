//
//  PreferencesViewController.swift
//  Rendenwoo
//
//  Created by goyal on 28/02/18.
//  Copyright © 2018 ankita. All rights reserved.
//

import UIKit


class PreferencesViewController: UIViewController ,UITextFieldDelegate {

    @IBOutlet weak var txtReligion: UITextField!
    @IBOutlet weak var txtEthicity: UITextField!
    @IBOutlet weak var txtDistance: UITextField!
    @IBOutlet weak var txtHeight: UITextField!
    @IBOutlet weak var txtAge: UITextField!
    
    @IBOutlet weak var lblLookingFor: UILabel!
    @IBOutlet weak var imgSingle: UIImageView!
    
    @IBOutlet weak var imgDivider2: UIImageView!
    @IBOutlet weak var imgDivider1: UIImageView!
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var imgTaken: UIImageView!
    @IBOutlet weak var topViewHeight: NSLayoutConstraint!
    @IBOutlet weak var btnBoth: UIButton!
    @IBOutlet weak var btnWomen: UIButton!
    @IBOutlet weak var btnTaken: UIButton!
    
    @IBOutlet weak var lblPartnerPreference: UILabel!
    var isFromEditProfile = Bool()

    @IBOutlet weak var lblTaken: UILabel!
    @IBOutlet weak var lblSingle: UILabel!
    @IBOutlet weak var btnSave: UIButton!
    @IBOutlet weak var lblBoth: UILabel!
    @IBOutlet weak var lblWomen: UILabel!
    @IBOutlet weak var lblMen: UILabel!
    @IBOutlet weak var imgBoth: UIImageView!
    @IBOutlet weak var imgWomen: UIImageView!
    @IBOutlet weak var imgMen: UIImageView!
    @IBOutlet weak var btnMen: UIButton!
    @IBOutlet weak var btnSingle: UIButton!
    
    var partnerPrefernce = PartnerPreference.init(json: JSON(JSON.self))
     var userData = UserData.init(json: JSON(JSON.self))
    
    override func viewDidLoad() {
        super.viewDidLoad()
         txtReligion.rightview(Img: #imageLiteral(resourceName: "imgDropDown"))
         txtEthicity.rightview(Img: #imageLiteral(resourceName: "imgDropDown"))
         txtDistance.rightview(Img: #imageLiteral(resourceName: "imgDropDown"))
         txtHeight.rightview(Img: #imageLiteral(resourceName: "imgDropDown"))
         txtAge.rightview(Img: #imageLiteral(resourceName: "imgDropDown"))
        
        btnMen.backgroundColor = red
       
        txtAge.delegate = self
        txtEthicity.delegate = self
        txtReligion.delegate = self
        txtDistance.delegate = self
        txtHeight.delegate = self
    
          btnMen.addshodowToButton()
          btnTaken.addshodowToButton()
          btnSingle.addshodowToButton()
          btnWomen.addshodowToButton()
          btnBoth.addshodowToButton()
        
        
        
          self.view.backgroundColor = appColor
        
        self.setPartnerPreferencesValues()
        
        if UIDevice.isIphoneX == true{
            topViewHeight.constant = 84
        }
        userData = UserData.init(json: JSON(standard.value(forKey: "userData") as? [String : Any] ?? [String : Any]()))
        self.setIniitalData()
        // Do any additional setup after loading the view.
    }
    //MARK:- SetPartnerPreferencesValues Functions
    func setPartnerPreferencesValues(){
        txtAge.text =   self.partnerPrefernce.age
        txtEthicity.text =  self.partnerPrefernce.ethnicity
        txtReligion.text =  self.partnerPrefernce.religion
        txtDistance.text =  self.partnerPrefernce.distance
        txtHeight.text =  self.partnerPrefernce.height?.base64Decoded()
     
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func btnBackTapped(_ sender: Any) {
        if self.isFromEditProfile == true{
            self.navigationController?.popToRootViewController(animated: true)
        }
        else{
            self.navigationController?.popViewController(animated: true)
        }
    
    }
    
    
    func  setIniitalData (){
        
        if userData.genderPreference == 0{
            self.btnMenTapped(btnMen)
        }
        else   if userData.genderPreference == 1{
             self.btnWomenTapped(btnWomen)
            
        }
        else  if userData.genderPreference == 2{
            self.btnBothTapped(btnBoth)
        }
        
        if  userData.usertype == 0 {
          
            self.btnSingleTapped(btnSingle)
        }
        else{
           
            self.btnTakenTapped(btnTaken)
        }
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        let parameter : Parameters = ["user_id" :"\(standard.value(forKey: "userid")!)"]
        self.fetchPreferencesApiCall(parameters: parameter)
    }
    
    func setDataForAccordingToUser(value : Bool){
        
        self.bottomView.isHidden = value
        self.lblPartnerPreference.isHidden = value
        self.lblLookingFor.isHidden = value
        btnMen.isHidden = value
          lblMen.isHidden = value
          imgMen.isHidden = value
        
        btnWomen.isHidden = value
        lblWomen.isHidden = value
        imgWomen.isHidden = value
        
        btnBoth.isHidden = value
        lblBoth.isHidden = value
        imgBoth.isHidden = value
        imgDivider2.isHidden = value
        imgDivider1.isHidden = value
    }
    
    
    

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
       
    }
    //MARK:- Get Substring Function
    func getSubstring(string : String,startIndex : Int, stopIndex: Int)->String{
        
        if string != "" {
            let start = String.Index(encodedOffset: startIndex)
            let end = String.Index(encodedOffset: stopIndex)
            let substring = String(string[start..<end])
            print(substring)
            return substring
        }
        else{
            return ""
        }
    }
    
    @IBAction func btnSavetapped(_ sender: UIButton) {
                
        let heightInCm = getSubstring(string: partnerPrefernce.height?.base64Decoded() ?? "5'8\" (175 cms) to 6'2\" (188 cms)".base64Encoded()!, startIndex: 6, stopIndex: 9)
        let distanceInCm = getSubstring(string: partnerPrefernce.distance ?? "100 km / 62 Miles", startIndex: 0, stopIndex: 3)
        
        let parameter : Parameters = ["height":  txtHeight.text?.base64Encoded() ?? "5'8\" (175 cms) to 6'2\" (188 cms)".base64Encoded()!, "age": partnerPrefernce.age ?? "29 to 34", "relationship_status": userData.usertype ?? 0, "distance": partnerPrefernce.distance ?? "100 km / 62 Miles", "user_id": "\(standard.value(forKey: "userid")!)", "ethnicity": partnerPrefernce.ethnicity ?? "No Preference", "gender_preference": partnerPrefernce.genderPreference ?? 0, "religion": partnerPrefernce.religion ?? "No Preference", "height_in_cm":heightInCm,"distance_in_km":distanceInCm ]
        
           print(parameter)
        self.updatePreferencesApiCall(parameters: parameter)
        
    }
    
    func setImagesLblWithColor(arrBtn : [UIButton], backgroundColor : [UIColor], tntColor: [UIColor],arrLbl : [UILabel],arrImage:[UIImageView]){
        
         btnSave.setTitle("Save Changes", for: .normal)
        for i in 0..<arrBtn.count{
            let btn = arrBtn[i]
             btn.backgroundColor = backgroundColor[i]
              let img =  arrImage[i]
            img.tintColor = tntColor[i]
            let lbl = arrLbl[i]
            if i == 0 {
                let font = UIFont.init(name: "ProximaNova-Semibold", size: 14)
                 lbl.font = font
            }
            else{
                let font = UIFont.init(name: "ProximaNova-Regular", size: 14)
                lbl.font = font
            }
            lbl.textColor = tntColor[i]
           
        }
    }
    
    func setImagesLblWithImage(arrBtn : [UIButton], backgroundColor : [UIColor], tntColor: [UIColor],arrLbl : [UILabel],arrImage:[UIImageView],images:[UIImage]){
        
        btnSave.setTitle("Save Changes", for: .normal)
        for i in 0..<arrBtn.count{
            let btn = arrBtn[i]
            btn.backgroundColor = backgroundColor[i]
            let img =  arrImage[i]
            img.image = images[i]
            let lbl = arrLbl[i]
            if i == 0 {
                let font = UIFont.init(name: "ProximaNova-Semibold", size: 14)
                lbl.font = font
            }
            else{
                let font = UIFont.init(name: "ProximaNova-Regular", size: 14)
                lbl.font = font
            }
            lbl.textColor = tntColor[i]
            
        }
    }

    
    
    
    func setImages(arrBtn : [UIButton], arrStr: [String]){
        btnSave.setTitle("Save Changes", for: .normal)
        for i in 0..<arrBtn.count{
            let btn = arrBtn[i]
            btn.setImage(UIImage.init(named: arrStr[i]), for: .normal)
        }
    }
    
    
    //MARK: Button Actions Functions
    
    @IBAction func btnSingleTapped(_ sender: Any) {
       
        userData.usertype = 0
         self.setDataForAccordingToUser(value: false)
        self.setImagesLblWithImage(arrBtn: [btnSingle,btnTaken], backgroundColor: [red, UIColor.white], tntColor: [UIColor.white,UIColor.black], arrLbl: [lblSingle , lblTaken], arrImage: [imgSingle,imgTaken], images: [#imageLiteral(resourceName: "single"),#imageLiteral(resourceName: "taken")])
        
      
        
    }
    @IBAction func btnTakenTapped(_ sender: Any) {
       
          userData.usertype = 1
        
          self.setDataForAccordingToUser(value: true)
        self.setImagesLblWithImage(arrBtn: [btnTaken,btnSingle], backgroundColor: [red, UIColor.white], tntColor: [UIColor.white,UIColor.black], arrLbl: [lblTaken , lblSingle], arrImage: [imgTaken,imgSingle], images: [#imageLiteral(resourceName: "imgTakenWhite") ,#imageLiteral(resourceName: "imgSingleBlack")])
    }
    @IBAction func btnMenTapped(_ sender: Any) {
   
        
      partnerPrefernce.genderPreference = 0
          self.setImagesLblWithColor(arrBtn: [btnMen,btnWomen,btnBoth], backgroundColor: [red, UIColor.white,UIColor.white], tntColor: [UIColor.white,UIColor.black,UIColor.black], arrLbl: [lblMen , lblWomen,lblBoth], arrImage: [imgMen,imgWomen, imgBoth])
        
    }

    @IBAction func btnWomenTapped(_ sender: Any) {
       
          partnerPrefernce.genderPreference = 1
        self.setImagesLblWithColor(arrBtn: [btnWomen,btnMen,btnBoth], backgroundColor: [red, UIColor.white,UIColor.white], tntColor: [UIColor.white,UIColor.black,UIColor.black], arrLbl: [lblWomen , lblMen,lblBoth], arrImage: [imgWomen,imgMen, imgBoth])

        
        
    }
    @IBAction func btnBothTapped(_ sender: Any) {
        
            partnerPrefernce.genderPreference = 2
        
         self.setImagesLblWithColor(arrBtn: [btnBoth,btnWomen,btnMen], backgroundColor: [red, UIColor.white,UIColor.white], tntColor: [UIColor.white,UIColor.black,UIColor.black], arrLbl: [lblBoth , lblWomen,lblMen], arrImage: [imgBoth,imgWomen, imgMen])
        
    }
    
    
    //MARK:- SetTextfields Values
    func selectTextFieldsValues( array : [String], tag : Int , strValue : String ,numberOfComponents : Int) {
        
        let vc = PickerView.init(frame: self.view.frame, array: array, str: strValue, tag: tag , numberOfComponents : numberOfComponents,isStartingPreference : false)
        
        vc.delegates = self
        self.view.addSubview(vc)
        
    }
    func showOptionsInTableView(array : [String] ,tag : Int,strValue:String ,isStartingPreference : Bool){
        let vc = OptionTable.init(frame: self.view.frame, dataArr:array , tableframe: CGRect.init(x: 10, y: Int(self.view.frame.size.height / 2)  - Int(array.count * 25) , width: Int(self.view.frame.size.width - 20), height:  Int(array.count * 50)), tag: tag,strValue : strValue ,isStartingPreference :isStartingPreference,titleString:"dummy not need")
        vc.delegates = self
        self.view.addSubview(vc)
    }
    //MARK: - UITEXTFIELD DELEGATES
    func textFieldDidBeginEditing(_ textField: UITextField) {
        textField.resignFirstResponder()
        if textField.tag == 1 {
             var array = [String]()
            for i in 18..<100 {
                array.append(String(i))
            }
            
            
            self.selectTextFieldsValues(array: array, tag: textField.tag, strValue: textField.text ?? "", numberOfComponents: 3)
        }
       
      else  if textField.tag == 2 {
            var array = [String]()
            for i in 122..<273 {
                
            let  feet  =  Double(i) * 0.3937008
            
             let  f1 = Int(feet / 12 )
               let f2 = Int(feet) % 12
               
              array.append("\(f1)'\(f2)\" (\(i) cms)")
            }
            
             self.selectTextFieldsValues(array: array, tag: textField.tag, strValue: textField.text ?? "", numberOfComponents: 3)
            
        }
        
      else  if textField.tag == 3 {
             var array = [String]()
            for i in 10..<1005 {
                if i % 10 == 0 {
                    let miles =   Int(Float(i) / 1.609344)
                    
                   array.append("\(i) km / \(miles) Miles")
                }
                
            }
            
            self.selectTextFieldsValues(array: array, tag: textField.tag, strValue: textField.text ?? "", numberOfComponents: 1)
            
            
        }
            
        
      else  if textField.tag == 4 {
            let array = ["American Indian"," Black/African Descent","East Indian","Hispanic/Latino","Middle Eastern","Pacific Islander","South Asian","White/Caucasian","Other","No Preference"]
            
            self.showOptionsInTableView(array: array, tag: textField.tag, strValue: textField.text ?? "", isStartingPreference: false)
            
    
        }
        else  if textField.tag == 5 {
           let array = ["Buddhist","Christian","Hindu","Jewish","Muslim","Sikh","No Religion","No Preference"]
             self.showOptionsInTableView(array: array, tag: textField.tag, strValue: textField.text ?? "", isStartingPreference: false)
            
        }
        
        
        
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        textField.resignFirstResponder()
    }
    
    //MARK:- Update Preferences Call Function
    
    func updatePreferencesApiCall(parameters: Parameters){
        print(parameters)
        AccountApiInterface.sharedInstance.postRequestWithParam(fromViewController: self, actionName: "updatePreferences", dictParam: parameters as [String : AnyObject], isShowIndicator: true, success: { (json) in
            
            print(json)
              self.partnerPrefernce =  PartnerPreference.init(json: json["data"])
            DispatchQueue.main.async {
                self.userData.genderPreference = self.partnerPrefernce.genderPreference ?? 0
                
                standard.set(self.userData.dictionaryRepresentation(), forKey: "userData")
                self.setIniitalData()
                
                 self.btnSave.setTitle("Saved", for: .normal)
            }
            
        }) { (error) in
            self.showAlert(messageStr: error)
        }
        
    }
    //MARK:-  Fetch Preferences Call Function
    
    func fetchPreferencesApiCall(parameters: Parameters){
          print(parameters)
        AccountApiInterface.sharedInstance.postRequestWithParam(fromViewController: self, actionName: "fetch_partner_preference", dictParam: parameters as [String : AnyObject], isShowIndicator: true, success: { (json) in
            
            print(json)
            self.partnerPrefernce =  PartnerPreference.init(json: json["data"])
            
            DispatchQueue.main.async {
               self.setPartnerPreferencesValues()
            }
            
        }) { (error) in
            self.showAlert(messageStr: error)
        }
        
    }

}
extension PreferencesViewController : setValuesDelegates{
    
    func  setTextfieldsvalues (str : String , tag: Int){
         btnSave.setTitle("Save Changes", for: .normal)
        if tag == 3{
              self.partnerPrefernce.distance = str
        }
            
        else if tag == 4{
         
             self.partnerPrefernce.ethnicity = str
        }
        else if tag == 5{
           
              self.partnerPrefernce.religion = str
        }
        self.setPartnerPreferencesValues()
    }
    func  setTextFieldsWithTwoValues (str : String , str1 : String , tag: Int){
         btnSave.setTitle("Save Changes", for: .normal)
        if tag == 1 {
               self.partnerPrefernce.age = "\(str) to \(str1)"
    }
            
        else  if tag == 2 {
           self.partnerPrefernce.height = "\(str) to \(str1)"
        }
          self.setPartnerPreferencesValues()
    }
    
}
