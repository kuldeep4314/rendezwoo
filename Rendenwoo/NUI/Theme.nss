
@AkrobatBold: Akrobat-Bold;
@ProximaNovaBold: ProximaNova-Bold;
@ProximaNovaLight: ProximaNova-Light;
@ProximaNovaRegular: ProximaNova-Regular;
@ProximaNovaSemibold: ProximaNova-Semibold;

@AppColor : #00cdd2;
@DarkGray : #737373;
@lightGray : #9e9e9e;
@moreLight : #f8f8f8;

@orange : #ff9a8c;
@red : #f27273;

ThemeColor {
background-color:@AppColor;
}
bgColor {
background-color:@moreLight;
}

redColor {
background-color:@red;
}

Green {
font-color :@AppColor;
}

DarkGray {
font-color :@DarkGray;
}
lightGray {
font-color :@lightGray;
}
moreLight {
font-color :@moreLight;
}
orange {
font-color :@orange;
}
red {
font-color :@red;
}


pageControl {
color :@DarkGray;
current-color :@AppColor;
}

fontRegular {
font-name: @ProximaNovaRegular;
}

fontSemibold {
font-name: @ProximaNovaSemibold;
}

