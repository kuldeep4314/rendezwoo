//
//  ViewController.swift
//  Rendenwoo
//
//  Created by goyal on 23/02/18.
//  Copyright © 2018 ankita. All rights reserved.
//

import UIKit

class ViewController: UIViewController ,SKSplashDelegate{

    override func viewDidLoad() {
        super.viewDidLoad()
        self.animationLikeTwiiter()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    func animationLikeTwiiter(){
        let twitterSplashIcon = SKSplashIcon.init(image: UIImage.init(named: "imgLogo"), animationType: .bounce)
        let twitterColor  = appColor
        let splashView = SKSplashView.init(splashIcon: twitterSplashIcon, animationType: .none)
        splashView?.delegate = self
         splashView?.delegate = self
        splashView?.backgroundColor = twitterColor
         splashView?.animationDuration = 2
        self.view.addSubview(splashView!)
          splashView?.startAnimation()
       
        }
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    //MARK: - SplashView Delegates
    func splashView(_ splashView: SKSplashView!, didBeginAnimatingWithDuration duration: Float) {
        
    }
    
    func splashViewDidEndAnimating(_ splashView: SKSplashView!) {
        
    }
    
    
    

}

